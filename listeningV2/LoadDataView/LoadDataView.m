//
//  LoadDataView.m
//  ListenToMe
//
//  Created by zhw on 15/6/2.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "LoadDataView.h"

@interface LoadDataView ()
@property(nonatomic,strong)UIWebView *loadDataGifView;
@property(nonatomic,strong)UIImageView *gifImageView;
@property(nonatomic,strong)UIView *loadDataBgView;
@end


@implementation LoadDataView
-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        [self setUI];
    }
    return self;
}

-(void)setUI{
    
    self.backgroundColor = [UIColor whiteColor];
    
    NSMutableArray *gifArray = [NSMutableArray array];
    for (NSInteger index =0; index < 67; index++) {
        UIImage *gifImg ;
        if(index < 10){
            gifImg = [UIImage imageNamed:[NSString stringWithFormat:@"启动条_0000%ld.png",index]];
        }else{
            gifImg = [UIImage imageNamed:[NSString stringWithFormat:@"启动条_000%ld.png",index]];
        }
        
        [gifArray addObject:gifImg];
    }
    
    CGFloat gifImageViewW = 100;
    CGFloat gifImageViewH = 85;
    CGFloat gifImageViewX = (self.width - gifImageViewW) * 0.5;
    CGFloat gifImageViewY = (self.height - gifImageViewH) * 0.5;
    
    self.gifImageView = [[UIImageView alloc]initWithFrame:CGRectMake(gifImageViewX, gifImageViewY, gifImageViewW, gifImageViewH)];
    self.gifImageView.animationImages = gifArray;
    //时间间隔
    self.gifImageView.animationDuration = 0;
    //重复次数
    self.gifImageView.animationRepeatCount = 0;
    [self.gifImageView startAnimating];
    
    [self addSubview:self.gifImageView];
    
    
    
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
