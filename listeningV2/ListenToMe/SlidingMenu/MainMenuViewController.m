//
//  MainMenuViewController.m
//  SlidingMenuDemo
//
//  Created by Ryan Tang on 13-4-23.
//  Copyright (c) 2013年 Ericsson Labs. All rights reserved.
//

#import "MainMenuViewController.h"
#import "SWRevealViewController.h"
#import "FrontViewController.h"
#import "YDSquareSearchVC.h"
#import "YDNavigationController.h"
#import "MeViewController.h"
#import "EventsViewController.h"
#import "ManagementCenterViewController.h"
#import "EntertainmentViewController.h"
@interface MainMenuViewController ()
{
    NSInteger _previouslySelectedRow;
    NSArray *arr_text;
}
@end

@implementation MainMenuViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    // self.view.backgroundColor = [UIColor orangeColor];
    //self.clearsSelectionOnViewWillAppear = NO;
   //  self.tableView.backgroundColor = [UIColor colorWithWhite:0.5 alpha:1.0];
   //  self.tableView.separatorColor = [UIColor colorWithWhite:0.5 alpha:1.0];
    
    arr_text = @[@"趣咖咖",@"购买",@"我",@"活动",@"娱乐",@"设置",@"管理中心"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    _previouslySelectedRow = -1;
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return arr_text.count;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        UILabel *lb_text = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 20.0)];
        lb_text.backgroundColor = [UIColor orangeColor];
        lb_text.textColor = [UIColor whiteColor];
        lb_text.text = @"Title";
        return lb_text;
    }
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];    
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    int index = indexPath.row;
    UIImage *img_item;
    
    switch (index) {
        case 0:
            img_item = [UIImage imageNamed:@"heart"];
            break;
        case 1:
            img_item = [UIImage imageNamed:@"star"];
            break;
        default:
            break;
    }
    
//    cell.imageView.image = img_item;
//    cell.imageView.frame = CGRectMake(0,0,44,44);
    cell.textLabel.text = [arr_text objectAtIndex:index];
    cell.textLabel.textColor = [UIColor colorWithWhite:0.0 alpha:1.0];
    cell.textLabel.textAlignment = UITextAlignmentCenter;
    return cell;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath;
}

#pragma mark - Table view delegate
/**
 点击菜单切换视图
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    //UIViewController类别，获取父实例
    SWRevealViewController *revelController = self.revealViewController;
    
    NSInteger row = indexPath.row;
    
    //如果选择项没有变化，则直接关闭菜单
    if (row == _previouslySelectedRow) {
        [revelController revealToggleAnimated:YES];
        return;
    }
    
    //如果选择项发生变化，则切换至相应视图
    _previouslySelectedRow = row;
    UIViewController *viewController = nil;
    
    switch (row) {
        case 0:
        {
//            FrontViewController *firstController = [[FrontViewController alloc] init];
//            firstController.text = @"First View";
//            firstController.view.backgroundColor = [UIColor lightGrayColor];
//            viewController = firstController;
 
            YDSquareSearchVC *firstController = [[YDSquareSearchVC alloc] init];
            YDNavigationController *navController2 = [[YDNavigationController alloc]initWithRootViewController:firstController];
            viewController = navController2;
            
        }
            break;
        case 1:
        {
            FrontViewController *secondController = [[FrontViewController alloc] init];
            YDNavigationController *navController2 = [[YDNavigationController alloc]initWithRootViewController:secondController];
            viewController = navController2;
        }
            break;
        case 2:
        {
            MeViewController *secondController = [[MeViewController alloc] init];
            YDNavigationController *navController2 = [[YDNavigationController alloc]initWithRootViewController:secondController];
            viewController = navController2;
            break;
        }
        case 3:
        {
            EventsViewController *secondController = [[EventsViewController alloc] init];
            YDNavigationController *navController2 = [[YDNavigationController alloc]initWithRootViewController:secondController];
            viewController = navController2;
            break;
        }
        case 4:
        {
            EntertainmentViewController *secondController = [[EntertainmentViewController alloc] init];
            YDNavigationController *navController2 = [[YDNavigationController alloc]initWithRootViewController:secondController];
            viewController = navController2;
            break;
        }
        case 6:
        {
            ManagementCenterViewController *secondController = [[ManagementCenterViewController alloc] init];
            YDNavigationController *navController2 = [[YDNavigationController alloc]initWithRootViewController:secondController];
            viewController = navController2;
            break;
        }
        default:
            break;
    }
    
    //切换视图（是否带弹回动画）
    if(viewController)
    {
    [revelController setFrontViewController:viewController animated:YES];
    [revelController setFrontViewPosition:FrontViewPositionLeft animated:YES];
    }
}

@end
