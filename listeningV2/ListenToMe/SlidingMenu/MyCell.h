//
//  MyCell.h
//  ListenToMe
//
//  Created by zhaojianxi on 15/6/27.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCell : UITableViewCell
{
    UITextView *myTextView;
    UILabel *myUILabel;
}
- (IBAction)btnAction:(id)sender;
@property (retain, nonatomic) IBOutlet UITextView *myTextView;
@property (retain, nonatomic) IBOutlet UILabel *myUILabel;
@end
