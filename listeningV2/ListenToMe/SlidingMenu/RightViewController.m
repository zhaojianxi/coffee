//
//  RightViewController.m
//  SlidingMenuDemo
//
//  Created by Ryan Tang on 13-4-23.
//  Copyright (c) 2013年 Ericsson Labs. All rights reserved.
//

#import "RightViewController.h"

@interface RightViewController ()

@end

@implementation RightViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor orangeColor];
    
    UILabel *lb_text = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30.0)];
    lb_text.center = self.view.center;
    lb_text.backgroundColor = [UIColor clearColor];
    lb_text.textColor = [UIColor whiteColor];
    lb_text.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:lb_text];
    
    lb_text.text = @"Right View";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
