//
//  MainMenuViewController.m
//  SlidingMenuDemo
//
//  Created by Ryan Tang on 13-4-23.
//  Copyright (c) 2013年 Ericsson Labs. All rights reserved.
//

#import "EventsViewController.h"
#import "SWRevealViewController.h"
#import "FrontViewController.h"
#import "YDSquareSearchVC.h"
#import "YDNavigationController.h"
#import "AppDelegate.h"
#import "MyCell.h"
@interface EventsViewController ()
{
    NSInteger _previouslySelectedRow;
    NSArray *arr_text;
}
@end

@implementation EventsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    arr_text = @[@"活动1",@"活动2",@"活动3"];
    
    //获取根视图控制器
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    SWRevealViewController *revealController = (SWRevealViewController *)delegate.window.rootViewController;
    //添加页面滑动事件（展开隐藏菜单）
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    
    [self.navigationItem setHidesBackButton:YES];
    UIImage *searchImg = [UIImage imageNamed:@"reveal-icon.png"];
    UIButton *customBackBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, searchImg.size.width, searchImg.size.height)];
    [customBackBtn addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [customBackBtn setImage:searchImg forState:UIControlStateNormal];
    UIBarButtonItem *customBackItem = [[UIBarButtonItem alloc]initWithCustomView:customBackBtn];
    self.navigationItem.leftBarButtonItem = customBackItem;
    
    UIButton *customButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 100, 41)];
    customButton.backgroundColor = [UIColor clearColor];
    [customButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    customButton.titleLabel.font = [UIFont systemFontOfSize:16.5];
    customButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [customButton setTitle:@"活动" forState:UIControlStateNormal];
    self.navigationItem.titleView = customButton;
    
    //self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    _previouslySelectedRow = -1;
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return arr_text.count;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        UILabel *lb_text = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 20.0)];
        lb_text.backgroundColor = [UIColor orangeColor];
        lb_text.textColor = [UIColor whiteColor];
        lb_text.text = @"Title";
        return lb_text;
    }
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CustomCellIdentifier = @"CustomCellIdentifier";

    MyCell *mycell = [tableView dequeueReusableCellWithIdentifier:CustomCellIdentifier];
    if(!mycell)
    {
        UINib *nib = [UINib nibWithNibName:@"MyCell" bundle:nil];
        [tableView registerNib:nib forCellReuseIdentifier:CustomCellIdentifier];
        mycell = [tableView dequeueReusableCellWithIdentifier:CustomCellIdentifier];
    }
    int myindex = indexPath.row;
    mycell.myUILabel.text = [arr_text objectAtIndex:myindex];
    mycell.myUILabel.textColor = [UIColor blackColor];
    mycell.myUILabel.font = [UIFont systemFontOfSize:25];
    mycell.myUILabel.textAlignment = NSTextAlignmentCenter;
    return mycell;
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];    
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }

    int index = indexPath.row;
    UIImage *img_item;
    
    switch (index) {
//        case 0:
//            img_item = [UIImage imageNamed:@"me_icon"];
//            break;
//        case 1:
//            img_item = [UIImage imageNamed:@"star"];
//            break;
        default:
            break;
    }
    
    cell.imageView.image = img_item;
    cell.imageView.frame = CGRectMake(0,0,64,64);
    cell.textLabel.text = [arr_text objectAtIndex:index];
    cell.textLabel.textColor = [UIColor colorWithWhite:0.0 alpha:1.0];
    cell.textLabel.textAlignment = UITextAlignmentLeft;
    return cell;
}
#pragma mark Table Delegate Methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80.0;
}

//- (NSIndexPath *)tableView:(UITableView *)tableView
//  willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    return nil;
//}
//
- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath;
}

#pragma mark - Table view delegate
/**
 点击菜单切换视图
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    
//    //UIViewController类别，获取父实例
//    SWRevealViewController *revelController = self.revealViewController;
//    
//    NSInteger row = indexPath.row;
//    
//    //如果选择项没有变化，则直接关闭菜单
//    if (row == _previouslySelectedRow) {
//        [revelController revealToggleAnimated:YES];
//        return;
//    }
//    
//    //如果选择项发生变化，则切换至相应视图
//    _previouslySelectedRow = row;
//    UIViewController *viewController = nil;
//    
//    switch (row) {
//        case 0:
//        {
////            FrontViewController *firstController = [[FrontViewController alloc] init];
////            firstController.text = @"First View";
////            firstController.view.backgroundColor = [UIColor lightGrayColor];
////            viewController = firstController;
// 
//            YDSquareSearchVC *firstController = [[YDSquareSearchVC alloc] init];
//            YDNavigationController *navController2 = [[YDNavigationController alloc]initWithRootViewController:firstController];
//            viewController = navController2;
//            
//        }
//            break;
//        case 1:
//        {
//            FrontViewController *secondController = [[FrontViewController alloc] init];
//            secondController.text = @"Second View";
//           // secondController.view.backgroundColor = [UIColor darkGrayColor];
////            YDSquareSearchVC *firstController = [[YDSquareSearchVC alloc] init];
//            YDNavigationController *navController2 = [[YDNavigationController alloc]initWithRootViewController:secondController];
//            viewController = navController2;
////            viewController = secondController;
//        }
//            break;
//            
//        default:
//            break;
//    }
    
    //切换视图（是否带弹回动画）
//    [revelController setFrontViewController:viewController animated:YES];
//    [revelController setFrontViewPosition:FrontViewPositionLeft animated:YES];
}

@end
