//
//  FrontViewController.h
//  SlidingMenuDemo
//
//  Created by Ryan Tang on 13-4-23.
//  Copyright (c) 2013年 Ericsson Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FrontViewController : UIViewController

@property(copy,nonatomic) NSString *text;

@end
