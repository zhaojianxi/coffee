//
//  FrontViewController.m
//  SlidingMenuDemo
//
//  Created by Ryan Tang on 13-4-23.
//  Copyright (c) 2013年 Ericsson Labs. All rights reserved.
//

#import "FrontViewController.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"
#import "YDSquareSearchVC.h"
@interface FrontViewController ()

@end

@implementation FrontViewController
@synthesize text;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    //顶部左侧按钮
//    UIImage *img_menu = [UIImage imageNamed:@"reveal-icon"];
    UIButton *btn_menu = [UIButton buttonWithType:UIButtonTypeInfoLight];
    btn_menu.frame = CGRectMake(self.view.frame.size.width/3, self.view.frame.size.height/3, self.view.frame.size.width/3, 30);
    //[btn_menu setImage:text forState:UIControlStateNormal];
    [btn_menu setTitle:@"扫码支付" forState:UIControlStateNormal];
    [btn_menu setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.view addSubview:btn_menu];

    //顶部右侧按钮
    UIButton *btn_menu_right = [UIButton buttonWithType:UIButtonTypeInfoLight];
    btn_menu_right.frame = CGRectMake(self.view.frame.size.width/3, self.view.frame.size.height/2, self.view.frame.size.width/3, 30);
    //[btn_menu_right setImage:img_menu forState:UIControlStateNormal];
    [btn_menu_right setTitle:@"连接咖啡机" forState:UIControlStateNormal];
    [btn_menu_right setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.view addSubview:btn_menu_right];

    //获取根视图控制器
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    SWRevealViewController *revealController = (SWRevealViewController *)delegate.window.rootViewController;
    
    //菜单按钮点击事件（展开隐藏菜单）
    [btn_menu addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    //菜单按钮点击事件（展开右侧隐藏视图）
    [btn_menu_right addTarget:revealController action:@selector(rightRevealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    //添加页面滑动事件（展开隐藏菜单）
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    
    //////
    UIButton *customButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 100, 41)];
    customButton.backgroundColor = [UIColor clearColor];
    [customButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    customButton.titleLabel.font = [UIFont systemFontOfSize:16.5];
    customButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [customButton setTitle:@"趣咖咖" forState:UIControlStateNormal];
    self.navigationItem.titleView = customButton;
    
    [self.navigationItem setHidesBackButton:YES];
    UIImage *searchImg = [UIImage imageNamed:@"reveal-icon.png"];
    UIButton *customBackBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, searchImg.size.width, searchImg.size.height)];
    [customBackBtn addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [customBackBtn setImage:searchImg forState:UIControlStateNormal];
    UIBarButtonItem *customBackItem = [[UIBarButtonItem alloc]initWithCustomView:customBackBtn];
    self.navigationItem.leftBarButtonItem = customBackItem;
    
    UIImage *rightBtImg = [UIImage imageNamed:@"more.png"];
    UIButton *rightBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, rightBtImg.size.width,rightBtImg.size.height)];
    [rightBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -25)];
    [rightBtn setImage:rightBtImg forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(clickRightBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customRightBtnItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
//    self.navigationItem.rightBarButtonItem = customRightBtnItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
