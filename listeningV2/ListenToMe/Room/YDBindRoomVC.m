//
//  YDBindRoomVC.m
//  ListenToMe
//
//  Created by afei on 15/4/9.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "YDBindRoomVC.h"
#import <AVFoundation/AVFoundation.h>
#import "NetReqManager.h"

@interface YDBindRoomVC ()<AVCaptureMetadataOutputObjectsDelegate>

//**
@property (strong, nonatomic) AVCaptureSession *captureSession;
@property (strong, nonatomic) AVCaptureVideoPreviewLayer *videoPreviewLayer;
@property (strong, nonatomic) AVCaptureMetadataOutput *captureMetadataOutput;
@property (strong, nonatomic) UIView *viewPreview;//扫描窗口
@end

@implementation YDBindRoomVC
#pragma mark -生命周期
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}

#pragma mark -UI
-(void)setUI
{
    [self setupCamera];
}


#pragma mark -其他
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)setupCamera
{
    /**
     CaptureSession 这是个捕获会话，也就是说你可以用这个对象从输入设备捕获数据流。
     AVCaptureVideoPreviewLayer 可以通过输出设备展示被捕获的数据流。
     首先，应该判断当前设备是否有捕获数据流的设备。
     */
    NSError *error;
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    
    if (!input) {
        NSLog(@"%@", [error localizedDescription]);
    }else{
        //设置会话的输入设备
        if (!_captureSession) {
            _captureSession = [[AVCaptureSession alloc] init];
        }
        [_captureSession addInput:input];
        
        //对应输出
        if (!_captureMetadataOutput) {
            _captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
        }
        [_captureSession addOutput:_captureMetadataOutput];
        
        //创建一个队列
        dispatch_queue_t dispatchQueue;
        dispatchQueue = dispatch_queue_create("myQueue",NULL);
        [_captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
        [_captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObject:AVMetadataObjectTypeQRCode]];//设置条码类型。更多类型底下补上
        
        //降捕获的数据流展现出来
        _videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
        [_videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
        
        if (!_viewPreview) {
            _viewPreview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
            [self.view addSubview:_viewPreview];
        }
        [_videoPreviewLayer setFrame:_viewPreview.layer.bounds];
        [_viewPreview.layer addSublayer:_videoPreviewLayer];
        
        //开始捕获
        [_captureSession startRunning];
    }
}

//获得的数据在 AVCaptureMetadataOutputObjectsDelegate 唯一定义的方法中
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    //判断是否有数据，是否是二维码数据
    if (metadataObjects != nil && [metadataObjects count] > 0) {
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode]) {
            //获得扫描的数据，并结束扫描
            [self performSelectorOnMainThread:@selector(stopReading:)withObject:metadataObj.stringValue waitUntilDone:NO];
            //线程更新label的text值
            //            [_result performSelectorOnMainThread:@selector(setText:) withObject:metadataObj.stringValue waitUntilDone:NO];
            //block传值
//            _stringValue(metadataObj.stringValue);
            //代理传值
//            [_delegate setStringValue:metadataObj.stringValue];
            
            [ListenToMeData getInstance].qrcode = metadataObj.stringValue;
            //发送绑定包房的协议
        }
    }
}

- (void)stopReading:(id)sender{
    [_captureSession stopRunning];
    _captureSession = nil;
    [_videoPreviewLayer removeFromSuperlayer];
    
    [self.navigationController popViewControllerAnimated:YES];
}

// 导航栏的标题栏
-(void)setNavigatonTitleVeiw
{
    CGFloat titleViewH = 35;
    UIView *titleView = [[UIView alloc]initWithFrame:CGRectMake(0,0, screenWidth * 0.80, titleViewH)];
    titleView.backgroundColor = [UIColor clearColor];
    
    CGFloat mySubcribeBtnW = 55;
    UIButton *mySubcribeBtn = [[UIButton alloc]initWithFrame:CGRectMake((titleView.width - mySubcribeBtnW) * 0.5, 0, 80, titleViewH)];
    [mySubcribeBtn setTintColor:[UIColor whiteColor]];
    [mySubcribeBtn setBackgroundColor:[UIColor clearColor]];
    [mySubcribeBtn setTitle:@"包房" forState:UIControlStateNormal];
    [mySubcribeBtn.titleLabel setFont:[UIFont systemFontOfSize:17.5]];
    [titleView addSubview:mySubcribeBtn];
    
    self.navigationItem.titleView = titleView;
    
}

@end
