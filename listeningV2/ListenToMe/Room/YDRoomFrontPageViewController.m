//
//  YDRoomFrontPageViewController.m
//  ListenToMe
//
//  Created by afei on 15/4/9.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "YDRoomFrontPageViewController.h"
#import "YDBindRoomVC.h"
#import "YDRoomOutInputViewController.h"
#import "NetReqManager.h"
#import "YDRoomVC.h"

@interface YDRoomFrontPageViewController ()<UITextFieldDelegate>
@end

@implementation YDRoomFrontPageViewController
@synthesize mTextFInput;

#pragma mark -生命周期
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}

#pragma mark -UI
-(void)setUI{
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    UILabel *roomTitle=[[UILabel alloc] initWithFrame:CGRectMake(20, 80, 280, 20)];
    [roomTitle setText:@"请先连接包房"];
    [roomTitle setTextColor:[UIColor redColor]];
    [roomTitle setBackgroundColor:[UIColor clearColor]];
    [roomTitle setFont:[UIFont systemFontOfSize:13]];
    [roomTitle setTextAlignment:NSTextAlignmentCenter];
    [self.view addSubview:roomTitle];
    
    UIButton *erCodeBtn = [[UIButton alloc]init];
    UIImage *erCodeBtnImg = [UIImage imageNamed:@"ercode.png"];
    erCodeBtn.frame = CGRectMake(160-erCodeBtnImg.size.width/4, 120, erCodeBtnImg.size.width/2, erCodeBtnImg.size.height/2);
    [erCodeBtn setImage:erCodeBtnImg forState:UIControlStateNormal];
    [erCodeBtn addTarget:self action:@selector(clickLeftBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:erCodeBtn];
    
    UIImageView *frontroomorImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 310, screenWidth, 26)];
    [frontroomorImageView setImage:[UIImage imageNamed:@"frontroomor.png"]];
    [self.view addSubview:frontroomorImageView];
    
    UIButton *frontroomoutBtn = [[UIButton alloc]initWithFrame:CGRectMake(50, 340, 223, 40)];
    [frontroomoutBtn setBackgroundImage:[UIImage imageNamed:@"frontroom_out.png"] forState:UIControlStateNormal];
    [frontroomoutBtn addTarget:self action:@selector(clickFrontRoomOutBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:frontroomoutBtn];

}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getBindKTVRoomRsp:) name:NOTIFY_BIND_KTVROOM_SUCCESS object:nil];
    
    NSLog(@"YDRoomFrontPageViewControllerYDRoomFrontPageViewController : %@" , [ListenToMeData getInstance].qrcode);
    if ([ListenToMeData getInstance].qrcode !=nil && [[ListenToMeData getInstance].qrcode length] > 0) {
        [[NetReqManager getInstance] sendBindDevice:[ListenToMeData getInstance].qrcode];
        [ListenToMeData getInstance].qrcode = @"";
    }
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewDidDisappear:animated];
}
#pragma mark -其他
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)clickLeftBarBtnItem{
    YDBindRoomVC *roomVC = [[YDBindRoomVC alloc]init];
    [self.navigationController pushViewController:roomVC animated:YES];
}

-(void)clickFrontRoomOutBtn{
//    YDRoomOutInputViewController *roomVC = [[YDRoomOutInputViewController alloc]init];
//    [self.navigationController pushViewController:roomVC animated:YES];
    YDLog(@"sendLoginReq");
    
    [[NetReqManager getInstance] sendKTVControlReq:KTV_ROOM_CONTROL_TYPEKtvRoomControlPlay ];
}

#pragma mark - rsp
- (void)getBindKTVRoomRsp:(NSNotification *)notification{
    NSDictionary *info = notification.object;
    id response = [CCNotify getObj:info byNum:0];
    
    if ([response isKindOfClass:[SCBindKTVRoomRsp class]]) {
        SCBindKTVRoomRsp *scBindKTVRoomRsp = (SCBindKTVRoomRsp*)response;
        
        //对数据进行操作

        if (scBindKTVRoomRsp.iErrCode == 0) {
            YDRoomVC *roomVC = [[YDRoomVC alloc]init];
            roomVC.stKTVRoomInfo = scBindKTVRoomRsp.stKtvroomInfo;
            
            [self.navigationController pushViewController:roomVC animated:YES];
        }else{
            if (iOS8_greater) {
                UIAlertController *alertController = [UIAlertController
                                                      alertControllerWithTitle:@"绑定失败"
                                                      message:@"绑定出错，请重新绑定。"
                                                      preferredStyle:UIAlertControllerStyleAlert];
                
                [alertController addAction:[UIAlertAction actionWithTitle:@"确定"
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction *action) {
                                                                      NSLog(@"绑定确定按键。");
                                                                  }]];
                
                [self presentViewController:alertController animated:YES completion:nil];
            }else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"绑定失败"
                                                                message:@"绑定出错，请重新绑定。"
                                                               delegate:self
                                                      cancelButtonTitle:nil
                                                      otherButtonTitles:@"确定" , nil];
                [alert show];
            }
        }
    }
}
@end
