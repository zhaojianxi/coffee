//
//  YDRoomMsgCell.h
//  ListenToMe
//
//  Created by yadong on 2/10/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YDCircleAvatarBtn.h"

@interface YDRoomMsgCell : UITableViewCell
/**
 *  点击头像的button(暂时未使用)
 */
@property(strong,nonatomic) YDCircleAvatarBtn *btnAvatar;
/**
 *  显示头像的视图
 */
@property(strong,nonatomic) UIImageView *avatarImgView;
/**
 *  显示评论的人名
 */
@property(strong,nonatomic) YDScrollLabel *lbFromPerson;
@property(strong,nonatomic) YDScrollLabel *lbToPerson;
/**
 *  显示发送评论的时间
 */
@property(strong,nonatomic) YDScrollLabel *lbTime;
/**
 *  评论的内容
 */
@property(strong,nonatomic) YDScrollLabel *lbContents;
@property(strong,nonatomic) UIImageView *imgHorizLineIn;
@property(strong,nonatomic) UIImageView *imgHorizLineOut;
@end
