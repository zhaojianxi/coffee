//
//  YDRoomVC.m
//  ListenToMe
//
//  Created by yadong on 1/26/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "YDRoomVC.h"


@interface YDRoomVC ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
/**
 * 遥控器背景界面
 */
@property(strong,nonatomic)UIView *telControlBgView;
/**
 * 遥控器界面
 */
@property(strong,nonatomic) UIView *telecontrollerView;
/**
 * 遥控器
 */
@property(strong,nonatomic) UIButton *btnTelecontroller;
/**
 * 现场~数据
 */
@property(strong,nonatomic) NSMutableArray *arrayLive;
/**
 * 观众~数据
 */
@property(strong,nonatomic) NSMutableArray *arrayAudience;
/**
 * 消息~数据
 */
@property(strong,nonatomic) NSMutableArray *arrayMsgs;
@property(assign,nonatomic) int numOfRows; // 消息的条数
@property(strong,nonatomic) UITableView *mTableView;
@property(strong,nonatomic) UIView *viewBtnBg;
@property(strong,nonatomic) NSArray *arrayFourBtns;
@property(strong,nonatomic) UIView *viewAudienceLive;
@property(strong,nonatomic) UILabel *lbLive;
@property(strong,nonatomic) UILabel *lbAudience;
@property(strong,nonatomic) UIButton *btnFlowers;
@property(strong,nonatomic) UIButton *btnMore;
@property(strong,nonatomic) UIView *viewTools;
@property(strong,nonatomic) UIButton *btnDeliver;
@property(strong,nonatomic) UIButton *btnFace;
@property(strong,nonatomic) UIButton *btnComments;
@property(strong,nonatomic) UIButton *btnRemoteControl;
@property(strong,nonatomic) NSMutableArray *mArrayPresent;
@property(strong,nonatomic) UIButton *btnAddAudience;
@property(strong,nonatomic) UIView *tempView;



/**
 *  导航栏更多菜单
 */
@property(nonatomic,strong)YDPopBt *btnMsg;
@property(nonatomic,strong)YDPopBt *btnPlaying;
@property(nonatomic,strong)YDPopBt *btnRoom;
@property(nonatomic,strong)YDPopBt *btnReport;

/**
 *  播放按键
 */
@property(strong,nonatomic) UIButton *btnPlay;
/**
 *  播放状态
 */
@property(assign,nonatomic) BOOL isPlay;

@end

@implementation YDRoomVC
@synthesize telecontrollerView;
@synthesize btnMsg;
@synthesize btnPlaying;
@synthesize btnRoom;
@synthesize btnReport;
@synthesize stKTVRoomInfo;

#pragma mark - 生命周期
-(void)viewDidLoad
{
    [super viewDidLoad];
    
    _numOfRows = 0;
    
    [self setUI];
}

-(void)setUI
{
    [self setNav];
    
    [self setFourBtns];
    
    [self setAudienceLive];
    
    [self setBottomToolBar];
    
    [self setMsgTableView];
    
    [self setTelecontroller];
}

#pragma mark 导航栏
-(void)setNav
{
    [self setRightBtnItem];
    
    [self setLeftBtnItem];
    
    [self setNavigatonTitleVeiw];
}

#pragma mark 遥控器
-(void)setTelecontroller
{
    CGFloat btnWh = 44; // 遥控器的宽

# warning ---  64是导航栏和状态栏的高度和，他有时候被计算在了屏幕高度screenHeight中，有时候没有呢
    _btnTelecontroller = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - 15 - btnWh, screenHeight - 64 - tabBarH - 30, btnWh, btnWh)];
    [_btnTelecontroller setImage:[UIImage imageNamed:@"telecontroller.png"] forState:UIControlStateNormal];
    [_btnTelecontroller addTarget:self action:@selector(clcikTelecontroller) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnTelecontroller];
}

#pragma mark - 遥控器的控制界面
-(void)clcikTelecontroller
{
    self.telControlBgView = [[UIView alloc]init];
    _telControlBgView.frame =[UIScreen mainScreen].bounds;
    //    _telControlBgView.backgroundColor = [UIColor colorWithRed:203/255.0 green:203/255.0 blue:203/255.0 alpha:0.5];
    _telControlBgView.backgroundColor = [UIColor colorWithRed:172/255.0 green:172/255.0 blue:172/255.0 alpha:0.5];
    
    [self setTelecontrollerUI];
}


#pragma mark - button相关属性设置的快捷方法
-(YDPopBt *)PopBtnWithImg:(NSString *)img title:(NSString *)title count:(int)count frame:(CGRect)frame
{
    YDPopBt *btn = [[YDPopBt alloc]initWithFrame:frame];
    
    btn.imgPop.image = [UIImage imageNamed:img];
    btn.lbPop.text = title;
    
    [btn.btnBadge setTitle:[NSString stringWithFormat:@"%d",count] forState:UIControlStateNormal];
    if ([btn.btnBadge.titleLabel.text isEqual:@""] || [btn.btnBadge.titleLabel.text isEqual:@"0" ] ) {
        btn.btnBadge.hidden = YES;
    }
    
    return btn;
}

#pragma mark -弹出窗口的点击事件
-(void)clickMsg
{
    YDLog(@"clickMsg");
    [_tempView.superview.superview removeFromSuperview];
}

-(void)clickPlaying
{
    YDLog(@"clickPlaying");
    [_tempView.superview.superview removeFromSuperview];
}

-(void)clickRoom
{
    YDLog(@"clickRoom");
    [_tempView.superview.superview removeFromSuperview];
}

-(void)clickReport
{
    YDLog(@"clickReport");
    [_tempView.superview.superview removeFromSuperview];
}

#pragma mark -点击事件
#pragma mark 点击了更多
-(void)clickRightBarBtnItem
{
    YDLog(@"点击了导航栏右边的Btn");
    
    UIImage *popImg = [UIImage imageNamed:@"popMenuBg.png"];
    CGFloat popMenuW = popImg.size.width;
    CGFloat popMenuH = popImg.size.height;
    _tempView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, popMenuW, popMenuH)];
    _tempView.userInteractionEnabled = YES;
    
    // 第一个btn的 y
    CGFloat btnW = 145;
    CGFloat btnH = 39;
    CGFloat firstBtnY = 5.5;
    
    btnMsg = [self PopBtnWithImg:@"msg.png" title:@"消息" count:18 frame:CGRectMake(0,firstBtnY, btnW, btnH)];
    btnPlaying = [self PopBtnWithImg:@"jiniance_btn.png" title:@"编辑纪念册" count:5 frame:CGRectMake(0,firstBtnY + btnH, btnW, btnH)];
    btnRoom = [self PopBtnWithImg:@"jiniance_btn.png" title:@"使用礼券" count:0 frame:CGRectMake(0, firstBtnY + 2 * btnH,btnW, btnH)];
    btnReport = [self PopBtnWithImg:@"changroom_btn.png" title:@"换包房" count:0 frame:CGRectMake(0, firstBtnY + 3 * btnH,btnW, btnH)];
    btnReport.imgLine.hidden = YES;
    
    [_tempView addSubview:btnMsg];
    [_tempView addSubview:btnPlaying];
    [_tempView addSubview:btnRoom];
    [_tempView addSubview:btnReport];
    
    [btnMsg.BtnPop addTarget:self action:@selector(clickMsg) forControlEvents:UIControlEventTouchUpInside];
    [btnPlaying.BtnPop addTarget:self action:@selector(clickPlaying) forControlEvents:UIControlEventTouchUpInside];
    [btnRoom.BtnPop addTarget:self action:@selector(clickRoom) forControlEvents:UIControlEventTouchUpInside];
    [btnReport.BtnPop addTarget:self action:@selector(clickReport) forControlEvents:UIControlEventTouchUpInside];
    
    HMPopMenu *menu = [HMPopMenu popMenuWithContentView:_tempView];
    CGFloat menuW = popMenuW;
    CGFloat menuH = popMenuH;
    CGFloat menuY = 55;
    CGFloat menuX = self.view.width - menuW - 20;
    menu.dimBackground = YES;
    menu.arrowPosition = HMPopMenuArrowPositionRight;
    [menu showInRect:CGRectMake(menuX, menuY, menuW, menuH)];
}


#pragma -mark - 布局遥控器UI
-(void)setTelecontrollerUI{

    // 遥控器界面
    CGFloat viewH = 277; // 遥控器的高度
    telecontrollerView = [[UIView alloc]init];
    telecontrollerView.frame = CGRectMake(0, screenHeight, screenWidth, viewH);
    telecontrollerView.backgroundColor = [UIColor whiteColor];
    //    [self.view addSubview:telecontrollerView];
    
    [_telControlBgView addSubview:telecontrollerView];
    
    // 八叉 & 头部紫色边栏
    UIView *viewTop = [[UIView alloc]init];
    viewTop.frame = CGRectMake(0, 0, screenWidth, 34);
    viewTop.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.35];
    [telecontrollerView addSubview:viewTop];
    
    
    
    
    //八叉关闭按钮
    UIButton *btnOct = [[UIButton  alloc]init];
    btnOct.frame = CGRectMake(screenWidth - 44 - 15,  screenHeight - viewH - 22, 44, 44);//screenHeight - viewH
    [btnOct setImage:[UIImage imageNamed:@"purpleOCT.png"] forState:UIControlStateNormal];
    [btnOct addTarget:self action:@selector(clickOCT) forControlEvents:UIControlEventTouchUpInside];
    
    [_telControlBgView addSubview:btnOct];
    
    //将遥控器隐藏掉
    self.btnTelecontroller.hidden = YES;
    
    
    //播放界面
    CGFloat cellHeight = 45; //每一个区域的基本高度
    CGFloat fromX = 15; //距左侧屏幕的距离
    
    //播放按钮
   
    
    UIImage *playImg = [UIImage imageNamed:@"播放.png"];
    self.btnPlay = [[UIButton alloc]initWithFrame:CGRectMake(fromX,viewTop.y + viewTop.height + (cellHeight - 1 - playImg.size.height * 0.65) * 0.5, playImg.size.width * 0.65 , playImg.size.height * 0.65)];
    [_btnPlay setImage:playImg forState:UIControlStateNormal];
    [_btnPlay addTarget:self action:@selector(playMusicHandle:) forControlEvents:UIControlEventTouchUpInside];
    [telecontrollerView addSubview:_btnPlay];
    
    //当前播放歌曲的名字
    UILabel *lbSongName = [[UILabel alloc]initWithFrame:CGRectMake(_btnPlay.x + _btnPlay.width + 25, _btnPlay.y, 136, 16)];
    lbSongName.text = @"路边的野花你不要踩";
    [lbSongName setTextColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
    [lbSongName setTextAlignment:NSTextAlignmentLeft];
    [lbSongName setFont:[UIFont systemFontOfSize:14.0]];
    [telecontrollerView addSubview:lbSongName];
    
    
    //重置按钮
    UIImage *resetImg = [UIImage imageNamed:@"重新演唱.png"];
    
    UIButton *btnReset = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - fromX - resetImg.size.width * 0.65, _btnPlay.y, resetImg.size.width * 0.65, resetImg.size.height * 0.65)];
    [btnReset setImage:resetImg forState:UIControlStateNormal];
    [telecontrollerView addSubview:btnReset];
    
    //歌曲的长度
    
    UILabel *lbSongLength = [[UILabel alloc]initWithFrame:CGRectMake(btnReset.x - 25 - 25 , _btnPlay.y, 30, 12)];
    lbSongLength.text = @"04:20";
    [lbSongLength setTextColor:[UIColor rgbFromHexString:@"#D8D8D8" alpaa:1.0]];
    [lbSongLength setFont:[UIFont systemFontOfSize:10.0]];
    [telecontrollerView addSubview:lbSongLength];
    
    //分割线1
    CGFloat lineHeight = 1;
    UIImageView *imgLine_1 = [[UIImageView alloc]initWithFrame:CGRectMake(fromX,viewTop.height + cellHeight - lineHeight, screenWidth - 2 * fromX, lineHeight)];
    imgLine_1.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
    [telecontrollerView addSubview:imgLine_1];

    //原唱
    UILabel *lbLboriginal = [[UILabel alloc]initWithFrame:CGRectMake(fromX, imgLine_1.y + lineHeight + (cellHeight - 15 - lineHeight) * 0.5, 28, 15)];
    lbLboriginal.text = @"原唱";
    [lbLboriginal setTextColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
    [lbLboriginal setFont:[UIFont systemFontOfSize:14.0]];
    [telecontrollerView addSubview:lbLboriginal];
    
    //开关
    UISwitch *switchReal = [[UISwitch alloc]initWithFrame:CGRectMake(screenWidth - fromX - 52, imgLine_1.y + imgLine_1.height + (cellHeight - 32 - lineHeight) * 0.5, 52, 32)];
    
    //设置on时的颜色
    switchReal.onTintColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
    [telecontrollerView addSubview:switchReal];
    
    //分割线2
    UIImageView *imgLine_2 = [[UIImageView alloc]initWithFrame:CGRectMake(fromX, imgLine_1.y + cellHeight, screenWidth - 2 * fromX, lineHeight)];
    imgLine_2.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
    [telecontrollerView addSubview:imgLine_2];
    
    
//    //调节麦克风音量
//    
    UILabel *lbMicrophoneVolume = [[UILabel alloc]initWithFrame:CGRectMake(fromX, lbLboriginal.y + cellHeight, 60, 16)];
    lbMicrophoneVolume.text = @"麦克音量";
    [lbMicrophoneVolume setTextColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
    [lbMicrophoneVolume setFont:[UIFont systemFontOfSize:14.0]];
    [telecontrollerView addSubview:lbMicrophoneVolume];
    
    UISlider *microphoneVolume = [[UISlider alloc]initWithFrame:CGRectMake(lbMicrophoneVolume.x + 60, imgLine_2.y + (cellHeight - 40 - lineHeight) * 0.5 , screenWidth - 2 * fromX - 60, 40)];
    microphoneVolume.minimumValue = 0;
    microphoneVolume.maximumValue = 10;
    //设置默认音量
    microphoneVolume.value = 3;
    microphoneVolume.minimumTrackTintColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
    microphoneVolume.maximumTrackTintColor = [UIColor rgbFromHexString:@"#000000" alpaa:1.0];
    [telecontrollerView addSubview:microphoneVolume];
    
    //修改麦克音量控制为按钮
    
    
    
    //分割线3
    UIImageView *imgLine_3 = [[UIImageView alloc]initWithFrame:CGRectMake(fromX, imgLine_2.y + cellHeight, screenWidth - 2 * fromX, lineHeight)];
    imgLine_3.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
    [telecontrollerView addSubview:imgLine_3];
    
    
    //调节音乐音量
    UILabel *lbMusicVolume = [[UILabel alloc]initWithFrame:CGRectMake(fromX, lbMicrophoneVolume.y + cellHeight, 60, 16)];
    lbMusicVolume.text = @"音乐音量";
    [lbMusicVolume setTextColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
    [lbMusicVolume setFont:[UIFont systemFontOfSize:14.0]];
    [telecontrollerView addSubview:lbMusicVolume];
    
    UISlider *musicVolume = [[UISlider alloc]initWithFrame:CGRectMake(lbMusicVolume.x + 60, imgLine_3.y + (cellHeight - 40 - lineHeight) * 0.5 , screenWidth - 2 * fromX - 60, 40)];
    musicVolume.minimumValue = 0;
    musicVolume.maximumValue = 10;
    //设置默认音量
    musicVolume.value = 8;
    //    UIImage *controlImg2 =  [UIImage imageNamed:@"调整钮.png"];
    //    [microphoneVolume setThumbImage:controlImg2 forState:UIControlStateNormal];
    musicVolume.minimumTrackTintColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
    musicVolume.maximumTrackTintColor = [UIColor rgbFromHexString:@"#000000" alpaa:1.0];
    [telecontrollerView addSubview:musicVolume];
    
    //修改音乐音量控制为按钮
    
    
    
    //分割线4
    UIImageView *imgLine_4 = [[UIImageView alloc]initWithFrame:CGRectMake(fromX, imgLine_3.y + cellHeight, screenWidth - 2 * fromX, lineHeight)];
    imgLine_4.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
    [telecontrollerView addSubview:imgLine_4];
    
    //切歌
//    CGFloat btnWH = 39; //button的宽和高
    UIImage *nextSongImg = [UIImage imageNamed:@"切歌.png"];
    UIButton *btnNextSong = [[UIButton alloc]initWithFrame:CGRectMake(fromX, imgLine_4.y + imgLine_4.height + (cellHeight * 2 - nextSongImg.size.height * 0.5) * 0.25 , nextSongImg.size.width * 0.5, nextSongImg.size.height * 0.5)];
    [btnNextSong setImage:nextSongImg forState:UIControlStateNormal];
    [telecontrollerView addSubview:btnNextSong];
    
    UILabel *lbNextSong = [[UILabel alloc]initWithFrame:CGRectMake(btnNextSong.x + btnNextSong.width, btnNextSong.centerY - 7, 26, 14)];
    lbNextSong.text = @"切歌";
    [lbNextSong setFont:[UIFont systemFontOfSize:12.0]];
    [lbNextSong setTextColor:[UIColor rgbFromHexString:@"#D8D8D8" alpaa:1.0]];
    [telecontrollerView addSubview:lbNextSong];
    
    
    //下一首
    UIButton *btnNextMusic = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - fromX * 2 - 100, btnNextSong.centerY - 8, 100, 16)];
    UILabel *lbNext = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 50, 16)];
    lbNext.text = @"下一首";
    [lbNext setTextColor:[UIColor rgbFromHexString:@"#000000" alpaa:1.0]];
    [lbNext setFont:[UIFont systemFontOfSize:12.0]];
    
    UILabel *lbNextMusicName = [[UILabel alloc]initWithFrame:CGRectMake(50, 0, 50, 16)];
    lbNextMusicName.text = @"简单爱";
    [lbNextMusicName setTextColor:[UIColor rgbFromHexString:@"AC56FF" alpaa:1.0]];
    [lbNextMusicName setFont:[UIFont systemFontOfSize:14.0]];
    [btnNextMusic addSubview:lbNext];
    [btnNextMusic addSubview:lbNextMusicName];
    
    [telecontrollerView addSubview:btnNextMusic];
    
    //暂时保留延时
//    [UIView beginAnimations:nil context:nil];  // 遥控界面，从底部弹出的动画
//    [UIView setAnimationDuration:0.35];
    
    telecontrollerView.frame = CGRectMake(0, screenHeight - viewH  , screenWidth, viewH);
    [[[UIApplication sharedApplication]keyWindow] addSubview:_telControlBgView];
    
//    [UIView commitAnimations];
    
   
    
}

#pragma mark - playMusicHandle播放音乐事件
-(void)playMusicHandle:(UIButton *)playBtn{

    if (!_isPlay) {
        [self.btnPlay setImage:[UIImage imageNamed:@"暂停.png"] forState:UIControlStateNormal];
        _isPlay = YES;
    }else{
        [self.btnPlay setImage:[UIImage imageNamed:@"播放.png"] forState:UIControlStateNormal];
        _isPlay = NO;
    }
}

#pragma mark - 点击八叉关闭控制器页面
-(void)clickOCT
{
    [UIView beginAnimations:nil context:nil];
//    [UIImage setVersion:0.35];
    [UIView setAnimationDuration:0.35];
    
    telecontrollerView.frame = CGRectMake(0, screenHeight + 44 * 0.5, screenWidth, 277);  // 277是遥控器界面的高 ，44 * 0.5 是八叉的高度的一半
    [_telControlBgView removeFromSuperview];
    
    //显示遥控器
    self.btnTelecontroller.hidden = NO;
    
    [UIView commitAnimations];
    YDLog(@"点击了八叉");
}

// 导航栏右边的Btn
-(void)setRightBtnItem
{
    UIButton *rightBtn = [[UIButton alloc]init];
    UIImage *imgMore = [UIImage imageNamed:@"more.png"];
    rightBtn.frame = CGRectMake(0, 0, imgMore.size.width, imgMore.size.height);
    [rightBtn setImage:imgMore forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(clickRightBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customRightBtnItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = customRightBtnItem;
}

// 导航栏左边的Btn
-(void)setLeftBtnItem
{
    UIButton *leftBtn = [[UIButton alloc]init];
    UIImage *leftBtnImg = [UIImage imageNamed:@"whiteBack.png"];
    leftBtn.frame = CGRectMake(0, 0, leftBtnImg.size.width, leftBtnImg.size.height);
    [leftBtn setImage:leftBtnImg forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(clickLeftBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customLeftBtnItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = customLeftBtnItem;
}

// 导航栏的标题栏
-(void)setNavigatonTitleVeiw
{
    CGFloat titleViewH = 35;
    UIView *titleView = [[UIView alloc]initWithFrame:CGRectMake(0,0, screenWidth * 0.80, titleViewH)];
    titleView.backgroundColor = [UIColor clearColor];
    
    CGFloat mySubcribeBtnW = 55;
    UIButton *mySubcribeBtn = [[UIButton alloc]initWithFrame:CGRectMake((titleView.width - mySubcribeBtnW) * 0.5, 0, 80, titleViewH)];
    [mySubcribeBtn setTintColor:[UIColor whiteColor]];
    [mySubcribeBtn setBackgroundColor:[UIColor clearColor]];
    [mySubcribeBtn setTitle:@"包房" forState:UIControlStateNormal];
    [mySubcribeBtn.titleLabel setFont:[UIFont systemFontOfSize:17.5]];
    [titleView addSubview:mySubcribeBtn];
    
    CGFloat gotoSubcribeBtnW = 85;
    UIButton *gotoSubcribeBtn = [[UIButton alloc]initWithFrame:CGRectMake(titleView.width - gotoSubcribeBtnW, 0, gotoSubcribeBtnW, titleViewH)];
    [gotoSubcribeBtn setTintColor:[UIColor lightTextColor]];
    [gotoSubcribeBtn setTitle:@"去隔壁" forState:UIControlStateNormal];
    [gotoSubcribeBtn.titleLabel setFont:[UIFont systemFontOfSize:14.5]];
    gotoSubcribeBtn.titleLabel.textAlignment = NSTextAlignmentRight;
    gotoSubcribeBtn.backgroundColor = [UIColor clearColor];
    [gotoSubcribeBtn addTarget:self action:@selector(clickLaunchParty) forControlEvents:UIControlEventTouchUpInside];
    [titleView addSubview:gotoSubcribeBtn];
    self.navigationItem.titleView = titleView;
}

-(void)clickLeftBarBtnItem
{
    
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark -Data
/*
 条件后面会变，当数据发生变化的时候，进来，而不是为空的时候进来
 */
#pragma mark 现场~
-(NSMutableArray *)arrayLive
{
    NSMutableArray *tempMArray = [NSMutableArray arrayWithArray:nil];
    if (_arrayLive == nil) {
        tempMArray = [NSMutableArray arrayWithArray:nil];
        
        _arrayLive = tempMArray;
    }

    return _arrayLive;
}

#pragma mark 观众~
-(NSMutableArray *)arrayAudience
{
    NSMutableArray *tempMArray = [NSMutableArray arrayWithArray:nil];
    if (_arrayAudience == nil) {
        tempMArray = [NSMutableArray arrayWithArray:nil];
        
        
        _arrayAudience = tempMArray;
        
    }

    return _arrayAudience;
}

#pragma mark 消息
-(NSMutableArray *)arrayMsgs
{
    NSMutableArray *tempArray =[NSMutableArray arrayWithArray:nil];
    if (_arrayMsgs == nil) {
        tempArray = [NSMutableArray arrayWithArray:nil];
        
        _arrayMsgs = tempArray;
    }
    
    return _arrayMsgs;
}

#pragma mark - UI
#pragma mark 顶部四个Btn
-(void)setFourBtns
{
    
    // 4个Btn背景视图
    CGFloat viewBtnBgW = screenWidth;
    CGFloat viewBtnBgH = 100;
    CGFloat viewBtnBgX = 0;
    CGFloat viewBtnBgY = naviAndStatusH;
    _viewBtnBg = [[UIView alloc]initWithFrame:CGRectMake(viewBtnBgX, viewBtnBgY, viewBtnBgW, viewBtnBgH)];
    [self.view addSubview:_viewBtnBg];
    _viewBtnBg.backgroundColor = [UIColor whiteColor];
    // 按钮frame
    CGFloat btnW = screenWidth * 0.25;
    CGFloat btnH = 50;
    CGFloat btnY = 15;
    
    // labelframe
    CGFloat lbW = btnW;
    CGFloat lbH = 15;
    CGFloat lbY = btnY + btnH + 10;
    
    // 初始化4个Btn
    NSMutableArray *arrayBtnsTemp = [NSMutableArray arrayWithCapacity:4];
    
    for (int index = 0; index < 4; index ++) {
        
        CGFloat btnX = index * btnW;
        UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(btnX, btnY, btnW, btnH)];
        
        CGFloat lbX = btnX;
        UILabel *lb = [[UILabel alloc]initWithFrame:CGRectMake(lbX, lbY, lbW, lbH)];
        lb.textColor = [UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0];
        lb.textAlignment = NSTextAlignmentCenter;
        lb.font = [UIFont systemFontOfSize:11.0];

        
        lb.backgroundColor = [UIColor clearColor];
        btn.backgroundColor = [UIColor clearColor];
        
        
        [_viewBtnBg addSubview:lb];
        
        switch (index) {
            case 0:{
                [btn setImage:[UIImage imageNamed:@"selectSong.png"] forState:UIControlStateNormal];
                [btn addTarget:self action:@selector(clickSelectSong) forControlEvents:UIControlEventTouchUpInside];

                lb.text = @"点歌";
                
                break;
            }case 1:{
                [btn setImage:[UIImage imageNamed:@"alreadySelect.png"] forState:UIControlStateNormal];
                [btn addTarget:self action:@selector(clickHadSelect) forControlEvents:UIControlEventTouchUpInside];

                lb.text = @"已点";
                
                break;
            }case 2:{
                
                [btn setImage:[UIImage imageNamed:@"partyAlbum.png"] forState:UIControlStateNormal];
                [btn addTarget:self action:@selector(clickAlbum) forControlEvents:UIControlEventTouchUpInside];

                lb.text = @"聚会相册";
                
                break;
            }case 3:{
                [btn setImage:[UIImage imageNamed:@"alreadyRecord.png"] forState:UIControlStateNormal];
                [btn addTarget:self action:@selector(clickHadRec) forControlEvents:UIControlEventTouchUpInside];

                lb.text = @"已录歌曲";
                
            }
            default:
                break;
        }
        
        [arrayBtnsTemp addObject:btn];
        [_viewBtnBg addSubview:btn];
        [btn setTitleColor:[UIColor lightTextColor] forState:UIControlStateNormal];
    }
 
}

#pragma mark 现场 & 观众   
-(void)setAudienceLive
{
    // 现场观众的cell的高度是50
    CGFloat viewH = 50;
    // 现场 & 观众 众多控件底部的承载视图
    CGFloat viewAudienceLiveW = screenWidth;
    CGFloat viewAudienceLiveH = 100;
    CGFloat viewAudienceLiveX = 0;
    CGFloat viewAudienceLiveY = _viewBtnBg.height + _viewBtnBg.y;
    _viewAudienceLive = [[UIView alloc]initWithFrame:CGRectMake(viewAudienceLiveX, viewAudienceLiveY, viewAudienceLiveW, viewAudienceLiveH)];
    [self.view addSubview:_viewAudienceLive];
    
    _viewAudienceLive.backgroundColor = [UIColor whiteColor];
    
    // 水平分割线
    UIImageView *imgHorizline_1 = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 1)];
    imgHorizline_1.backgroundColor = [UIColor lightGrayColor];
    [_viewAudienceLive addSubview:imgHorizline_1];
    
    // 现场-label
    _lbLive = [[UILabel alloc]initWithFrame:CGRectMake(0,imgHorizline_1.y + imgHorizline_1.height, 40, 49)];
    [_viewAudienceLive addSubview:_lbLive];
    [_lbLive setText:@"现场"];
    [_lbLive setFont:[UIFont systemFontOfSize:14.0]];
    _lbLive.textColor = [UIColor rgbFromHexString:@"4A4A4A" alpaa:1.0];
    [_lbLive setTextAlignment:NSTextAlignmentCenter];
    
    _mArrayPresent = [[NSMutableArray alloc]initWithCapacity:5];
    for (int index = 0; index < [ListenToMeData getInstance].stKTVRoomInfo.vInUserBaseInfos.count; index ++) {
        UserBaseInfoNet *stInUserBaseInfo = [stKTVRoomInfo.vInUserBaseInfos objectAtIndex:index];
        
        CGFloat imgWh = 30; // 头像的宽高都是30
        UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(index * (imgWh + 2) + 50, imgHorizline_1.y +  imgHorizline_1.height + (viewH - imgWh) * 0.5, imgWh, imgWh)];
        if (stInUserBaseInfo!=nil && stInUserBaseInfo.sCovver!=nil && [stInUserBaseInfo.sCovver length] > 0) {
            [img sd_setImageWithURL:[NSURL URLWithString:stInUserBaseInfo.sCovver]];
        }else{
            img.image = [UIImage imageNamed:@"temp1.png"];
        }
        
        [_mArrayPresent addObject:img];
    }
    
    
    for (UIImageView *tempImg in _mArrayPresent) {
        [_viewAudienceLive addSubview:tempImg];
    }
    
    // 水平分割线
    UIImageView *imgHorizline_2 = [[UIImageView alloc]initWithFrame:CGRectMake(10,_lbLive.y + _lbLive.height, screenWidth - 20, 1)];
    imgHorizline_2.backgroundColor = [UIColor lightGrayColor];
    [_viewAudienceLive addSubview:imgHorizline_2];
    
    // 观众-label
    _lbAudience = [[UILabel alloc]initWithFrame:CGRectMake(0, imgHorizline_2.y + imgHorizline_2.height, 40, 49)];
    [_viewAudienceLive addSubview:_lbAudience];
    [_lbAudience setText:@"观众"];
    [_lbAudience setFont:[UIFont systemFontOfSize:14.0]];
    _lbAudience.textColor = [UIColor rgbFromHexString:@"4A4A4A" alpaa:1.0];
    _lbAudience.backgroundColor = [UIColor clearColor];
    [_lbAudience setTextAlignment:NSTextAlignmentCenter];
    
    // 邀请观众
    _btnAddAudience = [[UIButton alloc]init];
    _btnAddAudience.frame = CGRectMake(_lbAudience.x + _lbAudience.width, _lbAudience.y, 120, _lbAudience.height);
    [_btnAddAudience setImage:[UIImage imageNamed:@"addAudience.png"] forState:UIControlStateNormal];
    [_btnAddAudience setTitle:@"邀请观众" forState:UIControlStateNormal];
    [_btnAddAudience setTitleColor:[UIColor rgbFromHexString:@"#BF7DFF" alpaa:1.0] forState:UIControlStateNormal];
    _btnAddAudience.titleLabel.font = [UIFont systemFontOfSize:12.0];
    [_btnAddAudience addTarget:self action:@selector(clickAddAudience) forControlEvents:UIControlEventTouchUpInside];
    [_btnAddAudience setTitleEdgeInsets:UIEdgeInsetsMake(0,15, 0, 0)];
    [_viewAudienceLive addSubview:_btnAddAudience];
    _btnAddAudience.backgroundColor = [UIColor clearColor];
    _btnAddAudience.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
 
//    // 现场花
//    _btnFlowers = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - 70, imgHorizline_1.height,65 , 49)];
//    [_viewAudienceLive addSubview:_btnFlowers];
//    [_btnFlowers setImage:[UIImage imageNamed:@"temp2.png"] forState:UIControlStateNormal];
//    [_btnFlowers setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -8)];
//    [_btnFlowers setTitle:@"1125" forState:UIControlStateNormal];
//    [_btnFlowers setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
//    [_btnFlowers.titleLabel setFont:[UIFont systemFontOfSize:10.0]];
//    [_btnFlowers addTarget:self action:@selector(clickLiveFlower) forControlEvents:UIControlEventTouchUpInside];
    
    
    // 更多
    _btnMore = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - 70, imgHorizline_2.height + imgHorizline_2.y,65 , 49)];
    [_viewAudienceLive addSubview:_btnMore];
    [_btnMore setImage:[UIImage imageNamed:@"pureMore.png"] forState:UIControlStateNormal];
    [_btnMore setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -8)];
    [_btnMore setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [_btnMore.titleLabel setFont:[UIFont systemFontOfSize:10.0]];
    [_btnMore addTarget:self action:@selector(clickMore) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark 底部工具栏
-(void)setBottomToolBar
{
   // 底部toolBar
    _viewTools = [[UIView alloc]initWithFrame:CGRectMake(0, screenHeight - 70, screenWidth, 70)]; // 44是假值---toolbar的高度
    [self.view addSubview:_viewTools];
    [_viewTools setBackgroundColor:[UIColor whiteColor]];
    
    CGFloat btnW = screenWidth / 3;
    
    // 送花Btn
    _btnFlowers = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, btnW, 70)];
    [_btnFlowers setImage:[UIImage imageNamed:@"giveFlower.png"] forState:UIControlStateNormal];
    [_viewTools addSubview:_btnFlowers];
    _btnFlowers.backgroundColor = [UIColor whiteColor];
    [_btnFlowers addTarget:self action:@selector(clickFlower) forControlEvents:UIControlEventTouchUpInside];
    
    // 表情动作Btn
    _btnFace = [[UIButton alloc]initWithFrame:CGRectMake(btnW, 0, btnW, 70)];
    [_btnFace setImage:[UIImage imageNamed:@"emotionAction.png"] forState:UIControlStateNormal];
    [_viewTools addSubview:_btnFace];
    _btnFace.backgroundColor = [UIColor whiteColor];
    [_btnFace addTarget:self action:@selector(clickFace) forControlEvents:UIControlEventTouchUpInside];
    
    // 评论Btn
    _btnComments = [[UIButton alloc]initWithFrame:CGRectMake(2 * btnW, 0,btnW, 70)];
    [_btnComments setImage:[UIImage imageNamed:@"toolSpeak.png"] forState:UIControlStateNormal];
    [_viewTools addSubview:_btnComments];
    _btnComments.backgroundColor = [UIColor whiteColor];
    [_btnComments addTarget:self action:@selector(clickComments) forControlEvents:UIControlEventTouchUpInside];
    
    UITextField *stUITextField = [[UITextField alloc] initWithFrame:CGRectMake(2 * btnW, 0,btnW, 70)];
    stUITextField.returnKeyType = UIReturnKeyGo;
    stUITextField.delegate = self;
    [stUITextField setTextColor:[UIColor clearColor]];
    [stUITextField setBackgroundColor:[UIColor clearColor]];
    [_viewTools addSubview:stUITextField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    //回收键盘,取消第一响应者
    [textField resignFirstResponder]; return YES;
    //发送消息
    NSLog(@"textFieldShouldReturntextFieldShouldReturn");
}

#pragma mark 消息
-(void)setMsgTableView
{
    CGFloat mTableViewH = screenHeight - _viewAudienceLive.height - _viewAudienceLive.y - _viewTools.height;
    CGFloat mTableViewY = _viewAudienceLive.y + _viewAudienceLive.height;
    _mTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, mTableViewY, screenWidth, mTableViewH) style:UITableViewStylePlain];
    [self.view addSubview:_mTableView];
    
    _mTableView.backgroundColor = [UIColor rgbFromHexString:@"#F1F1F1" alpaa:1.0];
    _mTableView.separatorColor = [UIColor clearColor];
    
    _mTableView.bounces = NO;
    _mTableView.delegate = self;
    _mTableView.dataSource = self;
}

#pragma mark - tableview的代理 & 数据源
-(YDRoomMsgCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *ID = @"roomMsg";
    YDRoomMsgCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (cell == nil) {
        cell = [[YDRoomMsgCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    
    // 判断当是最后一行cell的时候，隐藏分割横线
    if (indexPath.row == 7) {
        cell.imgHorizLineOut.hidden = YES;
    }
    
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _numOfRows;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 130;
}


#pragma mark - 点击事件
-(void)clickComments
{
    YDLog(@"评论");
}

-(void)clickFace
{
    YDLog(@"发表情");
}

-(void)clickFlower
{
     YDLog(@"工具栏送花");
}

-(void)clickMore
{
    YDLog(@"更多现场 & 观众");
}

-(void)clickLiveFlower
{
    YDLog(@"送花");
}

-(void)clickSelectSong
{
    YDLog(@"-点歌");
}

-(void)clickHadSelect
{
    YDLog(@"-已点");
    [[NetReqManager getInstance] sendGetKtvListInfo:0];
}

-(void)clickAlbum
{
    YDLog(@"-相册");
//    NSString *filePath = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"m_qingrou.mp3"];
//    NSData *voiceData = [NSData dataWithContentsOfFile:filePath];
//    [[ListenToMeData getInstance].sendingDataDic removeAllObjects];
//    [[ListenToMeData getInstance] addDataForUpload:voiceData withPartSize:102400];
//    [[NetReqManager getInstance] sendUploadMusicWorkCurSeq:0 sendDataDic:[ListenToMeData getInstance].sendingDataDic desc:@"222" songid:127 musicworkid:0];
}

-(void)clickHadRec
{
    YDLog(@"-已录歌曲");
//    [[NetReqManager getInstance] sendSetUserInfo:nil];
//    [[NetReqManager getInstance] sendSearchSong:@"明" offset:0 num:20];
//    [[NetReqManager getInstance] sendFeedback:@"哈哈哈哈哈哈"];
//    [[NetReqManager getInstance] sendSearchKTV:@"明" offset:0 num:20];
//    [[NetReqManager getInstance] sendLogout];
//    [[NetReqManager getInstance] sendUploadPhoneNumber:@"18501190800"];
//    [[NetReqManager getInstance] sendVerifyCode:@"368825"];
//    [[NetReqManager getInstance] sendGetCollectWorkList:0 num:20];
//    [[NetReqManager getInstance] sendGetMyMusicWork:0 num:20];
//    [[NetReqManager getInstance] sendGetDateWorkList:0 num:20];
    [[NetReqManager getInstance] sendAddDateWorkInfo];
//    [[NetReqManager getInstance] sendMySongListInfo:0 num:20];
//    [[NetReqManager getInstance] sendGetUserCouponList:0 num:20];
//    [[NetReqManager getInstance] sendGetCommemorateInfo:0 num:20];
//    [[NetReqManager getInstance] sendSearchCommemorate:@"明" offset:0 num:20];
//    [[NetReqManager getInstance] sendGetPictureList:0 num:20];
//    [[NetReqManager getInstance] sendGetMusicWorkByID:1334343];
//     [[NetReqManager getInstance] sendGetNewMusicList:0 num:20];
//    [[NetReqManager getInstance] sendUpdate];
    
}

-(void)clickLaunchParty
{
    YDLog(@"去隔壁");
}

-(void)clickAddAudience
{
    YDLog(@"邀请观众");
}



@end
