//
//  YDRoomVC.h
//  ListenToMe
//
//  Created by yadong on 1/26/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YDBaseBtn.h"
#import "YDRoomMsgCell.h"
#import "NetReqManager.h"
#import "ListenToMeData.h"
#import "YDPopBt.h"
#import "HMPopMenu.h"
#import "UIImageView+WebCache.h"

@interface YDRoomVC : YDBaseVC{
    KTVRoomInfo *stKTVRoomInfo;
}
@property(nonatomic,retain) KTVRoomInfo *stKTVRoomInfo;

@end
