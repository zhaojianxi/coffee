//
//  YDRoomCell.m
//  ListenToMe
//
//  Created by yadong on 2/10/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#define cellH 130

#import "YDRoomMsgCell.h"

@interface YDRoomMsgCell ()

@end

@implementation YDRoomMsgCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
       
        // 头像
        _btnAvatar = [[YDCircleAvatarBtn alloc]initWithFrame:CGRectMake(15, 15, 30, 30)];
        _btnAvatar.layer.masksToBounds = YES;
        _btnAvatar.layer.cornerRadius = _btnAvatar.width * 0.2;
        
        self.avatarImgView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 15, 30, 30)];
        self.avatarImgView.layer.masksToBounds = YES;
        self.avatarImgView.layer.cornerRadius = self.avatarImgView.width * 0.2;
        self.avatarImgView.userInteractionEnabled = YES;
        [self.contentView addSubview:self.avatarImgView];
        
        
        [self addSubview:_btnAvatar];
//        [_btnAvatar setImage:[UIImage imageNamed:@"temp2.png"] forState:UIControlStateNormal];
        
        
        // 用户名--from
        _lbFromPerson = [[YDScrollLabel alloc]initWithFrame:CGRectMake(_btnAvatar.width + 30, _btnAvatar.centerY - 15, 160, 30)];
        [self.contentView addSubview:_lbFromPerson];
        [_lbFromPerson setFont:[UIFont systemFontOfSize:12.0]];
        [_lbFromPerson setTextColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
        _lbFromPerson.text = @"Peter:";
        
        // 时间
        _lbTime = [[YDScrollLabel alloc]initWithFrame:CGRectMake(screenWidth - 80 - 15, _lbFromPerson.y, 80, 20)];
        [self.contentView addSubview:_lbTime];
        [_lbTime setTextColor:[UIColor rgbFromHexString:@"#C2C2C2" alpaa:1.0]];
        [_lbTime setTextAlignment:NSTextAlignmentRight];
        [_lbTime setFont:[UIFont systemFontOfSize:10.0]];
        _lbTime.text = @"刚刚";
        
        
        // 内部分割线
        _imgHorizLineIn = [[UIImageView alloc]initWithFrame:CGRectMake(_lbFromPerson.x, _lbFromPerson.y + _lbFromPerson.height, screenWidth - _lbFromPerson.x - 15, 1)];
        [self.contentView addSubview:_imgHorizLineIn];
        _imgHorizLineIn.backgroundColor = [UIColor lightGrayColor];
        _imgHorizLineIn.alpha = 0.5;
        
        // 消息内容
        _lbContents = [[YDScrollLabel alloc]initWithFrame:CGRectMake(_lbFromPerson.x, _imgHorizLineIn.y + 5, screenWidth - _lbFromPerson.x  - _lbTime.width, 50)];
        [self.contentView addSubview:_lbContents];
        [_lbContents setFont:[UIFont systemFontOfSize:12.0]];
        _lbContents.textColor = [UIColor rgbFromHexString:@"4A4A4A" alpaa:1.0];
        _lbContents.text = @"别跟他们去，跟我去/他们都是骗子，千万别跟他们去";
        _lbContents.backgroundColor = [UIColor clearColor];
        
        // 外部分割线
        _imgHorizLineOut = [[UIImageView alloc]initWithFrame:CGRectMake(15,cellH - 1, screenWidth - 30, 1)];
        [self addSubview:_imgHorizLineOut];
        _imgHorizLineOut.backgroundColor = [UIColor lightGrayColor];
    }
    
    return self;
}




- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
