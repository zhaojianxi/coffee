//
//  YDRoomFrontPageViewController.h
//  ListenToMe
//
//  Created by afei on 15/4/9.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YDRoomFrontPageViewController : UIViewController{
    UITextField *mTextFInput;
    
}

@property(nonatomic,retain) UITextField *mTextFInput;
-(void)clickLeftBarBtnItem;

@end
