//
//  YDRoomOutInputViewController.m
//  ListenToMe
//
//  Created by afei on 15/4/10.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "YDRoomOutInputViewController.h"

@interface YDRoomOutInputViewController ()<UITextFieldDelegate>
@property(nonatomic,retain)UITextField *mTextFInput;
@end

@implementation YDRoomOutInputViewController
@synthesize mTextFInput;

#pragma mark -生命周期
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigatonTitleVeiw];
    [self setUI];
}

#pragma mark -UI
-(void)setUI{
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    UIImageView *mTextInputIV = [[UIImageView alloc] initWithFrame:CGRectMake(50, 100, 220, 39)];
    [mTextInputIV setImage:[UIImage imageNamed:@"compartment_connect_input_bg.png" ]];
    [self.view addSubview:mTextInputIV];
    
    mTextFInput = [[UITextField alloc]initWithFrame:CGRectMake(60, 100 ,200, 39)];
    mTextFInput.backgroundColor = [UIColor clearColor];
    mTextFInput.keyboardType = UIKeyboardTypeNumberPad;
    mTextFInput.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [self.view addSubview:mTextFInput];
    
    UIButton *connectBtn = [[UIButton alloc]initWithFrame:CGRectMake(50, 150, 223, 40)];
    [connectBtn setBackgroundImage:[UIImage imageNamed:@"connect.png"] forState:UIControlStateNormal];
    [connectBtn setTitle:@"链接" forState:UIControlStateNormal];
    [connectBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [connectBtn addTarget:self action:@selector(clickConnectBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:connectBtn];
}

#pragma mark -其他
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// 导航栏的标题栏
-(void)setNavigatonTitleVeiw
{
    CGFloat titleViewH = 35;
    UIView *titleView = [[UIView alloc]initWithFrame:CGRectMake(0,0, screenWidth * 0.80, titleViewH)];
    titleView.backgroundColor = [UIColor clearColor];
    
    CGFloat mySubcribeBtnW = 55;
    UIButton *mySubcribeBtn = [[UIButton alloc]initWithFrame:CGRectMake((titleView.width - mySubcribeBtnW) * 0.5, 0, 80, titleViewH)];
    [mySubcribeBtn setTintColor:[UIColor whiteColor]];
    [mySubcribeBtn setBackgroundColor:[UIColor clearColor]];
    [mySubcribeBtn setTitle:@"包房" forState:UIControlStateNormal];
    [mySubcribeBtn.titleLabel setFont:[UIFont systemFontOfSize:17.5]];
    [titleView addSubview:mySubcribeBtn];
    
    self.navigationItem.titleView = titleView;
}

-(void)clickConnectBtn{
    //发送进入包房协议
}
@end
