//
//  NXDBDAO.h
//  vshare
//
//  Created by cloudwu cloudwu on 12-1-31.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface NXDBDAO : NSObject
{
    NSManagedObjectModel *managedObjectModel;
    NSPersistentStoreCoordinator *persistentStoreCoordinator;
    long long currentUuid;
}

@property (nonatomic, strong, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, strong, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic) long long currentUuid;

+ (NXDBDAO *)shareInstance;

- (void)removePersistentStoreCoordinator;

- (void)insertToTable:(NSString*)table withDictionary:(NSDictionary *)dataDic context:(NSManagedObjectContext *)context;
- (BOOL)updateToTable:(NSString*)table withDictionary:(NSDictionary *)dataDic condition:(NSString*)condition seqKey:(NSString*)seqKey context:(NSManagedObjectContext *)context;
- (void)commitWithContext:(NSManagedObjectContext *)context;
- (NSFetchedResultsController*) selectFromTable:(NSString*)tableName 
                                       delegate:(id)delegate 
                                     seqWithKey:(NSString*)seqKey 
                                      ascending:(BOOL)isAscending
                                      condition:(NSString*)condition
                                          limit:(int)limit
                                        context:(NSManagedObjectContext *)context;
- (void)deleteFromTable:(NSString*)table condition:(NSString*)condition seqKey:(NSString*)seqKey context:(NSManagedObjectContext *)context;
- (void)migrateDatabase;

@end
