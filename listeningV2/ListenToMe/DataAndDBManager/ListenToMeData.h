//
//  ListenToMeData.h
//  ListenToMe
//
//  Created by yadong on 1/27/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetRecvDelegate.h"
#import "ListenToMe.pb.h"

@interface ListenToMeData : NSObject

@property(strong,nonatomic) AsyncSocket *socketIns;
@property(strong,nonatomic) NetRecvDelegate *netRecvDelegate;
@property(assign,nonatomic) BOOL bConnecting;
@property(assign,nonatomic) BOOL bConnected;
@property(assign,nonatomic) BOOL bAPMode;
@property (nonatomic,retain)NSMutableDictionary *sendingDataDic;
@property(nonatomic,retain) NSString *qrcode;
@property(nonatomic,retain) KTVRoomInfo *stKTVRoomInfo;

+(ListenToMeData *)getInstance;
-(int)addDataForUpload:(NSData *)data withPartSize:(NSInteger)size;


/**
 *  用户的基本信息
 */
@property(nonatomic,strong) UserBaseInfoNet *stUserBaseInfoNet;
/**
 *  用户的头像
 */
@property(nonatomic,strong) NSURL *sCoverUrl;
/**
 *  用户名
 */
@property(nonatomic,strong) NSString *sNick;
/**
 *  用户的性别
 */
@property(nonatomic,assign) int iGender;
/**
 *  用户的个人签名
 */
@property(nonatomic,strong) NSString *sDesc;
/**
 *  Uuid标识
 */
@property(nonatomic,assign) long long uuid;
/**
 *  //KTVList列表数据
 */
@property(nonatomic,strong) NSMutableArray *ktvListBaseInfo;
/**
 *  Ktv 信息
 */
@property(nonatomic,strong) KtvBaseInfo *ktvBaseInfo;
/**
 *  KTV 更换新一批用户
 */
@property(nonatomic,strong) NSMutableArray *arrUserBaseInfoNet;
/**
 *  //KtvList中的轮播图
 */
@property(nonatomic,strong) NSMutableArray *ktvListBannerInfo;
/**
 *  人气歌王
 */
@property(nonatomic,strong) NSMutableArray *popularityList;
@property(nonatomic,retain)NSString *token;
/**
 *  获取用户的作品数组
 */
@property(nonatomic,strong) NSMutableArray *arrMusicWorkBaseInfo;
/**
 *  用户作品详情
 */
@property(nonatomic,strong) MusicWorkBaseInfo *musicWorkBaseInfo;
/**
 *  用户纪念册的信息
 */
@property(nonatomic,strong) NSMutableArray *arrCommemorateBaseInfo;
/**
 *  人气歌王
 */
@property(nonatomic,strong) NSMutableArray *arrPopularityList;
/**
 *  新歌速递列表 对应深情星空
 */
@property(nonatomic,strong) NSMutableArray *arrNewMusicList;
/**
 *  每日推荐列表, 对应时空胶囊
 */
@property(nonatomic,strong) NSMutableArray *arrRecommendList;

/**
 *  用户相册列表数组
 */
@property(nonatomic,strong) NSMutableArray *arrPictureBaseInfo;
/**
 *  用户相册
 */
@property(nonatomic,strong) PictureBaseInfo *stPictureBaseInfo;
/**
 *  搜索ktv获取的ktv数组
 */
@property(nonatomic,strong) NSMutableArray *arrSearchKtvBaseInfo;
/**
 *  搜索歌曲作品获取的歌曲数组
 */
@property(nonatomic,strong) NSMutableArray *arrSearchSongInfo;
/**
 *  搜索纪念册获取的纪念册数组
 */
@property(nonatomic,strong) NSMutableArray *arrSearchCommemorateBaseInfo;
/**
 *  当前要收藏的歌曲作品的信息
 */
@property(nonatomic,strong) MusicWorkBaseInfo *collectMusicWorkBaseInfo;
/**
 *  当前要收藏的纪念册的信息
 */
@property(nonatomic,strong) CommemorateBaseInfo *collectCommemorateBaseInfo;
/**
 *  当前取消收藏的歌曲作品信息
 */
@property(nonatomic,strong) MusicWorkBaseInfo *unCollectMusicWorkBaseInfo;
/**
 *  当前取消收藏的纪念册信息
 */
@property(nonatomic,strong) CommemorateBaseInfo *unCollectCommemorateBaseInfo;
/**
 *  用户的收藏列表里的信息
 */
@property(nonatomic,strong) NSMutableArray *arrCollectWorkInfo;
/**
 *  用户礼券列表信息
 */
@property(nonatomic,strong) NSMutableArray *arrCouponBaseInfo;
/**
 *  要删除的用户纪念册
 */
@property(nonatomic,strong) CommemorateBaseInfo *stCommemorateBaseInfo;
/**
 *  要删除的用户的音乐作品
 */
@property(nonatomic,strong) MusicWorkBaseInfo *stMusicWorkBaseInfo;
/**
 *  根据id获取的纪念册信息
 */
@property(nonatomic,strong) NSMutableArray *arrCommemorateInfoByUserId;
/**
 *  当前试听的音乐作品
 */
@property(nonatomic,strong) MusicWorkBaseInfo *currentMusicAudition;
/**
 *  用户评论音乐作品的信息
 */
@property(nonatomic,strong) CommentInfo *addMusicCommntInfo;
/**
 *  获取音乐作品的评论信息列表
 */
@property(nonatomic,strong) NSMutableArray *arrCommentListInfo;
/**
 *  当前要送花的音乐作品
 */
@property(nonatomic,strong) MusicWorkBaseInfo *sendFlowerToMusicWork;
/**
 *  当前要送花给某个人
 */
@property(nonatomic,strong) UserBaseInfoNet *sendFlowerToUser;
/**
 *  当前要送花的纪念册
 */
@property(nonatomic,strong) CommemorateBaseInfo *sendFlowerToCommemorate;
/**
 *  要删除的礼券
 */
@property(nonatomic,strong) CouponBaseInfo *deleteCouponBaseInfo;
/**
 *  用户小黑屋状态 0 放出小黑屋 1关进小黑屋
 */
@property(nonatomic,assign) int bMute;
/**
 *  相同礼券的数量的礼券数组
 */
@property(nonatomic,strong) NSMutableArray *sameCouponBaseInfoArr;
/**
 *  私聊的好友
 */
@property(nonatomic,strong) UserBaseInfoNet *privateChatUser;
/**
 *  用户的消息记录
 */
@property(nonatomic,strong) NSMutableArray *arrUserMsgRecord;
/**
 *  聊天的对象
 */
@property(nonatomic,assign) int64_t lTalkWith;
/**
 *  聊天是发送消息的消息id
 */
@property(nonatomic,assign) int64_t lMsgId;
/**
 *  聊天发送的消息内容
 */
@property(nonatomic,strong) NSString *sMsg;
@end
