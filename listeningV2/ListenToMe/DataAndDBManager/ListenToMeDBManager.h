//
//  ListenToMeDBManager.h
//  ListenToMe
//
//  Created by afei on 15/3/12.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NXDBDAO.h"

#define USER_ID                 @"USER_ID"

@interface ListenToMeDBManager : NSObject{
    NXDBDAO *mDBDAO;
    NSMutableDictionary *contextPool;
    NSMutableDictionary *threadPool;
    NSLock *dbLock;
}
@property (nonatomic, strong, readonly) NSMutableDictionary *contextPool;
@property (nonatomic, strong, readonly) NSMutableDictionary *threadPool;
@property (nonatomic, strong, readonly) NSLock *dbLock;

+ (ListenToMeDBManager *)shareInstance;

+ (void)setSqliteForManager:(long long)userid;
+ (void)removeAllContext;


//是否要上传绑定信息
+ (void)setUuid:(long long)uuid;
+ (long long)getUuid;

@end
