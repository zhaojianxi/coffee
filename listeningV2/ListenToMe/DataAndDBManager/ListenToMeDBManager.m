//
//  ListenToMeDBManager.m
//  ListenToMe
//
//  Created by afei on 15/3/12.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "ListenToMeDBManager.h"

static ListenToMeDBManager * gDBManager = nil;

@implementation ListenToMeDBManager
@synthesize contextPool,threadPool,dbLock;

+ (ListenToMeDBManager *)shareInstance
{
    if(gDBManager == nil)
    {
        gDBManager = [[ListenToMeDBManager alloc] init];
    }
    NSString *key = [NSThread currentThread].name;
    if(key && ![key isEqualToString:@""])
    {
        [gDBManager.dbLock lock];
        NSManagedObjectContext *managedObjectContext = [gDBManager.contextPool objectForKey:key];
        if(managedObjectContext == nil)
        {
            managedObjectContext = [[NSManagedObjectContext alloc] init];
            [gDBManager.contextPool setObject:managedObjectContext forKey:key];
            [gDBManager.threadPool setObject:[NSThread currentThread] forKey:key];
        }
        [gDBManager.dbLock unlock];
    }
    return gDBManager;
}

- (id)init
{
    if(self = [super init])
    {
        mDBDAO = [NXDBDAO shareInstance];
        [self contextPool];
        [self threadPool];
        [self dbLock];
        mDBDAO.currentUuid = 0;
    }
    return self;
}

- (NSMutableDictionary *)contextPool
{
    if(contextPool == nil)
    {
        contextPool = [[NSMutableDictionary alloc] init];
    }
    return contextPool;
}

- (NSMutableDictionary *)threadPool
{
    if(threadPool == nil)
    {
        threadPool = [[NSMutableDictionary alloc] init];
    }
    return threadPool;
}

- (NSLock *)dbLock
{
    if(dbLock == nil)
    {
        dbLock = [[NSLock alloc] init];
    }
    return dbLock;
}

+ (void)setSqliteForManager:(long long)userid
{
    [gDBManager.dbLock lock];
    if(gDBManager && userid > 0)
    {
        for(NSString *key in [gDBManager.contextPool allKeys])
        {
            NSManagedObjectContext *managedObjectContext = [gDBManager.contextPool objectForKey:key];
            if(managedObjectContext.persistentStoreCoordinator)
                continue;
            NXDBDAO *dbDAO = [NXDBDAO shareInstance];
            dbDAO.currentUuid = userid;
            NSPersistentStoreCoordinator *persistentStoreCoordinator = [dbDAO persistentStoreCoordinator];
            if(persistentStoreCoordinator)
            {
                [managedObjectContext setPersistentStoreCoordinator:persistentStoreCoordinator];
                [[NSNotificationCenter defaultCenter] addObserver:gDBManager
                                                         selector:@selector(mergeContextChangesForNotification:)
                                                             name:NSManagedObjectContextDidSaveNotification
                                                           object:managedObjectContext];
            }
        }
    }
    [gDBManager.dbLock unlock];
}

+ (void)removeAllContext
{
    [gDBManager.dbLock lock];
    if(gDBManager && gDBManager.contextPool)
    {
        [gDBManager.contextPool removeAllObjects];
        [gDBManager.threadPool removeAllObjects];
    }
    [[NXDBDAO shareInstance] removePersistentStoreCoordinator];
    [gDBManager.dbLock unlock];
}

- (void)mergeContextChangesForNotification:(NSNotification *)aNotification
{
    [gDBManager.dbLock lock];
    for(NSString *key in [gDBManager.contextPool allKeys])
    {
        NSManagedObjectContext *managedObjectContext = [gDBManager.contextPool objectForKey:key];
        if(managedObjectContext && managedObjectContext.persistentStoreCoordinator && managedObjectContext != aNotification.object)
        {
            NSThread *thread = [gDBManager.threadPool objectForKey:key];
            if(thread == [NSThread mainThread])
            {
                [managedObjectContext performSelectorOnMainThread:@selector(mergeChangesFromContextDidSaveNotification:) withObject:aNotification waitUntilDone:NO];
            }
            else if(thread)
            {
                [managedObjectContext performSelector:@selector(mergeChangesFromContextDidSaveNotification:) onThread:thread withObject:aNotification waitUntilDone:NO];
            }
        }
    }
    [gDBManager.dbLock unlock];
}

- (NSManagedObjectContext *)getManagedObjectContext
{
    [gDBManager.dbLock lock];
    NSManagedObjectContext *managedObjectContext = nil;
    NSString *key = [NSThread currentThread].name;
    if(key && ![key isEqualToString:@""])
    {
        managedObjectContext = [contextPool objectForKey:key];
    }
    if(managedObjectContext == nil)
    {
        NSLog(@"NXDBManager error : unknown thread access NXDBManager");
    }
    else if(!managedObjectContext.persistentStoreCoordinator)
    {
        managedObjectContext = nil;
        NSLog(@"NXDBManager error : not set sqlite for context");
    }
    [gDBManager.dbLock unlock];
    return managedObjectContext;
}


//用户uuid
+ (void)setUuid:(long long)uuid{
    NSUserDefaults *userDefualts = [NSUserDefaults standardUserDefaults];
    NSArray *list = [userDefualts objectForKey:USER_ID];
    NSNumber *uuidObj = [NSNumber numberWithLongLong:uuid];
    if(list == nil)
    {
        list = [NSArray arrayWithObject:uuidObj];
        [userDefualts setValue:list forKey:USER_ID];
        [userDefualts synchronize];
    }
    else
    {
        NSMutableArray *temp = [NSMutableArray arrayWithArray:list];
        if([temp containsObject:uuidObj])
        {
            [temp removeObject:uuidObj];
        }
        [temp insertObject:uuidObj atIndex:0];
        NSArray *newList = [NSArray arrayWithArray:temp];
        [userDefualts setValue:newList forKey:USER_ID];
        [userDefualts synchronize];
    }
}

+ (long long)getUuid{
    NSUserDefaults *userDefualts = [NSUserDefaults standardUserDefaults];
    NSArray *list = [userDefualts objectForKey:USER_ID];
    if(list != nil)
    {
        return [[list objectAtIndex:0] longLongValue];
    }
    return 0;
}



@end
