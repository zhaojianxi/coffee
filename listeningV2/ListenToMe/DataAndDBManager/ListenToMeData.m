//
//  ListenToMeData.m
//  ListenToMe
//
//  Created by yadong on 1/27/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "ListenToMeData.h"

@implementation ListenToMeData
@synthesize socketIns;
@synthesize netRecvDelegate;
@synthesize bConnecting;
@synthesize bAPMode;
@synthesize sendingDataDic;
@synthesize qrcode;
@synthesize stKTVRoomInfo;

static ListenToMeData *mEngine;
+(ListenToMeData *)getInstance
{
    @synchronized(self)
    {
        if (!mEngine) {
            mEngine = [[self alloc]init];
        }
    }
    
    return mEngine;
}

-(id)init
{
    if (self = [super init]) {
       
        NSError *ermsg;
        
        if (!self.netRecvDelegate) {
            netRecvDelegate = [[NetRecvDelegate alloc]init];
        }
        
        socketIns = [[AsyncSocket alloc]initWithDelegate:netRecvDelegate];
        bAPMode = NO;
        sendingDataDic = [[NSMutableDictionary alloc] init];
        qrcode = @"";
        
        if ([socketIns isConnected] == false) {
            
            if (![socketIns connectToHost:SERVER_IP onPort:ON_PORT error:&ermsg]) {
                [CCNotify sentNotify:NOTIFY_NET_DISCONNECT obj:nil, nil];
                bConnecting = NO;
                YDLog(@"CONNECT_FAILED,SERVER_IP = %@\nON_PORT = %d,ermsg = %@",SERVER_IP,ON_PORT,ermsg);
            }
        }
    }
    
    return self;
}


-(int)addDataForUpload:(NSData *)data withPartSize:(NSInteger)size
{
    @autoreleasepool {
        
        NSInteger len = [data length];
        const char* byte = [data bytes];
        
        for (int i = 0; i<len; i+=size) {
            
            NSData *partData = [[NSData alloc] initWithBytes:byte+i length:((len-i)>size?size:(len-i))];
            [sendingDataDic setObject:partData forKey:[NSString stringWithFormat:@"%lu",(unsigned long)[sendingDataDic count]]];
        }
        
        return  0;
    }
}

@end


