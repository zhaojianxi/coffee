//
//  NXDBDAO.m
//  vshare
//
//  Created by cloudwu cloudwu on 12-1-31.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "NXDBDAO.h"

static NXDBDAO * gInstance = nil;

@implementation NXDBDAO

@synthesize currentUuid;

+ (NXDBDAO *)shareInstance
{
    @synchronized(self)
    {
        if(gInstance == nil)
        {
            gInstance = [[NXDBDAO alloc] init];
        }
    }
    
    return gInstance;
}


- (void)dealloc
{
//    [managedObjectModel release];
    managedObjectModel = nil;
    
    if(persistentStoreCoordinator)
//        [persistentStoreCoordinator release];
        persistentStoreCoordinator = nil;
    
//    [gInstance release];
    gInstance = nil;
    
//    [super dealloc];
}

#pragma mark - Core Data stack

// Returns the path to the application's documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created from the application's model.
 */
- (NSManagedObjectModel *)managedObjectModel
{
    if (managedObjectModel != nil)
    {
        return managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"BabyTalkModel" withExtension:@"momd"];
    managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];    
    return managedObjectModel;
}

/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (persistentStoreCoordinator != nil)
    {
        return persistentStoreCoordinator;
    }
    
    long long strUuid = currentUuid;
    
    NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *dirPath = [docPath stringByAppendingPathComponent:@"sqlite"];
    if(![[NSFileManager defaultManager] fileExistsAtPath:dirPath])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:dirPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    NSString *filePath = [dirPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%lld.sqlite", strUuid]];
    
    
    NSURL *storeURL = [NSURL fileURLWithPath:filePath];
    NSError *error = nil;
    NSDictionary *options = @{ NSSQLitePragmasOption : @{@"journal_mode" : @"DELETE"} };
    
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error])
    {
        NSLog(@"persistentStoreCoordinator error %@, %@", error, [error userInfo]);
    }    
    
    return persistentStoreCoordinator;
}

- (void)removePersistentStoreCoordinator
{
    if(persistentStoreCoordinator)
    {
        persistentStoreCoordinator = nil;
    }
}

#pragma mark - public method
- (void)insertToTable:(NSString*)table withDictionary:(NSDictionary *)dataDic context:(NSManagedObjectContext *)context
{
    
    NSManagedObject *newManagedObject = [NSEntityDescription insertNewObjectForEntityForName:table 
                                                                      inManagedObjectContext:context];
    
    // If appropriate, configure the new managed object.
    
    [newManagedObject setValuesForKeysWithDictionary:dataDic];
}

- (BOOL)updateToTable:(NSString*)table withDictionary:(NSDictionary *)dataDic condition:(NSString*)condition seqKey:(NSString*)seqKey context:(NSManagedObjectContext *)context
{
    NSFetchedResultsController *resultsController = [self selectFromTable:table 
                                                                 delegate:self 
                                                               seqWithKey:seqKey 
                                                                ascending:NO
                                                                condition:condition
                                                                    limit:0
                                                                  context:context];
    id<NSFetchedResultsSectionInfo> sectionInfo = [[resultsController sections] objectAtIndex:0];
    if ([sectionInfo numberOfObjects] > 0) 
    {
        NSManagedObject *mObject = [[sectionInfo objects] objectAtIndex:0];
        if (mObject) 
        {
            [mObject setValuesForKeysWithDictionary:dataDic];
        } 
        else 
        {
            return NO;
        }
    } 
    else 
    {
        NSManagedObject *newManagedObject = [NSEntityDescription insertNewObjectForEntityForName:table inManagedObjectContext:context];
        [newManagedObject setValuesForKeysWithDictionary:dataDic];
    }
    return YES;
}

- (void) commitWithContext:(NSManagedObjectContext *)context
{
    // Save the context.
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) 
    {
        NSLog(@"commitWithContext error %@, %@", error, [error userInfo]);
    }    
}


- (NSFetchedResultsController*) selectFromTable:(NSString*)tableName 
                                       delegate:(id)delegate 
                                     seqWithKey:(NSString*)seqKey 
                                      ascending:(BOOL)isAscending
                                      condition:(NSString*)condition
                                          limit:(int)limit
                                        context:(NSManagedObjectContext *)context
{ 
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:tableName inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    [fetchRequest setFetchBatchSize:20];
    [fetchRequest setFetchLimit:limit];

    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:seqKey ascending:isAscending];
    //    NSLog(@"initWithObjects2 begin");
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    //     NSLog(@"initWithObjects2 end");
    [fetchRequest setSortDescriptors:sortDescriptors];
   
    
    //数据过滤条件
    if (condition && ![@""isEqualToString:condition]) {
        [fetchRequest setPredicate:[NSPredicate predicateWithFormat:condition]];
    }
    @try{
    
        __autoreleasing NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
        fetchedResultsController.delegate = delegate;
       
        NSError *error = nil;
        //TO-DO:是否放到接口之外执行
        if (![fetchedResultsController performFetch:&error])
        {
            NSLog(@"performFetch error %@, %@", error, [error userInfo]);
        }
        
        return fetchedResultsController;
    }
    @catch (NSException *e) {
        NSLog(@"error :%@" , e.reason);
    }
    @finally {
        
    }
//    fetchRequest = nil;
//    sortDescriptor = nil;
//    sortDescriptors = nil;
//    [fetchRequest release];
//    [sortDescriptor release];
//    [sortDescriptors release];
    

}

- (void)deleteFromTable:(NSString *)tableName 
              condition:(NSString *)condition 
                 seqKey:(NSString*)seqKey
                context:(NSManagedObjectContext *)context
{
    
    NSFetchedResultsController *resultsController = [self selectFromTable:tableName 
                                                                 delegate:self 
                                                               seqWithKey:seqKey 
                                                                ascending:NO
                                                                condition:condition
                                                                    limit:0
                                                                  context:context];
  long  int objSize = [resultsController.fetchedObjects count] ;
    if (objSize > 0) 
    {
        for (int i = 0; i < objSize; i++) 
        {
            [context deleteObject:[resultsController.fetchedObjects objectAtIndex:i]];
        }
        
    }
}

//数据库合并(字段升级)
- (void)migrateDatabase
{
    NSString *defaultUserdataPath = [[NSBundle mainBundle] resourcePath];
    NSString *modelPath = [defaultUserdataPath stringByAppendingPathComponent:@"BabyTalkModel.momd"];
    NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *destPath = [docPath stringByAppendingPathComponent:@"BabyTalkModel.momd"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSString *sqlitePath = [docPath stringByAppendingPathComponent:@"sqlite"];
    
    if (![fileManager fileExistsAtPath:destPath]) 
    {
        //文件不存在,保存model
        NSError *error = nil;
        if (![fileManager copyItemAtPath:modelPath toPath:destPath error:&error]) 
        {
            NSLog(@"copyItemAtPath error %@, %@", error, [error userInfo]);
        }
    }
    
    //删除所有sqlite文件
    NSError *error = nil;
    if([fileManager removeItemAtPath:sqlitePath error:&error]) {
        NSLog(@"removeItemAtPath error %@, %@", error, [error userInfo]);
    }
}

@end
