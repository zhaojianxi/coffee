//
//  PIVC.m
//  ListenToMe
//
//  Created by yadong on 2/7/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "PIVC.h"
#import <MobileCoreServices/MobileCoreServices.h>
@interface PIVC ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate>
@property(strong,nonatomic) UIScrollView *mScrollView;
@property(strong,nonatomic) UITableView *mTableView;
@property(strong,nonatomic) UIButton *btnAvatar;
@property(strong,nonatomic) UITextField *textFdNickName;
@property(strong,nonatomic) UITextField *textFdLocation;
@property(strong,nonatomic) UITextField *textFdDesc;
@property(strong,nonatomic) UIDatePicker *datePicker;
@property(strong,nonatomic) UIButton *timeCancelBtn;
@property(strong,nonatomic) UIButton *timeChooseBtn;
@property(strong,nonatomic) UIView *viewBtnBG;
@property(strong,nonatomic) UILabel *lbBirthday;
@property(assign,nonatomic) BOOL keyboardIsShow;
@property(strong,nonatomic) UIAlertController *alertCtrl;
/**
 *  返回修改生日时间
 */
@property(nonatomic,strong) NSString *brithDayString;
/**
 *  修改的时间
 */
@property(nonatomic,strong) NSDateFormatter *changDate;
/**
 *  性别男
 */
@property(nonatomic,strong) UIButton *btnBoy;
/**
 *  选择boy
 */
@property(nonatomic,assign) BOOL isSelectBoy;
/**
 *  性别女
 */
@property(nonatomic,strong)UIButton *btnGirl;
/**
 *  选择girl
 */
@property(nonatomic,assign) BOOL isSelectGirl;
@property(nonatomic,assign) ListenToMeData *listenToMeData;

@end

@implementation PIVC
@synthesize btnAvatar;
@synthesize mTableView;
@synthesize textFdNickName;
@synthesize textFdLocation;
@synthesize textFdDesc;
@synthesize datePicker;
@synthesize timeCancelBtn;
@synthesize timeChooseBtn;
@synthesize viewBtnBG;
@synthesize lbBirthday;
@synthesize keyboardIsShow;
@synthesize mScrollView;

#pragma mark -生命周期
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    
    [self setUI];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>=8.0) {
        self.modalPresentationStyle=UIModalPresentationOverCurrentContext;
    }
    
    
    
}

#pragma mark -Data
-(void)initData
{
    keyboardIsShow = NO;
    self.listenToMeData = [ListenToMeData getInstance];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShowRec:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHideRec:) name:UIKeyboardWillHideNotification object:nil];
    
    
}

-(void)refreshUserInfo{
//    [[NetReqManager getInstance] sendGetUserInfo:[ListenToMeDBManager getUuid]];
}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
}

#pragma mark -UI
-(void)setUI
{
    // 滚动视图
    CGFloat mScrollViewW = screenWidth;
    CGFloat mScrollViewH = screenHeight;
    CGFloat mScrollViewX = 0;
    CGFloat mScrollViewY = 0;
    mScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(mScrollViewX, mScrollViewY, mScrollViewW, mScrollViewH)];
    mScrollView.delegate = self;
    mScrollView.scrollEnabled = NO;
    [self.view addSubview:mScrollView];
    
    mScrollView.contentSize = CGSizeMake(screenWidth, screenHeight);
    
    // tableView
    CGFloat mTableViewW = screenWidth;
    CGFloat mTableViewH = screenHeight + 20;
    CGFloat mTableViewX = 0;
    CGFloat mTableViewY = -20;
    mTableView = [[UITableView alloc]initWithFrame:CGRectMake(mTableViewX, mTableViewY, mTableViewW, mTableViewH) style:UITableViewStyleGrouped];
    mTableView.delegate = self;
    mTableView.dataSource = self;
    [mScrollView addSubview:mTableView];
    
    mTableView.scrollEnabled = NO;
    
    // 导航栏
    [self setUpNav];
    
}

#pragma mark 导航栏
-(void)setUpNav
{
    self.navigationItem.title = @"个人信息";
    
    [self setRightBtnItem];
    
    [self setLeftBtnItem];
}

// 导航栏右边的Btn
-(void)setRightBtnItem
{
    UIButton *rightBtn = [[UIButton alloc]init];
    rightBtn.frame = CGRectMake(0, 0, 50, 40);
    [rightBtn setTitle:@"完成" forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(clickRightBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customRightBtnItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = customRightBtnItem;
   

}

// 导航栏左边的Btn
-(void)setLeftBtnItem
{
    UIButton *leftBtn = [[UIButton alloc]init];
    UIImage *leftBtnImg = [UIImage imageNamed:@"whiteBack.png"];
    leftBtn.frame = CGRectMake(0, 0, leftBtnImg.size.width, leftBtnImg.size.height);
    [leftBtn setImage:leftBtnImg forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(clickLeftBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customLeftBtnItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = customLeftBtnItem;
}

-(void)clickRightBarBtnItem
{
    YDLog(@"编辑完成");
    //上传用户信息设置
    int32_t iGender = 0;
    if (self.btnBoy.selected) {
        iGender = 1;
    }else{
        iGender = 0;
    }
    
    [[NetReqManager getInstance] sendGetSetUserInfo:self.listenToMeData.stUserBaseInfoNet.uuid SNickName:textFdNickName.text IGender:iGender SBirthday:lbBirthday.text SSignature:textFdDesc.text IAge:self.listenToMeData.stUserBaseInfoNet.iAge IPrivataSwitch:self.listenToMeData.stUserBaseInfoNet.iPrivateSwitch Address:textFdLocation.text];
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)clickLeftBarBtnItem
{
    YDLog(@"返回");
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -点击Actionsheet对应Btn的事件
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    YDLog(@"这是第几个btn--%ld",buttonIndex);
    if (buttonIndex == 0) {
        
    }else if(buttonIndex == 1){
        
        [self openAlbum];
        
    }else if(buttonIndex == 2){
        [self openCamera];
    }else if(buttonIndex == 3){
        
    }else {
        YDLog(@"不可能");
    }
}

#pragma mark 初始化actionsheet
-(void)presentSheet
{
    
    if (iOS8_greater) { // iOS8及其以上
        _alertCtrl = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        [_alertCtrl addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
//        [_alertCtrl addAction:[UIAlertAction actionWithTitle:@"我的相册" style:UIAlertActionStyleDefault handler:nil]];
        [_alertCtrl addAction:[UIAlertAction actionWithTitle:@"本地相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self openAlbum];
        }]];
        [_alertCtrl addAction:[UIAlertAction actionWithTitle:@"拍摄" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self openCamera];
        
        }]];
        
       
        
        [[UIView appearanceWhenContainedIn:[UIAlertController class], nil] setTintColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
       
        [self presentViewController:_alertCtrl animated:YES completion:^{
        
        }];

        
    }else{ // iOS7及其以下
    
        UIActionSheet *menu = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"本地相册",@"拍摄", nil];
        
        [menu showInView:self.view];
    }
}

// ActionSheet的代理方法，设置其文字颜色
// iOS8 无法这样设置, actionSheet.subviews.count == 0
-(void)willPresentActionSheet:(UIActionSheet *)actionSheet
{
    if (iOS7_less) {
        for (UIView *subView in actionSheet.subviews) {
            
            if ([subView isKindOfClass:[UIButton class]]) {
                UIButton *btn = (UIButton *)subView;
                [btn setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateNormal];
            }
        }
    } else if(iOS8_greater){
        
                YDLog(@"iOS8下，ActionSheet的字体颜色设置不能通过遍历其子控件的方式来实现\n改用actionsheete为alertController");
    }
}



#pragma mark -键盘遮挡输入框
#pragma mark 显示 or 隐藏键盘
-(void)keyboardDidShowRec:(NSNotification *)noti
{
    if (keyboardIsShow) {
        return;
    }
    
    NSDictionary *userInfo = [noti userInfo];
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    // get original scrollview,then reSize it'frame
    CGRect oldToNewSViewFrame = mScrollView.frame;
    oldToNewSViewFrame.size.height -= (keyboardSize.height);
    
    [mScrollView setFrame:oldToNewSViewFrame];
    [UIView commitAnimations];
    
    // 滚动到当前UITextView
    //    CGRect textViewFrame = [textFdDesc frame];
    CGRect textViewFrame = CGRectMake(0,300, screenWidth, 20);
    
    [mScrollView scrollRectToVisible:textViewFrame animated:YES];
    
    keyboardIsShow = YES;
}

-(void)keyboardWillHideRec:(NSNotification *)noti
{
    
    //    NSDictionary *userinfo = [noti userInfo];
    //    CGSize keyboardSize = [[userinfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    // newToOldFrame
    //    CGRect newToOldFrame = mScrollView.frame;
    //    newToOldFrame.size.height += keyboardSize.height;
    CGRect scrollToFrame = CGRectMake(0, 20, screenWidth, 20);
    [mScrollView scrollRectToVisible:scrollToFrame animated:YES];
    
    mScrollView.frame = CGRectMake(0, 0, screenWidth, screenHeight);
    
    if (!keyboardIsShow) {
        return;
    }
    
    keyboardIsShow = NO;
}



#pragma mark -textField的代理方法
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    
    // 保存操作
    
    return YES;
}

#pragma mark -delegate & datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"personalInfo";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
    }
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.textLabel.text = @"头像";
            
            CGFloat btnAvatarW = 30;
            CGFloat btnAvatarH = 30;
            CGFloat btnAvatarX = screenWidth - 80;
            CGFloat btnAvatarY = ( 50 - btnAvatarH ) * 0.5;
            btnAvatar = [[UIButton alloc]initWithFrame:CGRectMake(btnAvatarX, btnAvatarY, btnAvatarW, btnAvatarH)];
            [cell addSubview:btnAvatar];
            
            btnAvatar.layer.cornerRadius = 5;
            btnAvatar.layer.masksToBounds = YES;
            UIImageView *avatarImagView = [[UIImageView alloc]initWithFrame:btnAvatar.bounds];
            [avatarImagView sd_setImageWithURL:[NSURL URLWithString:self.listenToMeData.stUserBaseInfoNet.sCovver] placeholderImage:[UIImage imageNamed:@"temp2.png"]];
            [btnAvatar addSubview:avatarImagView];
            
        }else if(indexPath.row == 1){
            cell.textLabel.text = @"昵称";
            
            CGFloat textFdNickNameW = screenWidth * 0.5;
            CGFloat textFdNickNameH = 44;
            CGFloat textFdNickNameX = screenWidth - textFdNickNameW - 30; // 30是更多图标的宽度
            CGFloat textFdNickNameY = 0;
            textFdNickName = [[UITextField alloc]initWithFrame:CGRectMake(textFdNickNameX, textFdNickNameY, textFdNickNameW, textFdNickNameH)];
            [cell addSubview:textFdNickName];
            
            textFdNickName.text = self.listenToMeData.stUserBaseInfoNet.sNick;
            textFdNickName.delegate = self;
            textFdNickName.textColor = [UIColor lightGrayColor];
            [textFdNickName setTintColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:0.8]];
            textFdNickName.textAlignment = NSTextAlignmentRight;
            
        }else if(indexPath.row == 2){
            cell.textLabel.text = @"生日";
            
            CGFloat lbBirthdayW = screenWidth * 0.5;
            CGFloat lbBirthdayH = 44;
            CGFloat lbBirthdayX = screenWidth - lbBirthdayW - 30; // 30是更多图标的宽度
            CGFloat lbBirthdayY = 0;
            if (lbBirthday == nil) {
                lbBirthday = [[UILabel alloc]initWithFrame:CGRectMake(lbBirthdayX, lbBirthdayY, lbBirthdayW, lbBirthdayH)];
                
            }
           
            
//            lbBirthday.text = @"06-12 双子座";
            lbBirthday.text = self.listenToMeData.stUserBaseInfoNet.sBirthday;
            lbBirthday.textColor = [UIColor lightGrayColor];
            lbBirthday.textAlignment = NSTextAlignmentRight;
            [cell addSubview:lbBirthday];
            
        }else if(indexPath.row == 3){
            cell.textLabel.text = @"性别";
//            cell.detailTextLabel.text = @"男";
            UIImage *imgNotChoice = nil;
            UIImage *imgChoice = nil;
            if (imgNotChoice == nil) {
                imgNotChoice = [UIImage imageNamed:@"notSelectedCircle.png"]; // 未选择图片
            }
            
            if (imgChoice == nil) {
                imgChoice = [UIImage imageNamed:@"selectedCircle.png"]; // 已选中图片
            }
            
            CGFloat btnW = 60;
            if (self.btnBoy == nil) {
                
                self.btnBoy = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - 15 - btnW, 5, btnW , cell.height )];
                [cell.contentView addSubview:self.btnBoy];
                [self.btnBoy setTitle:@"男" forState:UIControlStateNormal];
                [self.btnBoy setImage:imgNotChoice forState:UIControlStateNormal];
                [self.btnBoy setImage:imgChoice forState:UIControlStateSelected];
                
                [self.btnBoy setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
                [self.btnBoy addTarget:self action:@selector(selectBoyHandle:) forControlEvents:UIControlEventTouchUpInside];
                
//                self.btnBoy.selected = NO;
                
                [self.btnBoy setTitleColor:[UIColor rgbFromHexString:@"#9B9B9B" alpaa:1.0] forState:UIControlStateNormal];
                [self.btnBoy setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateSelected];
            }
            
            if (self.btnGirl == nil) {
                self.btnGirl = [[UIButton alloc]initWithFrame:CGRectMake(self.btnBoy.x - btnW - 15, 5, btnW, cell.height)];
                [cell.contentView addSubview:self.btnGirl];
                [self.btnGirl setTitle:@"女" forState:UIControlStateNormal];
                [self.btnGirl setImage:imgNotChoice forState:UIControlStateNormal];
                [self.btnGirl setImage:imgChoice forState:UIControlStateSelected];
                [self.btnGirl setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
                [self.btnGirl addTarget:self action:@selector(selectGirlHandle:) forControlEvents:UIControlEventTouchUpInside];
                //默认选择男
//                self.btnGirl.selected = YES;
                
                [self.btnGirl setTitleColor:[UIColor rgbFromHexString:@"#9B9B9B" alpaa:1.0] forState:UIControlStateNormal];
                [self.btnGirl setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateSelected];
                
            }
            
            if (self.listenToMeData.stUserBaseInfoNet.iGender == 0) {
                self.btnBoy.selected = NO;
                self.btnGirl.selected = YES;
            }else{
                self.btnGirl.selected = NO;
                self.btnBoy.selected = YES;
            }
            
            
        }else if (indexPath.row == 4){
            cell.textLabel.text = @"所在地";
            
            CGFloat textFdLocationW = screenWidth * 0.5;
            CGFloat textFdLocationH = 44;
            CGFloat textFdLocationX = screenWidth - textFdLocationW - 30; // 30是更多图标的宽度
            CGFloat textFdLocationY = 0;
            textFdLocation = [[UITextField alloc]initWithFrame:CGRectMake(textFdLocationX, textFdLocationY, textFdLocationW, textFdLocationH)];
            [cell addSubview:textFdLocation];
            
            textFdLocation.text = self.listenToMeData.stUserBaseInfoNet.sAddress;
            textFdLocation.delegate = self;
            textFdLocation.textColor = [UIColor lightGrayColor];
            [textFdLocation setTintColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:0.8]];
            textFdLocation.textAlignment = NSTextAlignmentRight;
        }
        
    }else if(indexPath.section == 1){
        cell.textLabel.text = @"个性签名";
        
        CGFloat textFdDescW = screenWidth * 0.5;
        CGFloat textFdDescH = 44;
        CGFloat textFdDescX = screenWidth - textFdDescW - 30; // 30是更多图标的宽度
        CGFloat textFdDescY = 0;
        textFdDesc = [[UITextField alloc]initWithFrame:CGRectMake(textFdDescX, textFdDescY, textFdDescW, textFdDescH)];
        [cell addSubview:textFdDesc];
        
        textFdDesc.text = self.listenToMeData.stUserBaseInfoNet.sDesc;
        textFdDesc.delegate = self;
        textFdDesc.textAlignment = NSTextAlignmentRight;
        [textFdDesc setTextColor:[UIColor lightGrayColor]];
    }
    
    
    

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (datePicker.hidden == NO) {
        datePicker.hidden = YES;
        viewBtnBG.hidden = YES;
    }
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            YDLog(@"修改头像");
            [self presentSheet];
        }else if(indexPath.row == 1){
            YDLog(@"修改昵称");
        }else if(indexPath.row == 2){
            
            YDLog(@"修改生日");
            [self setupDatePicker];
            
        }else if(indexPath.row == 3){
            YDLog(@"修改性别");
        }else if (indexPath.row == 4){
            YDLog(@"修改所在地");
        }
    }else if(indexPath.section == 1){
        YDLog(@"修改个性签名");
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 5;
    }else{
        return 1;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}


#pragma mark  - 时间选择器
-(void)setupDatePicker
{
    if (datePicker == nil) {
        // 时间选择器
        CGFloat datePickerW = screenWidth;
        CGFloat datePickerH = 200;
        CGFloat datePickerX = 0;
        CGFloat datePickerY = screenHeight - datePickerH + 20;
        CGRect datePickerFrame = CGRectMake(datePickerX, datePickerY, datePickerW, datePickerH);
        self.datePicker = [[UIDatePicker alloc]init];
        [self.datePicker setFrame:datePickerFrame];
        [mScrollView addSubview:self.datePicker];
        
        self.datePicker.backgroundColor = [UIColor colorWithRed:255 green:255 blue:255 alpha:1.0];
        self.datePicker.datePickerMode = UIDatePickerModeDate;
//        [self.datePicker setDate:[NSDate date]];

        
        NSString *minDateString = @"1900-01-01 10:45:32";
//        NSString *maxDateString = @"2015-01-01 10:45:32";
        NSDateFormatter *dateFormater = [[NSDateFormatter alloc]init];
        [dateFormater setDateFormat:@"yyyy-MM-DD HH:mm:ss"];
        
        NSDate *minDate = [dateFormater dateFromString:minDateString];
        NSDate *maxDate = [NSDate date];
        YDLog(@"%@",maxDate);
        self.datePicker.minimumDate = minDate;
        self.datePicker.maximumDate = [NSDate date];
        [self.datePicker setDate:maxDate animated:YES];
        
//        self.datePicker.maximumDate =  
        [self.datePicker addTarget:self action:@selector(dateTimeChange) forControlEvents:UIControlEventValueChanged];
        
        // 按钮的承载视图
        CGFloat viewBtnBGW = screenWidth;
        CGFloat viewBtnBGH = 30;
        CGFloat viewBtnBGX = 0;
        CGFloat viewBtnBGY = datePickerY - viewBtnBGH;
        viewBtnBG = [[UIView alloc]initWithFrame:CGRectMake(viewBtnBGX, viewBtnBGY, viewBtnBGW, viewBtnBGH)];
        [mScrollView addSubview:viewBtnBG];
        
        [viewBtnBG setBackgroundColor:[UIColor colorWithRed:255 green:255 blue:255 alpha:1.0]];
        
        
        // 取消Btn
        CGFloat timeCancelBtnW = 50;
        CGFloat timeCancelBtnH = 20;
        CGFloat timeCancelBtnX = 50;
        CGFloat timeCancelBtnY = 0.5 * (viewBtnBGH - timeCancelBtnH);
        CGRect timeCancelBtnFrame = CGRectMake(timeCancelBtnX, timeCancelBtnY, timeCancelBtnW, timeCancelBtnH);
        self.timeCancelBtn = [[UIButton alloc]initWithFrame:timeCancelBtnFrame];
        [viewBtnBG addSubview:self.timeCancelBtn];
        
        [self.timeCancelBtn setTitle:@"取消" forState:UIControlStateNormal];
        [self.timeCancelBtn setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateNormal];
        [self.timeCancelBtn addTarget:self action:@selector(clickTimeCancelBtn) forControlEvents:UIControlEventTouchUpInside];
        
        // 确定Btn
        CGFloat timeChooseBtnW = timeCancelBtnW;
        CGFloat timeChooseBtnH = timeCancelBtnH;
        CGFloat timeChooseBtnX = screenWidth - timeCancelBtnX - timeChooseBtnW;
        CGFloat timeChooseBtnY = 0.5 * ( viewBtnBGH - timeChooseBtnH);
        CGRect timeChooseBtnFrame = CGRectMake(timeChooseBtnX, timeChooseBtnY, timeChooseBtnW, timeChooseBtnH);
        self.timeChooseBtn = [[UIButton alloc]initWithFrame:timeChooseBtnFrame];
        [viewBtnBG addSubview:self.timeChooseBtn];
        
        [self.timeChooseBtn setTitle:@"确定" forState:UIControlStateNormal];
        [self.timeChooseBtn setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateNormal];
        [self.timeChooseBtn addTarget:self action:@selector(clickTimeChooseBtn) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    if (viewBtnBG.hidden == YES && datePicker.hidden == YES) {
        viewBtnBG.hidden = NO;
        datePicker.hidden = NO;
    }
    
}

#pragma mark - dateTiemChange修改生日
-(void)dateTimeChange
{
//    NSDate *select = [self.datePicker date];
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
//    [dateFormatter setDateFormat:@"MM-dd"];
    //    NSString *dateAndTime = [dateFormatter stringFromDate:select];
    //    [lbBirthday setText:dateAndTime];
    
    //    [lbBirthday setFont:[UIFont systemFontOfSize:12.0]];
    //    [lbBirthday setTextColor:[UIColor whiteColor]];
    
    NSDate *select = [self.datePicker date];
    if (self.changDate == nil) {
        self.changDate = [[NSDateFormatter alloc]init];
    }
    [self.changDate setDateFormat:@"MM-dd"];
    
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps;
    
    // 年月日获得
    if (iOS8_greater) {
    
        comps = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:select];
    }else{
        
        comps = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:select];
    }
    
    NSInteger year = [comps year];
    NSInteger month = [comps month];
    NSInteger day = [comps day];
    YDLog(@"year: %ld month: %ld, day: %ld", year, month, day);
    
    self.brithDayString = [NSString stringWithFormat:@"%@ %@座",[self.changDate stringFromDate:select],[self getAstroWithMonth:(int)month day:(int)day]];
    
}

-(NSString *)getAstroWithMonth:(int)m day:(int)d{
    
    NSString *astroString = @"魔羯水瓶双鱼白羊金牛双子巨蟹狮子处女天秤天蝎射手魔羯";
    NSString *astroFormat = @"102123444543";
    NSString *result;
    
    if (m<1||m>12||d<1||d>31){
        return @"错误日期格式!";
    }
    
    if(m==2 && d>29)
    {
        return @"错误日期格式!!";
    }else if(m==4 || m==6 || m==9 || m==11) {
        
        if (d>30) {
            return @"错误日期格式!!!";
        }
    }
    
    result=[NSString stringWithFormat:@"%@",[astroString substringWithRange:NSMakeRange(m*2-(d < [[astroFormat substringWithRange:NSMakeRange((m-1), 1)] intValue] - (-19))*2,2)]];
    
    return result;
}

#pragma mark -按钮点击事件
/**
 * 打开听我的相册
 */

/**
 * 打开本地相册
 */
-(void)openAlbum
{
    UIImagePickerController *pickerImage = [[UIImagePickerController alloc] init];
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        pickerImage.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        //pickerImage.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
//        pickerImage.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:pickerImage.sourceType];
        
    }
    pickerImage.delegate = (id)self;
    pickerImage.allowsEditing = YES;
    [self presentViewController:pickerImage animated:YES completion:^{
        
    }];
    
}

/**
 * 拍摄--打开相机
 */
-(void)openCamera
{
    //先设定sourceType为相机，然后判断相机是否可用（ipod）没相机，不可用将sourceType设定为相片库
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
    //判断是否摄像头
        if (![UIImagePickerController isSourceTypeAvailable: sourceType]) {
            sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
    //sourceType = UIImagePickerControllerSourceTypeCamera; //照相机
    //sourceType = UIImagePickerControllerSourceTypePhotoLibrary; //图片库
    //sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum; //保存的相片
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];//初始化
    picker.delegate = (id)self;
    picker.allowsEditing = YES;//设置可编辑
    picker.sourceType = sourceType;
    picker.showsCameraControls = YES;
    [self presentViewController:picker animated:YES completion:^{
        
    }];
    
}

/**
 * 当选择一张照片后，进入这里
 */
#pragma makr 当选择一张照片之后进入这里

#pragma mark - 完成拍照
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
//    // 定义类型
//    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
//    // 当选择的类型是图片
//    if ([type isEqualToString:@"public.image"]) {
//        // 先把图片转成NSData
//        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
//        
//        if (image == nil) {
//            //            NSData *tempData = UIImageJPEGRepresentation(image, 0.1);
//            image = [info objectForKey:UIImagePickerControllerOriginalImage];
//            [self performSelector:@selector(saveImage:) withObject:image];
//        }else{
//            //            data = UIImagePNGRepresentation(image);
//            //             NSData *tempData = UIImageJPEGRepresentation(image, 0.1);
//        }
//        
//        // 关闭相册界面
//        [picker dismissViewControllerAnimated:YES completion:^{
//            
//            // 替换头像为选择的图片
//            
//            
//        }];
//        
//    }
    
    NSString *tyep = [info objectForKey:UIImagePickerControllerMediaType];
    
    //使用照相机选取图片
    if ([tyep isEqualToString:(NSString *)kUTTypeImage] && picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        UIImage *originalImg = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        UIImage *editImg = [info objectForKey:UIImagePickerControllerEditedImage];
//
//        UIImage *cropImg = [info objectForKey:UIImagePickerControllerCropRect];
//        
//        NSURL * url = [info objectForKey:UIImagePickerControllerMediaURL];
//        
//        NSDictionary *metaData = [info objectForKey:UIImagePickerControllerMediaMetadata];
        
        [self performSelector:@selector(saveImage:) withObject:originalImg afterDelay:0.5];
        
        UIImageWriteToSavedPhotosAlbum(originalImg, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
        
        UIImage *smallImage = [self thumbnailWithImageWithoutScale:editImg size:CGSizeMake(40, 40)];
        [btnAvatar setImage:smallImage forState:UIControlStateNormal];
        NSData *data = UIImageJPEGRepresentation(smallImage, 1);
        [[NetReqManager getInstance] sendgetUploadIcon:self.listenToMeData.stUserBaseInfoNet.uuid VIconDatas:data SPictureUrl:nil];
    }
    
    //使用手机相册选去图片
    if ([tyep isEqualToString:(NSString *)kUTTypeImage]  && picker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary) {
//        UIImage *originalImg = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        UIImage *editImg = [info objectForKey:UIImagePickerControllerEditedImage];
        
        UIImage *smallImage = [self thumbnailWithImageWithoutScale:editImg size:CGSizeMake(40, 40)];
        [btnAvatar setImage:smallImage forState:UIControlStateNormal];
        NSData *data = UIImageJPEGRepresentation(smallImage, 1);
        [[NetReqManager getInstance] sendgetUploadIcon:self.listenToMeData.stUserBaseInfoNet.uuid VIconDatas:data SPictureUrl:nil];
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - 用户取消拍照
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - 保存图片
-(void)saveImage:(UIImage *)image{
    
    
    NSData *imageData = UIImagePNGRepresentation(image);
    if(imageData == nil)
    {
        imageData = UIImageJPEGRepresentation(image, 1.0);
    }
    
    NSDate *date = [NSDate date];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMddHHmmss"];
    NSString *fileName = [[formatter stringFromDate:date] stringByAppendingPathExtension:@"png"] ;
    
    
//    NSURL *saveURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:fileName];
//    YDLog(@"save Url: %@",saveURL);
//    [imageData writeToURL:saveURL atomically:YES];
    
    
    //    NSLog(@"保存头像！");
    //    [userPhotoButton setImage:image forState:UIControlStateNormal];
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imageFilePath = [documentsDirectory stringByAppendingPathComponent:fileName];
    YDLog(@"imageFile->>%@",imageFilePath);
    success = [fileManager fileExistsAtPath:imageFilePath];
    if(success) {
        success = [fileManager removeItemAtPath:imageFilePath error:&error];
    }
    //    UIImage *smallImage=[self scaleFromImage:image toSize:CGSizeMake(80.0f, 80.0f)];//将图片尺寸改为80*80
    UIImage *smallImage =image; //[self thumbnailWithImageWithoutScale:image size:CGSizeMake(40, 40)];
    [UIImageJPEGRepresentation(smallImage, 1.0f) writeToFile:imageFilePath atomically:YES];//写入文件
//    UIImage *selfPhoto = [UIImage imageWithContentsOfFile:imageFilePath];//读取图片文件
    //    [userPhotoButton setImage:selfPhoto forState:UIControlStateNormal];
    
    
    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString *imageFilePath = [documentsDirectory stringByAppendingPathComponent:@"selfPhoto.jpg"];
//    NSLog(@"imageFile->>%@",imageFilePath);
//    UIImage *selfPhoto = [UIImage imageWithContentsOfFile:imageFilePath];//
//    self.img.image = selfPhoto;
//    [self.img.layer setCornerRadius:CGRectGetHeight([self.img bounds]) / 2];  //修改半径，实现头像的圆形化
//    self.img.layer.masksToBounds = YES

    
}
#pragma mark -保持原来的长宽比，生成一个缩略图
- (UIImage *)thumbnailWithImageWithoutScale:(UIImage *)image size:(CGSize)asize
{
    UIImage *newimage;
    if (nil == image) {
        newimage = nil;
    }
    else{
        CGSize oldsize = image.size;
        CGRect rect;
        if (asize.width/asize.height > oldsize.width/oldsize.height) {
            rect.size.width = asize.height*oldsize.width/oldsize.height;
            rect.size.height = asize.height;
            rect.origin.x = (asize.width - rect.size.width)/2;
            rect.origin.y = 0;
        }
        else{
            rect.size.width = asize.width;
            rect.size.height = asize.width*oldsize.height/oldsize.width;
            rect.origin.x = 0;
            rect.origin.y = (asize.height - rect.size.height)/2;
        }
        UIGraphicsBeginImageContext(asize);
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetFillColorWithColor(context, [[UIColor clearColor] CGColor]);
        UIRectFill(CGRectMake(0, 0, asize.width, asize.height));//clear background
        [image drawInRect:rect];
        newimage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    return newimage;
}

#pragma mark - 改变图像的尺寸，方便上传服务器
- (UIImage *) scaleFromImage: (UIImage *) image toSize: (CGSize) size
{
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - 保存照片成功后调用
- (void)image:(UIImage*)image didFinishSavingWithError:(NSError*)error contextInfo:(void*)contextInfo{
    
    if (!error) {
        
        YDLog(@"picture saved success");
    }else{
        
        YDLog(@"error occured while saving the picture%@", error);
        
    }
    
}

//Return the URL to the application Documents directory
-(NSURL *)applicationDocumentsDirectory{

    return [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentationDirectory inDomains:NSUserDomainMask ] lastObject];
}

/**
 * 点击取消Btn
 */
-(void)clickTimeCancelBtn
{
    YDLog(@"取消时间选择");
    self.datePicker.hidden = YES;
    self.viewBtnBG.hidden = YES;
    
}

/**
 * 点击确定Btn
 */
-(void)clickTimeChooseBtn
{
    YDLog(@"确定选择时间");
    [self dateTimeChange];
    self.datePicker.hidden = YES;
    self.viewBtnBG.hidden = YES;
    lbBirthday.text = self.brithDayString;
//    [self.mTableView reloadData];
}

#pragma mark - 选择性别
-(void)selectBoyHandle:(UIButton *)btnBoy{
    
    if (!_isSelectBoy) {
        self.btnBoy.selected = YES;
        self.btnGirl.selected = NO;
        _isSelectBoy = YES;
        _isSelectGirl = NO;
    }else{
        self.btnBoy.selected = NO;
        _isSelectBoy = NO;
    }
    
}

-(void)selectGirlHandle:(UIButton *)btnGirl{
    if (!_isSelectGirl) {
        self.btnGirl.selected = YES;
        self.btnBoy.selected = NO;
        _isSelectGirl = YES;
        _isSelectBoy = NO;
    }else{
        self.btnGirl.selected = NO;
        _isSelectGirl = NO;
    }
}

#pragma mark -其他
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
