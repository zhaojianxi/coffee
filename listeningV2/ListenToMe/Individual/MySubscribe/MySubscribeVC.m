//
//  MySubscribeVC.m
//  ListenToMe
//
//  Created by yadong on 2/5/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "MySubscribeVC.h"
#import "YDSubscribeCell.h"
#import "HMPopMenu.h"
#import "YDPopBt.h"

@interface MySubscribeVC ()<UITableViewDataSource,UITableViewDelegate>
@property(strong,nonatomic) UITableView *mTableView;
@property(strong,nonatomic) UIView *tempView; // 更多按钮底部视图
@end

@implementation MySubscribeVC
@synthesize mTableView;
#pragma mark -生命周期
-(void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setUI];
}

#pragma mark -UI
-(void)setUI
{
    [self setNavigation];
    
    [self setTableView];
}

#pragma mark tableview
-(void)setTableView
{
    
    mTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, -35, screenWidth, screenHeight) style:UITableViewStyleGrouped];
    [self.view addSubview:mTableView];
    
    mTableView.showsVerticalScrollIndicator = NO;
    mTableView.backgroundColor = [UIColor clearColor];
    mTableView.delegate = self;
    mTableView.dataSource = self;
}

#pragma mark 导航栏
-(void)setNavigation
{
    [self setLeftBtnItem];
    [self setRightBtnItem];
    [self setNavigatonTitleVeiw];
}

// 导航栏的标题栏
-(void)setNavigatonTitleVeiw
{
    self.navigationItem.title = @"我的有约歌";
}

// 导航栏左边的Btn
-(void)setLeftBtnItem
{
    UIImage *leftSearchImg = [UIImage imageNamed:@"whiteBack.png"];
    UIButton *leftBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, leftSearchImg.size.width,leftSearchImg.size.height)];
    [leftBtn setImage:[UIImage resizableImageWithName:@"whiteBack.png"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(clickLeftBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customLeftBtnItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = customLeftBtnItem;
}

// 导航栏右边的Btn
-(void)setRightBtnItem
{
    UIImage *rightBtImg = [UIImage imageNamed:@"more.png"];
    UIButton *rightBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, rightBtImg.size.width,rightBtImg.size.height)];
    [rightBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -25)];
    [rightBtn setImage:rightBtImg forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(clickRightBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customRightBtnItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = customRightBtnItem;
}

#pragma mark -tableview的 delegate & datasource
-(YDSubscribeCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"subscribe";
    YDSubscribeCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[YDSubscribeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    
    [cell.imgAvatar setImage:[UIImage imageNamed:@"temp3.png"]];
    cell.lbDesc.text = @"北京的山东的朋友，有没有时间聚聚";
    cell.lbUserName.text = @"痘痘天使";
    cell.lbPartyName.text = @"山东朋友聚会";
    cell.lbLocation.text = @"同一首歌 - 苏州桥店";
    cell.lbSubsTime.text = @"2014-01-12 16:40";
    cell.lbDistance.text = @"5.8KM";
    cell.lbLinmit.text = @"5-10人 不限男女 可带朋友";
    cell.lbAffordPerson.text = @"AA";
    [cell.btnApply setTitle:@"20" forState:UIControlStateNormal];
    [cell.btnDiscuss setTitle:@"5" forState:UIControlStateNormal];
    [cell.btnJoin setTitle:@"5" forState:UIControlStateNormal];
    
    cell.btnWhoLaunch.hidden = NO;
    [cell.btnWhoLaunch setTitle:@"我参与" forState:UIControlStateNormal];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 292;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 5;
}

#pragma mark -点击事件
-(void)clickLeftBarBtnItem
{
    [self.navigationController popToRootViewControllerAnimated:YES];
    YDLog(@"点击了导航栏左边的Btn");
}


-(void)clickRightBarBtnItem
{
    YDLog(@"点击了导航栏右边的Btn");
    
    UIImage *popImg = [UIImage imageNamed:@"popMenuBg.png"];
    CGFloat popMenuW = popImg.size.width;
    CGFloat popMenuH = popImg.size.height;
    _tempView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, popMenuW, popMenuH)];
    _tempView.userInteractionEnabled = YES;
    
    // 第一个btn的 y
    CGFloat btnW = 46;
    CGFloat btnH = 39;
    CGFloat firstBtnY = 5.5;
    
    YDPopBt *btnMsg = [self PopBtnWithImg:@"msg.png" title:@"消息" count:18 frame:CGRectMake(0,firstBtnY, btnW, btnH)];
    YDPopBt *btnPlaying = [self PopBtnWithImg:@"playing.png" title:@"正在播放" count:0 frame:CGRectMake(0,firstBtnY + btnH, btnW, btnH)];
    YDPopBt *btnRoom = [self PopBtnWithImg:@"yuegebaofang.png" title:@"包房" count:0 frame:CGRectMake(0, firstBtnY + 2 * btnH,btnW, btnH)];
    YDPopBt *btnReport = [self PopBtnWithImg:@"report.png" title:@"举报" count:0 frame:CGRectMake(0, firstBtnY + 3 * btnH,btnW, btnH)];
    btnReport.imgLine.hidden = YES;
    
    [_tempView addSubview:btnMsg];
    [_tempView addSubview:btnPlaying];
    [_tempView addSubview:btnRoom];
    [_tempView addSubview:btnReport];
    
    [btnMsg.BtnPop addTarget:self action:@selector(clickMsg) forControlEvents:UIControlEventTouchUpInside];
    [btnPlaying.BtnPop addTarget:self action:@selector(clickPlaying) forControlEvents:UIControlEventTouchUpInside];
    [btnRoom.BtnPop addTarget:self action:@selector(clickRoom) forControlEvents:UIControlEventTouchUpInside];
    [btnReport.BtnPop addTarget:self action:@selector(clickReport) forControlEvents:UIControlEventTouchUpInside];
    
    HMPopMenu *menu = [HMPopMenu popMenuWithContentView:_tempView];
    CGFloat menuW = popMenuW;
    CGFloat menuH = popMenuH;
    CGFloat menuY = 55;
    CGFloat menuX = self.view.width - menuW - 20;
    menu.dimBackground = YES;
    menu.arrowPosition = HMPopMenuArrowPositionRight;
    [menu showInRect:CGRectMake(menuX, menuY, menuW, menuH)];
}

-(YDPopBt *)PopBtnWithImg:(NSString *)img title:(NSString *)title count:(int)count frame:(CGRect)frame
{
    YDPopBt *btn = [[YDPopBt alloc]initWithFrame:frame];
    
    btn.imgPop.image = [UIImage imageNamed:img];
        btn.lbPop.text = title;
    
    [btn.btnBadge setTitle:[NSString stringWithFormat:@"%d",count] forState:UIControlStateNormal];
    if ([btn.btnBadge.titleLabel.text isEqual:@""] || [btn.btnBadge.titleLabel.text isEqual:@"0" ] ) {
        btn.btnBadge.hidden = YES;
    }
    
    return btn;
}

#pragma mark 点击事件

-(void)clickMsg
{
    YDLog(@"clickMsg");
    [self.tempView removeFromSuperview];
}

-(void)clickPlaying
{
    YDLog(@"clickPlaying");
}

-(void)clickRoom
{
    YDLog(@"clickRoom");
}

-(void)clickReport
{
    YDLog(@"clickReport");
}


@end
