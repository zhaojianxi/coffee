//
//  MyCollectionShowCell.h
//  ListenToMe
//
//  Created by yadong on 3/9/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCollectionShowCell : UITableViewCell
+(instancetype)cellWithTableView:(UITableView *)tableView;
// 数据
@property(assign,nonatomic) int tagTheme;
@property(nonatomic,copy) NSString *strPartyName;
@property(nonatomic,copy) NSString *strTopMusicName;
@property(nonatomic,strong) NSMutableArray *aryAvatar;
@property(assign,nonatomic) int numSongs;
@property(assign,nonatomic) int numAlbums;
@property(assign,nonatomic) int numSee;
@property(assign,nonatomic) int numFlower;
// 控件
/**
 * 封面
 */
@property(strong,nonatomic) UIImageView *imgCover;
/**
 * 聚会名
 */
@property(strong,nonatomic) UILabel *lbPartyName;

/**
 * 歌曲名
 */
@property(strong,nonatomic) UILabel *lbMusicName;
/**
 * 聚会主题
 */
@property(strong,nonatomic) UILabel *lbTheme;
/**
 * 聚会主题图片标识
 */
@property(strong,nonatomic) UIImageView *imgTheme;
/**
 * 歌曲数目
 */
@property(strong,nonatomic) UIButton *btnNumSongs;
/**
 * 聚会照片数目
 */
@property(strong,nonatomic) UIButton *btnNumAlbums;
/**
 * 聚会查看数目
 */
@property(strong,nonatomic) UIButton *btnNumSee;
/**
 * 聚会送花数目
 */
@property(strong,nonatomic) UIButton *btnNumFlower;
/**
 * 更多btn
 */
@property(strong,nonatomic) UIButton *btnPureMore;
/**
 * 讨论按钮
 
 */
@property(strong,nonatomic) UIButton *btnDiscuss;
/**
 * 送花
 */
@property(strong,nonatomic) UIButton *btnActionFlowes;
/**
 * 分享
 */
@property(strong,nonatomic) UIButton *btnActionShare;
/**
 * 收藏
 */
@property(strong,nonatomic) UIButton *btnActionCollect;
@end
