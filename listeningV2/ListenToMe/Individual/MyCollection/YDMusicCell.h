//
//  YDMusicCell.h
//  ListenToMe
//
//  Created by yadong on 3/6/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YDMusicCell : UITableViewCell


+ (instancetype)cellWithTableView:(UITableView *)tableView;
/**
 * 歌名lb
 */
@property(strong,nonatomic) UILabel *lbMusicName;

/**
* 歌者lb
*/
@property(strong,nonatomic) UILabel *lbSinger;

/**
* 播放btn
*/
@property(strong,nonatomic) UIButton *btnPlay;

/**
* 头像bt
*/
@property(strong,nonatomic) UIImageView *imgAvatar;

/**
 * 歌曲时间lb
 */
@property(strong,nonatomic) UILabel *lbTime;

/**
 * 听众数btn
 */
@property(strong,nonatomic) UIButton *btnListener;


/**
* 鲜花数btn
*/
@property(strong,nonatomic) UIButton *btnFlower;

/**
 * 分享btn
 */
@property(strong,nonatomic) UIButton *btnShare;

/**
 * 收藏btn
 */
@property(strong,nonatomic) UIButton *btnCollection;
/**
 * 波浪
 */
@property(strong,nonatomic) UIImageView *imgWave;

@end
