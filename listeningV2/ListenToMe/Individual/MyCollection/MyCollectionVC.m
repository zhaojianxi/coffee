//
//  MyCollectionVC.m
//  ListenToMe
//
//  Created by yadong on 2/5/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "MyCollectionVC.h"
#import "MyCollectionShowCell.h"
#import "YDMusicCell.h"
#import "NSDate+ZHW.h"
#import "YDKMemmoryCell.h"
#import "YDTopListCell.h"
#import "HWPerHomePageVC.h"

@interface MyCollectionVC ()<UITableViewDataSource,UITableViewDelegate>
@property(strong,nonatomic) UITableView *mTableView;
@property(strong,nonatomic) NSArray *a; // 假数据，模仿收藏页的某行cell是party还是歌曲播放
@property(nonatomic,strong) ListenToMeData *listenToMeData;
@end

@implementation MyCollectionVC
#pragma mark -生命周期
-(void)viewDidLoad
{
    [super viewDidLoad];
    

    
    [self setUI];
    
    [self initData];
}

-(void)initData{
    self.listenToMeData =[ListenToMeData getInstance];
   
    [[NetReqManager getInstance] sendGetCollectWorkList:self.listenToMeData.stUserBaseInfoNet.uuid IOffset:0 INum:20];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTable) name:NOTIFY_GET_COLLECT_WORKLIST_PAGE_RESP object:nil];
}

-(void)refreshTable{
    
    
    [self.mTableView reloadData];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_COLLECT_WORKLIST_PAGE_RESP object:nil];
}

#pragma mark -UI
-(void)setUI
{
    [self setUpTableView];
    
    [self setNav];
}

#pragma mark 导航栏 
-(void)setNav
{
    self.navigationItem.title = @"我的收藏";
    
    [self setRightBtnItem];
    
    [self setLeftBtnItem];
}

// 导航栏右边的Btn
-(void)setRightBtnItem
{
    UIButton *rightBtn = [[UIButton alloc]init];
    UIImage *imgMore = [UIImage imageNamed:@"more.png"];
    rightBtn.frame = CGRectMake(0, 0, imgMore.size.width, imgMore.size.height);
    [rightBtn setImage:imgMore forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(clickRightBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customRightBtnItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = customRightBtnItem;
}

// 导航栏左边的Btn
-(void)setLeftBtnItem
{
    UIButton *leftBtn = [[UIButton alloc]init];
    UIImage *leftBtnImg = [UIImage imageNamed:@"whiteBack.png"];
    leftBtn.frame = CGRectMake(0, 0, leftBtnImg.size.width, leftBtnImg.size.height);
    [leftBtn setImage:leftBtnImg forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(clickLeftBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customLeftBtnItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = customLeftBtnItem;
}

-(void)clickRightBarBtnItem
{
    YDLog(@"点击了导航栏的右边的按钮");
}

-(void)clickLeftBarBtnItem
{
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark tableview
-(void)setUpTableView
{
    _mTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight) style:UITableViewStyleGrouped];
    _mTableView.backgroundColor = [UIColor clearColor];
    _mTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_mTableView];
    
    _mTableView.delegate = self;
    _mTableView.dataSource = self;

}

#pragma mark delegate & datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    CollectWorkInfo *collectWrokInfo = self.listenToMeData.arrCollectWorkInfo[indexPath.section];
    
    if (collectWrokInfo.stMusicWorkBaseInfo.lMusicWorkId) {
        
        MusicWorkBaseInfo *musicWorkBaseInfo = collectWrokInfo.stMusicWorkBaseInfo;
        
        
        YDTopListCell *cell = [YDTopListCell cellWithTableView:tableView];
//        cell.delegate = self;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UserBaseInfoNet *userBaseInfoNet = musicWorkBaseInfo.stUserBaseInfoNet;
        SongInfo *songInfo = musicWorkBaseInfo.stSongInfo;
        cell.lbMusicName.text = songInfo.sSongName;
        cell.lbSinger.text = songInfo.sSongSinger;
        [cell.imgAvatar sd_setImageWithURL:[NSURL URLWithString:userBaseInfoNet.sCovver]];
        
        UIButton *btnAvatar = [[UIButton alloc]initWithFrame:cell.imgAvatar.frame];
        btnAvatar.backgroundColor = [UIColor clearColor];
        btnAvatar.tag = 1000 + indexPath.section;
        
        [btnAvatar addTarget:self action:@selector(clickAvatar:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:btnAvatar];
        
        [cell.btnFlower setTitle:[NSString stringWithFormat:@"%d",musicWorkBaseInfo.iFlowerNum] forState:UIControlStateNormal];
        [cell.btnListener setTitle:[NSString stringWithFormat:@"%d",musicWorkBaseInfo.iListenNum] forState:UIControlStateNormal];
        cell.btnCollection.selected = YES;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return (id)cell;
    }else{
        
        CommemorateBaseInfo *commemorateBaseInfo = collectWrokInfo.stCommemorateBaseInfo;
        
        YDKMemmoryCell *cell = [YDKMemmoryCell cellWithTableView:tableView];
        
        cell.arrUserBaseInfoNet = commemorateBaseInfo.stUserBaseInfoNet;
        
        if ([commemorateBaseInfo.sCommIcon isEqualToString:@""] || commemorateBaseInfo == nil) {
            
            cell.imgCover.image = [UIImage imageNamed:@"temp5.png"];
        }else{
            
            [cell.imgCover sd_setImageWithURL:[NSURL URLWithString:commemorateBaseInfo.sCommIcon]];
        }
        
        cell.lbTheme.text = @"闺蜜秀";
        
        if ([commemorateBaseInfo.sCommName isEqualToString:@""] || commemorateBaseInfo.sCommName == nil) {
            cell.lbPartyName.text = @"咸蛋超人的生日趴";
        }else{
            
            cell.lbPartyName.text = commemorateBaseInfo.sCommName;
        }
        
        cell.imgTheme.image = [UIImage imageNamed:@"memoryTopShow.png"];
        [cell.btnNumSongs setTitle:[NSString stringWithFormat:@"%d",commemorateBaseInfo.iMusicNum] forState:UIControlStateNormal];
        [cell.btnNumAlbums setTitle:[NSString stringWithFormat:@"%d",commemorateBaseInfo.iPictureNum] forState:UIControlStateNormal];
        [cell.btnNumSee setTitle:[NSString stringWithFormat:@"%d",commemorateBaseInfo.iWatchedNum] forState:UIControlStateNormal];
        [cell.btnNumFlower setTitle:[NSString stringWithFormat:@"%d",commemorateBaseInfo.iFlowerNum] forState:UIControlStateNormal];
        cell.btnActionCollect.selected = YES;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return (id)cell;

    }
    
   
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

     CollectWorkInfo *collectWrokInfo = self.listenToMeData.arrCollectWorkInfo[indexPath.section];
    if (collectWrokInfo.stMusicWorkBaseInfo.lMusicWorkId) {
        return 140;
    }else{
        return 273;
    }
    
//    return 140;//140 273
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.listenToMeData.arrCollectWorkInfo.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 15;
}

#pragma mark - 计算时间间隔
-(NSString *)getCreatedTime:(int64_t)lCreateTime{
    
    int64_t time = lCreateTime;
    NSDate *createdDate = [[NSDate alloc]initWithTimeIntervalSince1970:time/1000.0];
    
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"EEE MMM dd HH:mm:ss Z yyyy";
    
#warning 真机调试下, 必须加上这段
    fmt.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    
    // 2..判断创建时间 和 现在时间 的差距
    if (createdDate.isToday) { // 今天
        if (createdDate.deltaWithNow.hour >= 1) {
            return [NSString stringWithFormat:@"%ld小时前", (long)createdDate.deltaWithNow.hour];
        } else if (createdDate.deltaWithNow.minute >= 1) {
            return [NSString stringWithFormat:@"%ld分钟前", (long)createdDate.deltaWithNow.minute];
        } else {
            return @"刚刚";
        }
    } else if (createdDate.isYesterday) { // 昨天
        fmt.dateFormat = @"昨天 HH:mm";
        return [fmt stringFromDate:createdDate];
    } else if (createdDate.isThisYear) { // 今年(至少是前天)
        fmt.dateFormat = @"MM-dd";
        return [fmt stringFromDate:createdDate];
    } else { // 非今年
        fmt.dateFormat = @"yyyy-MM-dd";
        return [fmt stringFromDate:createdDate];
    }
    
}


#pragma mark 点击头像
-(void)clickAvatar:(UIButton *)btnAvatar{
    
    CollectWorkInfo *collectWrokInfo = self.listenToMeData.arrCollectWorkInfo[btnAvatar.tag - 1000];
     MusicWorkBaseInfo *musicWorkBaseInfo = collectWrokInfo.stMusicWorkBaseInfo;
    
    [self browsePerHmePage:musicWorkBaseInfo.stUserBaseInfoNet];
}

#pragma mark -YDSquareCellDelegate 点击头像进入个人/他人主页
-(void)browsePerHmePage:(UserBaseInfoNet *)userBaseInfoNet{
    HWPerHomePageVC *personHomePageVC = [[HWPerHomePageVC alloc]init];
    personHomePageVC.userBaseInfoNet = userBaseInfoNet;
    [self.navigationController pushViewController:personHomePageVC animated:YES];
    
}

@end
