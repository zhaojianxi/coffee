//
//  YDMusicCell.m
//  ListenToMe
//
//  Created by yadong on 3/6/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#define cellHeight 140  // 80 + 4 + 56  单个cell的高度 40 ，分割线 1 ，中间高的cell高度 56

#import "YDMusicCell.h"

@implementation YDMusicCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"musicCell";
    YDMusicCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[YDMusicCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
    }
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {

        CGFloat frontX = 15; // 子控件距离screen前后的距离
        CGFloat horizonLineH = 1; // 分割线的高度
        CGFloat fsCellH = 40; // 第一个和第三个cell的高度
        
        // 歌名 & 歌者 @ 播放btn
        // 横线
        UIImageView *imgLine_0 = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, horizonLineH)];
        imgLine_0.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
        [self.contentView addSubview:imgLine_0];
        // 歌名
        _lbMusicName = [[UILabel alloc]initWithFrame:CGRectMake(frontX, horizonLineH, 0.5 * screenWidth, fsCellH)];
        _lbMusicName.font = [UIFont systemFontOfSize:14.0];
        _lbMusicName.textColor = [UIColor blackColor];
        [self.contentView addSubview:_lbMusicName];
        // 歌者
        _lbSinger = [[UILabel alloc]initWithFrame:CGRectMake(_lbMusicName.x + _lbMusicName.width, _lbMusicName.y, 0.3 * screenWidth, _lbMusicName.height)];
        _lbSinger.font = [UIFont systemFontOfSize:11.0];
        _lbSinger.textColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
        [self.contentView addSubview:_lbSinger];
        // 横线
        UIImageView *imgLine_1 = [[UIImageView alloc]initWithFrame:CGRectMake(frontX, _lbMusicName.y + _lbMusicName.height, screenWidth - 2 * frontX, horizonLineH)];
        imgLine_1.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
        [self.contentView addSubview:imgLine_1];
        
        // 头像btn & 波浪 & 播放btn & 时间lb
        // 头像Btn
        CGFloat avatarWh = 40; // 头像的高度
        CGFloat secondCellHeight = 56;// 该行cell的高度
        _imgAvatar = [[UIImageView alloc]initWithFrame:CGRectMake(frontX, imgLine_1.y + horizonLineH + (secondCellHeight - avatarWh) * 0.5, avatarWh, avatarWh)];
//        _imgAvatar.image = [UIImage imageNamed:@"temp4.png"];
        _imgAvatar.layer.cornerRadius = avatarWh * 0.5;
        _imgAvatar.layer.masksToBounds = YES;
        [self.contentView addSubview:_imgAvatar];

        // 波浪
        CGFloat waveMarginX = 8; // 波浪与前后的控件的间距
        CGFloat waveW = 150; // 波浪的宽度
        _imgWave = [[UIImageView alloc]initWithFrame:CGRectMake(_imgAvatar.x + _imgAvatar.width + waveMarginX, imgLine_1.y + imgLine_1.height, waveW, secondCellHeight)];
        [self.contentView addSubview:_imgWave];
        // 时间
        CGFloat timeLbW = 35;
        _lbTime = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - frontX - timeLbW , imgLine_1.y + imgLine_1.height, timeLbW, secondCellHeight)];
        _lbTime.font = [UIFont systemFontOfSize:10.0];
        _lbTime.textColor = [UIColor rgbFromHexString:@"#9B9B9B" alpaa:1.0];
        _lbTime.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_lbTime];
        // 播放btn
        UIImage *imgPlay = [UIImage imageNamed:@"play.png"];
        _btnPlay = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - frontX - _lbTime.width - imgPlay.size.width, imgLine_1.y + imgLine_1.height + (secondCellHeight - imgPlay.size.height * 0.5) * 0.5, imgPlay.size.width * 0.5, imgPlay.size.height * 0.5)];
        [_btnPlay setImage:imgPlay forState:UIControlStateNormal];
        [self.contentView addSubview:_btnPlay];
        // 横线
        UIImageView *imgLine_2 = [[UIImageView alloc]initWithFrame:CGRectMake(frontX, imgLine_1.y + imgLine_1.height + secondCellHeight, screenWidth - 2 * frontX, 1)];
        imgLine_2.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
        [self.contentView addSubview:imgLine_2];
        
        // 试听 & 送花 & 分享 & 收藏
        // 试听
        CGFloat listenerBtnW = 55;
        UIImage *listenImg = [UIImage imageNamed:@"listenersCount.png"];
        _btnListener = [[UIButton alloc]initWithFrame:CGRectMake(frontX, imgLine_2.y + imgLine_2.height + (fsCellH - listenImg.size.height) * 0.5, listenerBtnW, fsCellH)];
        _btnListener.titleLabel.font = [UIFont systemFontOfSize:10.0];
        [_btnListener setTitleColor:[UIColor rgbFromHexString:@"#FF0053" alpaa:1.0] forState:UIControlStateNormal];
        [_btnListener setTitleEdgeInsets:UIEdgeInsetsMake(0,15, 0, 0)];
        [_btnListener setImage:listenImg forState:UIControlStateNormal];
        [self.contentView addSubview:_btnListener];
        // 送花
        UIImage *flowerImg = [UIImage imageNamed:@"flowerSquareKtv.png"];
        _btnFlower = [[UIButton alloc]initWithFrame:CGRectMake(_btnListener.x + _btnListener.width, _btnListener.y ,listenerBtnW , fsCellH)];
        _btnFlower.titleLabel.font = [UIFont systemFontOfSize:10.0];
        [_btnFlower setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateNormal];
        [_btnFlower setTitleEdgeInsets:UIEdgeInsetsMake(0,15, 0, 0)];
        [_btnFlower setImage:flowerImg forState:UIControlStateNormal];
        [_btnFlower setImage:flowerImg forState:UIControlStateNormal];
        [self.contentView addSubview:_btnFlower];
        // 收藏
        UIImage *collectionImg = [UIImage imageNamed:@"squareStarCollection.png"];
        _btnCollection = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - frontX - collectionImg.size.width, imgLine_2.y + imgLine_2.height, collectionImg.size.width, collectionImg.size.height)];
        [_btnCollection setImage:collectionImg forState:UIControlStateNormal];
        [self.contentView addSubview:_btnCollection];
        // 分享
        UIImage *shareImg = [UIImage imageNamed:@"squareStarShare.png"];
        CGFloat shareCollectionMargin = 30;
        _btnShare = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - frontX - shareImg.size.width - _btnCollection.width - shareCollectionMargin, imgLine_2.y + imgLine_2.height, shareImg.size.width, shareImg.size.height)];
        [_btnShare setImage:shareImg forState:UIControlStateNormal];
        [self.contentView addSubview:_btnShare];
        // 分割线
        
    }
    
    
    return self;
}


@end