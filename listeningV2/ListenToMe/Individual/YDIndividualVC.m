//
//  YDIndividualVC.m
//  ListenToMe
//
//  Created by yadong on 1/26/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "YDIndividualVC.h"
#import "MyAlbumVC.h"
#import "MyBookVC.h"
#import "MyCollectionVC.h"
#import "MyGiftCertificateVC.h"
#import "MyMessageVC.h"
#import "MyRecommendVC.h"
#import "MyMusicListVC.h"
#import "MySubscribeVC.h"
#import "MyWorksVC.h"
#import "SettingsVC.h"
#import "PIVC.h"
#import <MobileCoreServices/MobileCoreServices.h>

@interface YDIndividualVC () <UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate,UIImagePickerControllerDelegate> // UIScrollViewDelegate 哪里用得到
@property(strong,nonatomic) UIScrollView *mScrollView; // 整个页面的滚动视图
@property(strong,nonatomic) UITableView *mTableView; // tableview
@property(strong,nonatomic) UIView *viewUserInfo; // 头像 & 个性签名 等个人信息的承载视图
@property(strong,nonatomic) UIImageView *imgAlbum; // 相册封面页
@property(strong,nonatomic) UIButton *btnAvatar; // 头像
@property(strong,nonatomic) UIImageView *imgMask; // 头像外围的白框
@property(strong,nonatomic) UILabel *lbUserName; // 用户名
@property(strong,nonatomic) UIButton *btnRank; // 等级
@property(strong,nonatomic) UIButton *btnFlower; // 送花
@property(strong,nonatomic) UILabel *lbDesc; // 个性签名
@property(strong,nonatomic) UIButton *btnAccessoty; // 更改签名的Btn
@property(strong,nonatomic) UILabel *lbLocation; // 位置
@property(strong,nonatomic) UIButton *btnAlbum;
@property(strong,nonatomic) UIAlertController *alertCtrl;//头像设置提示
@property(nonatomic,strong) ListenToMeData *listenToMeData; //数据
@property(nonatomic,strong) UIImageView *avatarImageView;
@end

@implementation YDIndividualVC                                                                                                                                                                                                                                                                       
@synthesize mScrollView;
@synthesize mTableView;
@synthesize viewUserInfo;
@synthesize imgAlbum;
@synthesize btnAvatar;
@synthesize imgMask;
@synthesize lbUserName;
@synthesize btnRank;
@synthesize btnFlower;
@synthesize lbDesc;
@synthesize btnAccessoty;
@synthesize btnAlbum;
@synthesize avatarImageView;

#pragma mark -生命周期
- (void)viewDidLoad {
    [super viewDidLoad];

    [self initData];
    
    [self setUI];
    
    
    
//    [self.view setExclusiveTouch:YES];
    
    //设置分割线左对齐
    if ([mTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [mTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([mTableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [mTableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
#warning 如果不在视图出现时将系统的UITabBarButton移除,那么在pop回来是,系统的tabar会显示出来,会跟自定义的tarbar重叠影响视图
    for (UIView *child in self.tabBarController.tabBar.subviews) {
        // 删除系统自动生成的UITabBarButton
        if ([child isKindOfClass:[UIControl class]]) {
            [child removeFromSuperview];
        }
    }
    
    //重写获取用户信息
    [[NetReqManager getInstance] sendGetUserInfo:[ListenToMeDBManager getUuid]];
}
#pragma mark - data
-(void)initData{
    self.listenToMeData = [ListenToMeData getInstance];
    if ([ListenToMeDBManager getUuid] > 0) {
        [[NetReqManager getInstance] sendQuickLoginReq:[ListenToMeDBManager getUuid]];//10012
    }
    //快速登录的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshAvatar) name:NOTIFY_GET_QUICKLOGIN_PAGE_RESP object:nil];
   //添加上传头像通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshIcon) name:NOTIFY_UPLOADICON_PAGE_RESP object:nil];
   //获取用户最新信息的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshAvatar) name:NOTIFY_GET_USERINFO_PAGE_RESP object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshAvatar) name:NOTIFY_SET_USERINFO_PAGE_RESP object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshAvatar) name:NOTIFY_GET_LOGOUT_PAGE_RESP object:nil];
    
}

-(void)refreshIcon{

    [avatarImageView sd_setImageWithURL:self.listenToMeData.sCoverUrl placeholderImage:[UIImage imageNamed:@"icon.png"]];

    [self.mTableView reloadData];
}

-(void)refreshAvatar{
    
    [imgAlbum sd_setImageWithURL:[NSURL URLWithString:self.listenToMeData.stUserBaseInfoNet.sBackground] placeholderImage:[UIImage imageNamed:@"album.png"]];
    
 
    [avatarImageView sd_setImageWithURL:[NSURL URLWithString:self.listenToMeData.stUserBaseInfoNet.sCovver ] placeholderImage:[UIImage imageNamed:@"icon.png"]];
    
    if (self.listenToMeData.stUserBaseInfoNet) {
        
        lbUserName.text = [NSString stringWithFormat:@"%@",self.listenToMeData.stUserBaseInfoNet.sNick];
    }else{
        lbUserName.text = @"";
    }
    
    if (self.listenToMeData.stUserBaseInfoNet) {
        [btnFlower setTitle:[NSString stringWithFormat:@"%d",self.listenToMeData.stUserBaseInfoNet.iFlowerNum] forState:UIControlStateNormal];
    }else{
        
        [btnFlower setTitle:@"0" forState:UIControlStateNormal];
    }
    
    if ([self.listenToMeData.stUserBaseInfoNet.sAddress isEqualToString:@""] || self.listenToMeData.stUserBaseInfoNet == nil) {
        
        _lbLocation.text = @"未知";
    }else{
        _lbLocation.text = self.listenToMeData.stUserBaseInfoNet.sAddress;
    }
    
    if ([self.listenToMeData.stUserBaseInfoNet.sDesc isEqualToString:@""] || self.listenToMeData.stUserBaseInfoNet == nil) {
        lbDesc.text = @" TA还没有个性签名 ";
    }else{
        lbDesc.text = self.listenToMeData.stUserBaseInfoNet.sDesc;
    }
    
    [self.mTableView reloadData];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_QUICKLOGIN_PAGE_RESP object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_UPLOADICON_PAGE_RESP object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_USERINFO_PAGE_RESP object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_SET_USERINFO_PAGE_RESP object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_LOGOUT_PAGE_RESP object:nil];
}

#pragma mark -UI
-(void)setUI
{
 
    
    [self setRightBtnItem];
    
    [self setScrollView];
    
    [self setImgAlbum];
    
    [self setViewUserInfo];
    
    [self setTableView];
    
    mScrollView.contentSize = CGSizeMake(screenWidth, screenHeight - naviAndStatusH);
}

#pragma mark  滚动视图
-(void)setScrollView
{
    mScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight - 49)];
//    mScrollView.contentSize = CGSizeMake(screenWidth, screenHeight + 100);
    [self.view addSubview:mScrollView];
    
    mScrollView.showsVerticalScrollIndicator = NO;
    mScrollView.scrollEnabled = NO;

}

#pragma mark tableview
-(void)setTableView
{
    mTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, viewUserInfo.frame.origin.y + viewUserInfo.frame.size.height + 10, screenWidth, screenHeight - imgAlbum.height - viewUserInfo.height - naviAndStatusH - 49) style:UITableViewStyleGrouped];
    [mScrollView addSubview:mTableView];
    
    //设置tableview的分割线左对齐
    mTableView.separatorInset = UIEdgeInsetsZero;
    
    mTableView.backgroundColor = [UIColor clearColor];
    mTableView.scrollEnabled = YES;
    mTableView.delegate = self;
    mTableView.dataSource = self;
}

#pragma mark 相册封页
-(void)setImgAlbum
{
    imgAlbum = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 175)];
    [imgAlbum sd_setImageWithURL:[NSURL URLWithString:self.listenToMeData.stUserBaseInfoNet.sBackground] placeholderImage:[UIImage imageNamed:@"album.png"]];
    imgAlbum.backgroundColor = [UIColor clearColor];
    [mScrollView addSubview:imgAlbum];
    imgAlbum.layer.masksToBounds = YES;
    
    imgAlbum.contentMode = UIViewContentModeScaleAspectFill;
    
    btnAlbum = [[UIButton alloc]initWithFrame:imgAlbum.frame];
    [mScrollView addSubview:btnAlbum];
    btnAlbum.backgroundColor = [UIColor clearColor];
    [btnAlbum addTarget:self action:@selector(clickImgAlbum) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark 个人信息的承载页
-(void)setViewUserInfo
{
    // 个人信息承载页
    viewUserInfo = [[UIView alloc]initWithFrame:CGRectMake(0,imgAlbum.frame.origin.y + imgAlbum.frame.size.height, screenWidth, 70)];
    viewUserInfo.backgroundColor = [UIColor whiteColor];
    [mScrollView addSubview:viewUserInfo];
    
    // 头像
    CGFloat marginX = 15;
    
    UIView *avatarBgView = [[UIView alloc]initWithFrame:CGRectMake(marginX - 1, imgAlbum.y + imgAlbum.height - 15, 42, 42)];
    avatarBgView.backgroundColor = [UIColor whiteColor];
    avatarBgView.layer.masksToBounds = YES;
    avatarBgView.layer.cornerRadius = 5;
   
    [mScrollView addSubview:avatarBgView];
//    btnAvatar = [[UIButton alloc]initWithFrame:CGRectMake(marginX, - 20, 40, 40)];
    btnAvatar = [[UIButton alloc]initWithFrame:CGRectMake(1, 1, 40, 40)];
    
//    网络头像
    avatarImageView = [[UIImageView alloc]initWithFrame:btnAvatar.bounds];
    [avatarImageView sd_setImageWithURL:[NSURL URLWithString:self.listenToMeData.stUserBaseInfoNet.sCovver ] placeholderImage:[UIImage imageNamed:@"icon.png"]];
    [btnAvatar addSubview:avatarImageView];
    
    [viewUserInfo addSubview:btnAvatar];
    
    [avatarBgView addSubview:btnAvatar];
    
    [btnAvatar addTarget:self action:@selector(clickAvatar) forControlEvents:UIControlEventTouchUpInside];
    btnAvatar.layer.cornerRadius = 0.2 * btnAvatar.frame.size.width;
    btnAvatar.layer.masksToBounds = YES;
    
    // mask
    imgMask = [[UIImageView alloc]initWithFrame:btnAvatar.frame];
    
    // 用户名
    lbUserName = [[UILabel alloc]initWithFrame:CGRectMake(avatarBgView.frame.size.height + avatarBgView.frame.origin.x + 10, 10, screenWidth - marginX - avatarBgView.width - 160, 20)];
    lbUserName.backgroundColor = [UIColor clearColor];
    
    if (self.listenToMeData.stUserBaseInfoNet) {
        
        lbUserName.text = [NSString stringWithFormat:@"%@",self.listenToMeData.stUserBaseInfoNet.sNick];
    }else{
        lbUserName.text = @"";
    }
    
    lbUserName.font = [UIFont systemFontOfSize:15.0];
    lbUserName.textColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
    [viewUserInfo addSubview:lbUserName];
    
    UIButton *btnSetPerInf = [[UIButton alloc]initWithFrame:CGRectMake(0, viewUserInfo.height - 35, screenWidth , 35)];
    
    
    
    // 个性签名
    lbDesc = [[UILabel alloc]initWithFrame:CGRectMake(marginX, 0, screenWidth - marginX - 15, 35)];
    lbDesc.backgroundColor = [UIColor clearColor];
    
    
    if ([self.listenToMeData.stUserBaseInfoNet.sDesc isEqualToString:@""] || self.listenToMeData.stUserBaseInfoNet == nil) {
        lbDesc.text = @" TA还没有个性签名 ";
    }else{
        lbDesc.text = self.listenToMeData.stUserBaseInfoNet.sDesc;
    }
    
    
    lbDesc.textColor = [UIColor rgbFromHexString:@"#9B9B9B" alpaa:1.0];
    lbDesc.font = [UIFont systemFontOfSize:12.0];
//    [viewUserInfo addSubview:lbDesc];
    
    //label的自适应高度和宽度
    lbDesc.numberOfLines = 0;
    lbDesc.textAlignment = NSTextAlignmentLeft;
    CGSize size =[self sizeWithLabelText:lbDesc.text font:lbDesc.font];
    lbDesc.frame = CGRectMake(marginX, 0, screenWidth - marginX - 15, size.height);
    
    [btnSetPerInf addSubview:lbDesc];

    // 个人信息设置
    UIImage *leftArrowImg = [UIImage imageNamed:@"setInfo.png"];
    
//    btnAccessoty = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - 30, lbDesc.y + 10, leftArrow.size.width * 0.6, leftArrow.size.height * 0.6)];
//    btnAccessoty.backgroundColor = [UIColor purpleColor];
//    [btnAccessoty addTarget:self action:@selector(setInfHandle:) forControlEvents:UIControlEventTouchUpInside];
//    [btnAccessoty setImage:leftArrowImg forState:UIControlStateNormal];
//    [viewUserInfo addSubview:btnAccessoty];
    
    UIImageView *leftArrowView = [[UIImageView alloc]initWithFrame:CGRectMake(btnSetPerInf.width - leftArrowImg.size.width * 0.5 - marginX, lbDesc.centerY - leftArrowImg.size.height * 0.3, leftArrowImg.size.width* 0.6, leftArrowImg.size.height * 0.6)];
    [leftArrowView setImage:leftArrowImg];
    [btnSetPerInf addSubview:leftArrowView];
    
    [btnSetPerInf addTarget:self action:@selector(setInfHandle:) forControlEvents:UIControlEventTouchUpInside];
    
    [viewUserInfo addSubview:btnSetPerInf];
    
    
    // 等级
//    btnRank = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - 230, lbUserName.y, 30, 20)];
//    [btnRank setTitle:@"lv 9" forState:UIControlStateNormal];
//    btnFlower.titleLabel.font = [UIFont systemFontOfSize:12.0];
//    [btnRank setTitleColor:[UIColor rgbFromHexString:@"#FC9A69" alpaa:1.0] forState:UIControlStateNormal];
//    [viewUserInfo addSubview:btnRank];
    
    // 花
    btnFlower = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - 160, lbUserName.y, 70, 20)];
    
    if (self.listenToMeData.stUserBaseInfoNet) {
        [btnFlower setTitle:[NSString stringWithFormat:@"%d",self.listenToMeData.stUserBaseInfoNet.iFlowerNum] forState:UIControlStateNormal];
    }else{
        
        [btnFlower setTitle:@"0" forState:UIControlStateNormal];
    }
    [btnFlower setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateNormal];
    [btnFlower.titleLabel setFont:[UIFont systemFontOfSize:12.0]];
    [viewUserInfo addSubview:btnFlower];
    
    //鲜花图标
    UIImage *flower = [UIImage imageNamed:@"歌曲鲜花.png"];
    UIImageView *flowerView = [[UIImageView alloc]initWithFrame:CGRectMake(btnFlower.x - flower.size.width * 0.5, 0, flower.size.width * 0.5, flower.size.height * 0.5)];
    flowerView.centerY = btnFlower.centerY;
    flowerView.image = flower;
    [viewUserInfo addSubview:flowerView];
    
    // 位置 （省份 || 城市）
    _lbLocation = [[UILabel alloc]init];
    _lbLocation.frame = CGRectMake(screenWidth - marginX - 50, lbUserName.y, 50, 20);
    _lbLocation.font = [UIFont systemFontOfSize:12.0];
    [_lbLocation setTextColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0]];
    
    if ([self.listenToMeData.stUserBaseInfoNet.sAddress isEqualToString:@""] || self.listenToMeData.stUserBaseInfoNet == nil) {
        
        _lbLocation.text = @"未知";
    }else{
        _lbLocation.text = self.listenToMeData.stUserBaseInfoNet.sAddress;
    }
    
    _lbLocation.textAlignment = NSTextAlignmentRight;
    [viewUserInfo addSubview:_lbLocation];
    
    
    
    
    
}

-(void)setInfHandle:(UIButton *)button{
    
    
    PIVC *piVC = [[PIVC alloc]init];
    [self.navigationController pushViewController:piVC animated:YES];
}

#pragma mark 导航栏右边的Btn
-(void)setRightBtnItem
{
    UIButton *rightBtn = [[UIButton alloc]init];
    UIImage *imgRight = [UIImage imageNamed:@"more.png"];
    rightBtn.frame = CGRectMake(0, 0, imgRight.size.width, imgRight.size.height);
    [rightBtn setImage:imgRight forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(clickRightBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customRightBtnItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = customRightBtnItem;
}


#pragma mark -点击事件
-(void)clickRightBarBtnItem
{
    YDLog(@"点击了导航栏的右边按钮");
}

-(void)clickImgAlbum
{
    YDLog(@"点击了相册封面");
}

-(void)clickAvatar
{
    YDLog(@"点击了头像");
    
    if (iOS8_greater) { // iOS8及其以上
        _alertCtrl = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        [_alertCtrl addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
        //        [_alertCtrl addAction:[UIAlertAction actionWithTitle:@"我的相册" style:UIAlertActionStyleDefault handler:nil]];
        [_alertCtrl addAction:[UIAlertAction actionWithTitle:@"本地相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self openAlbum];
        }]];
        [_alertCtrl addAction:[UIAlertAction actionWithTitle:@"拍摄" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self openCamera];
            
        }]];
        
        [[UIView appearanceWhenContainedIn:[UIAlertController class], nil] setTintColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
        
        [self presentViewController:_alertCtrl animated:YES completion:^{
            
        }];
        
        
    }else{ // iOS7及其以下
        
        UIActionSheet *menu = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"本地相册",@"拍摄", nil];
        
        [menu showInView:self.view];
    }

    
}

// ActionSheet的代理方法，设置其文字颜色
// iOS8 无法这样设置, actionSheet.subviews.count == 0
-(void)willPresentActionSheet:(UIActionSheet *)actionSheet
{
    if (iOS7_less) {
        for (UIView *subView in actionSheet.subviews) {
            
            if ([subView isKindOfClass:[UIButton class]]) {
                UIButton *btn = (UIButton *)subView;
                [btn setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateNormal];
            }
        }
    } else if(iOS8_greater){
        
        YDLog(@"iOS8下，ActionSheet的字体颜色设置不能通过遍历其子控件的方式来实现\n改用actionsheete为alertController");
    }
}

/**
 * 打开本地相册
 */
-(void)openAlbum
{
    UIImagePickerController *pickerImage = [[UIImagePickerController alloc] init];
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        pickerImage.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        //pickerImage.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        //        pickerImage.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:pickerImage.sourceType];
        
    }
    pickerImage.delegate = (id)self;
    pickerImage.allowsEditing = YES;
    [self presentViewController:pickerImage animated:YES completion:^{
        
    }];
    
}

/**
 * 拍摄--打开相机
 */
-(void)openCamera
{
    //先设定sourceType为相机，然后判断相机是否可用（ipod）没相机，不可用将sourceType设定为相片库
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
    //判断是否摄像头
    if (![UIImagePickerController isSourceTypeAvailable: sourceType]) {
        sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    //sourceType = UIImagePickerControllerSourceTypeCamera; //照相机
    //sourceType = UIImagePickerControllerSourceTypePhotoLibrary; //图片库
    //sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum; //保存的相片
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];//初始化
    picker.delegate = (id)self;
    picker.allowsEditing = YES;//设置可编辑
    picker.sourceType = sourceType;
    picker.showsCameraControls = YES;
    [self presentViewController:picker animated:YES completion:^{
        
    }];
    
}

/**
 * 当选择一张照片后，进入这里
 */
#pragma makr 当选择一张照片之后进入这里

#pragma mark - 完成拍照
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //    // 定义类型
    //    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
    //    // 当选择的类型是图片
    //    if ([type isEqualToString:@"public.image"]) {
    //        // 先把图片转成NSData
    //        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    //
    //        if (image == nil) {
    //            //            NSData *tempData = UIImageJPEGRepresentation(image, 0.1);
    //            image = [info objectForKey:UIImagePickerControllerOriginalImage];
    //            [self performSelector:@selector(saveImage:) withObject:image];
    //        }else{
    //            //            data = UIImagePNGRepresentation(image);
    //            //             NSData *tempData = UIImageJPEGRepresentation(image, 0.1);
    //        }
    //
    //        // 关闭相册界面
    //        [picker dismissViewControllerAnimated:YES completion:^{
    //
    //            // 替换头像为选择的图片
    //
    //
    //        }];
    //
    //    }
    
    NSString *tyep = [info objectForKey:UIImagePickerControllerMediaType];
    
    //使用照相机选取图片
    if ([tyep isEqualToString:(NSString *)kUTTypeImage] && picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        UIImage *originalImg = [info objectForKey:UIImagePickerControllerOriginalImage];
//        NSData *data = UIImagePNGRepresentation(originalImg);
        UIImage *editImg = [info objectForKey:UIImagePickerControllerEditedImage];
        NSData *data = UIImageJPEGRepresentation(editImg, 0.5);
        //
        //        UIImage *cropImg = [info objectForKey:UIImagePickerControllerCropRect];
        //
        //        NSURL * url = [info objectForKey:UIImagePickerControllerMediaURL];
        //
        //        NSDictionary *metaData = [info objectForKey:UIImagePickerControllerMediaMetadata];
        
        [self performSelector:@selector(saveImage:) withObject:originalImg afterDelay:0.5];
        
        UIImageWriteToSavedPhotosAlbum(originalImg, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
        
        [[NetReqManager getInstance] sendgetUploadIcon:self.listenToMeData.stUserBaseInfoNet.uuid VIconDatas:data SPictureUrl:nil];
    }
    
    //使用手机相册选去图片
    if ([tyep isEqualToString:(NSString *)kUTTypeImage]  && picker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary) {
//        UIImage *originalImg = [info objectForKey:UIImagePickerControllerOriginalImage];
//        NSData *data = UIImagePNGRepresentation(originalImg);
        
        UIImage *editImg = [info objectForKey:UIImagePickerControllerEditedImage];
        NSData *data = UIImageJPEGRepresentation(editImg, 0.5);
//        UIImage *smallImage = [self thumbnailWithImageWithoutScale:editImg size:CGSizeMake(40, 40)];
//        [btnAvatar setImage:smallImage forState:UIControlStateNormal];
        
        
        
        [[NetReqManager getInstance] sendgetUploadIcon:self.listenToMeData.stUserBaseInfoNet.uuid VIconDatas:data SPictureUrl:nil];

        
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - 用户取消拍照
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - 保存图片
-(void)saveImage:(UIImage *)image{
    
    
    NSData *imageData = UIImagePNGRepresentation(image);
    if(imageData == nil)
    {
        imageData = UIImageJPEGRepresentation(image, 1.0);
    }
    
    NSDate *date = [NSDate date];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMddHHmmss"];
    NSString *fileName = [[formatter stringFromDate:date] stringByAppendingPathExtension:@"png"] ;
    
    
    //    NSURL *saveURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:fileName];
    //    YDLog(@"save Url: %@",saveURL);
    //    [imageData writeToURL:saveURL atomically:YES];
    
    
    //    NSLog(@"保存头像！");
    //    [userPhotoButton setImage:image forState:UIControlStateNormal];
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imageFilePath = [documentsDirectory stringByAppendingPathComponent:fileName];
    YDLog(@"imageFile->>%@",imageFilePath);
    success = [fileManager fileExistsAtPath:imageFilePath];
    if(success) {
        success = [fileManager removeItemAtPath:imageFilePath error:&error];
    }
    //    UIImage *smallImage=[self scaleFromImage:image toSize:CGSizeMake(80.0f, 80.0f)];//将图片尺寸改为80*80
    UIImage *smallImage = image;//[self thumbnailWithImageWithoutScale:image size:CGSizeMake(40, 40)];
    [UIImageJPEGRepresentation(smallImage, 1.0f) writeToFile:imageFilePath atomically:YES];//写入文件
//    UIImage *selfPhoto = [UIImage imageWithContentsOfFile:imageFilePath];//读取图片文件
    //    [userPhotoButton setImage:selfPhoto forState:UIControlStateNormal];
    
    //    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //    NSString *documentsDirectory = [paths objectAtIndex:0];
    //    NSString *imageFilePath = [documentsDirectory stringByAppendingPathComponent:@"selfPhoto.jpg"];
    //    NSLog(@"imageFile->>%@",imageFilePath);
    //    UIImage *selfPhoto = [UIImage imageWithContentsOfFile:imageFilePath];//
    //    self.img.image = selfPhoto;
    //    [self.img.layer setCornerRadius:CGRectGetHeight([self.img bounds]) / 2];  //修改半径，实现头像的圆形化
    //    self.img.layer.masksToBounds = YES
    
    
}
#pragma mark -保持原来的长宽比，生成一个缩略图
- (UIImage *)thumbnailWithImageWithoutScale:(UIImage *)image size:(CGSize)asize
{
    UIImage *newimage;
    if (nil == image) {
        newimage = nil;
    }
    else{
        CGSize oldsize = image.size;
        CGRect rect;
        if (asize.width/asize.height > oldsize.width/oldsize.height) {
            rect.size.width = asize.height*oldsize.width/oldsize.height;
            rect.size.height = asize.height;
            rect.origin.x = (asize.width - rect.size.width)/2;
            rect.origin.y = 0;
        }
        else{
            rect.size.width = asize.width;
            rect.size.height = asize.width*oldsize.height/oldsize.width;
            rect.origin.x = 0;
            rect.origin.y = (asize.height - rect.size.height)/2;
        }
        UIGraphicsBeginImageContext(asize);
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetFillColorWithColor(context, [[UIColor clearColor] CGColor]);
        UIRectFill(CGRectMake(0, 0, asize.width, asize.height));//clear background
        [image drawInRect:rect];
        newimage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    return newimage;
}

#pragma mark - 改变图像的尺寸，方便上传服务器
- (UIImage *) scaleFromImage: (UIImage *) image toSize: (CGSize) size
{
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - 保存照片成功后调用
- (void)image:(UIImage*)image didFinishSavingWithError:(NSError*)error contextInfo:(void*)contextInfo{
    
    if (!error) {
        
        YDLog(@"picture saved success");
    }else{
        
        YDLog(@"error occured while saving the picture%@", error);
        
    }
    
}

//Return the URL to the application Documents directory
-(NSURL *)applicationDocumentsDirectory{
    
    return [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentationDirectory inDomains:NSUserDomainMask ] lastObject];
}


#pragma mark 点击了cell
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    YDLog(@"点击了第 %ld组- 第 %ld行",(long)indexPath.section,(long)indexPath.row);
    switch (indexPath.section) {
            //  组
        case 0:{
            // row
            switch (indexPath.row) {
                case 0:{
                    MyAlbumVC *myAlbumVC = [[MyAlbumVC alloc]init];
                    myAlbumVC.navigationItem.title = @"我的相册";
                    myAlbumVC.uuid = self.listenToMeData.stUserBaseInfoNet.uuid;
                    myAlbumVC.iOffset = 0;
                    myAlbumVC.iNum = self.listenToMeData.stUserBaseInfoNet.iPictureNum;
                    myAlbumVC.lWatchUserId = 0;
                    myAlbumVC.lCommemorateId = 0;
                    myAlbumVC.isPushFromPerCenter = YES;
                    [self.navigationController pushViewController:myAlbumVC animated:YES];
                    break;
                }case 1:{
                    MyMessageVC *myMessageVC = [[MyMessageVC alloc]init];
                    [self.navigationController pushViewController:myMessageVC animated:YES];
                    break;
                }case 2:{
                    MyWorksVC *myWorksVC = [[MyWorksVC alloc]init];
                    myWorksVC.isPushFromPerCenter = YES;
                    myWorksVC.uuid = self.listenToMeData.stUserBaseInfoNet.uuid;
                    myWorksVC.iOffset = 0;
                    myWorksVC.iNum = 10;
                    myWorksVC.lWatchUserId = 0;
                    myWorksVC.navigationItem.title = [NSString stringWithFormat:@"我的作品"];
                    [self.navigationController pushViewController:myWorksVC animated:YES];
                    break;
                    
                }case 3:{
                    MyBookVC *myBookVC = [[MyBookVC alloc]init];
                    myBookVC.navigationItem.title = @"我的纪念册";
                    [self.navigationController pushViewController:myBookVC animated:YES];
                    break;
                }
                
//                case 3:{
//                    MySubscribeVC *mySubscribeVC = [[MySubscribeVC alloc]init];
//                    [self.navigationController pushViewController:mySubscribeVC animated:YES];
//                    break;
//                    
//                }
//                case 5:{
//                    PIVC *piVC = [[PIVC alloc]init];
//                    [self.navigationController pushViewController:piVC animated:YES];
//                    break;
//                }
                    
                default:
                    break;
            }
            break;
        }
            //  组
        case 1:{
            // row
            switch (indexPath.row) {
                    
                case 0:{
                    MyCollectionVC *myCollectionVC = [[MyCollectionVC alloc]init];
                    [self.navigationController pushViewController:myCollectionVC animated:YES];
                    break;
                }case 1:{
                    MyGiftCertificateVC *myGiftCertificateVC = [[MyGiftCertificateVC alloc]init];
                    //从个人中心进入我的礼券
                    myGiftCertificateVC.isPushFromPerCenter = YES;
                    [self.navigationController pushViewController:myGiftCertificateVC animated:YES];
                    break;
                }
                default:
                    break;
            }
            break;
            
        }
            //  组
//        case 2:{
//            MyRecommendVC *myRecommendVC = [[MyRecommendVC alloc]init];
//            [self.navigationController pushViewController:myRecommendVC animated:YES];
//            break;
//        }
            //  组
//        case 3:{
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/cn/app/jie-zou-da-shi/id493901993?mt=8"]];
//            break;
//            
//        }
            // 组
        case 2:{
            SettingsVC *settingsVC = [[SettingsVC alloc]init];
            [self.navigationController pushViewController:settingsVC animated:YES];
            break;
        }
        default:
            break;
    }
    
}

#pragma mark - 设置分割线左对齐
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark -tableview代理 & 数据源
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *identifier = @"individual";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (nil == cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    NSString *rowImgName = nil;
    
    switch (indexPath.section) {
            // 0 组
        case 0:{
            // row
            switch (indexPath.row) {
                case 0:{
                    cell.textLabel.text = @"我的相册";
                    rowImgName = @"Group.png";
                    break;
                }case 1:{
                    cell.textLabel.text = @"我的消息";
                    rowImgName = @"Group76.png";
                    break;
                }case 2:{
                    cell.textLabel.text = @"我的作品";
                    rowImgName = @"我的作品.png";
                    break;
                    
                }case 3:{
                    cell.textLabel.text = @"我的纪念册";
                    rowImgName = @"GroupMyBook.png";
                    break;
                }
                
//                case 3:{
//                    cell.textLabel.text = @"我的约歌";
//                    rowImgName = @"GroupYueGe.png";
//                    break;
//                    
//                }
//                case 5:{
//                    cell.textLabel.text = @"个人信息";
//                    break;
//                }
                    
                default:
                    break;
            }
            break;
        }
            // 0 组
        case 1:{
            // row
            switch (indexPath.row) {
//                case 0:{
//                    cell.textLabel.text = @"我的歌单";
//                    rowImgName = @"Group6786.png";
//                    break;
//                }case 1:{
//                    cell.textLabel.text = @"我的收藏";
//                    rowImgName = @"Star1.png";
//                    break;
//                }case 2:{
//                    cell.textLabel.text = @"我的礼券";
//                    rowImgName = @"MyPresent.png";
//                    break;
//                }
//                default:
//                    break;
                    
                case 0:{
                    cell.textLabel.text = @"我的收藏";
                    rowImgName = @"Star1.png";
                    break;
                }case 1:{
                    cell.textLabel.text = @"我的礼券";
                    rowImgName = @"MyPresent.png";
                    break;
                }
                default:
                    break;
            }
            break;
            
        }
            // 0 组
//        case 2:{
//            cell.textLabel.text = @"推荐使用";
//            rowImgName = @"RecommendApp.png";
//            break;
//        }
            // 0 组
//        case 2:{
//            cell.textLabel.text = @"写好评";
//            rowImgName = @"WritingEvaluation.png";
//            break;
//            
//        }
            // 0 组
        case 2:{
            cell.textLabel.text = @"设置";
            rowImgName = @"AppSet.png";
            break;
        }
        default:
            break;
    }
    
    cell.accessoryType =  UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIImage *iconImg = [UIImage imageNamed:rowImgName];
    [cell.imageView setImage:iconImg];
    
    

    return cell;
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:{
            return 4;//
            break;
        }
        case 1:{
            return 2;
            break;
        }
        case 2:{
            return 1;
            break;
        }
        
        default:
            return 0;
            break;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 25;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *ydHeadView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 25)];
    ydHeadView.backgroundColor = [UIColor clearColor];
    
    return ydHeadView;
}

#pragma mark -其他
#pragma mark - UILabel自适应
-(CGSize )sizeWithLabelText:(NSString *)text font:(UIFont *)font{
    
    CGRect rect = [text boundingRectWithSize:CGSizeMake(self.lbDesc.width, MAXFLOAT) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
    return rect.size;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
