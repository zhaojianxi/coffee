//
//  MsgCell.h
//  ListenToMe
//
//  Created by yadong on 2/6/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YDBadgeBtn.h"
#import "YDCircleAvatarBtn.h"

@interface MsgCell : UITableViewCell
/**
 *  显示消息数量
 */
@property(strong,nonatomic) YDBadgeBtn *badgeBtn;
/**
 *  发送消息的对象的头像
 */
@property(strong,nonatomic) YDCircleAvatarBtn *btnMsgAvatar;
/**
 *  发送消息对象的名字
 */
@property(strong,nonatomic) UILabel *lbTalkWith;
/**
 *  消息内容的简介
 */
@property(strong,nonatomic) UILabel *lbLastMsg;
/**
 *  接收消息的最近时间
 */
@property(strong,nonatomic) UILabel *lbLastTime;
/**
 *  自定义分割线
 */
@property(strong,nonatomic)UIImageView *lineView;
@end
