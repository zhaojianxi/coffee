//
//  MyMessageVC.m
//  ListenToMe
//
//  Created by yadong on 2/5/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "MyMessageVC.h"
#import "OfficialMsgVC.h"
#import "MsgCell.h"
#import "FriendsMsgVC.h"

@interface MyMessageVC ()<UISearchBarDelegate,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>
@property(strong,nonatomic) UISearchBar *msgSearchBar;
@property(strong,nonatomic) UIButton *BtnGrayBg;
@property(strong,nonatomic) UITableView *mTableView;
/**
 *  数据单例
 */
@property(nonatomic,strong) ListenToMeData *listenToMeData;
@end

@implementation MyMessageVC
@synthesize msgSearchBar;
@synthesize BtnGrayBg;
@synthesize mTableView;

#pragma mark -生命周期
-(void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [self setUI];
    
    [self initData];
}

#pragma mark 初始化数据
-(void)initData{
    self.listenToMeData = [ListenToMeData getInstance];
}

#pragma mark -UI
-(void)setUI
{
    [self setNav];
    
    [self setSearchBar];
    
    [self setUpTableView];
}


#pragma mark 导航栏
-(void)setNav
{
    self.navigationItem.title = @"我的消息";
    
    [self setRightBtnItem];
    
    [self setLeftBtnItem];
}

// 导航栏右边的Btn
-(void)setRightBtnItem
{
    UIButton *rightBtn = [[UIButton alloc]init];
    UIImage *imgMore = [UIImage imageNamed:@"more.png"];
    rightBtn.frame = CGRectMake(0, 0, imgMore.size.width, imgMore.size.height);
    [rightBtn setImage:imgMore forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(clickRightBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customRightBtnItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = customRightBtnItem;
}

// 导航栏左边的Btn
-(void)setLeftBtnItem
{
    UIButton *leftBtn = [[UIButton alloc]init];
    UIImage *leftBtnImg = [UIImage imageNamed:@"whiteBack.png"];
    leftBtn.frame = CGRectMake(0, 0, leftBtnImg.size.width, leftBtnImg.size.height);
    [leftBtn setImage:leftBtnImg forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(clickLeftBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customLeftBtnItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = customLeftBtnItem;
}

-(void)clickRightBarBtnItem
{
    YDLog(@"点击了导航栏的右边的按钮");
}

-(void)clickLeftBarBtnItem
{
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark 搜索框
-(void)setSearchBar
{
    CGFloat msgSearchBarW = screenWidth;
    CGFloat msgSearchBarH = 40;
    CGFloat msgSearchBarX = 0;
    CGFloat msgSearchBarY = 64;
    msgSearchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(msgSearchBarX, msgSearchBarY, msgSearchBarW, msgSearchBarH)];
    [self.view addSubview:msgSearchBar];
    
    msgSearchBar.delegate = self;
    
    msgSearchBar.placeholder = @"搜索";
    
    // 去掉搜索栏边框
    msgSearchBar.backgroundColor = [UIColor clearColor];
    msgSearchBar.backgroundImage = nil;
}


#pragma mark tableView
-(void)setUpTableView
{
    CGFloat mTableViewW = screenWidth;
    CGFloat mTableViewH = screenHeight - msgSearchBar.frame.origin.y - msgSearchBar.frame.size.height;
    CGFloat mTableViewX = 0;
    CGFloat mTableViewY = msgSearchBar.frame.origin.y + msgSearchBar.frame.size.height ;
    mTableView = [[UITableView alloc]initWithFrame:CGRectMake(mTableViewX, mTableViewY, mTableViewW, mTableViewH) style:UITableViewStylePlain];
    [self.view addSubview:mTableView];
    mTableView.backgroundColor = [UIColor clearColor];
    mTableView.scrollsToTop = NO;
    mTableView.showsVerticalScrollIndicator = NO;
    mTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    mTableView.delegate = self;
    mTableView.dataSource = self;
}

#pragma mark - tableView的 delegate & datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }else{
    return 10;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 15;
}

-(MsgCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"msg";
    MsgCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[MsgCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSString *imgName = nil;
    switch (indexPath.section) {
        case 0:{
            imgName = @"officeMessageImg.png";
            cell.badgeBtn.badgeValue = 5;
            cell.lbTalkWith.text = @"官方消息";
            [cell.lbTalkWith setTextColor:[UIColor rgbFromHexString:@"#FF0053" alpaa:1.0]];
            cell.lbLastMsg.text = @"距离约歌还有一小时";
            cell.lbLastTime.text = @"下午 4:27";
            cell.lineView.hidden = YES;
            [cell.btnMsgAvatar setImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
        }
            break;
        case 1:{
            
            
            
            imgName = @"temp3.png";
            if (indexPath.row == 0) {
                cell.badgeBtn.badgeValue = 1;
                cell.lbTalkWith.text = @"张三";
                
                cell.lbLastMsg.text = @"距离约歌还有一小时,先干点什么......";
                cell.lbLastTime.text = @"下午 4:18";
            }else{
                cell.badgeBtn.badgeValue = 0;
                cell.lbTalkWith.text = @"赵六";
                cell.lbLastMsg.text = @"距离约歌还有一小时";
                cell.lbLastTime.text = @"下午 4:10";
            }
            
            [cell.lbTalkWith setTextColor:[UIColor rgbFromHexString:@"#000000" alpaa:1.0]];
#warning 明天继续调整消息记录
            UIImageView *avatarImgView = [[UIImageView alloc]initWithFrame:cell.btnMsgAvatar.bounds];
            [avatarImgView sd_setImageWithURL:[NSURL URLWithString:@""] placeholderImage:[UIImage imageNamed:@"icon.png"]];
            
            [cell.btnMsgAvatar setImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];

        }
            break;
            
        default:
            break;
    }
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 暂时这样，后面加上消息的类型来进行判断
    
    MsgCell *messageCell = (MsgCell *)[tableView cellForRowAtIndexPath:indexPath];
    // 如果有 2组的话，则第0组是官方消息
    if (2 == tableView.numberOfSections) {
        if (indexPath.section == 0) {
            [self.navigationController pushViewController:[[OfficialMsgVC alloc]init] animated:YES];
        } else if(indexPath.section == 1){
            FriendsMsgVC *friendVC = [[FriendsMsgVC alloc]init];
            friendVC.navigationItem.title = messageCell.lbTalkWith.text;
            [self.navigationController pushViewController:friendVC animated:YES];
        }
    }
    
    YDLog(@"你选择了%ld分区,%ld行",indexPath.section,indexPath.row);
}

#pragma mark -searchBar的代理
#pragma mark 开始输入
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [msgSearchBar setShowsCancelButton:YES animated:YES];
    
    [self setUpGrayBacground];
}

#pragma mark 点击取消Btn
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [UIView animateWithDuration:0.5 animations:^{
        
        [self cancelGrayBacground];
    }];
}

#pragma mark 点击搜索键
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    YDLog(@"点击搜索Btn");
    [msgSearchBar setShowsCancelButton:NO animated:YES];
    BtnGrayBg.alpha = 0.0;
    BtnGrayBg = nil;
    [msgSearchBar resignFirstResponder];
}


#pragma mark - 点击事件
-(void)btnGrayBgClick{

    [msgSearchBar setShowsCancelButton:NO animated:YES];
    BtnGrayBg.alpha = 0.0;
    BtnGrayBg = nil;
    [msgSearchBar resignFirstResponder];
}

#pragma mark -灰质背景
// 取消灰质背景
-(void)cancelGrayBacground
{
    // 隐藏cancelBtn & 取消灰质 & 注销第一响应
    [msgSearchBar setShowsCancelButton:NO animated:YES];
    BtnGrayBg.alpha = 0.0;
    BtnGrayBg = nil;
    msgSearchBar.text = @"";
    [msgSearchBar resignFirstResponder];
}

// 初始化灰质背景
-(void)setUpGrayBacground
{
    if (BtnGrayBg == nil) {
        BtnGrayBg = [UIButton buttonWithType:UIButtonTypeCustom];
        BtnGrayBg.frame = CGRectMake(0, msgSearchBar.frame.origin.y + msgSearchBar.frame.size.height, screenWidth, screenHeight - (msgSearchBar.frame.size.height + msgSearchBar.frame.origin.y));
        [self.view addSubview:BtnGrayBg];

        [BtnGrayBg addTarget:self action:@selector(btnGrayBgClick) forControlEvents:UIControlEventTouchUpInside];
    }
    
    BtnGrayBg.alpha = 0.0;
    BtnGrayBg.backgroundColor = [UIColor blackColor];
    [self.view bringSubviewToFront:BtnGrayBg];
    
    // screen灰质的block动画
    [UIView animateWithDuration:0.5 animations:^{
        BtnGrayBg.alpha = 0.35;
    }];
    
}
@end



