//
//  OfficialMsgVC.m
//  ListenToMe
//
//  Created by yadong on 2/6/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "OfficialMsgVC.h"
#import "OfficalMsgCell.h"

@interface OfficialMsgVC ()<UITableViewDataSource,UITableViewDelegate>
@property(strong,nonatomic) UITableView *mTableView;
@end

@implementation OfficialMsgVC
@synthesize mTableView;

#pragma mark - 生命周期
- (void)viewDidLoad {
    [super viewDidLoad];

  self.title = @"官方消息";
    
    [self setUpTableview];
}



#pragma mark tableview
-(void)setUpTableview
{
    CGFloat mTableViewW = screenWidth;
    CGFloat mTableViewH = screenHeight + 20;
    CGFloat mTableViewX = 0;
    CGFloat mTableViewY = 0;
    mTableView = [[UITableView alloc]initWithFrame:CGRectMake(mTableViewX, mTableViewY, mTableViewW, mTableViewH) style:UITableViewStylePlain];
    [self.view addSubview:mTableView];
    mTableView.tableHeaderView.height = 15;
    mTableView.scrollsToTop = NO;
    mTableView.showsVerticalScrollIndicator = NO;
    mTableView.backgroundColor = [UIColor clearColor];
    mTableView.delegate = self;
    mTableView.dataSource = self;
}

#pragma mark - tableView Delegate & Datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 10;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 0;
    }else{
        return 15;
    }
}

-(OfficalMsgCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"officalMsg";
    OfficalMsgCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell =  [[OfficalMsgCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSInteger temp = indexPath.section % 2;
    switch (temp) {
        case 0:{
            cell.lbOfficeMessageTitle.text = @"关于卡券的消息提醒";
            cell.lbOfficeMessageTime.text = @"下午4:27";
            cell.lbOfficeMessageContent.text = @"全球最大的中文搜索引擎、致力于让网民更便捷地获取信息，找到所求。百度超过千亿的中文网页数据库，可以瞬间找到相关的搜索结果。";
            cell.officeMessageView.image = [UIImage imageNamed:@"优惠劵.png"];
        }
            break;
        case 1:{
            cell.lbOfficeMessageTitle.text = @"官方传达消息";
            cell.lbOfficeMessageTime.text = @"2014-12-12";
            cell.lbOfficeMessageContent.text = @"百度新闻是包含海量资讯的新闻服务平台,真实反映每时每刻的新闻热点。";
            
            cell.officeMessageView.image = [UIImage imageNamed:@"news.png"];
        }
            break;
            
        default:
            break;
    }
       
    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 121;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
