//
//  FreindMsgFrameModel.h
//  ListenToMe
//
//  Created by yadong on 2/11/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <Foundation/Foundation.h>
#define NJTextFont [UIFont systemFontOfSize:15]
#define NJEdgeInsetsWidth 20

@class FreindMsgModel;

@interface FreindMsgFrameModel : NSObject

/**
 *  数据模型
 */
@property (nonatomic, strong) FreindMsgModel *message;

/**
 *  时间的frame
 */
@property (nonatomic, assign, readonly) CGRect timeF;
/**
 *  头像frame
 */
@property (nonatomic, assign, readonly) CGRect iconF;
/**
 *  正文frame
 */
@property (nonatomic, assign, readonly) CGRect textF;
/**
 *  cell的高度
 */
@property (nonatomic, assign, readonly) CGFloat cellHeight;

@end


