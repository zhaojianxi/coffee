//
//  FriendsMsgVC.m
//  ListenToMe
//
//  Created by zhw on 2/7/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "FriendsMsgVC.h"
#import "FreindMsgModel.h"
#import "FreindMsgFrameModel.h"
#import "FriendMsgCell.h"
#import "GivingFlowerView.h"
#import "MyGiftCertificateVC.h"
#import "MyWorksVC.h"

@interface FriendsMsgVC ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIScrollViewDelegate,UIGestureRecognizerDelegate>
@property(strong,nonatomic) UITableView *mTableView;
/**
 * 顶部工具栏
 */
@property(strong,nonatomic) UIView *viewTopBar;
/**
 * 底部工具栏
 */
@property(strong,nonatomic) UIView *viewBottom;
/**
 *  送礼物
 */
@property(strong,nonatomic) UIButton *btnGift;
/**
 *  送歌
 */
@property(strong,nonatomic) UIButton *btnSong;
/**
 *  关进小黑屋
 */
@property(strong,nonatomic) UIButton *btnBlackRoom;
/**
 * 存放---消息数据
 */
@property(strong,nonatomic) NSMutableArray *arrayMessages;
/**
 * 存放所有的自动回复数据
 */
@property(strong,nonatomic) NSDictionary *dicAutoReplays;
/**
 * 输入框架
 */
@property(strong,nonatomic) UITextField *textFInput;
/**
 *  遮盖视图
 */
@property(strong,nonatomic) UIView *coverView;
/**
 *  送礼物弹窗背景
 */
@property(strong,nonatomic) UIButton *alertForGift;
/**
 *  送礼物(鲜花,优惠券)
 */
@property(strong,nonatomic) UIView *giftView;
/**
 *  送歌弹窗背景
 */
@property(strong,nonatomic) UIButton *alertFroSong;
/**
 *  关进小黑屋弹窗背景
 */
@property(strong,nonatomic) UIButton *alertForBlackRoom;
/**
 *  送花
 */
@property(strong,nonatomic) GivingFlowerView *sendFlowerView;

/**
 *  默认送出鲜花数
 */
@property(assign,nonatomic) int sendFlowerNum;
/**
 *  用户拥有的鲜花数
 */
@property(assign,nonatomic) NSInteger haveFlowerNum;
/**
 *  数据
 */
@property(nonatomic,strong) ListenToMeData *listenToMeData;

@end

@implementation FriendsMsgVC

#pragma mark -生命周期
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
    
    [self setUpUI];
    
    [self initData];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.mTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.arrayMessages count] - 1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
}


#pragma mark - initData
-(void)initData{
    self.listenToMeData = [ListenToMeData getInstance];
    
    //首先要获取当前私聊对象的小黑屋状态,然后在根据其状态执行是否将其关进(释放)小黑屋
    [[NetReqManager getInstance] sendGetBlackRoomState:self.listenToMeData.stUserBaseInfoNet.uuid LToUserId:self.friendUserBaseInfoNet.uuid];
}

#pragma mark -UI
-(void)setUpUI
{
    [self setLeftBtnItem];
    
    [self setRightBtnItem];
    
    [self setUpviewTopBar];
    
    [self setUpBottom];
    
    [self setUpTableview];
    
   
}

// 导航栏左边的Btn
-(void)setLeftBtnItem
{
    UIButton *leftBtn = [[UIButton alloc]init];
    UIImage *leftBtnImg = [UIImage imageNamed:@"whiteBack.png"];
    leftBtn.frame = CGRectMake(0, 0, leftBtnImg.size.width, leftBtnImg.size.height);
    [leftBtn setImage:leftBtnImg forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(clickLeftBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customLeftBtnItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = customLeftBtnItem;
}

// 导航栏右边的Btn
-(void)setRightBtnItem
{
    UIButton *rightBtn = [[UIButton alloc]init];
    UIImage *imgMore = [UIImage imageNamed:@"more.png"];
    rightBtn.frame = CGRectMake(0, 0, imgMore.size.width, imgMore.size.height);
    [rightBtn setImage:imgMore forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(clickRightBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customRightBtnItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = customRightBtnItem;
}

-(void)clickRightBarBtnItem
{
    YDLog(@"点击了导航栏的右边的按钮");
}

-(void)clickLeftBarBtnItem
{
    
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark 顶部-工具栏
-(void)setUpviewTopBar
{
    _viewTopBar = [[UIView alloc]initWithFrame:CGRectMake(0,naviAndStatusH, screenWidth, 44)];
    [_viewTopBar setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:_viewTopBar];
    
    CGFloat eachW = _viewTopBar.width / 3;
    CGFloat frontY = 6;
    CGFloat frontX = (eachW - 15) * 0.5;
    CGFloat btnsW = 15;
    CGFloat btnsH = 15;
    CGFloat btnsY = 15 * 0.5;
    
    // 送礼物
    _btnGift = [[UIButton alloc]initWithFrame:CGRectMake(frontX, btnsY, btnsW, btnsH)];
    [_viewTopBar addSubview:_btnGift];
    
    [_btnGift setImage:[UIImage imageNamed:@"送礼物.png"] forState:UIControlStateNormal];
    UILabel *lbGift = [[UILabel alloc]initWithFrame:CGRectMake(_btnGift.centerX - 25, _btnGift.y + btnsH, 50, 20)];
    lbGift.text = @"送礼物";
    [lbGift setTextColor:[UIColor rgbFromHexString:@"#FF0053" alpaa:1.0]];
    [lbGift setTextAlignment:NSTextAlignmentCenter];
    [lbGift setFont:[UIFont systemFontOfSize:8.0]];
    [_viewTopBar addSubview:lbGift];
    UIButton *giftTemp = [[UIButton alloc]initWithFrame:CGRectMake(lbGift.x, _btnGift.y, lbGift.width, _btnGift.height + lbGift.height)];
    giftTemp.backgroundColor = [UIColor clearColor];
    [_viewTopBar addSubview:giftTemp];
    [giftTemp addTarget:self action:@selector(pushGiftView) forControlEvents:UIControlEventTouchUpInside];
    
    
    // 送歌
    _btnSong = [[UIButton alloc]initWithFrame:CGRectMake(_btnGift.x + eachW, btnsY, btnsW, btnsH)];
    [_viewTopBar addSubview:_btnSong];
    
    [_btnSong setImage:[UIImage imageNamed:@"送歌.png"] forState:UIControlStateNormal];
    UILabel *lbSong = [[UILabel alloc]initWithFrame:CGRectMake(_btnSong.centerX - 25, _btnSong.y + btnsH, 50, 20)];
    lbSong.text = @"送歌";
    [lbSong setTextColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
    [lbSong setTextAlignment:NSTextAlignmentCenter];
    [lbSong setFont:[UIFont systemFontOfSize:8.0]];
    [_viewTopBar addSubview:lbSong];
    
    UIButton *songTemp = [[UIButton alloc]initWithFrame:CGRectMake(lbSong.x, _btnSong.y, lbSong.width, _btnSong.height + lbSong.height)];
    songTemp.backgroundColor = [UIColor clearColor];
    [_viewTopBar addSubview:songTemp];
    [songTemp addTarget:self action:@selector(pushSongView) forControlEvents:UIControlEventTouchUpInside];

    
    
    // 关进小黑屋
    _btnBlackRoom = [[UIButton alloc]initWithFrame:CGRectMake(_btnGift.x + eachW * 2, btnsY, btnsW, btnsH)];
    [_viewTopBar addSubview:_btnBlackRoom];
    
    [_btnBlackRoom setImage:[UIImage imageNamed:@"关进小黑屋.png"] forState:UIControlStateNormal];
    UILabel *lbBlackRoom = [[UILabel alloc]initWithFrame:CGRectMake(_btnBlackRoom.centerX - 25, _btnBlackRoom.y + btnsH, 50, 20)];
    lbBlackRoom.text = @"关进小黑屋";
    [lbBlackRoom setTextColor:[UIColor darkGrayColor]];
    [lbBlackRoom setTextAlignment:NSTextAlignmentCenter];
    [lbBlackRoom setFont:[UIFont systemFontOfSize:8.0]];
    [_viewTopBar addSubview:lbBlackRoom];
    
    UIButton *blackRoomTemp = [[UIButton alloc]initWithFrame:CGRectMake(lbBlackRoom.x, _btnBlackRoom.y, lbBlackRoom.width, _btnBlackRoom.height + lbBlackRoom.height)];
    blackRoomTemp.backgroundColor = [UIColor clearColor];
    [_viewTopBar addSubview:blackRoomTemp];
    [blackRoomTemp addTarget:self action:@selector(pushBlackRoomView) forControlEvents:UIControlEventTouchUpInside];
    
    
    for (int index = 0; index < 2; index ++) {
        UIImageView *lineImgView = [[UIImageView alloc]initWithFrame:CGRectMake( eachW * (index + 1)  - 1 , btnsY, 1, _viewTopBar.height - btnsY - frontY)];
        UIImage *lineImg =[UIImage imageNamed:@"分割线"];
        lineImgView.alpha = 0.2;
        [lineImgView setImage:lineImg];
        [_viewTopBar addSubview:lineImgView];
    }
    
    
    //初始化遮盖视图
    self.coverView = [[UIView alloc]initWithFrame:[UIScreen mainScreen].bounds];
    self.coverView.backgroundColor = [UIColor clearColor];
    self.coverView.alpha  = 1.0;
    
    self.alertForGift = [[UIButton alloc]initWithFrame:[UIScreen mainScreen].bounds];
    //    [self.alertForGift setBackgroundColor:[UIColor clearColor]];
    //    self.alertForGift.alpha = 1;
    self.alertForGift.backgroundColor = [UIColor blackColor];
    self.alertForGift.alpha = 0.3;
    [self.alertForGift addTarget:self action:@selector(backToView:) forControlEvents:UIControlEventTouchUpInside];
    [self.coverView addSubview:self.alertForGift];
}

#pragma mark 底部-工具栏
-(void)setUpBottom
{
    // 底部承载视图
    _viewBottom = [[UIView alloc]initWithFrame:CGRectMake(0, screenHeight - 60, screenWidth, 60)];
    _viewBottom.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_viewBottom];
    
    // 输入框
    CGFloat inputW = _viewBottom.width - 30;
    CGFloat inputH = 35;
    CGFloat inputX = 15;
    CGFloat inputY =( _viewBottom.height - inputH ) * 0.5;
    _textFInput = [[UITextField alloc]initWithFrame:CGRectMake(inputX, inputY, inputW, inputH)];
    UIImage *inputBgImg = [UIImage imageNamed:@"inputBgImg.png"];
    [_textFInput setBackground:inputBgImg];
    [_textFInput setTextColor:[UIColor rgbFromHexString:@"#000000" alpaa:1.0]];
    _textFInput.clearButtonMode = UITextFieldViewModeWhileEditing;
//    _textFInput.backgroundColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0];
    [_viewBottom addSubview:_textFInput];
    
    _textFInput.delegate = self;
}

#pragma mark tableview
-(void)setUpTableview
{
    CGFloat tableviewH = screenHeight - naviAndStatusH - _viewTopBar.height - _viewBottom.height;  // tableview的高度
    _mTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, _viewTopBar.y + _viewTopBar.height, screenWidth,tableviewH) style:UITableViewStylePlain];
    _mTableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_mTableView];
    
    _mTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _mTableView.showsVerticalScrollIndicator = NO;
    _mTableView.allowsSelection = NO;
    _mTableView.delegate = self;
    _mTableView.dataSource = self;
    [self addAGesutreRecognizerForYourView];
}

#pragma mark - 懒加载--messages
-(NSMutableArray *)arrayMessages
{
    if (_arrayMessages == nil) {
        NSString *fullPath = [[NSBundle mainBundle] pathForResource:@"messages.plist" ofType:nil];
        NSArray *dictArray = [NSArray arrayWithContentsOfFile:fullPath];
        NSMutableArray *modelsArray = [NSMutableArray arrayWithCapacity:dictArray.count];
        for (NSDictionary *dict in dictArray) {
            // 1.创建当前数据模型
            FreindMsgModel *messageModel = [FreindMsgModel FreindMsgModelWithDict:dict];
            
            // 获取上一次的数据模型
            FreindMsgModel *previousMessageModel = (FreindMsgModel *)[[modelsArray lastObject] message];
            
            // 设置是否隐藏时间
            messageModel.hiddenTime = [messageModel.time isEqualToString:previousMessageModel.time];
            
            
            // 创建frame模型
            FreindMsgFrameModel *msgFrameModel = [[FreindMsgFrameModel alloc]init];
            msgFrameModel.message = messageModel;
            
            // 将frame模型保存到数组中
            [modelsArray addObject:msgFrameModel];
        }
        
        self.arrayMessages = [modelsArray mutableCopy];
    }
    return  _arrayMessages;
}

#pragma mark -delegate & datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FriendMsgCell *cell = [FriendMsgCell cellWithTableView:tableView];
    cell.thePerosnCover = self.friendUserBaseInfoNet.sCovver;
    cell.messageFrame = self.arrayMessages[indexPath.row];

    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FreindMsgFrameModel *modelFrame = self.arrayMessages[indexPath.row];
    return modelFrame.cellHeight;
}

#pragma mark - Data
#pragma mark 回复信息
-(NSDictionary *)dicAutoReplays
{
    if (_dicAutoReplays == nil) {
        NSString *fullPath = [[NSBundle mainBundle] pathForResource:@"autoReplay.plist" ofType:nil];
        _dicAutoReplays = [NSDictionary dictionaryWithContentsOfFile:fullPath];
    }
    
    return _dicAutoReplays;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayMessages.count;
}

#pragma mark 滚动的时候，注销输入
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.view endEditing:YES];
}

#pragma mark  -点击事件
#pragma mark 点击return键
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{

    if(textField.text.length > 0){
        //将输入的文字内容已经时间类型复制给消息对象,并将消息对象存入数组中,再刷新数据.
        FreindMsgModel *messageModel = [[FreindMsgModel alloc]init];
        messageModel.text = textField.text;
        NSDate *date = [NSDate date];
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        formatter.dateFormat = @"aa hh:mm";
    
        messageModel.time = [formatter stringFromDate:date];
        messageModel.type = FreindMsgModelTypeMe;
    
        FreindMsgModel *previousMessageModel = (FreindMsgModel *)[[self.arrayMessages lastObject] message];
    
    
        messageModel.hiddenTime = [messageModel.time isEqualToString:previousMessageModel.time];
    
        FreindMsgFrameModel *msgFrameModel = [[FreindMsgFrameModel alloc]init];
        msgFrameModel.message = messageModel;
    
        [self.arrayMessages addObject:msgFrameModel];
        
        //添加的测试数据
        FreindMsgModel *messageModelSec = [[FreindMsgModel alloc]init];
        messageModelSec.text = @"然后呢";
        NSDate *dateSec = [NSDate date];
        NSDateFormatter *formatterSec = [[NSDateFormatter alloc]init];
        formatterSec.dateFormat = @"aa hh:mm";
        
        messageModelSec.time = [formatterSec stringFromDate:dateSec];
        messageModelSec.type = FreindMsgModelTypeOther;
        
        FreindMsgModel *previousMessageModelSec = (FreindMsgModel *)[[self.arrayMessages lastObject] message];
        
        
        messageModelSec.hiddenTime = [messageModelSec.time isEqualToString:previousMessageModelSec.time];
        
        FreindMsgFrameModel *msgFrameModelSec = [[FreindMsgFrameModel alloc]init];
        msgFrameModelSec.message = messageModelSec;
        
        [self.arrayMessages addObject:msgFrameModelSec];
        textField.text = nil;
    
    }
    [self.mTableView reloadData];
    [self.mTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.arrayMessages count] - 1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
//    [self.view endEditing:YES];
    return YES;
}


#pragma mark - 其他
#pragma mark 键盘变化
- (void)keyboardWillChange:(NSNotification  *)notification
{
       // 1.获取键盘的Y值
    NSDictionary *dict  = notification.userInfo;
    CGRect keyboardFrame = [dict[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat keyboardY = keyboardFrame.origin.y;
    // 获取动画执行时间
    CGFloat duration = [dict[UIKeyboardAnimationDurationUserInfoKey]doubleValue];
    // 2.计算需要移动的距离
    CGFloat translationY = keyboardY - self.view.frame.size.height;

    [UIView animateWithDuration:duration delay:0.0 options:7 << 16 animations:^{
        // 需要执行动画的代码
        self.view.transform = CGAffineTransformMakeTranslation(0, translationY);
    } completion:^(BOOL finished) {
        // 动画执行完毕执行的代码
    }];
    
}

//解决办法,添加一个点击手势
- (void)addAGesutreRecognizerForYourView

{
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesturedDetected:)]; // 手势类型随你喜欢。
    
    tapGesture.delegate = self;
    
    [self.mTableView addGestureRecognizer:tapGesture];
    
}

- (void)tapGesturedDetected:(UITapGestureRecognizer *)recognizer

{
    [self.textFInput resignFirstResponder];
    [self.view endEditing:YES];
    // do something
    
}

#pragma mark - 弹出送礼物的视图
-(void)pushGiftView{
    YDLog(@"送礼物");
    //弹出遮盖视图
    [[[UIApplication sharedApplication]keyWindow] addSubview:self.coverView];
    
    //布局子视图
    CGFloat giftViewX = 0;
    CGFloat giftViewY = self.alertForGift.height - 164;
    CGFloat giftViewW = screenWidth;
    CGFloat giftViewH = 164;
    self.giftView = [[UIView alloc]initWithFrame:CGRectMake(giftViewX , giftViewY, giftViewW, giftViewH)];
    self.giftView.backgroundColor = [UIColor whiteColor];
    [self.coverView addSubview:self.giftView];
    
    //鲜花
    CGFloat frontX = 15;
    CGFloat cellHeight = 82;
    CGFloat btnW = self.giftView.width - frontX * 2;
    
    UIImage *flowerImg = [UIImage imageNamed:@"送花01.png"];
    CGFloat flowerW = flowerImg.size.width * 0.5;
    CGFloat flowerH = flowerImg.size.height * 0.5;
    CGFloat flowerY = (cellHeight - flowerH) * 0.5;
    UIImageView *flowerImgView = [[UIImageView alloc]initWithFrame:CGRectMake(frontX, flowerY, flowerW, flowerH)];
    [flowerImgView setImage:flowerImg];
    [self.giftView addSubview:flowerImgView];
    
    CGFloat lbFX = flowerImgView.x + flowerImgView.width + 55;
    CGFloat lbFY = 0;
    CGFloat lbFW = 100;
    UILabel *lbFlower = [[UILabel alloc]initWithFrame:CGRectMake(lbFX, lbFY, lbFW, cellHeight)];
    lbFlower.text = @"鲜花";
    [lbFlower setTextColor:[UIColor rgbFromHexString:@"#FF0053" alpaa:1.0]];
    [lbFlower setFont: [UIFont systemFontOfSize:15]];
    [self.giftView addSubview:lbFlower];
    
    UIImage *openImg = [UIImage imageNamed:@"送花展开.png"];
    UIImageView *openView = [[UIImageView alloc]initWithFrame:CGRectMake(self.giftView.width - frontX - openImg.size.width * 0.5, (cellHeight - openImg.size.height * 0.5) * 0.5, openImg.size.width * 0.5, openImg.size.height * 0.5)];
    [openView setImage:openImg];
    [self.giftView addSubview:openView];
    
    UIButton *btnFlower = [[UIButton alloc]initWithFrame:CGRectMake(frontX, 0, btnW, cellHeight)];
    btnFlower.backgroundColor = [UIColor clearColor];
    [btnFlower addTarget:self action:@selector(sendFlower) forControlEvents:UIControlEventTouchUpInside];
    [self.giftView addSubview:btnFlower];
    
    UIImageView *line = [[UIImageView alloc]initWithFrame:CGRectMake(frontX, cellHeight - 1, self.giftView.width - frontX * 2, 1)];
    [self.giftView addSubview:line];
    [line setBackgroundColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:0.12]];
    
    
    //优惠券
    
    
    UIImage *couponsImg = [UIImage imageNamed:@"优惠券.png"];
    CGFloat couponsW = couponsImg.size.width * 0.5;
    CGFloat couponsH = couponsImg.size.height * 0.5;
    CGFloat couponsY = (cellHeight - couponsH) * 0.5 + cellHeight;
    UIImageView *couponsImgView = [[UIImageView alloc]initWithFrame:CGRectMake(frontX, couponsY, couponsW, couponsH)];
    [couponsImgView setImage:couponsImg];
    [self.giftView addSubview:couponsImgView];
    
    CGFloat lbCouponsX = flowerImgView.x + flowerImgView.width + 55;
    CGFloat lbCouponsY = cellHeight;
    CGFloat lbCouponsW = 100;
    UILabel *lbCoupons = [[UILabel alloc]initWithFrame:CGRectMake(lbCouponsX, lbCouponsY, lbCouponsW, cellHeight)];
    lbCoupons.text = @"优惠券";
    [lbCoupons setTextColor:[UIColor rgbFromHexString:@"#FF0053" alpaa:1.0]];
    [lbCoupons setFont: [UIFont systemFontOfSize:15]];
    [self.giftView addSubview:lbCoupons];
    
    UIImage *openCouponsImg = [UIImage imageNamed:@"优惠券展开.png"];
    UIImageView *openCouponsView = [[UIImageView alloc]initWithFrame:CGRectMake(self.giftView.width - frontX - openCouponsImg.size.width * 0.5, (cellHeight - openCouponsImg.size.height * 0.5) * 0.5 + cellHeight, openCouponsImg.size.width * 0.5, openCouponsImg.size.height * 0.5)];
    [openCouponsView setImage:openCouponsImg];
    [self.giftView addSubview:openCouponsView];
    
    UIButton *btnCoupons = [[UIButton alloc]initWithFrame:CGRectMake(frontX, cellHeight, btnW, cellHeight)];
    btnCoupons.backgroundColor = [UIColor clearColor];
    [btnCoupons addTarget:self action:@selector(sendCoupons) forControlEvents:UIControlEventTouchUpInside];
    [self.giftView addSubview:btnCoupons];
    
    
}
-(void)pushSongView{
    YDLog(@"送歌");
    if ([ListenToMeDBManager getUuid] > 0) {
        MyWorksVC *myWorksVC = [[MyWorksVC alloc]init];
        myWorksVC.navigationItem.title = @"送歌";
        myWorksVC.isPushFromPerCenter = NO;
        myWorksVC.uuid = self.listenToMeData.stUserBaseInfoNet.uuid;
        myWorksVC.iOffset = 0;
        myWorksVC.iNum = 10;
        myWorksVC.lWatchUserId = 0;
        [self.navigationController pushViewController:myWorksVC animated:YES];
    }else{
    
        YDLog(@"还没有登录,请先登录,再送歌");
    }
   
    
}
-(void)pushBlackRoomView{
    
    if (self.listenToMeData.bMute == 0) {
        [[NetReqManager getInstance] sendSetBlackRoomState:self.listenToMeData.stUserBaseInfoNet.uuid LToUserid:self.friendUserBaseInfoNet.uuid BMute:1];
        YDLog(@"关进小黑屋");
    }else{
        [[NetReqManager getInstance] sendSetBlackRoomState:self.listenToMeData.stUserBaseInfoNet.uuid LToUserid:self.friendUserBaseInfoNet.uuid BMute:0];
        YDLog(@"放出小黑屋");
    }
}

#pragma mark - 弹出送花 送礼券界面
#pragma mark - 送花
-(void)sendFlower{
    YDLog(@"送多少花");
    [self.giftView removeFromSuperview];

    CGFloat sendFW = self.coverView.width - 30;
    CGFloat sendFH = 285;
    CGFloat sendFX = 15;
    CGFloat sendFY = 190;
    
    //设置临时数据
    self.sendFlowerNum = 0;
    self.haveFlowerNum = self.listenToMeData.stUserBaseInfoNet.iFlowerNum;
    
    self.sendFlowerView = [[GivingFlowerView alloc]init];
    self.sendFlowerView.frame = CGRectMake(sendFX, sendFY, sendFW, sendFH);
    self.sendFlowerView.receiveIcon = self.friendUserBaseInfoNet.sCovver;
    self.sendFlowerView.sendFlowerNum = self.sendFlowerNum;
//    [self.sendFlowerView addFlower:self.sendFlowerNum];
    [self.sendFlowerView.btnGiveFlower addTarget:self action:@selector(sendFlowerHandle) forControlEvents:UIControlEventTouchUpInside];
    [self.sendFlowerView.btnSendFNum setTitle:[NSString stringWithFormat:@"%d",self.sendFlowerNum] forState:UIControlStateNormal];

    //设置颜色
    [self.sendFlowerView setLbTextForDifferentColor:self.sendFlowerView.lbRemain text:[NSString stringWithFormat:@"剩余%ld朵",(self.haveFlowerNum - self.sendFlowerNum)] rangeString:[NSString stringWithFormat:@"%ld",(self.haveFlowerNum - self.sendFlowerNum)] color:[UIColor rgbFromHexString:@"#FF0053" alpaa:1.0]];
    //减一
    [self.sendFlowerView.btnSubFNum addTarget:self action:@selector(subSendNum) forControlEvents:UIControlEventTouchUpInside];
    //加一
    [self.sendFlowerView.btnAddFNum addTarget:self action:@selector(addSendNum) forControlEvents:UIControlEventTouchUpInside];
    [self.coverView addSubview:self.sendFlowerView];

    
}
#pragma mark - 送优惠券
-(void)sendCoupons{
    
    YDLog(@"送优惠券");
    [self.giftView removeFromSuperview];
    [self backToView:nil];
    MyGiftCertificateVC *myGiftCertificateVC = [[MyGiftCertificateVC alloc]init];
    //从其他界面进入我的礼券
    myGiftCertificateVC.isPushFromPerCenter = NO;
    [self.navigationController pushViewController:myGiftCertificateVC animated:YES];
    
    
}

#pragma mark - 关闭弹出的视图
-(void)backToView:button{
    [self.coverView removeFromSuperview];
    [self.sendFlowerView removeFromSuperview];
    
}

#pragma mark - 改变送花数
//减少
-(void)subSendNum{
    if (self.sendFlowerNum >= 1) {
        self.sendFlowerNum --;
        [self.sendFlowerView subFlower:self.sendFlowerNum];
        [self.sendFlowerView.btnSendFNum setTitle:[NSString stringWithFormat:@"%d",self.sendFlowerNum] forState:UIControlStateNormal];
      
        [self.sendFlowerView setLbTextForDifferentColor:self.sendFlowerView.lbRemain text:[NSString stringWithFormat:@"剩余%ld朵",(self.haveFlowerNum - self.sendFlowerNum)] rangeString:[NSString stringWithFormat:@"%ld",(self.haveFlowerNum - self.sendFlowerNum)] color:[UIColor rgbFromHexString:@"#FF0053" alpaa:1.0]];
    }
    
    
}
//增加
-(void)addSendNum{
    if (self.haveFlowerNum - self.sendFlowerNum >= 1) {
        self.sendFlowerNum ++;
        [self.sendFlowerView addFlower:self.sendFlowerNum];
        [self.sendFlowerView.btnSendFNum setTitle:[NSString stringWithFormat:@"%d",self.sendFlowerNum] forState:UIControlStateNormal];
        [self.sendFlowerView setLbTextForDifferentColor:self.sendFlowerView.lbRemain text:[NSString stringWithFormat:@"剩余%ld朵",(self.haveFlowerNum - self.sendFlowerNum)] rangeString:[NSString stringWithFormat:@"%ld",(self.haveFlowerNum - self.sendFlowerNum)] color:[UIColor rgbFromHexString:@"#FF0053" alpaa:1.0]];
    }
    
}


#pragma mark - sendFlowerHandle 确定送花
-(void)sendFlowerHandle{
    //发送送花请求
    if (self.sendFlowerNum >= 1) {
        [[NetReqManager getInstance] sendGetSendUserFlower:[ListenToMeDBManager getUuid] LUserId:self.friendUserBaseInfoNet.uuid IFlowerNum:self.sendFlowerNum];
        
    }
    [self.coverView removeFromSuperview];
//    [self.sendFlowerView.superview removeFromSuperview];
    [self.sendFlowerView removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
