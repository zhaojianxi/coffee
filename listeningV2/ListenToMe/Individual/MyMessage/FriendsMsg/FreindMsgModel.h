//
//  FreindMsgModel.h
//  ListenToMe
//
//  Created by yadong on 2/11/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * 消息类型--谁发的
 */
typedef enum
{
    FreindMsgModelTypeMe = 0,
    FreindMsgModelTypeOther = 1
}FreindMsgModelType;

@interface FreindMsgModel : NSObject

/**
 * 消息内容
 */
@property(nonatomic,copy) NSString *text;

/**
 * 时间
 */
@property(nonatomic,copy) NSString *time;

/**
 * 消息类型
 */
@property(assign,nonatomic) FreindMsgModelType type;

/**
 *  是否隐藏时间
 */
@property (nonatomic, assign) BOOL hiddenTime;

NJInitH(FreindMsgModel)

@end
