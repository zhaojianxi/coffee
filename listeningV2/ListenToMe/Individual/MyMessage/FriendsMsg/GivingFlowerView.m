//
//  GivingFlowerView.m
//  ListenToMe
//
//  Created by zhw on 15/4/24.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "GivingFlowerView.h"

@interface GivingFlowerView ()
@property(nonatomic,strong) UIButton *btnReceiveIcon;
@property(nonatomic,strong) UIImageView *showFlowerView;
@end

@implementation GivingFlowerView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(instancetype)init{
    self = [super init];
    if(self){
        [self setUI];
    }
    
    return self;
}

-(void)setUI{
//    self.backgroundColor = [UIColor whiteColor];
    self.width = screenWidth - 30;
    UIImageView *bgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.width, 285)];
    UIImage *bgImg = [UIImage imageNamed:@"bgImg.png"];
    [bgView setImage:bgImg];
    [self addSubview:bgView];
    
    CGFloat frontX = 15 * 0.5;
    //送花给 头像
    CGFloat cellHeight_1 = 64;
    CGFloat lbSendX = frontX;
    CGFloat lbSendY = 0;
    CGFloat lbSendW = (screenWidth - 30) - frontX * 2 - 40;
    CGFloat lbSendH = cellHeight_1;
    
    UILabel *lbSend = [[UILabel alloc]initWithFrame:CGRectMake(lbSendX, lbSendY, lbSendW, lbSendH)];
    [lbSend setText:@"送花给"];
    [lbSend setFont:[UIFont systemFontOfSize:15.0]];
    [lbSend setTextColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
    [self addSubview:lbSend];
    
    CGFloat btnReceiveWH = 40;
    CGFloat btnReceiveX = (screenWidth - 30) - frontX - btnReceiveWH;
    CGFloat btnReceiveY = (cellHeight_1 - btnReceiveWH) * 0.5;
    self.btnReceiveIcon = [[UIButton alloc]initWithFrame:CGRectMake(btnReceiveX, btnReceiveY, btnReceiveWH, btnReceiveWH)];
    [self addSubview:self.btnReceiveIcon];
    
    //分割线
    CGFloat seplineW = (screenWidth - 30) - frontX * 2;
    UIImageView *sepLine_1 = [[UIImageView alloc] initWithFrame:CGRectMake(frontX, cellHeight_1 - 1, seplineW, 1)];
    sepLine_1.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:0.12];
    [self addSubview:sepLine_1];
    
    //剩余鲜花
    CGFloat cellHeight_2 = 49;
    CGFloat lbRemainW = 80;
    CGFloat lbRemainH = cellHeight_2;
    CGFloat lbRemainX = self.width - lbRemainW - frontX;
    CGFloat lbRemianY = sepLine_1.y + sepLine_1.height;
    self.lbRemain = [[UILabel alloc]initWithFrame:CGRectMake(lbRemainX, lbRemianY, lbRemainW, lbRemainH)];
    [self.lbRemain setTextColor:[UIColor lightGrayColor]];
    [self.lbRemain setFont:[UIFont systemFontOfSize:15.0]];
    [self.lbRemain setTextAlignment:NSTextAlignmentRight];
    
//    //设置text中不同字体的颜色
//    [self setLbTextForDifferentColor:self.lbRemain text:self.lbRemain.text rangeString:[NSString stringWithFormat:@"%ld",self.sendFlowerNum] color:[UIColor rgbFromHexString:@"#FF0053" alpaa:1.0]];
    
    [self addSubview:self.lbRemain];
    
    self.showFlowerView = [[UIImageView alloc]initWithFrame:CGRectMake(frontX, lbRemianY, self.width - self.lbRemain.width, cellHeight_2)];
//    self.showFlowerView.backgroundColor = [UIColor grayColor];
    [self addSubview:self.showFlowerView];
    
    //分割线
    UIImageView *sepLine_2 = [[UIImageView alloc] initWithFrame:CGRectMake(frontX, sepLine_1.y + cellHeight_2, seplineW, 1)];
    sepLine_2.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:0.12];
    [self addSubview:sepLine_2];
    CGFloat cellHeight_3 = 84;
    
    //控制送花数
    UIImage *sendFlowerBgImg = [UIImage imageNamed:@"flowerNumBg.png"];
    CGFloat sendFlowerW = sendFlowerBgImg.size.width * 0.5;
    CGFloat sendFlowerH = sendFlowerBgImg.size.height * 0.5;
    CGFloat sendFlowerX = (screenWidth - 30 - sendFlowerW) * 0.5;
    CGFloat sendFlowerY = sepLine_2.y + sepLine_2.height + 25;
    UIImageView *sendFlowerBgView = [[UIImageView alloc]initWithFrame:CGRectMake(sendFlowerX, sendFlowerY, sendFlowerW, sendFlowerH)];
    [sendFlowerBgView setImage:sendFlowerBgImg];
    sendFlowerBgView.userInteractionEnabled = YES;
    [self addSubview:sendFlowerBgView];
    
    //鲜花加一 减一 送花数
    
    CGFloat add_subWH = sendFlowerBgView.height;
    CGFloat sendFW = sendFlowerBgView.width - add_subWH * 2;
    
    //减
    self.btnSubFNum = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, add_subWH, add_subWH)];
    
    [sendFlowerBgView addSubview:self.btnSubFNum];
    
    UIImage *subImg = [UIImage imageNamed:@"sub.png"];
    CGFloat subViewW = subImg.size.width * 0.5;
    CGFloat subViewH = subImg.size.height * 0.5;
    CGFloat subViewX = (self.btnSubFNum.width - subImg.size.width * 0.5) * 0.5;
    CGFloat subViewY = (self.btnSubFNum.height - subImg.size.height * 0.5) * 0.5;
    UIImageView *subView = [[UIImageView alloc]initWithFrame:CGRectMake(subViewX, subViewY, subViewW, subViewH)];
    subView.image = subImg;
    subView.userInteractionEnabled = YES;
    [self.btnSubFNum addSubview:subView];
    
    //加
    self.btnAddFNum = [[UIButton alloc]initWithFrame:CGRectMake(sendFlowerBgView.width - add_subWH, 0, add_subWH, add_subWH)];
    
    [sendFlowerBgView addSubview:self.btnAddFNum];
    
    UIImage *addImg = [UIImage imageNamed:@"add.png"];
    CGFloat addViewX = (self.btnAddFNum.width - addImg.size.width * 0.5) * 0.5;
    CGFloat addViewY = (self.btnAddFNum.height - addImg.size.height * 0.5) * 0.5;
    CGFloat addViewW = addImg.size.width * 0.5;
    CGFloat addViewH = addImg.size.height * 0.5;
    UIImageView *addView = [[UIImageView alloc]initWithFrame:CGRectMake(addViewX, addViewY,addViewW, addViewH)];
    addView.image = addImg;
    addView.userInteractionEnabled = YES;
    [self.btnAddFNum addSubview:addView];
    
    //当前送出数量
    self.btnSendFNum = [[UIButton alloc]initWithFrame:CGRectMake(add_subWH, 0, sendFW, add_subWH)];
    [self.btnSendFNum setTitleColor:[UIColor rgbFromHexString:@"#FF0053" alpaa:1.0] forState:UIControlStateNormal];
    [self.btnSendFNum.titleLabel setFont:[UIFont systemFontOfSize:15.0]];
    [sendFlowerBgView addSubview:self.btnSendFNum];
    
    //竖线
    UIImage *lineImg = [UIImage imageNamed:@"separateLine.png"];
    for (int index = 0; index < 2; index ++) {
        UIImageView *lineImgView = [[UIImageView alloc]initWithFrame:CGRectMake(add_subWH - 1 + sendFW * index  , 0, 1, add_subWH)];
        [lineImgView setImage:lineImg];
        [sendFlowerBgView addSubview:lineImgView];
    }
    
    //分割线
    UIImageView *sepLine_3 = [[UIImageView alloc] initWithFrame:CGRectMake(frontX, sepLine_2.y + cellHeight_3, seplineW, 1)];
    sepLine_3.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:0.12];
    [self addSubview:sepLine_3];
    
    //每日登录赠花提示
    CGFloat cellHeight_4 = 44;
    
    UILabel *lbTip = [[UILabel alloc]initWithFrame:CGRectMake(frontX, sepLine_3.y + sepLine_3.height , self.width, cellHeight_4)];
//    [lbTip setText:@"每日登录赠送5朵鲜花"];
    NSString *tipString = @"每日登录赠送5朵鲜花";
    [lbTip setTextColor:[UIColor lightGrayColor]];
    [lbTip setTextAlignment:NSTextAlignmentCenter];
    [lbTip setFont:[UIFont systemFontOfSize:15.0]];
    [self setLbTextForDifferentColor:lbTip text:tipString rangeString:@"5" color:[UIColor rgbFromHexString:@"#FF0053" alpaa:1.0]];
    [self addSubview:lbTip];
    
    //分割线
    UIImageView *sepLine_4 = [[UIImageView alloc] initWithFrame:CGRectMake(0, sepLine_3.y + cellHeight_4, seplineW + frontX * 2, 1)];
    sepLine_4.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:0.12];
    [self addSubview:sepLine_4];
    
    CGFloat cellHeight_5 = 44;
    CGFloat btn_W = self.width * 0.5;
    
    //取消
    self.btnCancle = [[UIButton alloc]initWithFrame:CGRectMake(0, sepLine_4.y + sepLine_4.height, btn_W, cellHeight_5)];
    [self.btnCancle setTitle:@"取消" forState:UIControlStateNormal];
    [self.btnCancle setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.btnCancle setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateHighlighted];
    [self.btnCancle.titleLabel setFont:[UIFont systemFontOfSize:15.0]];
    [self.btnCancle addTarget:self action:@selector(cancleHandle) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.btnCancle];
    
    //确定
    self.btnGiveFlower = [[UIButton alloc]initWithFrame:CGRectMake(btn_W, sepLine_4.y + sepLine_4.height, btn_W, cellHeight_5)];
    [self.btnGiveFlower setTitle:@"确定" forState:UIControlStateNormal];
    [self.btnGiveFlower setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateNormal];
    [self.btnGiveFlower.titleLabel setFont:[UIFont systemFontOfSize:15.0]];
//    [self.btnGiveFlower addTarget:self action:@selector(sendFlowerHandle) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.btnGiveFlower];
    
    //中间的竖线
    UIImageView * tempLine = [[UIImageView alloc]initWithFrame:CGRectMake(btn_W - 1, sepLine_4.y + sepLine_4.height, 1, cellHeight_5)];
    tempLine.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:0.5];
    [self addSubview:tempLine];
    
    
}

#pragma mark - cancleHandle 取消送花
-(void)cancleHandle{
    //先将父视图移除,再移除本视图(否则本视图会在下次触发点击时自动弹出)
    [self.superview removeFromSuperview];
    [self removeFromSuperview];
    
}



#pragma mark 设置接收人的头像
-(void)setReceiveIcon:(NSString *)receiveIcon{
//    [self.btnReceiveIcon setImage:[UIImage imageNamed:receiveIcon] forState:UIControlStateNormal];
    UIImageView *receiveIconView = [[UIImageView alloc]initWithFrame:self.btnReceiveIcon.bounds];
    [receiveIconView sd_setImageWithURL:[NSURL URLWithString:receiveIcon] placeholderImage:[UIImage imageNamed:@"icon.png"]];
    receiveIconView.userInteractionEnabled = YES;
    [self.btnReceiveIcon addSubview:receiveIconView];
}

#pragma mark - 初始化送花的数量
-(void)setSendFlowerNum:(NSInteger)sendFlowerNum{


    if (sendFlowerNum > 0) {
        UIImage *flowerImg = [UIImage imageNamed:@"送花01.png"];
        CGFloat flowerW = flowerImg.size.width * 0.5;
        CGFloat flowerH = flowerImg.size.height * 0.5;
        CGFloat flowerY = (self.showFlowerView.height - flowerH) * 0.5;
        for (int index = 0;index < sendFlowerNum ; index ++) {
            UIImageView *tempView = [[UIImageView alloc]initWithFrame:CGRectMake(flowerW * index, flowerY, flowerW, flowerH)];
            [tempView setImage:flowerImg];
            [self.showFlowerView addSubview:tempView];
        }
    }
    
}

#pragma mark - 增加送花数
-(void)addFlower:(NSInteger)flowerNum{
    if (flowerNum >=1) {
        
        UIImage *flowerImg = [UIImage imageNamed:@"送花01.png"];
        CGFloat flowerW = flowerImg.size.width * 0.5;
        CGFloat flowerH = flowerImg.size.height * 0.5;
        CGFloat flowerY = (self.showFlowerView.height - flowerH) * 0.5;
        
        if (flowerNum * flowerW < self.showFlowerView.width) {
            UIImageView *tempView = [[UIImageView alloc]initWithFrame:CGRectMake(flowerW * (flowerNum -1), flowerY, flowerW, flowerH)];
            [tempView setImage:flowerImg];
            [self.showFlowerView addSubview:tempView];
        }
        
    }
   
    
}
#pragma mark - 减少送花数
-(void)subFlower:(NSInteger)flowerNum{
    if (self.showFlowerView.subviews.count >= 1) {
        UIImage *flowerImg = [UIImage imageNamed:@"送花01.png"];
        CGFloat flowerW = flowerImg.size.width * 0.5;
//        CGFloat flowerH = flowerImg.size.height * 0.5;
//        CGFloat flowerY = (self.showFlowerView.height - flowerH) * 0.5;
        if ((flowerNum + 1) * flowerW < self.showFlowerView.width) {
            UIImageView *tempView = [self.showFlowerView.subviews lastObject];
            [tempView removeFromSuperview];
        }
        
    }
    
}

#pragma mark - 设置label中显示不同字体的颜色
-(void)setLbTextForDifferentColor:(UILabel *)label text:(NSString *)text rangeString:(NSString *)rangeString color:(UIColor *)color{
    NSMutableAttributedString *noteStr = [[NSMutableAttributedString alloc]initWithString:text];
    NSRange colorRange = NSMakeRange([[noteStr string] rangeOfString:rangeString].location, [[noteStr string]rangeOfString:rangeString].length);
    [noteStr addAttribute:NSForegroundColorAttributeName value:color range:colorRange];
    
    [label setAttributedText:noteStr];
//    [label sizeToFit];
    
    
}
@end
