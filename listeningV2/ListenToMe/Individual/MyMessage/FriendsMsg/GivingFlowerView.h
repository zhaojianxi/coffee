//
//  GivingFlowerView.h
//  ListenToMe
//
//  Created by zhw on 15/4/24.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GivingFlowerView : UIView
/**
 *  收花人的名字
 */
@property(nonatomic,strong) NSString *receiveName;
/**
 *  收花人的头像
 */
@property(nonatomic,strong) NSString *receiveIcon;
/**
 *  剩余鲜花数
 */
@property(nonatomic,strong) UILabel *lbRemain;
/**
 *  鲜花数减一
 */
@property(nonatomic,strong) UIButton *btnSubFNum;
/**
 *  鲜花数加一
 */
@property(nonatomic,strong) UIButton *btnAddFNum;
/**
 *  显示送花的数量
 */
@property(nonatomic,strong) UIButton *btnSendFNum;
/**
 *  取消赠送
 */
@property(nonatomic,strong) UIButton *btnCancle;
/**
 *  赠送鲜花
 */
@property(nonatomic,strong) UIButton *btnGiveFlower;
/**
 *  显示多少张花的图片
 */
@property(nonatomic,assign) NSInteger sendFlowerNum;

-(void)addFlower:(NSInteger)flowerNum;

-(void)subFlower:(NSInteger)flowerNum;

-(void)setLbTextForDifferentColor:(UILabel *)label text:(NSString *)text rangeString:(NSString *)rangeString color:(UIColor *)color;
@end
