//
//  FriendsMsgVC.h
//  ListenToMe
//
//  Created by zhw on 2/7/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendsMsgVC : YDBaseVC
/**
 *  私聊的用户信息
 */
@property(nonatomic,strong)UserBaseInfoNet *friendUserBaseInfoNet;
@end
