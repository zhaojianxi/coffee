//
//  FriendMsgCell.h
//  ListenToMe
//
//  Created by yadong on 2/11/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>
@class FreindMsgModel,FreindMsgFrameModel;

@interface FriendMsgCell : UITableViewCell

+(instancetype)cellWithTableView:(UITableView *)tableView;

@property(strong,nonatomic) FreindMsgFrameModel *messageFrame;
/**
 *  聊天对象的头像地址
 */
@property(nonatomic,strong) NSString *thePerosnCover;
@end
