//
//  FriendMsgCell.m
//  ListenToMe
//
//  Created by yadong on 2/11/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "FriendMsgCell.h"
#import "FreindMsgFrameModel.h"
#import "FreindMsgModel.h"
#import "UIImage+Extension.h"

@interface FriendMsgCell ()
/**
 * 时间
 */
@property(strong,nonatomic) UILabel *lbTime;

/**
 * 内容
 */
@property(strong,nonatomic) UIButton *btnContent;

/**
 * 头像
 */
@property(strong,nonatomic) UIImageView *imgAvatar;
@end

@implementation FriendMsgCell

+(instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *identifier = @"message";
    FriendMsgCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[FriendMsgCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    return cell;
}


/**
 * 重写init方法，让类一创建出来就拥有某些属性
 */
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{

    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        // 添加将来可能会用到的子控件
        // 1.添加-时间label
        UILabel *timeLabel = [[UILabel alloc]init];
        timeLabel.font = [UIFont systemFontOfSize:13.0];
        timeLabel.textAlignment = NSTextAlignmentCenter;
        timeLabel.textColor = [UIColor grayColor];
        [self.contentView addSubview:timeLabel]; // 把timeLable添加到cell的内容视图 self.contentView
        _lbTime = timeLabel;
        
        // 2.添加-消息内容
        UIButton *contentBtn = [[UIButton alloc]init];
        contentBtn.backgroundColor = [UIColor clearColor];
        contentBtn.titleLabel.font = NJTextFont;
        [contentBtn setTitleColor:[UIColor purpleColor] forState:UIControlStateNormal];
        contentBtn.titleLabel.numberOfLines = 0;
        [self.contentView addSubview:contentBtn];
        _btnContent = contentBtn;
        
        // 3.添加头像
        UIImageView *avatarImg = [[UIImageView alloc]init];
        [self.contentView addSubview:avatarImg];
        _imgAvatar = avatarImg;
        
        // 4.清空cell的背景色
        self.backgroundColor = [UIColor clearColor];
        
        // 5.设置按钮的内边框
        _btnContent.contentEdgeInsets = UIEdgeInsetsMake(NJEdgeInsetsWidth, NJEdgeInsetsWidth, NJEdgeInsetsWidth, NJEdgeInsetsWidth);
    
    }
    
    return self;
}


/**
 *  FreindMsgFrameModel的属性 messageFrame，使其一加载就做某些操作
 */
-(void)setMessageFrame:(FreindMsgFrameModel *)messageFrame
{
    _messageFrame = messageFrame;
    
    // 0.获取数据模型---一初始化msg的frame时，就会初始化msgmodel的对象
    FreindMsgModel *msgModel = _messageFrame.message;
    
    // 1.设置时间
    self.lbTime.text = msgModel.time;
    self.lbTime.frame = _messageFrame.timeF;
    
    // 2.设置头像
    if (FreindMsgModelTypeMe == msgModel.type) {

        //自己发的
//        self.imgAvatar.image = [UIImage imageNamed:@"me"];
        [self.imgAvatar sd_setImageWithURL:[NSURL URLWithString:[ListenToMeData getInstance].stUserBaseInfoNet.sCovver] placeholderImage:[UIImage imageNamed:@"icon.png"]];
    }else{
//        self.imgAvatar.image = [UIImage imageNamed:@"other"];
        [self.imgAvatar sd_setImageWithURL:[NSURL URLWithString:self.thePerosnCover] placeholderImage:[UIImage imageNamed:@"icon.png"]];
    }
    
    self.imgAvatar.frame = _messageFrame.iconF;
    self.imgAvatar.layer.masksToBounds = YES;
    self.imgAvatar.layer.cornerRadius = self.imgAvatar.width * 0.5;
    
    // 3.设置正文
    [self.btnContent setTitle:msgModel.text forState:UIControlStateNormal];
    self.btnContent.frame = _messageFrame.textF;
    
    // 4.设置背景图片
    UIImage *newImg = nil;
    if (FreindMsgModelTypeMe == msgModel.type) {
        // 自己发的
        newImg = [UIImage resizableImageWithName:@"chat_send_nor"];
    } else{
        // 别人发的
        newImg = [UIImage resizableImageWithName:@"chat_recive_nor"];
    }
    
    [self.btnContent setBackgroundImage:newImg forState:UIControlStateNormal];
}

#pragma mark - 其他
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
