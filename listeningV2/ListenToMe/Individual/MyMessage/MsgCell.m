//
//  MsgCell.m
//  ListenToMe
//
//  Created by yadong on 2/6/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "MsgCell.h"

@implementation MsgCell
@synthesize badgeBtn;
@synthesize btnMsgAvatar;
@synthesize lbTalkWith;
@synthesize lbLastMsg;
@synthesize lbLastTime;
@synthesize lineView;

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        CGFloat btnMsgAvatarW = 35;
        CGFloat btnMsgAvatarH = 35;
        CGFloat btnMsgAvatarX = 10;
        CGFloat btnMsgAvatarY = (60 - btnMsgAvatarH) * 0.5;
        btnMsgAvatar = [[YDCircleAvatarBtn alloc]initWithFrame:CGRectMake(btnMsgAvatarX, btnMsgAvatarY, btnMsgAvatarW, btnMsgAvatarH)];
        [self.contentView addSubview:btnMsgAvatar];
        btnMsgAvatar.backgroundColor = [UIColor purpleColor];
        
        CGFloat badgeBtnW = 15;
        CGFloat badgeBtnH = 15;
        CGFloat badgeBtnX = btnMsgAvatarX + btnMsgAvatarW * 0.65;
        CGFloat badgeBtnY = btnMsgAvatarY - 0.5 * badgeBtnH;
        badgeBtn = [[YDBadgeBtn alloc]initWithFrame:CGRectMake(badgeBtnX, badgeBtnY, badgeBtnW, badgeBtnH)];
        [self.contentView addSubview:badgeBtn];
//        badgeBtn.badgeValue = 5;
        badgeBtn.backgroundColor = [UIColor rgbFromHexString:@"#FF0053" alpaa:1.0];
        
        CGFloat lbTalkWithW = screenWidth * 0.6;
        CGFloat lbTalkWithH = 20;
        CGFloat lbTalkWithX = btnMsgAvatarX + btnMsgAvatarW + 5;
        CGFloat lbTalkWithY = 5;
        lbTalkWith = [[UILabel alloc]initWithFrame:CGRectMake(lbTalkWithX, lbTalkWithY, lbTalkWithW, lbTalkWithH)];
        [lbTalkWith setFont:[UIFont systemFontOfSize:15.0]];
//        [lbTalkWith setTextColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0]];
        [lbTalkWith setTextAlignment:NSTextAlignmentLeft];
        [self.contentView addSubview:lbTalkWith];
//        lbTalkWith.backgroundColor = [UIColor grayColor];
        
        lbLastMsg = [[UILabel alloc]initWithFrame:CGRectMake(lbTalkWithX, lbTalkWithY + lbTalkWithH + 2, lbTalkWithW, lbTalkWithH)];
        [lbLastMsg setFont:[UIFont systemFontOfSize:12.0]];
        [lbLastMsg setTextColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0]];
        [lbLastMsg setTextAlignment:NSTextAlignmentLeft];
        [self.contentView addSubview:lbLastMsg];
//        lbLastMsg.backgroundColor = [UIColor blueColor];
        
        lbLastTime = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - 50 - 15 , 5, 50, lbTalkWithH)];
        [lbLastTime setFont:[UIFont systemFontOfSize:10.0]];
        [lbLastTime setTextColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0]];
        [lbLastTime setTextAlignment:NSTextAlignmentRight];
        [self.contentView addSubview:lbLastTime];
//        lbLastTime.backgroundColor = [UIColor orangeColor];
//        lbLastTime.text = @"02-05";
        
        lineView = [[UIImageView alloc]initWithFrame:CGRectMake(btnMsgAvatarX, 59, screenWidth - btnMsgAvatarX * 2, 1)];
        lineView.backgroundColor = [UIColor lightGrayColor];
        lineView.alpha = 0.5;
        
        [self.contentView addSubview:lineView];
        
        [self setUI];

    }
    
    return self;
}



-(void)setUI{
    
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}


//
//-(void)layoutSubviews
//{
////    CGFloat btnMsgAvatarW = 35;
////    CGFloat btnMsgAvatarH = 35;
////    CGFloat btnMsgAvatarX = 10;
////    CGFloat btnMsgAvatarY = 0;
////    [btnMsgAvatar setFrame:CGRectMake(btnMsgAvatarX, btnMsgAvatarY, btnMsgAvatarW, btnMsgAvatarH)];
////
////    CGFloat badgeBtnW = 15;
////    CGFloat badgeBtnH = 15;
////    CGFloat badgeBtnX =btnMsgAvatarW * 0.65;
////    CGFloat badgeBtnY = btnMsgAvatarY - badgeBtnW;
////    [badgeBtn setFrame:CGRectMake(badgeBtnX, badgeBtnY, badgeBtnW, badgeBtnH)];
////    
////    CGFloat lbTalkWithW = screenWidth * 0.6;
////    CGFloat lbTalkWithH = 20;
////    CGFloat lbTalkWithX = btnMsgAvatarX + btnMsgAvatarW;
////    CGFloat lbTalkWithY = 5;
////    [lbTalkWith setFrame:CGRectMake(lbTalkWithX, lbTalkWithY, lbTalkWithW, lbTalkWithH)];
////    
////    [lbLastMsg  setFrame:CGRectMake(lbTalkWithX, lbTalkWithY + lbTalkWithH + 2, lbTalkWithW, lbTalkWithH)];
////    
////    [lbLastTime setFrame:CGRectMake(lbTalkWithX + lbTalkWithW + 2, 5, lbTalkWithY, lbTalkWithH)];
//    
//}




@end
