//
//  OfficalMsgCell.h
//  ListenToMe
//
//  Created by yadong on 2/6/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OfficalMsgCell : UITableViewCell
@property(strong,nonatomic) UIButton *btnMsgType;
@property(strong,nonatomic) UILabel *lbTime;
@property(strong,nonatomic) UILabel *lbContents;

/**
 *  官方消息类型图标
 */
@property(nonatomic,strong)UIImageView *officeMessageView;
/**
 *  官方消息标题
 */
@property(nonatomic,strong)UILabel *lbOfficeMessageTitle;
/**
 *  官方消息发送的时间
 */
@property(nonatomic,strong)UILabel *lbOfficeMessageTime;
/**
 *  官方消息内容
 */
@property(nonatomic,strong)UILabel *lbOfficeMessageContent;
@end
