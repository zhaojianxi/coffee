//
//  OfficalMsgCell.m
//  ListenToMe
//
//  Created by yadong on 2/6/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "OfficalMsgCell.h"

@interface OfficalMsgCell ()
@property(strong,nonatomic) UIImageView *imgHorizLine;

@end

@implementation OfficalMsgCell
@synthesize imgHorizLine;
@synthesize btnMsgType;
@synthesize lbTime;
@synthesize lbContents;

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
//        CGFloat btnMsgTypeW = screenWidth * 0.7;
//        CGFloat btnMsgTypeH = 40;
//        CGFloat btnMsgTypeX = 0;
//        CGFloat btnMsgTypeY = 0;
//        btnMsgType = [[UIButton alloc]initWithFrame:CGRectMake(btnMsgTypeX, btnMsgTypeY, btnMsgTypeW, btnMsgTypeH)];
//        [self addSubview:btnMsgType];
//        
//        btnMsgType.backgroundColor = [UIColor orangeColor];
//        
//        
//        CGFloat lbTimeW = screenWidth * 0.3;
//        CGFloat lbTimeH = 40;
//        CGFloat lbTimeX = btnMsgTypeW;
//        CGFloat lbTimeY = 0;
//        lbTime = [[UILabel alloc]initWithFrame:CGRectMake(lbTimeX, lbTimeY, lbTimeW, lbTimeH)];
//        [self addSubview:lbTime];
//        
//        lbTime.backgroundColor = [UIColor blueColor];
//        
//        CGFloat imgHorizLineW = screenWidth * 0.7;
//        CGFloat imgHorizLineH = 1;
//        CGFloat imgHorizLineX = 35;
//        CGFloat imgHorizLineY = lbTimeY + lbTimeH;
//        imgHorizLine = [[UIImageView alloc]initWithFrame:CGRectMake(imgHorizLineX, imgHorizLineY, imgHorizLineW, imgHorizLineH)];
//        [self addSubview:imgHorizLine];
//        
//        imgHorizLine.backgroundColor = [UIColor redColor];
//        
//        CGFloat lbContentsW = screenWidth * 0.8;
//        CGFloat lbContentsH = 80;
//        CGFloat lbContentsX = screenWidth * 0.1;
//        CGFloat lbContentsY = imgHorizLineY + imgHorizLineH;
//        lbContents = [[UILabel alloc]initWithFrame:CGRectMake(lbContentsX, lbContentsY, lbContentsW, lbContentsH)];
//        [self addSubview:lbContents];
//        
//        lbContents.backgroundColor = [UIColor yellowColor];
//        lbContents.numberOfLines = 0;
//        lbContents.lineBreakMode = NSLineBreakByCharWrapping;
//        lbContents.textAlignment = NSTextAlignmentLeft;
//        lbContents.text = @"肖亚东---肖亚东---肖亚东---肖亚东";
        
        
        [self setUI];
    }
    
    return self;
}

-(void)setUI{

    CGFloat frontX = 15;
    CGFloat lineH = 1;
    //
    
//    UIImage *favourable = [UIImage imageNamed:@"优惠活动.png"];
    self.officeMessageView = [[UIImageView alloc]initWithFrame:CGRectMake(frontX,13, 15, 13)];
//    self.officeMessageView.image = favourable;
    [self.contentView addSubview:self.officeMessageView];
    
    self.lbOfficeMessageTime = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - frontX - 60, self.officeMessageView.centerY - 5, 60, 10)];
    [self.lbOfficeMessageTime setTextColor:[UIColor lightGrayColor]];
    [self.lbOfficeMessageTime setFont:[UIFont systemFontOfSize:10]];
    [self.lbOfficeMessageTime setTextAlignment:NSTextAlignmentRight];
    [self.contentView addSubview:self.lbOfficeMessageTime];
    
    self.lbOfficeMessageTitle = [[UILabel alloc]initWithFrame:CGRectMake(self.officeMessageView.x + self.officeMessageView.width, self.officeMessageView.y, screenWidth - frontX * 2 - self.officeMessageView.width - self.lbOfficeMessageTime.width, self.officeMessageView.height)];
    [self.lbOfficeMessageTitle setTextColor:[UIColor rgbFromHexString:@"#FF0053" alpaa:1.0]];
    [self.lbOfficeMessageTitle setFont:[UIFont systemFontOfSize:15.0]];
    [self.lbOfficeMessageTitle setTextAlignment:NSTextAlignmentLeft];
    [self.contentView addSubview:self.lbOfficeMessageTitle];
    
    UIImageView *separatorLine01=[[UIImageView alloc]initWithFrame:CGRectMake(frontX + self.officeMessageView.width, self.officeMessageView.y + self.officeMessageView.height * 2 , screenWidth - frontX * 2 - self.officeMessageView.width, lineH)];
    separatorLine01.backgroundColor = [UIColor rgbFromHexString:@"#FF0053" alpaa:.12];
    [self.contentView addSubview:separatorLine01];
    
    self.lbOfficeMessageContent = [[UILabel alloc]initWithFrame:CGRectMake(self.lbOfficeMessageTitle.x, separatorLine01.y + lineH, separatorLine01.width, 121 - separatorLine01.y - lineH)];
    [self.lbOfficeMessageContent setTextColor:[UIColor blackColor]];
    [self.lbOfficeMessageContent setTextAlignment:NSTextAlignmentLeft];
    [self.lbOfficeMessageContent setFont:[UIFont systemFontOfSize:14.0]];
    self.lbOfficeMessageContent.numberOfLines = 0;
    
    [self.contentView addSubview:self.lbOfficeMessageContent];
    
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end


//@property(strong,nonatomic) UIButton *btnMsgType;
//@property(strong,nonatomic) UILabel *lbTime;
//@property(strong,nonatomic) UILabel *lbContents;