//
//  SettingsVC.m
//  ListenToMe
//
//  Created by yadong on 2/5/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "SettingsVC.h"
#import "OpinionFeedbackVC.h"
@interface SettingsVC ()<UITableViewDataSource,UITableViewDelegate>
@property(strong,nonatomic) UITableView *mTableView;
@property(strong,nonatomic) UIButton *btSignOut;
@property(strong,nonatomic) UISwitch *SwPrivacy;
@end

@implementation SettingsVC
@synthesize mTableView;
@synthesize btSignOut;
@synthesize SwPrivacy;

#pragma mark -生命周期
-(void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setNavigation];
    
    [self setUI];
}

-(void)setNavigation{
    self.navigationItem.title = @"设置";
    [self setLeftBtnItem];
    [self setRightBtnItem];

}


-(void)setLeftBtnItem{

    UIButton *leftBtn = [[UIButton alloc]init];
    UIImage *leftBtnImg = [UIImage imageNamed:@"whiteBack.png"];
    leftBtn.frame = CGRectMake(0, 0, leftBtnImg.size.width, leftBtnImg.size.height);
    [leftBtn setImage:leftBtnImg forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(clickLeftBarBtnItem:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customLeftBtnItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = customLeftBtnItem;
}

-(void)setRightBtnItem{

    UIButton *rightBtn = [[UIButton alloc]init];
    UIImage *imgMore = [UIImage imageNamed:@"more.png"];
    rightBtn.frame = CGRectMake(0, 0, imgMore.size.width, imgMore.size.height);
    [rightBtn setImage:imgMore forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(clickRightBarBtnItem:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customRightBtnItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = customRightBtnItem;
}

-(void)clickLeftBarBtnItem:(UIButton *)leftItem{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)clickRightBarBtnItem:(UIButton *)rightItem{
    YDLog(@"点击了跟多菜单");
}
#pragma mark -UI
-(void)setUI
{
    
    
    // mTableView
    CGFloat mTableViewW = screenWidth;
    CGFloat mTableViewH = screenHeight * 0.85;
    CGFloat mTableViewX = 0;
    CGFloat mTableViewY = -20;
    mTableView = [[UITableView alloc]initWithFrame:CGRectMake(mTableViewX, mTableViewY, mTableViewW, mTableViewH) style:UITableViewStyleGrouped];
    [self.view addSubview:mTableView];
    
    mTableView.backgroundColor = [UIColor clearColor];
    mTableView.scrollEnabled = NO;
    
    mTableView.delegate = self;
    mTableView.dataSource = self;
    
    
    // btSignOut
    btSignOut = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth * 0.1, mTableViewY + mTableViewH + 30, screenWidth * 0.8, 40)];
    [self.view addSubview:btSignOut];
    
    [btSignOut addTarget:self action:@selector(clickBtSigOut) forControlEvents:UIControlEventTouchUpInside];
    [btSignOut setTitle:@"退出登录" forState:UIControlStateNormal];
    [btSignOut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btSignOut setBackgroundColor:[UIColor redColor]];
    
    btSignOut.layer.cornerRadius = 8;
    btSignOut.layer.masksToBounds = YES;
   
    
    
}


#pragma mark - delegate & datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"settings";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }

    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    switch (indexPath.section) {
            // 0-section
        case 0:{
            if (indexPath.row == 0) {
                cell.textLabel.text = @"私聊";
                
//                SwPrivacy = [[UISwitch alloc]init];
//                SwPrivacy.onTintColor = [UIColor purpleColor];
//                cell.accessoryView = SwPrivacy;
            }else {
                cell.textLabel.text = @"清空缓存";
            }
            break;
        }
            // 0-section
        case 1:{
            
            //使用HTML5页面
            if (indexPath.row == 0) {
                cell.textLabel.text = @"意见反馈";
            }else if (indexPath.row == 1){
                cell.textLabel.text = @"帮助中心";
            }else if(indexPath.row == 2){
                cell.textLabel.text = @"免责声明与版权声明";
            }
            break;
        }
        // 0-section
        case 2:{
            if (indexPath.row == 0) {
                cell.textLabel.text = @"关于听我的";
            }else {
                cell.textLabel.text = @"听我的团队介绍";
            }
            
            break;
        }
            
        default:
            break;
    }
    

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell ;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:{
            return 2;
            break;
        }case 1:{
            return 3;
            break;
        }case 2:{
            return 2;
            break;
        }
            
        default:
            return 0;
            break;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
            // 0-section
        case 0:{
            if (indexPath.row == 0) {
                
                if (SwPrivacy.isOn) {
                    [SwPrivacy setOn:NO];
                    YDLog(@"私聊关--");
                }else{
                    [SwPrivacy setOn:YES];
                    YDLog(@"私聊开--");
                }
                
            }else {
                YDLog(@"清空缓存");
            }
            break;
        }
            // 0-section
        case 1:{
            if (indexPath.row == 0) {
                YDLog(@"意见反馈");
                OpinionFeedbackVC *opinionVC = [[OpinionFeedbackVC alloc]init];
                [self.navigationController pushViewController:opinionVC animated:YES];
            }else if (indexPath.row == 1){
                YDLog(@"帮助中心");
            }else if(indexPath.row == 2){
                YDLog(@"免责声明");
            }
            break;
        }
            // 0-section
        case 2:{
            if (indexPath.row == 0) {
                YDLog(@"关于听我的");
            }else {
                YDLog(@"听我的团队介绍");
            }
            
            break;
        }
            
        default:
            break;
    }
}

#pragma mark -点击事件
-(void)clickBtSigOut
{
    YDLog(@"退出登录");
    
    [[NetReqManager getInstance] sendLogoutReq:[ListenToMeDBManager getUuid]];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"launchStaue"];


}
#pragma mark - alertViewFunction
-(void)dismisFeedbackAlert:(UIAlertView *)alertView
{
    [alertView dismissWithClickedButtonIndex:[alertView cancelButtonIndex] animated:YES];
}
@end
