//
//  OpinionFeedbackVC.m
//  ListenToMe
//
//  Created by zhw on 15/4/22.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "OpinionFeedbackVC.h"

@interface OpinionFeedbackVC ()<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate>
/**
 *  加载反馈信息的视图
 */
@property(nonatomic,strong) UITableView * tableView;
/**
 *  输入框背景视图
 */
@property(nonatomic,strong) UIImageView *opinionView;
/**
 *  输入框
 */
@property(nonatomic,strong) UITextField * tfOpinion;
/**
 *  键盘高度
 */
@property(nonatomic,assign) NSInteger keyboardhight;
/**
 *  反馈的意见
 */
@property(nonatomic,strong) NSString *opinionContent;
/**
 *  存储反馈意见的数组
 */
@property(nonatomic,strong) NSMutableArray * opinionDataArray;
@end

@implementation OpinionFeedbackVC
@synthesize keyboardhight;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setNavigation];
    
    [self setUI];
    
    //初始化数据
    if (self.opinionDataArray == nil) {
        self.opinionDataArray = [NSMutableArray array];
    }
}


//在遇到有输入的情况下。由于现在键盘的高度是动态变化的。中文输入与英文输入时高度不同。所以输入框的位置也要做出相应的变化
#pragma mark - keyboardHight
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerForKeyboardNotifications];
    
    if (self.opinionDataArray.count != 0) {
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.opinionDataArray count] - 1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)setNavigation{
    
    self.navigationItem.title = @"意见反馈";
    [self setLeftItem];
}

-(void)setLeftItem{
    
    UIButton *leftBtn = [[UIButton alloc]init];
    UIImage *leftBtnImg = [UIImage imageNamed:@"whiteBack.png"];
    leftBtn.frame = CGRectMake(0, 0, leftBtnImg.size.width, leftBtnImg.size.height);
    [leftBtn setImage:leftBtnImg forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(clickLeftItem:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customLeftBtnItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = customLeftBtnItem;
}
-(void)clickLeftItem:(UIButton *)leftItem{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setUI{
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    [self setOpinion];
   
    [self setMessageTableView];
    
    
}

#pragma mark - setOpinion
-(void)setOpinion{

    //输入意见反馈
    CGFloat opinionH = 50;
    CGFloat opinionW = screenWidth;
    CGFloat opinionX = 0;
    CGFloat opinionY = screenHeight - 55;
    self.opinionView = [[UIImageView alloc]initWithFrame:CGRectMake(opinionX, opinionY, opinionW, opinionH)];
    //    opinionView.backgroundColor = [UIColor lightGrayColor];
    UIImage *opinionBgImg = [UIImage imageNamed:@"opinionBgImg"];
    [self.opinionView setImage:opinionBgImg];
    self.opinionView.userInteractionEnabled = YES;
    [self.view addSubview:self.opinionView];
    
    CGFloat tfOpW = self.opinionView.width - 30;
    CGFloat tfOpH = 30;
    CGFloat tfOpX = 15;
    CGFloat tfOpY = (self.opinionView.height - tfOpH) * 0.5;
    self.tfOpinion = [[UITextField alloc]initWithFrame:CGRectMake(tfOpX, tfOpY, tfOpW, tfOpH)];
    [self.tfOpinion setFont:[UIFont systemFontOfSize:12.0]];
    [self.tfOpinion setBackgroundColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:0.3]];
    self.tfOpinion.placeholder = @"意见反馈";
    self.tfOpinion.layer.masksToBounds = YES;
    self.tfOpinion.layer.cornerRadius = 5;
    self.tfOpinion.keyboardType = UIKeyboardAppearanceDefault;
    self.tfOpinion.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.tfOpinion.delegate = self;
    [self.opinionView addSubview:self.tfOpinion];
}

#pragma mark - setTableView
-(void)setMessageTableView{
    
    CGFloat tableViewW = screenWidth;
    CGFloat tableViewH = self.opinionView.y - naviAndStatusH;
    CGFloat tableViewX = 0;
    CGFloat tableViewY = naviAndStatusH;
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(tableViewX, tableViewY, tableViewW, tableViewH)];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self addAGesutreRecognizerForYourView];
    [self.view addSubview:self.tableView];
    
}

#pragma mark - UITableViewDataSourceDelegate UITableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.opinionDataArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"opinionCell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"opinionCell"];
    }
    
    cell.textLabel.text = [self.opinionDataArray objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}
#pragma mark - 监测键盘
- (void)registerForKeyboardNotifications
{
    //使用NSNotificationCenter 鍵盤出現時
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWasShown:)
     
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    //使用NSNotificationCenter 鍵盤隐藏時
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWillBeHidden:)
     
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    
}

//实现当键盘出现的时候计算键盘的高度大小。用于输入框显示位置
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    //kbSize即為鍵盤尺寸 (有width, height)
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;//得到鍵盤的高度
    keyboardhight = kbSize.height;
    //输入框位置动画加载
    [self begainMoveUpAnimation:keyboardhight];
    
    // 获取键盘弹出的时间
//    NSValue *animationDurationValue = [info objectForKey:UIKeyboardAnimationDurationUserInfoKey];
//    NSTimeInterval animationDuration;
//    [animationDurationValue getValue:&animationDuration];
//    CGFloat duration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
//    [UIView animateWithDuration:duration animations:^{
//        [self begainMoveUpAnimation:keyboardhight];
//    } completion:^(BOOL finished) {
//        
//    }];
    
    
}

-(void)begainMoveUpAnimation:(NSInteger) keyboardH{
    CGFloat opinionH = 50;
    CGFloat opinionW = screenWidth;
    CGFloat opinionX = 0;
    CGFloat opinionY = screenHeight - 55 - keyboardH;
    self.opinionView.frame = CGRectMake(opinionX, opinionY, opinionW, opinionH);
    
    CGFloat tableViewW = screenWidth;
    CGFloat tableViewH = self.opinionView.y - naviAndStatusH ;
    CGFloat tableViewX = 0;
    CGFloat tableViewY = naviAndStatusH;
    self.tableView.frame = CGRectMake(tableViewX, tableViewY, tableViewW, tableViewH);
    [self.tableView reloadData];
}

//当键盘隐藏的时候
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    CGFloat opinionH = 50;
    CGFloat opinionW = screenWidth;
    CGFloat opinionX = 0;
    CGFloat opinionY = screenHeight - 55;
    self.opinionView.frame = CGRectMake(opinionX, opinionY, opinionW, opinionH);
    
    CGFloat tableViewW = screenWidth;
    CGFloat tableViewH =  self.opinionView.y - naviAndStatusH;
    CGFloat tableViewX = 0;
    CGFloat tableViewY = naviAndStatusH;
    self.tableView.frame = CGRectMake(tableViewX, tableViewY, tableViewW, tableViewH);
    [self.tableView reloadData];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
//    [textField resignFirstResponder];
    if(textField.text.length > 0){
        self.opinionContent = textField.text;
        
        [self.opinionDataArray addObject:self.opinionContent];
        [self.tableView reloadData];
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.opinionDataArray count] - 1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        textField.text = nil;
    }
    
    
    return YES;
}


//tableView不会响应touchesBegan
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [self.tfOpinion resignFirstResponder];
}



//解决办法,添加一个点击手势
- (void)addAGesutreRecognizerForYourView

{
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesturedDetected:)]; // 手势类型随你喜欢。
    
    tapGesture.delegate = self;
    
    [self.tableView addGestureRecognizer:tapGesture];
    
}

- (void)tapGesturedDetected:(UITapGestureRecognizer *)recognizer

{
    [self.tfOpinion resignFirstResponder];
    // do something
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
