//
//  MYWorksListCell.m
//  ListenToMe
//
//  Created by yadong on 3/17/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "MYWorksListCell.h"

@implementation MYWorksListCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        CGFloat cellH = 60; // cell的高度
        CGFloat frontX = 15; // 控件距离screen前后的距离
        
        CGFloat lbMusicNameH = 16;
        CGFloat lbHigestMarkH = 13;  
        CGFloat lbSingCountH = 13;
        
        CGFloat marginH = (cellH - lbMusicNameH - lbHigestMarkH) * 0.33; // 控件间的y上的间距
        
        // 歌曲名字
        _lbMusicName = [[UILabel alloc]initWithFrame:CGRectMake(frontX, marginH, screenWidth * 0.7, lbMusicNameH)];
        _lbMusicName.font  = [UIFont systemFontOfSize:14.0];
        _lbMusicName.textColor = [UIColor rgbFromHexString:@"#4A4A4A" alpaa:1.0];
        [self addSubview:_lbMusicName];
        
        // 我的最高分
        _lbHighestMark = [[UILabel alloc]initWithFrame:CGRectMake(frontX, _lbMusicName.y + _lbMusicName.height + marginH, _lbMusicName.width, lbHigestMarkH)];
        _lbHighestMark.font  = [UIFont systemFontOfSize:11.0];
        _lbHighestMark.textColor = [UIColor rgbFromHexString:@"#CE9AFF" alpaa:1.0];
        [self addSubview:_lbHighestMark];
        
        // 演唱次数
        _lbSingCount = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - frontX - 80, cellH - 20, 80, lbSingCountH)];
        _lbSingCount.font  = [UIFont systemFontOfSize:11.0];
        _lbSingCount.textColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
        [self addSubview:_lbSingCount];
    }
    
    return self;
}

@end
