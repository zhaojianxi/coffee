//
//  MyMusicListVC.m
//  ListenToMe
//
//  Created by yadong on 2/5/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "MyMusicListVC.h"
#import "MYWorksListCell.h"

@interface MyMusicListVC ()<UITableViewDelegate,UITableViewDataSource>
@property(strong,nonatomic) UISearchBar *searchBar;
@property(strong,nonatomic) UITableView *mTableView;
@end

@implementation MyMusicListVC
#pragma mark -生命周期
-(void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setUI];
}

#pragma mark -UI
-(void)setUI
{
    self.navigationController.title = @"我的歌单";
    
    [self setUpSearchBar];
    
    [self setUpTableView];
}

#pragma mark 搜索栏
-(void)setUpSearchBar
{
    _searchBar = [[UISearchBar alloc]init];
    _searchBar.frame = CGRectMake(0, 0, screenWidth, 40);
    [self.view addSubview:_searchBar];
}

#pragma mark tableview
-(void)setUpTableView
{
    _mTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, screenHeight - 40) style:UITableViewStylePlain]; // 40是搜索栏高度
    _mTableView.backgroundColor = [UIColor clearColor];
    _mTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_mTableView];
    
    _mTableView.delegate = self;
    _mTableView.dataSource = self;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *ID = @"myWorksCell";
    MYWorksListCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (cell == nil) {
        cell = [[MYWorksListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    
    cell.lbMusicName.text = @"铁血丹心";
    cell.lbHighestMark.text = @"最高分:3586";
    cell.lbSingCount.text = @"我唱过6次";
    
    return cell;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}





@end
