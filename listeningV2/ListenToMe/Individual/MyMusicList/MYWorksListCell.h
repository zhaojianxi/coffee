//
//  MYWorksListCell.h
//  ListenToMe
//
//  Created by yadong on 3/17/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MYWorksListCell : UITableViewCell
@property(strong,nonatomic) UILabel *lbMusicName;
@property(strong,nonatomic) UILabel *lbHighestMark;
@property(strong,nonatomic) UILabel *lbSingCount;
@end
