//
//  MyAlbumVC.m
//  ListenToMe
//
//  Created by yadong on 2/5/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "MyAlbumVC.h"
#import "MyAlbumCell.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "NSDate+ZHW.h"
@interface MyAlbumVC()<UIActionSheetDelegate>
@property(strong,nonatomic) UITableView *mTableView;
/**
 *  上传头像背景视图
 */
@property(nonatomic,strong)UIView *upLoadBgView;
/**
 *  上传照片按钮
 */
@property(nonatomic,strong)UIButton *btnUpLoadPhoto;
/**
 *  相机
 */
@property(nonatomic,strong)UIButton *btnCamera;
/**
 * 点击cell弹出提示
 */
@property(nonatomic,strong)UIAlertController *alertForPhoto;
/**
 *  选中的cell 的indexPath
 */
@property(nonatomic,strong)NSIndexPath *selectIndexPath;
/**
 *  数据源
 */
@property(nonatomic,strong)ListenToMeData *listenToMeData;
/**
 *  删除弹窗
 */
@property(nonatomic,strong) UIView *bagView;
/**
 *  当前选择的cell的位置
 */
@property(nonatomic,assign) NSInteger currentSelect;
@end

@implementation MyAlbumVC
@synthesize mTableView;


#pragma mark -生命周期
-(void)viewDidLoad
{
    [super viewDidLoad];
    [self setNavigation];
    [self setUI];
    
    [self setDeleteView];
    
    [self initData];
}

#pragma mark 删除提示视图
-(void)setDeleteView{
    
    self.bagView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 324)];
    self.bagView.backgroundColor = [UIColor colorWithRed:203/255.0 green:203/255.0 blue:203/255.0 alpha:0.5];
    
    
    CGFloat btnBagViewW = screenWidth - 30;
    CGFloat btnBagViewH = 100;
    CGFloat btnBagViewX = 15;
    CGFloat btnBagViewY = (324 - btnBagViewH) * 0.5;
    
    UIImageView *btnBagView = [[UIImageView alloc] initWithFrame:CGRectMake(btnBagViewX, btnBagViewY, btnBagViewW, btnBagViewH)];
    btnBagView.userInteractionEnabled = YES;
    [btnBagView setImage:[UIImage imageNamed:@"background.png"]];
    btnBagView.layer.masksToBounds = YES;
    btnBagView.layer.cornerRadius = 5;
    [self.bagView addSubview:btnBagView];
    
    //添加两个button 删除 取消
    CGFloat btnW = 50; //button的高度
    UIButton *btnDelete = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, btnBagViewW, btnW)];
    [btnDelete setTitle:@"删除" forState:UIControlStateNormal];
    [btnDelete setTitleColor:[UIColor rgbFromHexString:@"#FF0053" alpaa:1.0] forState:UIControlStateNormal];
    [btnDelete addTarget:self action:@selector(deleteHandle:) forControlEvents:UIControlEventTouchUpInside];
    [btnBagView addSubview:btnDelete];
    
    UIButton *btnCancle = [[UIButton alloc] initWithFrame:CGRectMake(0, btnW, btnBagViewW, btnW)];
    [btnCancle setTitle:@"取消" forState:UIControlStateNormal];
    [btnCancle setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [btnCancle addTarget:self action:@selector(cancleHandle:) forControlEvents:UIControlEventTouchUpInside];
    [btnBagView addSubview:btnCancle];
    
    UILabel *lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, btnW, btnBagViewW, 1)];
    lineLabel.backgroundColor = [UIColor lightGrayColor];
    lineLabel.alpha = 0.5;
    [btnBagView addSubview:lineLabel];
    
}


#pragma mark -setData
-(void)initData{
    
    [[NetReqManager getInstance] sendGetPictureList:self.uuid IOffset:self.iOffset Inum:self.iNum LWatchUserId:self.lWatchUserId LCommemorateId:self.lCommemorateId];
    
    
    
    self.listenToMeData = [ListenToMeData getInstance];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTable) name:NOTIFY_GET_PICTURELIST_PAGE_RESP object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uploadPicture) name:NOTIFY_UPLOADPICTURE_PAGE_RESP object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deletePicture) name:NOTIFY_DELETEPICTURE_PAGE_RESP object:nil];
    
    

}

-(void)refreshTable{
    [[NetReqManager getInstance] sendGetUserInfo:[ListenToMeDBManager getUuid]];
    self.uuid = self.listenToMeData.stUserBaseInfoNet.uuid;
    self.iNum = self.listenToMeData.stUserBaseInfoNet.iPictureNum;
    [self.mTableView reloadData];
}

-(void)uploadPicture{
    
    [[NetReqManager getInstance] sendGetPictureList:self.uuid IOffset:self.iOffset Inum:(++self.iNum)  LWatchUserId:self.lWatchUserId LCommemorateId:self.lCommemorateId];
    [self.mTableView reloadData];
}

-(void)deletePicture{
    if(self.iNum >0){
    [[NetReqManager getInstance] sendGetPictureList:self.uuid IOffset:self.iOffset Inum:(--self.iNum)  LWatchUserId:self.lWatchUserId LCommemorateId:self.lCommemorateId];
    }
    
    [self.mTableView reloadData];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_PICTURELIST_PAGE_RESP object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_UPLOADPICTURE_PAGE_RESP object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_DELETEPICTURE_PAGE_RESP object:nil];
    self.listenToMeData.arrPictureBaseInfo = nil;
}

#pragma mark -UI
-(void)setUI
{

    [self setUpTableView];
    
    //设置上传图片的视图 如果传入的uuid传入的为0 ,则是浏览他人相册,无法使用图片上传
    if (self.isPushFromPerCenter) {
        [self setUpLoadPhoto];
    }
    
}

// set tableview
-(void)setUpTableView
{
    mTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight) style:UITableViewStyleGrouped];
    [self.view addSubview:mTableView];
    
    mTableView.backgroundColor = [UIColor clearColor];
    mTableView.delegate = self;
    mTableView.dataSource = self;
}

#pragma mark 导航栏
-(void)setNavigation
{
//    self.navigationItem.title = @"我的相册";
    [self setLeftBtnItem];
    [self setRightBtnItem];
}

// 导航栏左边的Btn
-(void)setLeftBtnItem
{
    UIButton *leftBtn = [[UIButton alloc]init];
    UIImage *leftBtnImg = [UIImage imageNamed:@"whiteBack.png"];
    leftBtn.frame = CGRectMake(0, 0, leftBtnImg.size.width, leftBtnImg.size.height);
    [leftBtn setImage:leftBtnImg forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(clickLeftBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customLeftBtnItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = customLeftBtnItem;
}

// 导航栏右边的Btn
-(void)setRightBtnItem
{
    UIButton *rightBtn = [[UIButton alloc]init];
    UIImage *imgMore = [UIImage imageNamed:@"more.png"];
    rightBtn.frame = CGRectMake(0, 0, imgMore.size.width, imgMore.size.height);
    [rightBtn setImage:imgMore forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(clickRightBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customRightBtnItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = customRightBtnItem;
}

#pragma mark - setUpLoadPhoto上传图片
-(void)setUpLoadPhoto{
    CGFloat upLoadViewX = 15;
    CGFloat upLoadViewY = screenHeight - 15 - 44;
    CGFloat upLoadViewW = screenWidth - 15 * 2;
    CGFloat upLoadVidwH = 44;
    self.upLoadBgView = [[UIView alloc]initWithFrame:CGRectMake(upLoadViewX, upLoadViewY, upLoadViewW, upLoadVidwH)];
    [self.view addSubview:self.upLoadBgView];
    
    CGFloat UIInterval = 20; //控件之间的间隔
    CGFloat cameraX = self.upLoadBgView.width - 44;
    CGFloat cameraY = 0;
    CGFloat uiWH = self.upLoadBgView.height; //控件的高度
    CGFloat btnUpLoadW = self.upLoadBgView.width - uiWH - UIInterval;
    UIImage *upLoadBgImg = [UIImage imageNamed:@"UpLoadPhotoBgImg.png"];
    self.btnUpLoadPhoto = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, btnUpLoadW, uiWH)];
    [self.upLoadBgView addSubview:self.btnUpLoadPhoto];
    [self.btnUpLoadPhoto setImage:upLoadBgImg forState:UIControlStateNormal];
    self.btnUpLoadPhoto.alpha = 0.8;
    self.btnUpLoadPhoto.imageView.backgroundColor  = [UIColor clearColor];
    [self.btnUpLoadPhoto setTitle:@"上传照片" forState:UIControlStateNormal];
    [self.btnUpLoadPhoto setTitleEdgeInsets:UIEdgeInsetsMake(0, - btnUpLoadW - (btnUpLoadW * 0.5), 0, 0)];
    [self.btnUpLoadPhoto setTitleColor:[UIColor rgbFromHexString:@"#000000" alpaa:1.0] forState:UIControlStateNormal];
    [self.btnUpLoadPhoto addTarget:self action:@selector(upLoadPhotoHandle:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnUpLoadPhoto.titleLabel setFont:[UIFont systemFontOfSize:15.0]];
    
    self.btnCamera = [[UIButton alloc]initWithFrame:CGRectMake(cameraX, cameraY, uiWH, uiWH)];
    [self.upLoadBgView addSubview:self.btnCamera];
    UIImage *cameraImg = [UIImage imageNamed:@"camera.png"];
    [self.btnCamera setImage:cameraImg forState:UIControlStateNormal];
    [self.btnCamera addTarget:self action:@selector(openCameraHandle:) forControlEvents:UIControlEventTouchUpInside];
    self.btnCamera.alpha = 0.8;
}


#pragma mark - tableview delegate & datasource
-(MyAlbumCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"myAlbumCell";
    MyAlbumCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[MyAlbumCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if(self.bagView){
        [self.bagView removeFromSuperview];
    }
    
    
    if (self.listenToMeData.arrPictureBaseInfo) {
        PictureBaseInfo *pictureBaseInfo = self.listenToMeData.arrPictureBaseInfo[indexPath.section];
        [cell.imgPicture sd_setImageWithURL:[NSURL URLWithString:pictureBaseInfo.sPictureUrl]];
        
        if ([pictureBaseInfo.sPictureName isEqualToString:@""]) {
            [cell.btnPicName setTitle:@"我的相片" forState:UIControlStateNormal];
        }else{
            [cell.btnPicName setTitle:pictureBaseInfo.sPictureName forState:UIControlStateNormal];
        }
        
        if (indexPath.section == 0) {
            [cell.btnPicName setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateNormal];

        }else{
            [cell.btnPicName setTitleColor:[UIColor rgbFromHexString:@"#000000" alpaa:0.8] forState:UIControlStateNormal];
        }
        
        
        [cell.lbTime setText:[self getCreatedTime:pictureBaseInfo.lCreateTime]];
        
    }else{

    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.isPushFromPerCenter) {
        YDLog(@"正在编辑自己的相册");
        MyAlbumCell *cell = (MyAlbumCell *)[tableView cellForRowAtIndexPath:indexPath];
        [cell addSubview:self.bagView];
        self.currentSelect = indexPath.section;
    }else{
        //查看他人的相册
    }
    
    
//    self.selectIndexPath = indexPath;
//    
//    if (iOS8_greater) {
//        self.alertForPhoto = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
//        
//        UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
//            [self actionSheet:nil clickedButtonAtIndex:0];
//        }];
//        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//            
//        }];
//        
//        [self.alertForPhoto addAction:deleteAction];
//        [self.alertForPhoto addAction:cancelAction];
//        
//        
//        [self presentViewController:self.alertForPhoto animated:YES completion:^{
//            
//        }];
//        
//        
//    }else{
//        UIActionSheet *selectPhotoAction  = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:@"删除" otherButtonTitles:@"取消", nil];
//        [selectPhotoAction showInView:self.view];
//        
//    }
    
    
}

-(void)willPresentActionSheet:(UIActionSheet *)actionSheet{
    if (iOS8_greater) {
        [[UIView appearanceWhenContainedIn:[UIAlertController class], nil] setTintColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
        
    }else{
        for (UIView *subView in actionSheet.subviews) {
            
            if ([subView isKindOfClass:[UIButton class]]) {
                UIButton *btn = (UIButton *)subView;
                [btn setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateNormal];
            }
        }
    }

}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        
        
        PictureBaseInfo *pictureBaseInfo = self.listenToMeData.arrPictureBaseInfo[self.selectIndexPath.section];
        
        [[NetReqManager getInstance] sendGetDeletePicture:self.uuid LPhotoId:pictureBaseInfo.lPhotoId LCommemorateId:0];
        
        [self.mTableView reloadData];
    }else{
        [self.mTableView reloadData];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 324;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.listenToMeData.arrPictureBaseInfo.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 8.5;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 8.5;
}

#pragma mark -点击事件
-(void)clickRightBarBtnItem
{
    YDLog(@"点击了导航栏的右边按钮");
}

-(void)clickLeftBarBtnItem
{
//    [self.navigationController popToRootViewControllerAnimated:YES];
    
    [self.navigationController popViewControllerAnimated:YES];
    YDLog(@"点击了导航栏左边的Btn");
}

#pragma mark - upLoadPhotoHandle上传照片 打开照相机
-(void)upLoadPhotoHandle:(UIButton *)upLoadBtn{
    
    YDLog(@"上传照片");
    
    UIImagePickerController *pickerImage = [[UIImagePickerController alloc] init];
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        pickerImage.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        //pickerImage.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        //        pickerImage.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:pickerImage.sourceType];
        
    }
    pickerImage.delegate = (id)self;
    pickerImage.allowsEditing = YES;
    [self presentViewController:pickerImage animated:YES completion:^{
        
    }];
}

-(void)openCameraHandle:(UIButton *)openCameraBtn{
    YDLog(@"打开相机");
    
    //先设定sourceType为相机，然后判断相机是否可用（ipod）没相机，不可用将sourceType设定为相片库
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
    //判断是否摄像头
    if (![UIImagePickerController isSourceTypeAvailable: sourceType]) {
        sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    //sourceType = UIImagePickerControllerSourceTypeCamera; //照相机
    //sourceType = UIImagePickerControllerSourceTypePhotoLibrary; //图片库
    //sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum; //保存的相片
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];//初始化
    picker.delegate = (id)self;
    picker.allowsEditing = YES;//设置可编辑
    picker.sourceType = sourceType;
    picker.showsCameraControls = YES;
    [self presentViewController:picker animated:YES completion:^{
        
    }];
}


/**
 * 当选择一张照片后，进入这里
 */
#pragma makr 当选择一张照片之后进入这里

#pragma mark - 完成拍照
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
  
    
    NSString *tyep = [info objectForKey:UIImagePickerControllerMediaType];
    
    //使用照相机选取图片
    if ([tyep isEqualToString:(NSString *)kUTTypeImage] && picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        UIImage *originalImg = [info objectForKey:UIImagePickerControllerOriginalImage];
//        NSData *data = UIImagePNGRepresentation(originalImg);
        UIImage *editImg = [info objectForKey:UIImagePickerControllerEditedImage];
        NSData *data = UIImageJPEGRepresentation(editImg, 0.5);
        
        
        [[NetReqManager getInstance] sendGetUploadPicture:self.uuid BPictures:data LCommenorateId:0];
    
        //
//                UIImage *cropImg = [info objectForKey:UIImagePickerControllerCropRect];
        //
        //        NSURL * url = [info objectForKey:UIImagePickerControllerMediaURL];
        //
        //        NSDictionary *metaData = [info objectForKey:UIImagePickerControllerMediaMetadata];
        
        [self performSelector:@selector(saveImage:) withObject:originalImg afterDelay:0.5];
        
        UIImageWriteToSavedPhotosAlbum(originalImg, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
    }
    
    //使用手机相册选去图片
    if ([tyep isEqualToString:(NSString *)kUTTypeImage]  && picker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary) {
//        UIImage *originalImg = [info objectForKey:UIImagePickerControllerOriginalImage];
//        NSData *data = UIImagePNGRepresentation(originalImg);
        UIImage *editImg = [info objectForKey:UIImagePickerControllerEditedImage];
        NSData *data = UIImageJPEGRepresentation(editImg, 0.5);
        
        
        
        [[NetReqManager getInstance] sendGetUploadPicture:self.uuid BPictures:data LCommenorateId:0];
        
        
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - 用户取消拍照
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - 保存图片
-(void)saveImage:(UIImage *)image{
    
    
    NSData *imageData = UIImagePNGRepresentation(image);
    if(imageData == nil)
    {
        imageData = UIImageJPEGRepresentation(image, 1.0);
    }
    
    NSDate *date = [NSDate date];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMddHHmmss"];
    NSString *fileName = [[formatter stringFromDate:date] stringByAppendingPathExtension:@"png"] ;
    
    
    //    NSURL *saveURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:fileName];
    //    YDLog(@"save Url: %@",saveURL);
    //    [imageData writeToURL:saveURL atomically:YES];
    
    
    //    NSLog(@"保存头像！");
    //    [userPhotoButton setImage:image forState:UIControlStateNormal];
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imageFilePath = [documentsDirectory stringByAppendingPathComponent:fileName];
    YDLog(@"imageFile->>%@",imageFilePath);
    success = [fileManager fileExistsAtPath:imageFilePath];
    if(success) {
        success = [fileManager removeItemAtPath:imageFilePath error:&error];
    }
    //    UIImage *smallImage=[self scaleFromImage:image toSize:CGSizeMake(80.0f, 80.0f)];//将图片尺寸改为80*80
    UIImage *smallImage = image;//[self thumbnailWithImageWithoutScale:image size:CGSizeMake(40, 40)];
    [UIImageJPEGRepresentation(smallImage, 1.0f) writeToFile:imageFilePath atomically:YES];//写入文件
//    UIImage *selfPhoto = [UIImage imageWithContentsOfFile:imageFilePath];//读取图片文件
    //    [userPhotoButton setImage:selfPhoto forState:UIControlStateNormal];

    
    
    //    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //    NSString *documentsDirectory = [paths objectAtIndex:0];
    //    NSString *imageFilePath = [documentsDirectory stringByAppendingPathComponent:@"selfPhoto.jpg"];
    //    NSLog(@"imageFile->>%@",imageFilePath);
    //    UIImage *selfPhoto = [UIImage imageWithContentsOfFile:imageFilePath];//
    //    self.img.image = selfPhoto;
    //    [self.img.layer setCornerRadius:CGRectGetHeight([self.img bounds]) / 2];  //修改半径，实现头像的圆形化
    //    self.img.layer.masksToBounds = YES
    
    
}
#pragma mark -保持原来的长宽比，生成一个缩略图
- (UIImage *)thumbnailWithImageWithoutScale:(UIImage *)image size:(CGSize)asize
{
    UIImage *newimage;
    if (nil == image) {
        newimage = nil;
    }
    else{
        CGSize oldsize = image.size;
        CGRect rect;
        if (asize.width/asize.height > oldsize.width/oldsize.height) {
            rect.size.width = asize.height*oldsize.width/oldsize.height;
            rect.size.height = asize.height;
            rect.origin.x = (asize.width - rect.size.width)/2;
            rect.origin.y = 0;
        }
        else{
            rect.size.width = asize.width;
            rect.size.height = asize.width*oldsize.height/oldsize.width;
            rect.origin.x = 0;
            rect.origin.y = (asize.height - rect.size.height)/2;
        }
        UIGraphicsBeginImageContext(asize);
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetFillColorWithColor(context, [[UIColor clearColor] CGColor]);
        UIRectFill(CGRectMake(0, 0, asize.width, asize.height));//clear background
        [image drawInRect:rect];
        newimage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    return newimage;
}

#pragma mark - 改变图像的尺寸，方便上传服务器
- (UIImage *) scaleFromImage: (UIImage *) image toSize: (CGSize) size
{
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - 保存照片成功后调用
- (void)image:(UIImage*)image didFinishSavingWithError:(NSError*)error contextInfo:(void*)contextInfo{
    
    if (!error) {
        
        YDLog(@"picture saved success");
    }else{
        
        YDLog(@"error occured while saving the picture%@", error);
        
    }
    
}

//Return the URL to the application Documents directory
-(NSURL *)applicationDocumentsDirectory{
    
    return [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentationDirectory inDomains:NSUserDomainMask ] lastObject];
}


#pragma mark - 计算时间间隔
-(NSString *)getCreatedTime:(int64_t)lCreateTime{
    
    int64_t time = lCreateTime;
    NSDate *createdDate = [[NSDate alloc]initWithTimeIntervalSince1970:time/1000.0];
    
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"EEE MMM dd HH:mm:ss Z yyyy";
    
#warning 真机调试下, 必须加上这段
    fmt.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    
    // 2..判断创建时间 和 现在时间 的差距
    if (createdDate.isToday) { // 今天
        if (createdDate.deltaWithNow.hour >= 1) {
            return [NSString stringWithFormat:@"%ld小时前", createdDate.deltaWithNow.hour];
        } else if (createdDate.deltaWithNow.minute >= 1) {
            return [NSString stringWithFormat:@"%ld分钟前", createdDate.deltaWithNow.minute];
        } else {
            return @"刚刚";
        }
    } else if (createdDate.isYesterday) { // 昨天
        fmt.dateFormat = @"昨天 HH:mm";
        return [fmt stringFromDate:createdDate];
    } else if (createdDate.isThisYear) { // 今年(至少是前天)
        fmt.dateFormat = @"MM-dd";
        return [fmt stringFromDate:createdDate];
    } else { // 非今年
        fmt.dateFormat = @"yyyy-MM-dd";
        return [fmt stringFromDate:createdDate];
    }
    
}

#pragma mark - 点击删除button
-(void)deleteHandle:(UIButton *)btnDelete{
    YDLog(@"删除选中的照片");
    PictureBaseInfo *pictureBaseInfo = self.listenToMeData.arrPictureBaseInfo[self.currentSelect];
    [[NetReqManager getInstance] sendGetDeletePicture:self.uuid LPhotoId:pictureBaseInfo.lPhotoId LCommemorateId:0];
}

#pragma mark - 点击取消button
-(void)cancleHandle:(UIButton *)btnCancle{
    [self.bagView removeFromSuperview];
}

@end
