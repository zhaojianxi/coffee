//
//  MyAlbumCell.h
//  ListenToMe
//
//  Created by yadong on 2/5/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyAlbumCell : UITableViewCell
@property(strong,nonatomic) UIImageView *imgPicture;
@property(strong,nonatomic) UIButton *btnPicName;
@property(strong,nonatomic) UILabel *lbTime;
@end
