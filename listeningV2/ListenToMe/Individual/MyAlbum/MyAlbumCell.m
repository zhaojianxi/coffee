//
//  MyAlbumCell.m
//  ListenToMe
//
//  Created by yadong on 2/5/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "MyAlbumCell.h"

@implementation MyAlbumCell
@synthesize imgPicture;
@synthesize btnPicName;
@synthesize lbTime;

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {

        [self setUI];
    }
    return self;
}

-(void)setUI
{
    CGFloat imgPictureW = screenWidth;
    CGFloat imgPictureH = 304;
    CGFloat imgPictureX = 0;
    CGFloat imgPictureY = 0;
    imgPicture = [[UIImageView alloc]initWithFrame:CGRectMake(imgPictureX, imgPictureY, imgPictureW, imgPictureH)];
    [self addSubview:imgPicture];
    
    
    btnPicName = [[UIButton alloc]initWithFrame:CGRectMake(0, imgPictureY + imgPictureH, 150, 20)];
    [self addSubview:btnPicName];
//    [btnPicName setTitle:@"同一首歌 - 苏州桥店" forState:UIControlStateNormal];
//    [btnPicName setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateNormal];
    [btnPicName.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [btnPicName setImage:[UIImage imageNamed:@"placePlace@2x.png"] forState:UIControlStateNormal];
    btnPicName.contentMode = UIViewContentModeLeft;
    [btnPicName setBackgroundColor:[UIColor clearColor]];
    
    lbTime = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - screenWidth * 0.3 - 15, imgPictureY + imgPictureH, screenWidth * 0.3, 20)];
    [self addSubview:lbTime];
//    [lbTime setText:@"2015-06-12"];
    [lbTime setTextColor:[UIColor rgbFromHexString:@"#D8D8D8" alpaa:1.0]];
    [lbTime setFont:[UIFont systemFontOfSize:10.0]];
    lbTime.textAlignment = NSTextAlignmentRight;
    [lbTime setBackgroundColor:[UIColor clearColor]];
}

@end

//@property(strong,nonatomic) UIImageView *imgPicture;
//@property(strong,nonatomic) UIButton *btnPicName;
//@property(strong,nonatomic) UILabel *lbTime;