//
//  MyRecommendVC.m
//  ListenToMe
//
//  Created by yadong on 2/5/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "MyRecommendVC.h"
#import "MyRecommendCell.h"

@interface MyRecommendVC ()<UITableViewDataSource,UITableViewDelegate>
@property(strong,nonatomic) UITableView *mTableView;
@end

@implementation MyRecommendVC
#pragma mark -生命周期
-(void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setUI];
}

#pragma mark -UI
-(void)setUI
{
    [self setUpTableView];
}

#pragma mark tableViwe
-(void)setUpTableView
{
    _mTableView = [[UITableView alloc]init];
    _mTableView.frame = CGRectMake(0, 0, screenWidth, screenHeight);
    _mTableView.backgroundColor = [UIColor whiteColor];
    _mTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_mTableView];
    
    _mTableView.delegate = self;
    _mTableView.dataSource = self;
    
}

#pragma mark -Delegate & DataSource
-(MyRecommendCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *ID = @"recommendCell";
    MyRecommendCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (cell == nil) {
        cell = [[MyRecommendCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    
    if (indexPath.row == 0) {
        cell.imgAppIcon.image = [UIImage imageNamed:@"tempRecommendIcon_0.png"];
        cell.lbAppName.text = @"播客";
        cell.lbAppDesc.text = @"简介、流畅、精彩";
        
        [cell.btnAppDownload setTitle:@"免费" forState:UIControlStateNormal];
        cell.btnAppDownload.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"recommendGreenIcon.png"]];
        [cell.btnAppDownload setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
    } else if(indexPath.row == 1){
        cell.imgAppIcon.image = [UIImage imageNamed:@"tempRecommendIcon_1.png"];
        cell.lbAppName.text = @"播客";
        cell.lbAppDesc.text = @"简介、流畅、精彩";
        
        [cell.btnAppDownload setTitle:@"免费" forState:UIControlStateNormal];
        cell.btnAppDownload.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"recommendGreenIcon.png"]];
        [cell.btnAppDownload setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }else if(indexPath.row == 2){
        cell.imgAppIcon.image = [UIImage imageNamed:@"tempRecommendIcon_2.png"];
        cell.lbAppName.text = @"播客";
        cell.lbAppDesc.text = @"简介、流畅、精彩";
        
        [cell.btnAppDownload setTitle:@"￥12.00" forState:UIControlStateNormal];
        cell.btnAppDownload.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"recommendPureIcon.png"]];
        [cell.btnAppDownload setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateNormal];
        
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}



@end
