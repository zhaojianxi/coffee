//
//  MyRecommendCell.m
//  ListenToMe
//
//  Created by yadong on 3/16/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "MyRecommendCell.h"

@implementation MyRecommendCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        // 图标
        CGFloat frontX = 15; //子控件距离前后screen的值
        CGFloat iconWh = 40; // icon的宽和高
        CGFloat cellH = 65; // cell的高
        _imgAppIcon = [[UIImageView alloc]init];
        _imgAppIcon.frame = CGRectMake(frontX, (cellH - iconWh) * 0.5, iconWh, iconWh);
        [self addSubview:_imgAppIcon];
        
        // 名称
        _lbAppName = [[UILabel alloc]init];
        _lbAppName.frame = CGRectMake(_imgAppIcon.x + _imgAppIcon.width + frontX, _imgAppIcon.y, screenWidth * 0.5, 20);
        _lbAppName.font = [UIFont systemFontOfSize:14.0];
        _lbAppName.textColor = [UIColor rgbFromHexString:@"#4A4A4A" alpaa:1.0];
        [self addSubview:_lbAppName];
        
        // 简介
        CGFloat lbAppDescH = 15; // 简介的高度
        _lbAppDesc = [[UILabel alloc]init];
        _lbAppDesc.frame = CGRectMake(_lbAppName.x, _imgAppIcon.y + _imgAppIcon.height - lbAppDescH, _lbAppName.width, lbAppDescH);
        _lbAppDesc.font = [UIFont systemFontOfSize:11.0];
        _lbAppDesc.textColor = [UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0];
        [self addSubview:_lbAppDesc];
        
        // 下载按钮
        _btnAppDownload = [[UIButton alloc]init];
        _btnAppDownload.frame = CGRectMake(screenWidth - frontX - 60, (cellH - 29 ) * 0.5, 60, 29); // 60是宽，高29
        _btnAppDownload.titleLabel.font = [UIFont systemFontOfSize:12.0];
        [self addSubview:_btnAppDownload];
        
    }
    
    return self;
}

@end
