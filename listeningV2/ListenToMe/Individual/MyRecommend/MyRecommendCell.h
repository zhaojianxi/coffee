//
//  MyRecommendCell.h
//  ListenToMe
//
//  Created by yadong on 3/16/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyRecommendCell : UITableViewCell
/**
 * 应用图标
 */
@property(strong,nonatomic) UIImageView *imgAppIcon;
/**
 * 应用名称
 */
@property(strong,nonatomic) UILabel *lbAppName;
/**
 * 应用介绍
 */
@property(strong,nonatomic) UILabel *lbAppDesc;
/**
 * 下载按钮
 */
@property(strong,nonatomic) UIButton *btnAppDownload;
@end
