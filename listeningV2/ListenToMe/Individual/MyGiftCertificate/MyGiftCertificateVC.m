//
//  MyGiftCertificateVC.m
//  ListenToMe
//
//  Created by yadong on 2/5/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "MyGiftCertificateVC.h"
#import "MyGiftCertificateCell.h"
#import "BindingPhoneNumAlertView.h"
#import "GivingGiftCertificateVC.h"
#import "GiftCertificateDetailVC.h"
@interface MyGiftCertificateVC ()<UITableViewDelegate,UITableViewDataSource>
@property(strong,nonatomic) UITableView *mTableView;
@property(strong,nonatomic) NSMutableArray *colorArray; // 假数据，模仿cell的颜色
/**
 *  存储礼券的背景图
 */
@property(strong,nonatomic) NSMutableArray *giftBgImgArray;
/**
 *  存储四种button的背景图
 */
@property(nonatomic,strong) NSMutableArray *detailBtnImgArray;
/**
 *  绑定手机号提示
 */
@property(nonatomic,strong) BindingPhoneNumAlertView *bindingPhoneNumAlert;
/**
 *  送礼券
 */
@property(nonatomic,strong)GivingGiftCertificateVC *givingGiftVC;
/**
 *  数据
 */
@property(nonatomic,strong) ListenToMeData *listenToMeData;

@end

@implementation MyGiftCertificateVC
#pragma mark -生命周期
-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setNav];
    
    //设置礼券的类型
    [self setCouponType];
    
    [self setUI];
    
    [self initData];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

#pragma mark - 设置礼券的类型
-(void)setCouponType{
    
    //礼券一共分类5类,欢唱券 代金券 打折券 兑换券 过期礼券
    //五中样式对应五中颜色
    
    //总共友5中分类
    UIColor *color_0 = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]; //蓝色
    UIColor *color_1 = [UIColor rgbFromHexString:@"#FC9169" alpaa:1.0]; //橘黄色
    UIColor *color_2 = [UIColor rgbFromHexString:@"#DB2927" alpaa:1.0]; //红色
    UIColor *color_3 = [UIColor rgbFromHexString:@"#1B8D03" alpaa:1.0]; //绿色
    UIColor *color_4 = [UIColor lightGrayColor];                        //灰色
    
    _colorArray = [NSMutableArray arrayWithObjects:color_0,color_1,color_2,color_3,color_4, nil];
    
    self.detailBtnImgArray = [NSMutableArray array];
    self.giftBgImgArray = [NSMutableArray array];
    
    
    for (int i = 0; i < 2; i ++) {
        for (int index = 0; index < 5; index ++) {
            UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"0%da",index + 1]];
            [self.detailBtnImgArray addObject:image];
            
            UIImage *bgImage =[UIImage imageNamed:[NSString stringWithFormat:@"0%d",index + 1]];
            [self.giftBgImgArray addObject:bgImage];
        }
    }
    
}

#pragma mark - 初始化数据
-(void)initData{
    [[NetReqManager getInstance] sendGetUserCouponList:[ListenToMeDBManager getUuid] IOffset:0 INum:20];
    
    self.listenToMeData = [ListenToMeData getInstance];
    //用户礼券列表回包通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshData) name:NOTIFY_GET_USER_COUPONLIST_PAGE_RESP object:nil];
    
    //验证成功通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bindingSuccess) name:NOTIFY_UPLOAD_VERIFY_CODE_SUCCESS_PAGE_RESP object:nil];
    //验证失败通知
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bindingFailure) name:NOTIFY_UPLOAD_VERIFY_CODE_FAILURE_PAGE_RESP object:nil];
}

-(void)refreshData{
    
    [self.mTableView reloadData];
}

-(void)bindingSuccess{
    [self.bindingPhoneNumAlert removeFromSuperview];
}

-(void)bindingFailure{
    self.bindingPhoneNumAlert.lbFailure.hidden = NO;
}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_USER_COUPONLIST_PAGE_RESP object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_UPLOAD_VERIFY_CODE_SUCCESS_PAGE_RESP object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_UPLOAD_VERIFY_CODE_FAILURE_PAGE_RESP object:nil];
}

#pragma mark -UI
#pragma mark 导航栏
-(void)setNav
{
    self.navigationItem.title = @"我的礼券";
    
    [self setRightBtnItem];
    
    [self setLeftBtnItem];
}

// 导航栏右边的Btn
-(void)setRightBtnItem
{
    UIButton *rightBtn = [[UIButton alloc]init];
    UIImage *imgMore = [UIImage imageNamed:@"more.png"];
    rightBtn.frame = CGRectMake(0, 0, imgMore.size.width, imgMore.size.height);
    [rightBtn setImage:imgMore forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(clickRightBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customRightBtnItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = customRightBtnItem;
}

// 导航栏左边的Btn
-(void)setLeftBtnItem
{
    UIButton *leftBtn = [[UIButton alloc]init];
    UIImage *leftBtnImg = [UIImage imageNamed:@"whiteBack.png"];
    leftBtn.frame = CGRectMake(0, 0, leftBtnImg.size.width, leftBtnImg.size.height);
    [leftBtn setImage:leftBtnImg forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(clickLeftBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customLeftBtnItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = customLeftBtnItem;
}

-(void)clickRightBarBtnItem
{
    YDLog(@"点击了导航栏的右边的按钮");
}

-(void)clickLeftBarBtnItem
{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setUI
{
    _mTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight) style:UITableViewStyleGrouped];
    _mTableView.backgroundColor = [UIColor clearColor];
    _mTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_mTableView];
    
    _mTableView.delegate = self;
    _mTableView.dataSource = self;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    MyGiftCertificateCell *cell = [tableView dequeueReusableCellWithIdentifier:@"giftCell"];
    if (!cell) {
        cell = [[MyGiftCertificateCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"giftCell"];
    }
    
    CouponBaseInfo *couponBaseInfo = self.listenToMeData.arrCouponBaseInfo[indexPath.section];
    UIColor *colorStyle; //颜色风格
    UIImage *detailImageStyle; //按钮背景风格
    UIImage *giftImageStyle; //礼券背景风格
    
    //判断礼券是否被使用过 0 未使用 1 使用过了
    if (couponBaseInfo.iUseWork == 0) {
        
        //判断是否是过期礼券
        if (couponBaseInfo.lEndTime > [self getNowTime]) {
            //判断优惠券类型,使用不同颜色的优惠券
            if ([couponBaseInfo.sCouponType rangeOfString:@"欢唱"].location != NSNotFound) {
                //欢唱券
                colorStyle = _colorArray[0];
                detailImageStyle = _detailBtnImgArray[0];
                giftImageStyle = _giftBgImgArray[0];
            }
            
            if ([couponBaseInfo.sCouponType rangeOfString:@"代金"].location != NSNotFound) {
                colorStyle = _colorArray[1];
                detailImageStyle = _detailBtnImgArray[1];
                giftImageStyle = _giftBgImgArray[1];
            }
            
            if ([couponBaseInfo.sCouponType rangeOfString:@"优惠"].location != NSNotFound) {
                colorStyle = _colorArray[2];
                detailImageStyle = _detailBtnImgArray[2];
                giftImageStyle = _giftBgImgArray[2];
            }
            
            if ([couponBaseInfo.sCouponType rangeOfString:@"兑换"].location != NSNotFound || [couponBaseInfo.sCouponType rangeOfString:@"爆米花"].location != NSNotFound) {
                colorStyle = _colorArray[3];
                detailImageStyle = _detailBtnImgArray[3];
                giftImageStyle = _giftBgImgArray[3];
            }
            
            cell.lbGiftName.text = couponBaseInfo.sCouponType;
            
        }else{
            //过期
            colorStyle = _colorArray[4];
            detailImageStyle = _detailBtnImgArray[4];
            giftImageStyle = _giftBgImgArray[4];
            cell.lbGiftName.text = @"过期礼券";
            
        }
        
        
    }else{
    
        //使用过了 1
        colorStyle = _colorArray[4];
        detailImageStyle = _detailBtnImgArray[4];
        giftImageStyle = _giftBgImgArray[4];
        cell.lbGiftName.text = [NSString stringWithFormat:@"%@  已使用",couponBaseInfo.sCouponType ];
    }
    
   

    cell.lbGiftName.textColor = colorStyle;
    cell.lbFromName.textColor = colorStyle;
    cell.lbValidity.textColor = colorStyle;
    [cell.btnGiftDetail setImage:detailImageStyle forState:UIControlStateNormal];
    [cell.giftBgImgView setImage:giftImageStyle];
   
    
    cell.lbSellerName.text =couponBaseInfo.stKtvInfo;
    cell.lbValidity.text = [NSString stringWithFormat:@"有效期至    %@",[self timeForDate:couponBaseInfo.lEndTime]];
    cell.lbFromName.text = [NSString stringWithFormat:@"%@赠送",couponBaseInfo.stCreateUser.sNick];
    CGSize lbFromNameSize = [self returnLabelSizeWithLabelText:cell.lbFromName.text font:cell.lbFromName.font width:cell.lbFromName.width];
    cell.lbFromName.size = lbFromNameSize;
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:bindingPhoneNumSuccess]) {
        //未绑定手机号提示绑定手机号
        self.bindingPhoneNumAlert =[[BindingPhoneNumAlertView alloc]init];
        [[[UIApplication sharedApplication]keyWindow] addSubview:self.bindingPhoneNumAlert];
        
    }else{
        
        
        MyGiftCertificateCell *cell = (MyGiftCertificateCell *)[tableView cellForRowAtIndexPath:indexPath];
        
        if (self.isPushFromPerCenter) {
            YDLog(@"从个人中心进入我的礼券");
            CouponBaseInfo *couponBaseInfo = self.listenToMeData.arrCouponBaseInfo[indexPath.section];
            //第一层循环判断是否是过期礼券
            if (couponBaseInfo.lEndTime > [self getNowTime] && couponBaseInfo.iUseWork == 0){
                //未过期则可以使用
                //在这里进入礼券详情界面
                GiftCertificateDetailVC *giftCertificateDetailVC = [[GiftCertificateDetailVC alloc]init];
                giftCertificateDetailVC.couponBaseInfo = couponBaseInfo;
                [self.navigationController pushViewController:giftCertificateDetailVC animated:YES];
                
            }else{
                
                YDLog(@"礼券已经过期(或者已经使用过了)不可使用");
                
            }
            
        
        }else{
            YDLog(@"从其他界面进入我的礼券,送礼券");
            
            CouponBaseInfo *couponBaseInfo = self.listenToMeData.arrCouponBaseInfo[indexPath.section];
            //判断是否是过期礼券
            if (couponBaseInfo.lEndTime > [self getNowTime] && couponBaseInfo.iUseWork == 0){
                //未过期则可以使用
                GivingGiftCertificateVC *givingGiftVC = [[GivingGiftCertificateVC alloc]init];
                //设置头像 和名字
//                self.receiveIcon = @"temp2.png";
//                self.givingGiftVC.receiveIcon = self.receiveIcon;
//                self.givingGiftVC.lbReceiveName.text = @"ListenToMe";
                givingGiftVC.selectCouponBaseInfo = couponBaseInfo;
                givingGiftVC.selectCell = cell;
                
                [self.navigationController pushViewController:givingGiftVC animated:YES];
                
            }else{
                
                YDLog(@"礼券已经过期(或者已经使用了)不可已赠送");
            }
            
            
        }
    }

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.listenToMeData.arrCouponBaseInfo.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 105;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 15.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

#pragma mark - 将当前时间转换成毫秒
-(int64_t)getNowTime{
    
    NSTimeInterval nowTime = [[NSDate date] timeIntervalSince1970];
    int64_t currentTime = [[NSNumber numberWithDouble:nowTime * 1000] longLongValue]; //将double转换为long long
    return currentTime;
    
}
#pragma mark - 将毫秒转换为时间日期格式
-(NSString *)timeForDate:(int64_t)time{
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:time/1000.0];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM--dd"];
    
    return [dateFormatter stringFromDate:date];
}

#pragma mark - UILabel自动计算高度方法
-(CGSize)returnLabelSizeWithLabelText:(NSString *)text font:(UIFont *)font width:(CGFloat)width{
    CGSize textSize = [text boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil].size;
    return textSize;
}

@end
