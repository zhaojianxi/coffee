//
//  MyGiftCertificateCell.m
//  ListenToMe
//
//  Created by yadong on 3/18/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "MyGiftCertificateCell.h"

@implementation MyGiftCertificateCell

-(instancetype)cellWithTableView:(UITableView *)tableView color:(UIColor *)color
{
    static NSString *identifier = @"giftCertificateCell";
    MyGiftCertificateCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[MyGiftCertificateCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    
//    _lbGiftName.textColor = color;
//    _lbFromName.textColor = color;
//    _lbValidity.textColor = color;
    // 以上的 label都是nil，为什么呢
    
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // 位置的计算使用了过多死值，如果需要，后期改
        
        // 礼券背景图片
        UIImage *viewBg = [UIImage imageNamed:@"certificateOrange.png"];
        CGFloat viewBgH = viewBg.size.height;
        CGFloat viewBgW = viewBg.size.width;
        
        CGFloat imgViewMargingX = (screenWidth - viewBgW) * 0.5; //礼券图片距离左右的间距
        
//        UIImageView *imgViewBg = [[UIImageView alloc]initWithFrame:CGRectMake(imgViewMargingX, 0, viewBgW, viewBgH)];
//        [imgViewBg setImage:viewBg];
//        [self addSubview:imgViewBg];
        self.giftBgImgView = [[UIImageView alloc]initWithFrame:CGRectMake(imgViewMargingX, 0, viewBgW, viewBgH)];
        [self addSubview:self.giftBgImgView];
        
        
        CGFloat frontX = 15 ; // 子控件距离screen的前后距离
        CGFloat lbFrontX = frontX + imgViewMargingX; // 标签距离左边的距离
        
        // 礼券名称
        _lbGiftName = [[UILabel alloc]initWithFrame:CGRectMake(lbFrontX, 15, 174, 16)];
        _lbGiftName.font = [UIFont systemFontOfSize:15.0];
//        _lbGiftName.textColor = [UIColor rgbFromHexString:@"#420666" alpaa:1.0];
        _lbGiftName.textAlignment = NSTextAlignmentLeft;
        _lbGiftName.backgroundColor = [UIColor clearColor];
        [self addSubview:_lbGiftName];
        
        // 分割横线
        UIImageView *imgLine = [[UIImageView alloc]initWithFrame:CGRectMake(_lbGiftName.x, _lbGiftName.y + _lbGiftName.height + 1.75, _lbGiftName.width, 0.5)];
        imgLine.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
        [self addSubview:imgLine];
        
        // Ktv地址
        _lbSellerName = [[UILabel alloc]initWithFrame:CGRectMake(lbFrontX, _lbGiftName.y + _lbGiftName.height + 4, 103.5, 13.5)]; // 4是礼券lb到该lb的高度差
        _lbSellerName.font = [UIFont systemFontOfSize:12.0];
        _lbSellerName.textColor = [UIColor rgbFromHexString:@"#4A4A4A" alpaa:1.0];
        _lbSellerName.textAlignment = NSTextAlignmentLeft;
        [self addSubview:_lbSellerName];
        
        // 有效期
        _lbValidity = [[UILabel alloc]initWithFrame:CGRectMake(lbFrontX, _lbSellerName.y + _lbSellerName.height + 23, 156, 13.5)];// 4是ktvlb到该lb的高度差
        _lbValidity.font = [UIFont systemFontOfSize:12.0];
//        _lbValidity.textColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
        _lbValidity.textAlignment = NSTextAlignmentLeft;
        [self addSubview:_lbValidity];
        
        // 赠送方
        CGFloat frontNameX = screenWidth - imgViewMargingX - 90; // 90是二维码放置位置的宽;
        _lbFromName = [[UILabel alloc]initWithFrame:CGRectMake(frontNameX, _lbSellerName.y, 90, 12)]; // 20是我随便加的值
        _lbFromName.font = [UIFont systemFontOfSize:10.0];
//        _lbFromName.textColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
        _lbFromName.numberOfLines = 0;
        _lbFromName.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_lbFromName];
        
//        // 二维码
//        UIImage *qrCodeImg = [UIImage imageNamed:@"qrCodeTempOrange.png"];
//        _imgQRCode = [[UIImageView alloc]initWithFrame:CGRectMake(_lbFromName.x + 20, 21, qrCodeImg.size.width, qrCodeImg.size.height)];
//        _imgQRCode.image = qrCodeImg;
//        [self addSubview:_imgQRCode];
        
        //详情
        CGFloat btnWH = 14;
        CGFloat btnX = screenWidth - imgViewMargingX - btnWH - 15;
        CGFloat btnY = 15;
        
//        UIImage *btnImg = [UIImage imageNamed:@"详情01.png"];
        self.btnGiftDetail = [[UIButton alloc]initWithFrame:CGRectMake(btnX, btnY, btnWH, btnWH)];
//        [self.btnGiftDetail setImage:btnImg forState:UIControlStateNormal];
        [self.contentView addSubview:self.btnGiftDetail];
        
    }
    
    return self;
}




@end
