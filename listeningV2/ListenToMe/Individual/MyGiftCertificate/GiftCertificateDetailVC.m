//
//  GiftCertificateDetailVC.m
//  ListenToMe
//
//  Created by zhw on 15/5/28.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "GiftCertificateDetailVC.h"

@interface GiftCertificateDetailVC ()<UIScrollViewDelegate>
/**
 *  添加一个ScrollView承载礼券详情的信息,同时,动态改变他的大小来适应屏幕
 */
@property(nonatomic,strong)UIScrollView *scrollView;
@end

@implementation GiftCertificateDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNav];
    
    [self setUI];
    
    [self initData];
    
}

#pragma mark - setNav 
-(void)setNav{
    self.navigationItem.title = [NSString stringWithFormat:@"%@的赠送",self.couponBaseInfo.stCreateUser.sNick];
    
    [self setRightBtnItem];
    
    [self setLeftBtnItem];
}

// 导航栏右边的Btn
-(void)setRightBtnItem
{
    UIButton *rightBtn = [[UIButton alloc]init];
    rightBtn.frame = CGRectMake(0, 0, 50, 40);
    [rightBtn setTitle:@"完成" forState:UIControlStateNormal];
    [rightBtn.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [rightBtn addTarget:self action:@selector(clickRightBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customRightBtnItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = customRightBtnItem;
}

// 导航栏左边的Btn
-(void)setLeftBtnItem
{
    UIButton *leftBtn = [[UIButton alloc]init];
    UIImage *leftBtnImg = [UIImage imageNamed:@"whiteBack.png"];
    leftBtn.frame = CGRectMake(0, 0, leftBtnImg.size.width, leftBtnImg.size.height);
    [leftBtn setImage:leftBtnImg forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(clickLeftBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customLeftBtnItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = customLeftBtnItem;
}

-(void)clickRightBarBtnItem
{
    YDLog(@"点击了完成");
}

-(void)clickLeftBarBtnItem
{
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark = setUI
-(void)setUI{

    self.view.backgroundColor = [UIColor rgbFromHexString:@"#F1F1F1" alpaa:1.0];

    
    //初始化ScrollView
    CGFloat scrollViewX = 15;
    CGFloat scrollViewY = 20 + naviAndStatusH;
    CGFloat scrollViewW = screenWidth - 30;
    CGFloat scrollViewH = screenHeight - naviAndStatusH - 40;
//    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(scrollViewX,scrollViewY ,scrollViewW ,scrollViewH)];
    self.scrollView = [[UIScrollView alloc] init];
    self.scrollView.frame = CGRectMake(scrollViewX, scrollViewY, scrollViewW, scrollViewH);
    self.scrollView.backgroundColor = [UIColor whiteColor];
    self.scrollView.delegate = self;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
//    self.scrollView.contentSize = CGSizeMake(scrollViewW, scrollViewH * 2);
    [self.view addSubview:self.scrollView];

    //设置scrollview要承载的视图
    [self setScrollviewSubView];
}


-(void)setScrollviewSubView{
    
    CGFloat frontX = 15;
    
    //头部视图
    CGFloat headerImgViewX = frontX;
    CGFloat headerImgViewY = self.scrollView.y - 8;
    CGFloat headerImgViewW = self.scrollView.width;
    CGFloat headerImgViewH = 8;
    UIImageView *headerImgView = [[UIImageView alloc]initWithFrame:CGRectMake(headerImgViewX, headerImgViewY, headerImgViewW, headerImgViewH)];
    UIImage *headerBgImg = [UIImage imageNamed:@"头部样式.png"];
    [headerImgView setImage:headerBgImg];
    [self.view addSubview:headerImgView];
    
    
    //设置底部视图
    CGFloat footImgViewX = frontX;
    CGFloat footImgViewY =self.scrollView.y + self.scrollView.height;
    CGFloat footImgViewW = self.scrollView.width;
    CGFloat footImgViewH = 8;
    UIImageView *footImgView = [[UIImageView alloc]initWithFrame:CGRectMake(footImgViewX, footImgViewY, footImgViewW, footImgViewH)];
    UIImage *footBgImg = [UIImage imageNamed:@"尾部背景样式.png"];
    [footImgView setImage:footBgImg];
    [self.view addSubview:footImgView];
    
    //有效期
    CGFloat lbEndTimeX = frontX;
    CGFloat lbEndTimeY = - naviAndStatusH;
    CGFloat lbEndTimeW= self.scrollView.width - frontX * 2;
    CGFloat lbEndTimeH = 44;
    UILabel *lbEndTime = [[UILabel alloc]initWithFrame:CGRectMake(lbEndTimeX, lbEndTimeY, lbEndTimeW, lbEndTimeH)];
    lbEndTime.text = [NSString stringWithFormat:@"有效日期至    %@",[self timeForDate:self.couponBaseInfo.lEndTime]];
    lbEndTime.textColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
    lbEndTime.font = [UIFont systemFontOfSize:14.0];
    [self.scrollView addSubview:lbEndTime];
    
    //分割线
    CGFloat line01X = frontX;
    CGFloat line01Y = lbEndTimeY + lbEndTimeH - 1;
    CGFloat line01W = lbEndTimeW;
    CGFloat line01H = 1;
    UIView *line01 = [[UIView alloc]initWithFrame:CGRectMake(line01X, line01Y, line01W, line01H)];
    line01.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:0.5];
    [self.scrollView addSubview:line01];
    
    //优惠券序列号
    CGFloat lbPromoCodeX = frontX;
    CGFloat lbPromoCodeY = line01.y + 1;
    CGFloat lbPromoCodeW = lbEndTimeW;
    CGFloat lbPromoCodeH = 79;
    UILabel *lbPromoCode =[[UILabel alloc]initWithFrame:CGRectMake(lbPromoCodeX, lbPromoCodeY, lbPromoCodeW, lbPromoCodeH)];
//    lbPromoCode.text = self.couponBaseInfo.lSerialnum;
    
    //分割序列号,4位一分离
    //使用一个可变数组来,存储分离出的序列号
    
    
    NSMutableArray *savePromoCodeArr = [NSMutableArray array];
    for (NSInteger index = 0; index < self.couponBaseInfo.lSerialnum.length / 4; index ++) {
        NSRange tempRange = {(index * 4),4};
        NSString *tempString = [self.couponBaseInfo.lSerialnum substringWithRange:tempRange];
        [savePromoCodeArr addObject:tempString];
    }
    
    //拼接分离的优惠券序列号
    NSMutableString *pormoCodeString = [NSMutableString string];
    for (NSInteger index = 0;index < savePromoCodeArr.count;index ++) {
        [pormoCodeString appendString:savePromoCodeArr[index]];
        if (index != savePromoCodeArr.count - 1) {
            [pormoCodeString appendString:@"-"];
        }
    }
    
    lbPromoCode.text = pormoCodeString;
    
    lbPromoCode.textColor = [UIColor rgbFromHexString:@"#000000" alpaa:1.0];
    lbPromoCode.font = [UIFont systemFontOfSize:14.0];
    [self.scrollView addSubview:lbPromoCode];
    
    //中间部分样式分割线
    CGFloat middleImgViewX = frontX;
    CGFloat middleImgViewY = lbPromoCode.y + lbPromoCode.height;
    CGFloat middleImgViewW = lbEndTimeW;
    CGFloat middleImgViewH = 3;
    UIImageView *middleImgView = [[UIImageView alloc]initWithFrame:CGRectMake(middleImgViewX, middleImgViewY, middleImgViewW, middleImgViewH)];
    UIImage *middleImg = [UIImage imageNamed:@"中间样式.png"];
    [middleImgView setImage:middleImg];
    [self.scrollView addSubview:middleImgView];
    
    //优惠券类型
    CGFloat lbCouponTypeX = frontX;
    CGFloat lbCouponTypeY = middleImgView.y + middleImgView.height;
    CGFloat lbCouponTypeW = lbEndTimeW;
    CGFloat lbCouponTypeH = 44;
    UILabel *lbCouponType = [[UILabel alloc]initWithFrame:CGRectMake(lbCouponTypeX, lbCouponTypeY, lbCouponTypeW, lbCouponTypeH)];
    lbCouponType.text = self.couponBaseInfo.sCouponType;
    lbCouponType.textColor = [UIColor purpleColor];
    lbCouponType.font = [UIFont systemFontOfSize:15.0];
    [self.scrollView addSubview:lbCouponType];
    
    //分割线
    CGFloat line02X = frontX;
    CGFloat line02Y = lbCouponType.y + lbCouponType.height - 1;
    CGFloat line02W = lbEndTimeW;
    CGFloat line02H = 1;
    UIView *line02 = [[UIView alloc]initWithFrame:CGRectMake(line02X, line02Y, line02W, line02H)];
    line02.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:0.5];
    [self.scrollView addSubview:line02];
    
    //KTV店铺名
    CGFloat lbKtvNameX = frontX;
    CGFloat lbKtvNameY = line02.y + 1;
    CGFloat lbKtvNameW = lbEndTimeW;
    CGFloat lbKtvNameH = 34;
    UILabel *lbKtvName = [[UILabel alloc]initWithFrame:CGRectMake(lbKtvNameX, lbKtvNameY, lbKtvNameW, lbKtvNameH)];
    lbKtvName.text = self.couponBaseInfo.stKtvInfo;
    lbKtvName.textColor = [UIColor purpleColor];
    lbKtvName.font = [UIFont systemFontOfSize:12.0];
    [self.scrollView addSubview:lbKtvName];
    
    //分割线
    CGFloat line03X = frontX;
    CGFloat line03Y = lbKtvName.y + lbKtvName.height - 1;
    CGFloat line03W = lbEndTimeW;
    CGFloat line03H = 1;
    UIView *line03 = [[UIView alloc]initWithFrame:CGRectMake(line03X, line03Y, line03W, line03H)];
    line03.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:0.5];
    [self.scrollView addSubview:line03];
    
    //使用方法
    CGFloat lbUseMethodX = frontX;
    CGFloat lbUseMethodY = line03.y + 1;
    CGFloat lbUseMethodW = lbEndTimeW;
    CGFloat lbUseMethodH = 44;
    UILabel *lbUseMethod = [[UILabel alloc]initWithFrame:CGRectMake(lbUseMethodX, lbUseMethodY, lbUseMethodW, lbUseMethodH)];
    lbUseMethod.text = @"使用方法";
    lbUseMethod.textColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
    lbUseMethod.font = [UIFont systemFontOfSize:15.0];
    [self.scrollView addSubview:lbUseMethod];
    
    //分割线
    CGFloat line04X = frontX;
    CGFloat line04Y = lbUseMethod.y + lbUseMethod.height - 1;
    CGFloat line04W = lbEndTimeW;
    CGFloat line04H = 1;
    UIView *line04 = [[UIView alloc]initWithFrame:CGRectMake(line04X, line04Y, line04W, line04H)];
    line04.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:0.5];
    [self.scrollView addSubview:line04];
    
    //使用礼券按钮
    CGFloat btnUseCouponX = frontX;
    CGFloat btnUseCouponY = line04.y + 1 + 15;
    CGFloat btnUseCouponW = lbEndTimeW;
    CGFloat btnUseCouponH = 40;
    UIButton *btnUseCoupon = [[UIButton alloc] initWithFrame:CGRectMake(btnUseCouponX, btnUseCouponY, btnUseCouponW, btnUseCouponH)];
    [btnUseCoupon setImage:[UIImage imageNamed:@"background06.png"] forState:UIControlStateNormal];
    [btnUseCoupon setTitle:@"使用礼券" forState:UIControlStateNormal];
    [btnUseCoupon setTitleColor:[UIColor rgbFromHexString:@"#FFFFFF" alpaa:1.0] forState:UIControlStateNormal];
    [btnUseCoupon setTitleEdgeInsets:UIEdgeInsetsMake(0, -(btnUseCouponW * 1.35), 0, 0)];
    [btnUseCoupon.titleLabel setFont:[UIFont systemFontOfSize:15.0]];
    [self.scrollView addSubview:btnUseCoupon];
    
    //有效期  9B9B9B
    CGFloat lbEffectiveTitleX = frontX;
    CGFloat lbEffectiveTitleY = btnUseCoupon.y + btnUseCoupon.height + 15;
    CGFloat lbEffectiveTitleW = lbEndTimeW;
    CGFloat lbEffectiveTitleH = 34;
    UILabel *lbEffectiveTitle = [[UILabel alloc]initWithFrame:CGRectMake(lbEffectiveTitleX, lbEffectiveTitleY, lbEffectiveTitleW, lbEffectiveTitleH)];
    lbEffectiveTitle.text = @"有效期";
    [lbEffectiveTitle setTextColor:[UIColor rgbFromHexString:@"#9B9B9B" alpaa:1.0]];
    [lbEffectiveTitle setFont:[UIFont systemFontOfSize:12.0]];
    [self.scrollView addSubview:lbEffectiveTitle];
    
    
    CGFloat lbEffectiveDateX = frontX;
    CGFloat lbEffectiveDateY =  lbEffectiveTitle.y + lbEffectiveTitle.height + 15;
    CGFloat lbEffectiveDateW = lbEndTimeW;
    CGFloat lbEffectiveDateH = 34;
    UILabel *lbEffectiveDate = [[UILabel alloc]initWithFrame:CGRectMake(lbEffectiveDateX, lbEffectiveDateY, lbEffectiveDateW, lbEffectiveDateH)];
    lbEffectiveDate.text = self.couponBaseInfo.sOtherTime;
    [lbEffectiveDate setTextColor:[UIColor rgbFromHexString:@"#9B9B9B" alpaa:0.8]];
    [lbEffectiveDate setFont:[UIFont systemFontOfSize:12.0]];
    [self.scrollView addSubview:lbEffectiveDate];
    
    //除外日期
    
    CGFloat lbUnusedTitleX = frontX;
    CGFloat lbUnusedTitleY = lbEffectiveDate.y + lbEffectiveDate.height + 15;
    CGFloat lbUnusedTitleW = lbEndTimeW;
    CGFloat lbUnusedTitleH = 34;
    UILabel *lbUnusedTitle = [[UILabel alloc]initWithFrame:CGRectMake(lbUnusedTitleX, lbUnusedTitleY, lbUnusedTitleW, lbUnusedTitleH)];
    lbUnusedTitle.text = @"除外日期";
    [lbUnusedTitle setTextColor:[UIColor rgbFromHexString:@"#9B9B9B" alpaa:1.0]];
    [lbUnusedTitle setFont:[UIFont systemFontOfSize:12.0]];
    [self.scrollView addSubview:lbUnusedTitle];
    
    CGFloat lbUnusedDateX = frontX;
    CGFloat lbUnusedDateY = lbUnusedTitle.y + lbUnusedTitle.height + 15;
    CGFloat lbUnusedDateW = lbEndTimeW;
//    CGFloat lbUnusedDateH = 34;
    UILabel *lbUnusedDate = [[UILabel alloc]init];
    //后台没有这个数据先添加临时数据
    NSString *string =@"2014-10-01至2014-10-07、2015-01-01至2015-01-03、2015-02-18至2015-02-24不可用";

    lbUnusedDate.numberOfLines = 0;
    [lbUnusedDate setTextColor:[UIColor rgbFromHexString:@"#9B9B9B" alpaa:0.8]];
    [lbUnusedDate setFont:[UIFont systemFontOfSize:12.0]];
    CGSize lbUnusedDateSize = [self returnLabelSizeWithLabelText:string font:lbUnusedDate.font width:lbUnusedDateW];
    lbUnusedDate.frame = CGRectMake(lbUnusedDateX, lbUnusedDateY, lbUnusedDateW, lbUnusedDateSize.height);
    lbUnusedDate.text = string;
    [self.scrollView addSubview:lbUnusedDate];
    
    //使用时间
    CGFloat lbUseTitleX = frontX;
    CGFloat lbUseTitleY = lbUnusedDate.y + lbUnusedDate.height + 15;
    CGFloat lbUseTitleW = lbEndTimeW;
    CGFloat lbUseTitleH = 34;
    UILabel *lbUseTitle = [[UILabel alloc]initWithFrame:CGRectMake(lbUseTitleX, lbUseTitleY, lbUseTitleW, lbUseTitleH)];
    lbUseTitle.text = @"使用时间";
    [lbUseTitle setTextColor:[UIColor rgbFromHexString:@"#9B9B9B" alpaa:1.0]];
    [lbUseTitle setFont:[UIFont systemFontOfSize:12.0]];
    [self.scrollView addSubview:lbUseTitle];
    
    
    
    CGFloat lbUseDateX = frontX;
    CGFloat lbUseDateY = lbUseTitle.y + lbUseTitle.height + 15;
    CGFloat lbUseDateW = lbEndTimeW;
    CGFloat lbUseDateH = 34;
    UILabel *lbUseDate = [[UILabel alloc]initWithFrame:CGRectMake(lbUseDateX, lbUseDateY, lbUseDateW, lbUseDateH)];
    lbUseDate.text = self.couponBaseInfo.sUseTime;
    [lbUseDate setTextColor:[UIColor rgbFromHexString:@"#9B9B9B" alpaa:0.8]];
    [lbUseDate setFont:[UIFont systemFontOfSize:12.0]];
    [self.scrollView addSubview:lbUseDate];
    
    
    //预约信息
    CGFloat lbReserTitleX = frontX;
    CGFloat lbReserTitleY = lbUseDate.y + lbUseDate.height + 15;
    CGFloat lbReserTitleW = lbEndTimeW;
    CGFloat lbReserTitleH = 34;
    UILabel *lbReserTitle = [[UILabel alloc]initWithFrame:CGRectMake(lbReserTitleX, lbReserTitleY, lbReserTitleW, lbReserTitleH)];
    lbReserTitle.text = @"预约信息";
    [lbReserTitle setTextColor:[UIColor rgbFromHexString:@"#9B9B9B" alpaa:1.0]];
    [lbReserTitle setFont:[UIFont systemFontOfSize:12.0]];
    [self.scrollView addSubview:lbReserTitle];
    
    
    //lbReserMessage
    
    CGFloat lbReserMessageX = frontX;
    CGFloat lbReserMessageY = lbReserTitle.y + lbReserTitle.height + 15;
    CGFloat lbReserMessageW = lbEndTimeW;
//    UILabel *lbReserMessage = [[UILabel alloc]initWithFrame:CGRectMake(lbReserMessageX, lbReserMessageY, lbReserMessageW, 34)];
    UILabel *lbReserMessage = [[UILabel alloc]init];
    lbReserMessage.text = self.couponBaseInfo.sReserInfo;
    [lbReserMessage setTextColor:[UIColor rgbFromHexString:@"#9B9B9B" alpaa:0.8]];
    [lbReserMessage setFont:[UIFont systemFontOfSize:12.0]];
    CGSize lbReserMessageSize = [self returnLabelSizeWithLabelText:lbReserMessage.text font:lbReserMessage.font width:lbReserMessageW];
    lbReserMessage.frame = CGRectMake(lbReserMessageX, lbReserMessageY, lbReserMessageW, lbReserMessageSize.height);
    [self.scrollView addSubview:lbReserMessage];
    
    
    //规则提醒
    CGFloat lbRulesTitleX = frontX;
    CGFloat lbRulesTitleY= lbReserMessage.y + lbReserMessage.height + 15;
    CGFloat lbRulesTitleW = lbEndTimeW;
    CGFloat lbRulesTitleH = 34;
    UILabel *lbRulesTitle = [[UILabel alloc]initWithFrame:CGRectMake(lbRulesTitleX, lbRulesTitleY, lbRulesTitleW, lbRulesTitleH)];
    lbRulesTitle.text = @"规则提醒";
    [lbRulesTitle setTextColor:[UIColor rgbFromHexString:@"#9B9B9B" alpaa:1.0]];
    [lbRulesTitle setFont:[UIFont systemFontOfSize:12.0]];
    [self.scrollView addSubview:lbRulesTitle];
    
    CGFloat lbRulesMessageX = frontX;
    CGFloat lbRulesMessageY = lbRulesTitle.y + lbRulesTitle.height + 15;
    CGFloat lbRulesMessageW = lbEndTimeW;
    UILabel *lbRulesMessage = [[UILabel alloc]init];
    lbRulesMessage.text = @"可累计使用,不兑现,不找零\n不再与店内其它优惠同享";
    lbRulesMessage.numberOfLines = 0;
    [lbRulesMessage setTextColor:[UIColor rgbFromHexString:@"#9B9B9B" alpaa:0.8]];
    [lbRulesMessage setFont:[UIFont systemFontOfSize:12.0]];
    CGSize lbRulesMessageSize = [self returnLabelSizeWithLabelText:lbRulesMessage.text font:lbRulesMessage.font width:lbRulesMessageW];
    lbRulesMessage.frame = CGRectMake(lbRulesMessageX, lbRulesMessageY, lbRulesMessageW, lbRulesMessageSize.height);
    [self.scrollView addSubview:lbRulesMessage];
    
    //温馨提示
    CGFloat lbPromptTitleX = frontX;
    CGFloat lbPromptTitleY = lbRulesMessage.y + lbRulesMessage.height + 15;
    CGFloat lbPromptTitleW = lbEndTimeW;
    CGFloat lbPromptTitleH = 34;
    UILabel *lbPromptTitle = [[UILabel alloc]initWithFrame:CGRectMake(lbPromptTitleX, lbPromptTitleY, lbPromptTitleW, lbPromptTitleH)];
    lbPromptTitle.text = @"温馨提示";
    [lbPromptTitle setTextColor:[UIColor rgbFromHexString:@"#9B9B9B" alpaa:1.0]];
    [lbPromptTitle setFont:[UIFont systemFontOfSize:12.0]];
    [self.scrollView addSubview:lbPromptTitle];
    
    CGFloat lbPromptMessageX = frontX;
    CGFloat lbPromptMessageY = lbPromptTitle.y + lbPromptTitle.height + 15;
    CGFloat lbPromptMessageW = lbEndTimeW;
    UILabel *lbPromptMessage = [[UILabel alloc]init];
    lbPromptMessage.text = @"如需团购券发票,请您在消费是向商户咨询";
    [lbPromptMessage setTextColor:[UIColor rgbFromHexString:@"#9B9B9B" alpaa:0.8]];
    [lbPromptMessage setFont:[UIFont systemFontOfSize:12.0]];
    lbPromptMessage.numberOfLines = 0;
    CGSize lbPromptMessageSize = [self returnLabelSizeWithLabelText:lbPromptMessage.text font:lbPromptMessage.font width:lbPromptMessageW];
    lbPromptMessage.frame = CGRectMake(lbPromptMessageX, lbPromptMessageY, lbPromptMessageW, lbPromptMessageSize.height);
    
    [self.scrollView addSubview:lbPromptMessage];
    
    
    
    //设置ScrollView的大小
    self.scrollView.contentSize = CGSizeMake(self.scrollView.width, lbPromptMessage.y + lbPromptMessage.height+ 20);
    
    
}

#pragma mark - initData

-(void)initData{
    
}


#pragma mark - 将毫秒转换为时间日期格式
-(NSString *)timeForDate:(int64_t)time{
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:time/1000.0];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM--dd"];
    
    return [dateFormatter stringFromDate:date];
}


#pragma mark - UILabel自动计算高度方法
-(CGSize)returnLabelSizeWithLabelText:(NSString *)text font:(UIFont *)font width:(CGFloat)width{
    CGSize textSize = [text boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil].size;
    return textSize;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
