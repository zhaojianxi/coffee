//
//  GiftCertificateDetailVC.h
//  ListenToMe
//
//  Created by zhw on 15/5/28.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "YDBaseVC.h"

@interface GiftCertificateDetailVC : YDBaseVC
/**
 *  礼券信息
 */
@property(nonatomic,strong) CouponBaseInfo *couponBaseInfo;
@end
