//
//  GivingGiftCertificateVC.h
//  ListenToMe
//
//  Created by zhw on 15/4/27.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "YDBaseVC.h"
#import "MyGiftCertificateCell.h"
@interface GivingGiftCertificateVC : YDBaseVC
/**
 *  礼券背景
 */
@property(nonatomic,strong)UIImageView *giftBgImageView;
/**
 * 礼券名称
 */
@property(nonatomic,strong) UILabel *lbGiftName;
/**
 * KTV地址
 */
@property(nonatomic,strong) UILabel *lbSellerName;
/**
 * 有效期
 */
@property(nonatomic,strong) UILabel *lbValidity;
/**
 * 二维码
 */
@property(nonatomic,strong) UIImageView *imgQRCode;
/**
 * 赠送方
 */
@property(nonatomic,strong) UILabel *lbFromName;
/**
 *  赠送张数
 */
@property(nonatomic,strong) UIButton *btnGiftCount;
/**
 *  收礼券的头像
 */
@property(nonatomic,strong) UIButton *btnReceiveIcon;
/**
 *  收礼券的人名
 */
@property(nonatomic,strong) UILabel *lbReceiveName;
/**
 *  送礼券数量减一
 */
@property(nonatomic,strong) UIButton *btnSubGNum;
/**
 *  送礼券数量加一
 */
@property(nonatomic,strong) UIButton *btnAddGNum;
/**
 *  当前要送的礼券数量
 */
@property(nonatomic,strong) UIButton *btnSendGNum;
/**
 *  剩余礼券张数
 */
@property(nonatomic,strong) UILabel *lbRemainGNum;
/**
 *  选中的礼券
 */
@property(nonatomic,strong) MyGiftCertificateCell *selectCell;
/**
 *  定义一个接收头像的字符串
 */
@property(nonatomic,retain) NSString *receiveIcon;
/**
 *  选中要赠送的礼券
 */
@property(nonatomic,retain)CouponBaseInfo *selectCouponBaseInfo;


/**
 *  设置label中显示不同颜色的字
 *
 *  @param label       label
 *  @param text        text
 *  @param rangeString range
 *  @param color       color
 */
-(void)setLbTextForDifferentColor:(UILabel *)label text:(NSString *)text rangeString:(NSString *)rangeString color:(UIColor *)color;

@end
