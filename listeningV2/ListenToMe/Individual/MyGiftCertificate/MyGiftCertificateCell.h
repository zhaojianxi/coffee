//
//  MyGiftCertificateCell.h
//  ListenToMe
//
//  Created by yadong on 3/18/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface MyGiftCertificateCell : UITableViewCell
/**
 *  礼券的背景图片
 */
@property(strong,nonatomic) UIImageView *giftBgImgView;
/**
 * 礼券名称
 */
@property(strong,nonatomic) UILabel *lbGiftName;
/**
* KTV地址
*/
@property(strong,nonatomic) UILabel *lbSellerName;
/**
* 有效期
*/
@property(strong,nonatomic) UILabel *lbValidity;
/**
* 二维码
*/
@property(strong,nonatomic) UIImageView *imgQRCode;
/**
* 赠送方
*/
@property(strong,nonatomic) UILabel *lbFromName;
/**
 *  查看优惠券详情
 */
@property(strong,nonatomic) UIButton *btnGiftDetail;

-(instancetype)cellWithTableView:(UITableView *)tableView color:(UIColor *)color;
@end
