//
//  GivingGiftCertificateVC.m
//  ListenToMe
//
//  Created by zhw on 15/4/27.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "GivingGiftCertificateVC.h"
#import "FriendsMsgVC.h"
@interface GivingGiftCertificateVC ()
/**
 *  送礼券底部视图
 */
@property(nonatomic,strong)UIView *givingGiftView;
/**
 *  默认送出的礼券数
 */
@property(assign,nonatomic) NSInteger sendGiftNum;
/**
 *  用户拥有礼券数
 */
@property(assign,nonatomic) NSInteger haveGiftNum;
/**
 *  数据
 */
@property(nonatomic,strong) ListenToMeData *listenToMeData;
@end

@implementation GivingGiftCertificateVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setNav];
    
    
    
    [self initData];
}

#pragma mark - initData 初始化数据
-(void)initData{
    self.listenToMeData = [ListenToMeData getInstance];
    
    //获取相同类型的礼券的请求
    [[NetReqManager getInstance] sendGetTypeAllNum:[ListenToMeDBManager getUuid] SCouponType:self.selectCouponBaseInfo.sCouponType SCouponAdress:self.selectCouponBaseInfo.stKtvInfo LEndTime:self.selectCouponBaseInfo.lEndTime];
    
    //获取相同类型的礼券的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setSameCouponBaseNum) name:NOTIFY_GET_TYPEALLNUM_PAGE_RESP object:nil];
}

-(void)setSameCouponBaseNum{
    [self setUI];

}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_TYPEALLNUM_PAGE_RESP object:nil];
}

#pragma mark 设置CustomNavigation
-(void)setNav{
    self.navigationItem.title = @"赠送礼券";
    
    [self setLeftBtnItem];
}

// 导航栏左边的Btn
-(void)setLeftBtnItem
{
    UIButton *leftBtn = [[UIButton alloc]init];
    UIImage *leftBtnImg = [UIImage imageNamed:@"whiteBack.png"];
    leftBtn.frame = CGRectMake(0, 0, leftBtnImg.size.width, leftBtnImg.size.height);
    [leftBtn setImage:leftBtnImg forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(clickLeftItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customLeftBtnItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = customLeftBtnItem;
}


-(void)clickLeftItem
{
    
    [self.navigationController popViewControllerAnimated:YES];
}


-(instancetype)init{
    self = [super init];
    if (self) {
//        [self setUI];
    }
    
    return self;
}


-(void)setUI{
    
    self.givingGiftView = [[UIView alloc]initWithFrame:CGRectMake(0, naviAndStatusH , screenWidth, screenHeight - naviAndStatusH)];
//    self.givingGiftView.backgroundColor = [UIColor orangeColor];
    
    [self.view addSubview:self.givingGiftView];
    
    CGFloat frontX = 15;
    CGFloat giftBgY = 10 ;
    CGFloat giftBgW = screenWidth - frontX * 2;
    CGFloat giftBgH = 105;
    self.giftBgImageView = [[UIImageView alloc]initWithFrame:CGRectMake(frontX, giftBgY, giftBgW, giftBgH)];
    self.giftBgImageView.image = self.selectCell.giftBgImgView.image;
    [self.givingGiftView addSubview:self.giftBgImageView];
    
    
    CGFloat lbFrontX = frontX ; // 标签距离左边的距离
    CGFloat lbGiftNameY =  15;
    // 礼券名称
    _lbGiftName = [[UILabel alloc]initWithFrame:CGRectMake(lbFrontX, lbGiftNameY, 174, 16)];
    _lbGiftName.font = [UIFont systemFontOfSize:15.0];
    //        _lbGiftName.textColor = [UIColor rgbFromHexString:@"#420666" alpaa:1.0];
    _lbGiftName.textAlignment = NSTextAlignmentLeft;
    _lbGiftName.backgroundColor = [UIColor clearColor];
    self.lbGiftName.text = self.selectCell.lbGiftName.text;
    self.lbGiftName.textColor = self.selectCell.lbGiftName.textColor;
    [self.giftBgImageView addSubview:_lbGiftName];
    
    // 分割横线
    UIImageView *imgLine1 = [[UIImageView alloc]initWithFrame:CGRectMake(_lbGiftName.x, _lbGiftName.y + _lbGiftName.height + 1.75, _lbGiftName.width, 0.5)];
    imgLine1.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
    [self.giftBgImageView addSubview:imgLine1];
    
    // Ktv地址
    _lbSellerName = [[UILabel alloc]initWithFrame:CGRectMake(lbFrontX, _lbGiftName.y + _lbGiftName.height + 4, 103.5, 13.5)]; // 4是礼券lb到该lb的高度差
    _lbSellerName.font = [UIFont systemFontOfSize:12.0];
    _lbSellerName.textColor = [UIColor rgbFromHexString:@"#4A4A4A" alpaa:1.0];
    _lbSellerName.textAlignment = NSTextAlignmentLeft;
    self.lbSellerName.text = self.selectCell.lbSellerName.text;
    self.lbSellerName.textColor = self.selectCell.lbSellerName.textColor;
    [self.giftBgImageView addSubview:_lbSellerName];
    
    // 有效期
    _lbValidity = [[UILabel alloc]initWithFrame:CGRectMake(lbFrontX, _lbSellerName.y + _lbSellerName.height + 23, 156, 13.5)];// 4是ktvlb到该lb的高度差
    _lbValidity.font = [UIFont systemFontOfSize:12.0];
    //        _lbValidity.textColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
    _lbValidity.textAlignment = NSTextAlignmentLeft;
    self.lbValidity.text = self.selectCell.lbValidity.text;
    self.lbValidity.textColor = self.selectCell.lbValidity.textColor;
    [self.giftBgImageView addSubview:_lbValidity];
    
    // 赠送方
    CGFloat frontNameX = screenWidth - frontX * 2 - 90; // 90是右侧空白的宽度;
//    _lbFromName = [[UILabel alloc]initWithFrame:CGRectMake(frontNameX, _lbSellerName.y, 90, 12)]; // 20是我随便加的值
    _lbFromName = [[UILabel alloc]init];
    _lbFromName.font = [UIFont systemFontOfSize:10.0];
    //        _lbFromName.textColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
    _lbFromName.textAlignment = NSTextAlignmentCenter;
    _lbFromName.numberOfLines = 0;
    self.lbFromName.text = self.selectCell.lbFromName.text;
    self.lbFromName.textColor = self.selectCell.lbFromName.textColor;
    CGSize lbFromNameSize = [self returnLabelSizeWithLabelText:self.lbFromName.text font:self.lbFromName.font width:90];
    self.lbFromName.frame =CGRectMake(frontNameX, _lbSellerName.y, 90, lbFromNameSize.height);
    [self.giftBgImageView addSubview:_lbFromName];
    
    
    //剩余张数
    CGFloat btnW = 50;
    CGFloat btnH = 20;
    CGFloat btnX = screenWidth - frontX * 2 - btnW ;
    CGFloat btnY = 15;
    self.btnGiftCount = [[UIButton alloc]initWithFrame:CGRectMake(btnX, btnY, btnW, btnH)];
    [self.btnGiftCount.titleLabel setFont:[UIFont systemFontOfSize:10.0]];
    [self.btnGiftCount setTitleColor:self.lbFromName.textColor forState:UIControlStateNormal ];
    [self.giftBgImageView addSubview:self.btnGiftCount];
    
    //分割线
    CGFloat imageLine2_Y = self.giftBgImageView.y + self.giftBgImageView.height + 15;
    CGFloat imageLine_W = screenWidth - frontX * 2;
    CGFloat imageLine_H = 1;
    UIImageView *imageLine2 = [[UIImageView alloc]initWithFrame:CGRectMake(frontX, imageLine2_Y, imageLine_W, imageLine_H)];
    imageLine2.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:0.12];
    [self.givingGiftView addSubview:imageLine2];
    
    UIImageView *imageLine3 = [[UIImageView alloc]initWithFrame:CGRectMake(frontX, imageLine2_Y + 84, imageLine_W, imageLine_H)];
    imageLine3.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:0.12];
    [self.givingGiftView addSubview:imageLine3];
    
    //头像
    
    CGFloat btnReceiveWH = 40;
    CGFloat btnReceiveY = imageLine2.y + imageLine_H + 22;
    self.btnReceiveIcon = [[UIButton alloc]initWithFrame:CGRectMake(frontX, btnReceiveY, btnReceiveWH, btnReceiveWH)];
    self.btnReceiveIcon.layer.masksToBounds = YES;
    self.btnReceiveIcon.layer.cornerRadius = 5;
    
    UIImageView *receiveIconView = [[UIImageView alloc]initWithFrame:self.btnReceiveIcon.bounds];
    [receiveIconView sd_setImageWithURL:[NSURL URLWithString:[ListenToMeData getInstance].stUserBaseInfoNet.sCovver] placeholderImage:[UIImage imageNamed:@"icon.png"]];
    receiveIconView.userInteractionEnabled = YES;
    [self.btnReceiveIcon addSubview:receiveIconView];
    
    [self.givingGiftView addSubview:self.btnReceiveIcon];
    
    //名字
    CGFloat lbReceiveNameX = self.btnReceiveIcon.x + btnReceiveWH + 10;
    CGFloat lbReceiveNameW = 100;
    CGFloat lbReceiveNameH = 30;
    CGFloat lbReceiveNameY = imageLine2.y + imageLine_H + (88 - lbReceiveNameH) * 0.5;
    self.lbReceiveName = [[UILabel alloc]initWithFrame:CGRectMake(lbReceiveNameX, lbReceiveNameY, lbReceiveNameW, lbReceiveNameH)];
    [self.lbReceiveName setTextColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
    [self.lbReceiveName setFont:[UIFont systemFontOfSize:12.0]];
    [self.givingGiftView addSubview:self.lbReceiveName];
    
    //调整送礼券的控制框
    UIImage *sendGiftBgImg = [UIImage imageNamed:@"控制框.png"];
    CGFloat sendGiftW = sendGiftBgImg.size.width * 0.5;
    CGFloat sendGiftH = sendGiftBgImg.size.height * 0.5;
    CGFloat sendGiftX = (screenWidth - 15 - sendGiftW);
    CGFloat sendGiftY = imageLine2.y + imageLine_H + 25;
    UIImageView *sendGiftBgView = [[UIImageView alloc]initWithFrame:CGRectMake(sendGiftX, sendGiftY, sendGiftW, sendGiftH)];
    [sendGiftBgView setImage:sendGiftBgImg];
    sendGiftBgView.userInteractionEnabled = YES;
    [self.givingGiftView addSubview:sendGiftBgView];
    
    //减一
    
    CGFloat btnAddSubW_H =  sendGiftBgView.height;
    self.btnSubGNum = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, btnAddSubW_H, btnAddSubW_H)];
    [sendGiftBgView addSubview:self.btnSubGNum];
    
    UIImage *subImg = [UIImage imageNamed:@"减.png"];
    CGFloat subViewW = subImg.size.width * 0.5;
    CGFloat subViewH = subImg.size.height * 0.5;
    CGFloat subViewX = (self.btnSubGNum.width - subImg.size.width * 0.5) * 0.5;
    CGFloat subViewY = (self.btnSubGNum.height - subImg.size.height * 0.5) * 0.5;
    UIImageView *subView = [[UIImageView alloc]initWithFrame:CGRectMake(subViewX, subViewY, subViewW, subViewH)];
    subView.image = subImg;
    subView.userInteractionEnabled = YES;
    [self.btnSubGNum addSubview:subView];
    
    
    //加一
    self.btnAddGNum = [[UIButton alloc]initWithFrame:CGRectMake(sendGiftBgView.width - btnAddSubW_H, 0, btnAddSubW_H, btnAddSubW_H)];
    [sendGiftBgView addSubview:self.btnAddGNum];
    
    UIImage *addImg = [UIImage imageNamed:@"加.png"];
    CGFloat addViewX = (self.btnSubGNum.width - addImg.size.width * 0.5) * 0.5;
    CGFloat addViewY = (self.btnSubGNum.height - addImg.size.height * 0.5) * 0.5;
    CGFloat addViewW = addImg.size.width * 0.5;
    CGFloat addViewH = addImg.size.height * 0.5;
    UIImageView *addView = [[UIImageView alloc]initWithFrame:CGRectMake(addViewX, addViewY,addViewW, addViewH)];
    addView.image = addImg;
    addView.userInteractionEnabled = YES;
    [self.btnAddGNum addSubview:addView];
    
    //当前要送礼券的张数
    
    self.btnSendGNum = [[UIButton alloc]initWithFrame:CGRectMake(btnAddSubW_H, 0, sendGiftBgView.width - btnAddSubW_H * 2, btnAddSubW_H)];
    [self.btnSendGNum setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateNormal];
    [self.btnSendGNum.titleLabel setFont:[UIFont systemFontOfSize:12.0]];
    [sendGiftBgView addSubview:self.btnSendGNum];
    
    //中间的竖线
    UIImage *lineImg = [UIImage imageNamed:@"分割线.png"];
    for (int index = 0; index < 2; index ++) {
        UIImageView *lineImgView = [[UIImageView alloc]initWithFrame:CGRectMake(btnAddSubW_H - 1 + self.btnSendGNum.width * index  , 0, 1, btnAddSubW_H)];
        [lineImgView setImage:lineImg];
        [sendGiftBgView addSubview:lineImgView];
    }
    
    //剩余的礼券
    CGFloat lbRemainX = frontX;
    CGFloat lbRemainY = imageLine3.y + 15;
    CGFloat lbRemainW = screenWidth - frontX * 2;
    CGFloat lbRemainH = 30;
    self.lbRemainGNum = [[UILabel alloc]initWithFrame:CGRectMake(lbRemainX, lbRemainY, lbRemainW, lbRemainH)];
    [self.lbRemainGNum setFont:[UIFont systemFontOfSize:12.0]];
    [self.lbRemainGNum setTextColor:[UIColor lightGrayColor]];
    [self.lbRemainGNum setTextAlignment:NSTextAlignmentRight];
    [self.givingGiftView addSubview:self.lbRemainGNum];
    
    //赠送
    UIButton *btnGivingGift = [[UIButton alloc]initWithFrame:CGRectMake(frontX, self.givingGiftView.height - 40 - 40, self.givingGiftView.width - frontX * 2, 40)];
    btnGivingGift.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
    [self.givingGiftView addSubview:btnGivingGift];
    [btnGivingGift setTitle:@"赠送" forState:UIControlStateNormal];
    btnGivingGift.layer.masksToBounds = YES;
    btnGivingGift.layer.cornerRadius = 5;
    [btnGivingGift setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnGivingGift addTarget:self action:@selector(backToChatView) forControlEvents:UIControlEventTouchUpInside];
    [btnGivingGift.titleLabel setFont:[UIFont systemFontOfSize:15.0]];
    
    
    //设置送礼券的数量了当前礼券数
    self.sendGiftNum = 1;
    self.haveGiftNum = self.listenToMeData.sameCouponBaseInfoArr.count;
    
    
    [self.btnSendGNum setTitle:[NSString stringWithFormat:@"%ld",self.sendGiftNum] forState:UIControlStateNormal];
    
    [self.btnGiftCount setTitle:[NSString stringWithFormat:@"%ld张",self.listenToMeData.sameCouponBaseInfoArr.count] forState:UIControlStateNormal];
    
    [self setLbTextForDifferentColor:self.lbRemainGNum text:[NSString stringWithFormat:@"剩余%ld张",(self.haveGiftNum - self.sendGiftNum)] rangeString:[NSString stringWithFormat:@"%ld",(self.haveGiftNum - self.sendGiftNum)] color:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
    
    [self.btnSubGNum addTarget:self action:@selector(subGiftNum) forControlEvents:UIControlEventTouchUpInside];
    [self.btnAddGNum addTarget:self action:@selector(addGiftNum) forControlEvents:UIControlEventTouchUpInside];
    
    
}


#pragma mark -加一 减一 礼券
-(void)subGiftNum{
    
    if (self.sendGiftNum >1) {
        self.sendGiftNum -- ;
        [self.btnSendGNum setTitle:[NSString stringWithFormat:@"%ld",self.sendGiftNum] forState:UIControlStateNormal];
//        [self.btnGiftCount setTitle:[NSString stringWithFormat:@"%ld",self.sendGiftNum] forState:UIControlStateNormal];
        [self setLbTextForDifferentColor:self.lbRemainGNum text:[NSString stringWithFormat:@"剩余%ld张",(self.haveGiftNum - self.sendGiftNum)] rangeString:[NSString stringWithFormat:@"%ld",(self.haveGiftNum - self.sendGiftNum)] color:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
    }
    
}

-(void)addGiftNum{
    if (self.haveGiftNum - self.sendGiftNum > 0) {
        self.sendGiftNum ++ ;
        [self.btnSendGNum setTitle:[NSString stringWithFormat:@"%ld",self.sendGiftNum] forState:UIControlStateNormal];
//        [self.btnGiftCount setTitle:[NSString stringWithFormat:@"%ld",self.sendGiftNum] forState:UIControlStateNormal];
        [self setLbTextForDifferentColor:self.lbRemainGNum text:[NSString stringWithFormat:@"剩余%ld张",(self.haveGiftNum - self.sendGiftNum)] rangeString:[NSString stringWithFormat:@"%ld",(self.haveGiftNum - self.sendGiftNum)] color:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
    }

}

//设置头像
-(void)setReceiveIcon:(NSString *)receiveIcon{

    [self.btnReceiveIcon setImage:[UIImage imageNamed:receiveIcon] forState:UIControlStateNormal];
}




#pragma mark - 设置label中显示不同字体的颜色
-(void)setLbTextForDifferentColor:(UILabel *)label text:(NSString *)text rangeString:(NSString *)rangeString color:(UIColor *)color{
    NSMutableAttributedString *noteStr = [[NSMutableAttributedString alloc]initWithString:text];
    NSRange colorRange = NSMakeRange([[noteStr string] rangeOfString:rangeString].location, [[noteStr string]rangeOfString:rangeString].length);
    [noteStr addAttribute:NSForegroundColorAttributeName value:color range:colorRange];
    
    [label setAttributedText:noteStr];
    //    [label sizeToFit];
    
    
}

#pragma mark - UILabel自动计算高度方法
-(CGSize)returnLabelSizeWithLabelText:(NSString *)text font:(UIFont *)font width:(CGFloat)width{
    CGSize textSize = [text boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil].size;
    return textSize;
}

#pragma mark - backToChatView 返回聊天界面 ,同时发送一条赠送礼券的消息记录
-(void)backToChatView{
    //在进入送礼券界面是,已经从聊天界面push了3次,返回时直接在navigationController的视图数组里面去倒数第三个视图
    FriendsMsgVC *tempVc = (FriendsMsgVC *)[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count - 3];
    [self.navigationController popToViewController:tempVc animated:YES];
    
    NSMutableArray *tempArr = [NSMutableArray array];
    
    for (NSInteger index = 0; index < self.sendGiftNum; index ++) {
        CouponBaseInfo *couponBaseInfo = [ListenToMeData getInstance].sameCouponBaseInfoArr[index];
        NSNumber *tempNum = [NSNumber numberWithLongLong:couponBaseInfo.lCouponId];
        [tempArr addObject:tempNum];
    }
    
    NSArray *lCouponIdArr = [NSArray arrayWithArray:tempArr];
    [[NetReqManager getInstance] sendCouponToFriend:[ListenToMeDBManager getUuid] LToUserId:[ListenToMeData getInstance].privateChatUser.uuid LCouponId:lCouponIdArr];
 
   
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
