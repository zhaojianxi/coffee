//
//  MyGiftCertificateVC.h
//  ListenToMe
//
//  Created by yadong on 2/5/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyGiftCertificateVC : YDBaseVC
/**
 *  是否是从个人中心页面跳转进入的
 */
@property(nonatomic,assign)BOOL isPushFromPerCenter;
/**
 *  定义一个接收头像的字符串
 */
@property(nonatomic,retain) NSString *receiveIcon;
@end
