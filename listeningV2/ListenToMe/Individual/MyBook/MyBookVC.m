//
//  MyBookVC.m
//  ListenToMe
//
//  Created by yadong on 2/5/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "MyBookVC.h"
#import "MyCollectionShowCell.h"
#import "YDKMemmoryCell.h"
@interface MyBookVC ()<UITableViewDelegate,UITableViewDataSource>
@property(strong,nonatomic) UITableView *mTableView;
@property(nonatomic,strong) ListenToMeData *listenToMeData;
@property(nonatomic,strong) UIView *bagView;
/**
 *  当前选择的cell的位置
 */
@property(nonatomic,assign) NSInteger currentSelect;
@end

@implementation MyBookVC
#pragma mark -生命周期
-(void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setUI];
    
    self.listenToMeData = [ListenToMeData getInstance];
    
    //获取纪念册列表 ,
    [[NetReqManager getInstance] sendGetCommemorateInfoByUserId:[ListenToMeDBManager getUuid] LWatchUserId:0 IOffset:0 INum:10];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTable) name:NOTIFY_GET_COMMEMORATEINFO_BYUSERID_PAGE_RESP object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshCommemorateList) name:NOTIFY_GET_DELETE_COMMEMORATEINFO_PAGE_RESP object:nil];
    
}

-(void)refreshTable{
    [self.mTableView reloadData];
}

-(void)refreshCommemorateList{
    YDLog(@"%@",self.listenToMeData.stCommemorateBaseInfo);
    
    NSArray *tempArr = [NSArray arrayWithArray:self.listenToMeData.arrCommemorateInfoByUserId];
    for (CommemorateBaseInfo *commemorateBaseInfo in tempArr) {
        if (commemorateBaseInfo.lCommid == self.listenToMeData.stCommemorateBaseInfo.lCommid) {
            [self.listenToMeData.arrCommemorateInfoByUserId removeObject:commemorateBaseInfo];
        }
    }
    [self.mTableView reloadData];
    
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_COMMEMORATEINFO_BYUSERID_PAGE_RESP object:nil];
    
    [[NSNotificationCenter  defaultCenter] removeObserver:self name:NOTIFY_GET_DELETE_COMMEMORATEINFO_PAGE_RESP object:nil];
}

#pragma mark -UI
-(void)setUI
{
    [self setUpTableView];
    
    [self setNav];
    
    [self setDeleteView];
}

#pragma mark 删除提示视图
-(void)setDeleteView{
    
    self.bagView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 273)];
    self.bagView.backgroundColor = [UIColor colorWithRed:203/255.0 green:203/255.0 blue:203/255.0 alpha:0.5];
    
    
    CGFloat btnBagViewW = screenWidth - 30;
    CGFloat btnBagViewH = 100;
    CGFloat btnBagViewX = 15;
    CGFloat btnBagViewY = (273 - btnBagViewH) * 0.5;
    
    UIImageView *btnBagView = [[UIImageView alloc] initWithFrame:CGRectMake(btnBagViewX, btnBagViewY, btnBagViewW, btnBagViewH)];
    btnBagView.userInteractionEnabled = YES;
    [btnBagView setImage:[UIImage imageNamed:@"background.png"]];
    btnBagView.layer.masksToBounds = YES;
    btnBagView.layer.cornerRadius = 5;
    [self.bagView addSubview:btnBagView];
    
    //添加两个button 删除 取消
    CGFloat btnW = 50; //button的高度
    UIButton *btnDelete = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, btnBagViewW, btnW)];
    [btnDelete setTitle:@"删除" forState:UIControlStateNormal];
    [btnDelete setTitleColor:[UIColor rgbFromHexString:@"#FF0053" alpaa:1.0] forState:UIControlStateNormal];
    [btnDelete addTarget:self action:@selector(deleteHandle:) forControlEvents:UIControlEventTouchUpInside];
    [btnBagView addSubview:btnDelete];
    
    UIButton *btnCancle = [[UIButton alloc] initWithFrame:CGRectMake(0, btnW, btnBagViewW, btnW)];
    [btnCancle setTitle:@"取消" forState:UIControlStateNormal];
    [btnCancle setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [btnCancle addTarget:self action:@selector(cancleHandle:) forControlEvents:UIControlEventTouchUpInside];
    [btnBagView addSubview:btnCancle];
    
    UILabel *lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, btnW, btnBagViewW, 1)];
    lineLabel.backgroundColor = [UIColor lightGrayColor];
    lineLabel.alpha = 0.5;
    [btnBagView addSubview:lineLabel];
    
}


#pragma mark 导航栏
-(void)setNav
{
    self.title = @"我的纪念册";
    
    [self setRightBtnItem];
    
    [self setLeftBtnItem];
}

// 导航栏右边的Btn
-(void)setRightBtnItem
{
    UIButton *rightBtn = [[UIButton alloc]init];
    UIImage *imgMore = [UIImage imageNamed:@"more.png"];
    rightBtn.frame = CGRectMake(0, 0, imgMore.size.width, imgMore.size.height);
    [rightBtn setImage:imgMore forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(clickRightBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customRightBtnItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = customRightBtnItem;
}

// 导航栏左边的Btn
-(void)setLeftBtnItem
{
    UIButton *leftBtn = [[UIButton alloc]init];
    UIImage *leftBtnImg = [UIImage imageNamed:@"whiteBack.png"];
    leftBtn.frame = CGRectMake(0, 0, leftBtnImg.size.width, leftBtnImg.size.height);
    [leftBtn setImage:leftBtnImg forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(clickLeftBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customLeftBtnItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = customLeftBtnItem;
}

-(void)clickRightBarBtnItem
{
    YDLog(@"点击了导航栏的右边的按钮");
}

-(void)clickLeftBarBtnItem
{
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark tableview
-(void)setUpTableView
{
    _mTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight) style:UITableViewStyleGrouped];
    _mTableView.backgroundColor = [UIColor clearColor];
    _mTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_mTableView];
    
    _mTableView.delegate = self;
    _mTableView.dataSource = self;
    
}

#pragma mark delegate & datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    YDKMemmoryCell *cell = [YDKMemmoryCell cellWithTableView:tableView];
    //刷新时移除删除背景
    if (self.bagView) {
        [self.bagView removeFromSuperview];
    }
    
    CommemorateBaseInfo *commemorateBaseInfo = self.listenToMeData.arrCommemorateInfoByUserId[indexPath.section];
    cell.arrUserBaseInfoNet = commemorateBaseInfo.stUserBaseInfoNet;
    
    if ([commemorateBaseInfo.sCommIcon isEqualToString:@""] || commemorateBaseInfo == nil) {
        
        cell.imgCover.image = [UIImage imageNamed:@"temp5.png"];
    }else{
        
        [cell.imgCover sd_setImageWithURL:[NSURL URLWithString:commemorateBaseInfo.sCommIcon]];
    }
    
    cell.lbTheme.text = @"闺蜜秀";
    
    if ([commemorateBaseInfo.sCommName isEqualToString:@""] || commemorateBaseInfo.sCommName == nil) {
        cell.lbPartyName.text = @"咸蛋超人的生日趴";
    }else{
        
        cell.lbPartyName.text = commemorateBaseInfo.sCommName;
    }
    
    cell.imgTheme.image = [UIImage imageNamed:@"memoryTopShow.png"];
    [cell.btnNumSongs setTitle:[NSString stringWithFormat:@"%d",commemorateBaseInfo.iMusicNum] forState:UIControlStateNormal];
    [cell.btnNumAlbums setTitle:[NSString stringWithFormat:@"%d",commemorateBaseInfo.iPictureNum] forState:UIControlStateNormal];
    [cell.btnNumSee setTitle:[NSString stringWithFormat:@"%d",commemorateBaseInfo.iWatchedNum] forState:UIControlStateNormal];
    [cell.btnNumFlower setTitle:[NSString stringWithFormat:@"%d",commemorateBaseInfo.iFlowerNum] forState:UIControlStateNormal];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.btnActionCollect.selected = NO;
//    [cell.btnActionCollect addTarget:self action:@selector(cancleCollectHandle:) forControlEvents:UIControlEventTouchUpInside];
//    cell.btnActionCollect.tag = 100 + indexPath.section;
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    YDKMemmoryCell *cell = (YDKMemmoryCell *)[tableView cellForRowAtIndexPath:indexPath];
    [cell addSubview:self.bagView];
    self.currentSelect = indexPath.section;
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
        return 273;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.listenToMeData.arrCommemorateInfoByUserId.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 15;
}


#pragma mark - 点击删除button
-(void)deleteHandle:(UIButton *)btnDelete{
    CommemorateBaseInfo *commemorateBaseInfo = self.listenToMeData.arrCommemorateInfoByUserId[self.currentSelect];
    YDLog(@"删除选中的纪念册");
    [[NetReqManager getInstance] sendDeleteCommemorateInfo:[ListenToMeDBManager getUuid] LCommemorateId:commemorateBaseInfo.lCommid];
    
}

#pragma mark - 点击取消button
-(void)cancleHandle:(UIButton *)btnCancle{
    [self.bagView removeFromSuperview];
}


@end
