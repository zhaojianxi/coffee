//
//  MyWorksVC.m
//  ListenToMe
//
//  Created by yadong on 2/5/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "MyWorksVC.h"
#import "YDTopListCell.h"
#import "FriendsMsgVC.h"
@interface MyWorksVC ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong) ListenToMeData *listenToMeData;
/**
 *  遮盖背景视图
 */
@property(nonatomic,strong) UIView *bagView;
/**
 *  当前选择的cell的位置
 */
@property(nonatomic,assign) NSInteger currentSelect;
/**
 *  赠送歌曲的弹窗
 */
@property(nonatomic,strong) UIView *alertForGivingMusicView;
/**
 *  当前选中的要赠送的歌曲
 */
@property(nonatomic,strong) MusicWorkBaseInfo *selectMusicWorkBaseInfo;
@end


@implementation MyWorksVC
#pragma mark -生命周期
-(void)viewDidLoad
{
    [super viewDidLoad];
    [self setUI];
    
    //初始化赠送歌曲的弹窗
    [self setAlertView];
    
    [self initData];
}

#pragma mark - setAlertView初始化赠送歌曲的弹窗
-(void)setAlertView{
    self.alertForGivingMusicView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    //弹窗的半透明灰色背景
    self.alertForGivingMusicView.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.3];
    
    CGFloat frontX = 15;//控件距离左侧屏幕边缘的位置
    CGFloat alertViewW = screenWidth - frontX * 2;
    CGFloat alertViewH = 120;
    CGFloat alertViewY = (screenHeight - alertViewH) * 0.5;
    UIImageView *alertView = [[UIImageView alloc]initWithFrame:CGRectMake(frontX, alertViewY, alertViewW, alertViewH)];
    [alertView setImage:[UIImage imageNamed:@"background.png"]];
    alertView.userInteractionEnabled = YES;
    alertView.layer.masksToBounds = YES;
    alertView.layer.cornerRadius = 5;
    [self.alertForGivingMusicView addSubview:alertView];
    
    //赠送音乐的提示
    UILabel *lbAlertMessage = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, alertView.width, 75)];
    lbAlertMessage.text = @"是否将该歌曲送给TA";
    lbAlertMessage.textColor = [UIColor blackColor];
    lbAlertMessage.font = [UIFont systemFontOfSize:15.0];
    lbAlertMessage.textAlignment = NSTextAlignmentCenter;
    [alertView addSubview:lbAlertMessage];
    
    //分割线
    UIView *line01 = [[UIView alloc]initWithFrame:CGRectMake(0, lbAlertMessage.y + lbAlertMessage.height - 1, alertView.width, 1)];
    line01.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:0.8];
    [alertView addSubview:line01];
    
    //取消
    CGFloat btnW = alertView.width * 0.5;
    CGFloat btnH = alertView.height - lbAlertMessage.height;
    UIButton *btnCancle =[[UIButton alloc]initWithFrame:CGRectMake(0, line01.y + 1, btnW , btnH)];
    [btnCancle setTitle:@"取消" forState:UIControlStateNormal];
    [btnCancle setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    btnCancle.titleLabel.font =[UIFont systemFontOfSize:15.0];
    [btnCancle addTarget:self action:@selector(cancleGivingWork:) forControlEvents:UIControlEventTouchUpInside];
    [alertView addSubview:btnCancle];
    
    //分割线
    UIView *line02 = [[UIView alloc]initWithFrame:CGRectMake(btnCancle.width - 1, line01.y + 1, 1, btnH)];
    line02.backgroundColor =[UIColor rgbFromHexString:@"#AC56FF" alpaa:0.8];
    [alertView addSubview:line02];
    
    //确定
    UIButton *btnDetermine = [[UIButton alloc]initWithFrame:CGRectMake(btnW, line01.y + 1, btnW, btnH)];
    [btnDetermine setTitle:@"确定" forState:UIControlStateNormal];
    [btnDetermine setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateNormal];
    btnDetermine.titleLabel.font = [UIFont systemFontOfSize:15.0];
    [btnDetermine addTarget:self action:@selector(determineGivingWork:) forControlEvents:UIControlEventTouchUpInside];
    [alertView addSubview:btnDetermine];
    
    //赠送作品的弹窗视图要添加到主屏幕上
    [[[UIApplication sharedApplication]keyWindow] addSubview:self.alertForGivingMusicView];
    self.alertForGivingMusicView.hidden = YES;
    
}

#pragma mark -initData初始化数据
-(void)initData{
    [[NetReqManager getInstance] sendGetMusicWorkWithUserId:self.uuid IOffset:self.iOffset INum:self.iNum LWatchUserId:self.lWatchUserId];
    self.listenToMeData = [ListenToMeData getInstance];
    //获取作品列表的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTable) name:NOTIFY_GET_MUSICWOKR_PAGE_RESP object:nil];
    //删除音乐作品的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshMusicWorkList) name:NOTIFY_GET_DELETE_MYMUSICWORK_PAGE_RESP object:nil];
}

#pragma mark - 接收通知后刷新列表
-(void)refreshTable{
    
    [self.mTableViw reloadData];
}

-(void)refreshMusicWorkList{
    
    NSArray *tempArr = [NSArray arrayWithArray:self.listenToMeData.arrMusicWorkBaseInfo];
    for (MusicWorkBaseInfo *musicWorkBaseInfo in tempArr) {
        if (self.listenToMeData.stMusicWorkBaseInfo.lMusicWorkId == musicWorkBaseInfo.lMusicWorkId) {
            [self.listenToMeData.arrMusicWorkBaseInfo removeObject:musicWorkBaseInfo];
        }
    }
    
    [self.mTableViw reloadData];
}

#pragma mark - 当视图销毁时要移除通知
-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_MUSICWOKR_PAGE_RESP object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_DELETE_MYMUSICWORK_PAGE_RESP object:nil];
}

#pragma mark -UI
-(void)setUI
{
    [self setNav];
    
    [self setUpTableiView];
    
    [self setDeleteView];
    
    
}

#pragma mark 删除提示视图
-(void)setDeleteView{

    self.bagView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 140)];
    self.bagView.backgroundColor = [UIColor colorWithRed:203/255.0 green:203/255.0 blue:203/255.0 alpha:0.5];

    
    CGFloat btnBagViewW = screenWidth - 30;
    CGFloat btnBagViewH = 100;
    CGFloat btnBagViewX = 15;
    CGFloat btnBagViewY = (140 - btnBagViewH) * 0.5;
    
    UIImageView *btnBagView = [[UIImageView alloc] initWithFrame:CGRectMake(btnBagViewX, btnBagViewY, btnBagViewW, btnBagViewH)];
    btnBagView.userInteractionEnabled = YES;
    [btnBagView setImage:[UIImage imageNamed:@"background.png"]];
    btnBagView.layer.masksToBounds = YES;
    btnBagView.layer.cornerRadius = 5;
    [self.bagView addSubview:btnBagView];
    
    //添加两个button 删除 取消
    CGFloat btnW = 50; //button的高度
    UIButton *btnDelete = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, btnBagViewW, btnW)];
    [btnDelete setTitle:@"删除" forState:UIControlStateNormal];
    [btnDelete setTitleColor:[UIColor rgbFromHexString:@"#FF0053" alpaa:1.0] forState:UIControlStateNormal];
    [btnDelete addTarget:self action:@selector(deleteHandle:) forControlEvents:UIControlEventTouchUpInside];
    [btnBagView addSubview:btnDelete];
    
    UIButton *btnCancle = [[UIButton alloc] initWithFrame:CGRectMake(0, btnW, btnBagViewW, btnW)];
    [btnCancle setTitle:@"取消" forState:UIControlStateNormal];
    [btnCancle setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [btnCancle addTarget:self action:@selector(cancleHandle:) forControlEvents:UIControlEventTouchUpInside];
    [btnBagView addSubview:btnCancle];
    
    UILabel *lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, btnW, btnBagViewW, 1)];
    lineLabel.backgroundColor = [UIColor lightGrayColor];
    lineLabel.alpha = 0.5;
    [btnBagView addSubview:lineLabel];
    
}

#pragma mark 导航栏
-(void)setNav
{
//    self.navigationItem.title = @"我的作品";
    
    [self setRightBtnItem];
    
    [self setLeftBtnItem];
}

// 导航栏右边的Btn
-(void)setRightBtnItem
{
    UIButton *rightBtn = [[UIButton alloc]init];
    UIImage *imgMore = [UIImage imageNamed:@"more.png"];
    rightBtn.frame = CGRectMake(0, 0, imgMore.size.width, imgMore.size.height);
    [rightBtn setImage:imgMore forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(clickRightBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customRightBtnItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = customRightBtnItem;
}

// 导航栏左边的Btn
-(void)setLeftBtnItem
{
    UIButton *leftBtn = [[UIButton alloc]init];
    UIImage *leftBtnImg = [UIImage imageNamed:@"whiteBack.png"];
    leftBtn.frame = CGRectMake(0, 0, leftBtnImg.size.width, leftBtnImg.size.height);
    [leftBtn setImage:leftBtnImg forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(clickLeftBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customLeftBtnItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = customLeftBtnItem;
}

-(void)clickRightBarBtnItem
{
    YDLog(@"点击了导航栏的右边的按钮");
}

-(void)clickLeftBarBtnItem
{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setUpTableiView
{
    _mTableViw = [[UITableView alloc]init];
    _mTableViw.frame = self.view.bounds;
    _mTableViw.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_mTableViw];

    _mTableViw.delegate = self;
    _mTableViw.dataSource = self;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *ID = @"myWorkscell";
    YDTopListCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (cell == nil) {
        cell = [[YDTopListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    if (self.bagView) {
        [self.bagView removeFromSuperview];
    }
    
    MusicWorkBaseInfo *musicWorkBaseInfo = self.listenToMeData.arrMusicWorkBaseInfo[indexPath.section];
    
    UserBaseInfoNet *userBaseInfoNet = musicWorkBaseInfo.stUserBaseInfoNet;
    SongInfo *songInfo = musicWorkBaseInfo.stSongInfo;
    cell.lbMusicName.text = songInfo.sSongName;
    cell.lbSinger.text = songInfo.sSongSinger;
    [cell.imgAvatar sd_setImageWithURL:[NSURL URLWithString:userBaseInfoNet.sCovver]];
    
    [cell.btnFlower setTitle:[NSString stringWithFormat:@"%d",musicWorkBaseInfo.iFlowerNum] forState:UIControlStateNormal];
    [cell.btnListener setTitle:[NSString stringWithFormat:@"%d",musicWorkBaseInfo.iListenNum] forState:UIControlStateNormal];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.btnCollection.selected = NO;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    YDTopListCell  *cell = (YDTopListCell *)[tableView cellForRowAtIndexPath:indexPath];
    if (self.isPushFromPerCenter) {
        [cell.contentView addSubview:self.bagView];
        self.currentSelect = indexPath.section;
    }else{
        YDLog(@"给他人送歌曲");
        //首先要判断该用户是否有可赠送的歌曲
        if(self.listenToMeData.arrMusicWorkBaseInfo.count > 0){
        
            self.alertForGivingMusicView.hidden = NO;
            //获取当前要赠送的歌曲
            self.selectMusicWorkBaseInfo = self.listenToMeData.arrMusicWorkBaseInfo[indexPath.section];
            
        }
       
    }
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
        return 140;

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return 1;
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return self.listenToMeData.arrMusicWorkBaseInfo.count;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 0.1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 15;
}

#pragma mark - 点击删除button
-(void)deleteHandle:(UIButton *)btnDelete{
    MusicWorkBaseInfo *musicWorkBaseInfo = self.listenToMeData.arrMusicWorkBaseInfo[self.currentSelect];
    YDLog(@"删除选中的音乐作品");
    [[NetReqManager getInstance] sendDeleteMyMusicWork:[ListenToMeDBManager getUuid] LMusicid:musicWorkBaseInfo.lMusicWorkId];
    
}

#pragma mark - 点击取消button
-(void)cancleHandle:(UIButton *)btnCancle{
    [self.bagView removeFromSuperview];
}

#pragma mark - 点击取消送歌button
-(void)cancleGivingWork:(UIButton *)btnCancle{
    self.alertForGivingMusicView.hidden = YES;
}
-(void)determineGivingWork:(UIButton *)btnDetermine{
    
    //点击赠送歌曲后将弹窗隐藏掉
    self.alertForGivingMusicView.hidden = YES;
    
    //给聊天对象送歌,发送送歌请求,然后跳转到私聊界面
    //在进入我的作品界面是,已经从聊天界面push了2次,返回时直接在navigationController的视图数组里面去倒数第二个视图
    FriendsMsgVC *tempVc = (FriendsMsgVC *)[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count - 2];
    [self.navigationController popToViewController:tempVc animated:YES];
    
    //这里要发送一条送歌的请求消息
    YDLog(@"选择赠送歌曲的id:%lld",self.selectMusicWorkBaseInfo.lMusicWorkId);
    
    //返送一条赠送音乐作品的消息
    [[NetReqManager getInstance] sendMsg:[ListenToMeDBManager getUuid] LTalkWith:[ListenToMeData getInstance].privateChatUser.uuid SMsg:nil LMsgId:self.selectMusicWorkBaseInfo.lMusicWorkId EMessageType:MESSAGE_TYPEMessageTypeSongs];
}
@end
