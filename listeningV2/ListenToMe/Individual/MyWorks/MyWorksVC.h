//
//  MyWorksVC.h
//  ListenToMe
//
//  Created by yadong on 2/5/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyWorksVC : YDBaseVC
@property(strong,nonatomic) UITableView *mTableViw;
@property(nonatomic,assign) int64_t uuid;
@property(nonatomic,assign) int32_t iOffset;
@property(nonatomic,assign) int32_t iNum;
@property(nonatomic,assign) int64_t lWatchUserId;
/**
 *  是否是从个人中心页面跳转进入的
 */
@property(nonatomic,assign)BOOL isPushFromPerCenter;
@end
