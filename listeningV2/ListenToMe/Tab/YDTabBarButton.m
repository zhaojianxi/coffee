//
//  YDTabBarButton.m
//  ListenToMe
//
//  Created by yadong on 1/24/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "YDTabBarButton.h"


@interface YDTabBarButton ()

@end


@implementation YDTabBarButton

-(id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        //图标居中
        self.imageView.contentMode = UIViewContentModeCenter;
//        [self setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    }
    
    return self;
}

// 重写去掉高亮状态
-(void)setHighlighted:(BOOL)highlighted{}

// 内部图片的frame
-(CGRect)imageRectForContentRect:(CGRect)contentRect
{
    CGFloat imageW = contentRect.size.width;
    CGFloat imageH = contentRect.size.height;
    
    return CGRectMake(0, 0, imageW, imageH);
}

// 设置item
-(void)setItem:(UITabBarItem *)item
{
    _item = item;
    
    // KVO监听属性改变
    [item addObserver:self forKeyPath:@"title" options:0 context:nil];
    [item addObserver:self forKeyPath:@"image" options:0 context:nil];
    [item addObserver:self forKeyPath:@"badgeValue" options:0 context:nil];
    [item addObserver:self forKeyPath:@"selectedImage" options:0 context:nil];
    
    // 监听到item的值发生变化，则改变自定义的Button的属性
    [self observeValueForKeyPath:nil ofObject:nil change:nil context:nil];
}

// 移除监听
-(void)dealloc
{
    [self.item removeObserver:self forKeyPath:@"title"];
    [self.item removeObserver:self forKeyPath:@"image"];
    [self.item removeObserver:self forKeyPath:@"badgeValue"];
    [self.item removeObserver:self forKeyPath:@"selectedImage"];
}

/**
 *  监听到某个对象的属性发生了改变，就会调用
 *
 *  @param KeyPath 属性名
 *  @param objecct 哪个对象的属性被改变
 *  @param change 属性发生的改变
 */
 
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    // 设置文字
    [self setTitle:self.item.title forState:UIControlStateNormal];
    [self setTitle:self.item.title forState:UIControlStateSelected];
    [self setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor purpleColor] forState:UIControlStateSelected];
    
    // 设置图片
    [self setImage:self.item.image forState:UIControlStateNormal];
    [self setImage:self.item.selectedImage forState:UIControlStateSelected];
    
}

@end


