//
//  YDTabBarButton.h
//  ListenToMe
//
//  Created by yadong on 1/24/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YDBaseBtn.h"

@interface YDTabBarButton : YDBaseBtn
@property(strong,nonatomic) UITabBarItem *item;
@end
