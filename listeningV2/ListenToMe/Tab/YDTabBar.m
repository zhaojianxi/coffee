//
//  YDTabBar.m
//  ListenToMe
//
//  Created by yadong on 1/24/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "YDTabBar.h"
#import "YDTabBarButton.h"

@interface YDTabBar ()
@property(strong,nonatomic) YDTabBarButton *selectedBtn;
@end

@implementation YDTabBar
// 初始化 YDTabBar
-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    self.backgroundColor = [UIColor whiteColor];
    return self;
}


// (初始化之后)
-(void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat btnW = self.frame.size.width / (self.subviews.count - 1);
    CGFloat btnH = self.frame.size.height;
    CGFloat btnY = 0;
    
    for (int index = 0; index < self.subviews.count; index ++) {
        
        // 1. 取出btn
        YDTabBarButton *btn = self.subviews[index];
        
        // 判断
        if ([btn isKindOfClass:[YDTabBarButton class]]) {
            
            // 2.设置btnframe
            CGFloat btnX = btnW * index;
            btn.frame = CGRectMake(btnX, btnY, btnW, btnH);
            
            // 3.设置btn.tag
            btn.tag = index;
            
//            if (index == 0) {
//                // 2.设置btnframe
//                CGFloat btnX = btnW * index;
//                btn.frame = CGRectMake(btnX, btnY, btnW, btnH);
//                
//                // 3.设置btn.tag
//                btn.tag = index;
//            } else{
//                // 2.设置btnframe
//                CGFloat btnX = btnW * (index);
//                btn.frame = CGRectMake(btnX, btnY, btnW, btnH);
//                
//                // 3.设置btn.tag
//                btn.tag = index;
//            }

        } else{
            CGFloat btnX = btnW;
            btn.frame = CGRectMake(btnX, btnY, btnW, btnH);
        
        }

    }
}

/**
 * 初始化Btn
 * 
 * @param item tabBar的UITabBarItem
 */
-(void)addTabBarButtonWithItem:(UITabBarItem *)item
{
    // 1.初始化 btn
    YDTabBarButton *btn = [[YDTabBarButton alloc]init];
    [self addSubview:btn];
    
    // 2.设置值
    btn.item = item;
    
    // 3.默认选中
    if (self.subviews.count == 1) {
        btn.selected = YES;
        self.selectedBtn = btn;
    }

    // 4.监听btn点击
    [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchDown];

}

/**
 * 监听btn点击
 *
 * @param btn 被点击的btn
 */
-(void)btnClick:(YDTabBarButton *)btn
{
    // 1.通知代理
    if ([self.delegate respondsToSelector:@selector(tabBar:didSelectButtonFrom:to:)]) {
        [self.delegate tabBar:self didSelectButtonFrom:self.selectedBtn.tag to:btn.tag];
    }
    
    // 2.改变btn状态
    self.selectedBtn.selected = NO;
    btn.selected = YES;
    self.selectedBtn = btn;
}



@end
