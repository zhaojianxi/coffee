//
//  YDTabBarController.m
//  ListenToMe
//
//  Created by yadong on 1/26/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "YDTabBarController.h"
#import "YDNavigationController.h"
#import "YDIndividualVC.h"
#import "YDSubscribeVC.h"
#import "YDSquareVC.h"
#import "YDRoomVC.h"
#import "YDBindRoomVC.h"
#import "YDRoomFrontPageViewController.h"
#import "FirstLaunchAlertVC.h"
#import "GiftGivenAlertView.h"
#import "MyGiftCertificateVC.h"

@interface YDTabBarController ()
@property(strong,nonatomic) YDTabBar *customTabBar; // 自定义的tabBar
@property(strong,nonatomic) UIButton *roomBtn;
@property(nonatomic,strong)FirstLaunchAlertVC *firstLanchAlertView;
@property(nonatomic,strong)GiftGivenAlertView *checkGiftAlertView;
@property(nonatomic,strong)MyGiftCertificateVC *myGiftVC;
@property(nonatomic,strong)ListenToMeData *listenToMeData;
/**
 *  用户通过友盟第三方登录后返回的基本数据包括用户名和用户帐号等信息
 */
@property(nonatomic,strong)UMSocialAccountEntity * account;
@end

@implementation YDTabBarController
@synthesize roomBtn;
@synthesize customTabBar;

#pragma mark -生命周期
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.frame = [[UIScreen mainScreen] bounds];
    // 初始化tabBar
    [self initCustomTabBar];
    
    // 初始化所有的子控制器
    [self setAllChildViewControllers];
    
    // 包房Btn---(二级视图，只是显示包房Btn，点击该Btn时才会到该控制器)
    [self setRoomBtn];
    
    //判断首次登录 弹出送礼券提示框
    BOOL first = [[NSUserDefaults standardUserDefaults]boolForKey:@"firstLaunch"];
    if (first) {
//         这里判断是否第一次
//        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"第一次"
//                                                      message:@"进入App"
//                                                     delegate:self
//                                            cancelButtonTitle:@"我知道了"
//                                            otherButtonTitles:nil];
//        [alert show];
        
        self.firstLanchAlertView = [[FirstLaunchAlertVC alloc]init];
        [self.view addSubview:self.firstLanchAlertView.view];
        [self.firstLanchAlertView.weiXinLoginBtn addTarget:self action:@selector(weiXinLoginHandle:) forControlEvents:UIControlEventTouchUpInside];
        [self.firstLanchAlertView.qqLoginBtn addTarget:self action:@selector(qqLoginHandle:) forControlEvents:UIControlEventTouchUpInside];
        [self.firstLanchAlertView.sinaLoginBtn addTarget:self action:@selector(sinaLoginHandle:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
//    BOOL isLaunch = [[NSUserDefaults standardUserDefaults] boolForKey:@"everLaunched"];
//    if (isLaunch) {
//        //         这里判断是否已经登录
//        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"礼券已经收入囊中"
//                                                      message:@"进入App"
//                                                     delegate:self
//                                            cancelButtonTitle:@"我知道了"
//                                            otherButtonTitles:nil];
//        [alert show];
//    }else{
//    
//        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"还未登录"
//                                                      message:@"进入App"
//                                                     delegate:self
//                                            cancelButtonTitle:@"我知道了"
//                                            otherButtonTitles:nil];
//        [alert show];
//    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    for (UIView *child in self.tabBar.subviews) {
        // 删除系统自动生成的UITabBarButton
        if ([child isKindOfClass:[UIControl class]]) {
            [child removeFromSuperview];
        }
    }
}


#pragma mark -UI
// 初始化RoomBtn
-(void)setRoomBtn
{
    CGFloat roomBtnW = screenWidth / 4;
    CGFloat roomBtnH = customTabBar.frame.size.height;
    CGFloat roomBtnX = roomBtnW;
    CGFloat roomBtnY = 0;
    roomBtn = [[UIButton alloc]initWithFrame:CGRectMake(roomBtnX, roomBtnY, roomBtnW, roomBtnH)];
    [customTabBar addSubview:roomBtn];
    
    [roomBtn setTitle:nil forState:UIControlStateNormal];
    [roomBtn setBackgroundColor:[UIColor clearColor]];
    [roomBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [roomBtn addTarget:self action:@selector(clickRoomBtn) forControlEvents:UIControlEventTouchUpInside];
}

// 初始化tabBar
-(void)initCustomTabBar
{
    YDTabBar *customeTabBar = [[YDTabBar alloc]initWithFrame:self.tabBar.bounds];
    customeTabBar.delegate = self;
    [self.tabBar addSubview:customeTabBar];
    self.customTabBar = customeTabBar;
}

#pragma mark -代理
/**
 * 监听tabBar的点击
 *
 * @param from 原来选中的btn的位置
 * @param to 当前选中的btn的位置
 */
-(void)tabBar:(YDTabBar *)tabBar didSelectButtonFrom:(NSInteger)from to:(NSInteger)to
{
    self.selectedIndex = to;
}

#pragma mark - 初始化tabBrVC子控制器
/**
 * 初始化tabBar的子控制器
 *
 * @param childVC 子控制器
 * @param imgName tabbaritem的图标的name
 * @param selectedImgName tabbaritem选中状态下的图片的name
 */
-(void)setupChildVC:(UIViewController *)childVC imgName:(NSString *)imgName selectedImgName:(NSString *)selectedImgName
{
    // 1.设置子控制器的image & selectedImg
    childVC.tabBarItem.image = [UIImage imageNamed:imgName];
    childVC.tabBarItem.selectedImage = [UIImage imageNamed:selectedImgName];
    
    // 2.为子控制器包装导航控制器
    YDNavigationController *navController = [[YDNavigationController alloc]initWithRootViewController:childVC];
    
    // 3.控制器入栈导航控制器ÅÅ
    [self addChildViewController:navController];
    
    // 4.初始化tabBar内部的按钮
    [self.customTabBar addTabBarButtonWithItem:childVC.tabBarItem];
}

/**
 * 初始化所有的子控制器
 */
-(void)setAllChildViewControllers
{
    YDSquareVC *squareVC = [[YDSquareVC alloc]init];
    squareVC.tabBarItem.title = @"";
    squareVC.navigationItem.title = @"";
    [self setupChildVC:squareVC imgName:@"K星广场01.png" selectedImgName:@"K星广场02.png"];
    
    
    YDRoomVC *roomVC = [[YDRoomVC alloc]init];
    roomVC.tabBarItem.title = @"";
    roomVC.navigationItem.title = @"包房";
    [self setupChildVC:roomVC imgName:@"包房01.png" selectedImgName:@"包房02.png"];
    
//    YDSubscribeVC *subscribeVC = [[YDSubscribeVC alloc]init];
//    subscribeVC.tabBarItem.title = @""; 
//    subscribeVC.navigationItem.title = @"约歌";
//    [self setupChildVC:subscribeVC imgName:@"yuege.png" selectedImgName:@"yuege_p.png"];
    
    YDIndividualVC *individualVC = [[YDIndividualVC alloc]init];
    individualVC.tabBarItem.title = @"";
    individualVC.navigationItem.title = @"个人中心";
    [self setupChildVC:individualVC imgName:@"我01.png" selectedImgName:@"我02.png"];
}

#pragma mark -Btn点击事件
-(void)clickRoomBtn
{
    
    //临时修改
    if ([ListenToMeData getInstance].stKTVRoomInfo != nil) {
        YDRoomVC *roomVC = [[YDRoomVC alloc]init];
        [self.childViewControllers[self.selectedIndex] pushViewController:roomVC animated:YES];
    }else{
        YDRoomFrontPageViewController *roomVC = [[YDRoomFrontPageViewController alloc]init];
        [self.childViewControllers[self.selectedIndex] pushViewController:roomVC animated:YES];
    }
}

#pragma mark -其他
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - 第三方登录
-(void)weiXinLoginHandle:(UIButton *)weiXingBtn{
    
    //需要到微信开放平台进行注册,申请开发者帐号.
    UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToWechatSession];
    
    snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
        
        if (response.responseCode == UMSResponseCodeSuccess) {
            
            UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary]valueForKey:UMShareToWechatSession];
            
            YDLog(@"WeiXin username is %@, uid is %@, token is %@ url is %@",snsAccount.userName,snsAccount.usid,snsAccount.accessToken,snsAccount.iconURL);
            
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"launchStaue"];
            
            self.account = snsAccount;
            [self getUserInfo:ID_TYPEIdTypeWeixin];
            
                [self.firstLanchAlertView.view removeFromSuperview];
                //            [self showCheckGiftAlertView];
                [self performSelector:@selector(showCheckGiftAlertView) withObject:self afterDelay:0.3];
            
            
            
            
        }
        
    });
    
    
}

-(void)qqLoginHandle:(UIButton *)qqBtn{
    
    //暂时使用QQQzone登录
    UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToQzone];
    
    snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
        
        //          获取微博用户名、uid、token等
        
        if (response.responseCode == UMSResponseCodeSuccess) {
            
            UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary] valueForKey:UMShareToQzone];
            
            YDLog(@"QQ username is %@, uid is %@, token is %@ url is %@",snsAccount.userName,snsAccount.usid,snsAccount.accessToken,snsAccount.iconURL);
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"launchStaue"];
            
            self.account = snsAccount;
            [self getUserInfo:ID_TYPEIdTypeQq];
            
                [self.firstLanchAlertView.view removeFromSuperview];
                //            [self showCheckGiftAlertView];
                [self performSelector:@selector(showCheckGiftAlertView) withObject:self afterDelay:0.3];
            
            
            
            
        }});
    
    
    
}
-(void)sinaLoginHandle:(UIButton *)sinaBtn{
    
    UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToSina];
    
    snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
        
        //        NSDictionary *dic = response.data[@"sina"];
        //        [self setNamelabelAndIcon:dic];
        //        [self setLoginStatus];
        
        if (response.responseCode == UMSResponseCodeSuccess) {
            
            UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary] valueForKey:UMShareToSina];
            
            YDLog(@"Sina username is %@, uid is %@, token is %@ url is %@",snsAccount.userName,snsAccount.usid,snsAccount.accessToken,snsAccount.iconURL);
            
            //获取新浪好友限制30个
//            [[UMSocialDataService defaultDataService] requestSnsFriends:UMShareToSina  completion:^(UMSocialResponseEntity *response){
//                YDLog(@"SnsFriends is %@",response.data);
//            }];
            
            
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"launchStaue"];
            
            self.account = snsAccount;
            [self getUserInfo:ID_TYPEIdTypeWeibo];
            
            
                [self.firstLanchAlertView.view removeFromSuperview];
                //            [self showCheckGiftAlertView];
                [self performSelector:@selector(showCheckGiftAlertView) withObject:self afterDelay:0.3];
            
            
        }
        
    });
    
    
    
    
}

#pragma mark - 获取登录用户的信息
-(void)getUserInfo:(ID_TYPE)idType{
    
    UserBaseInfoNetBuilder *userbaseInfoBuilder = [UserBaseInfoNet builder];
    userbaseInfoBuilder.uuid = 0;
    userbaseInfoBuilder.sId = self.account.usid;
    userbaseInfoBuilder.eType = idType;
    userbaseInfoBuilder.sCovver = self.account.iconURL;
    userbaseInfoBuilder.sNick = self.account.userName;
    userbaseInfoBuilder.sDesc = @"";
    userbaseInfoBuilder.iAge = 0;
    userbaseInfoBuilder.iFlowerNum = 0;
    
    self.listenToMeData = [ListenToMeData getInstance];
    self.listenToMeData.stUserBaseInfoNet = [userbaseInfoBuilder build];
    [[NetReqManager getInstance] sendLoginReq:self.listenToMeData.stUserBaseInfoNet];
    
    [[NetReqManager getInstance] sendQuickLoginReq:[ListenToMeDBManager getUuid]];
    
}

#pragma 弹出查看礼券的视图
-(void)showCheckGiftAlertView{

    self.checkGiftAlertView = [[GiftGivenAlertView alloc]init];
    [self.checkGiftAlertView.btnCheckGift addTarget:self action:@selector(checkGiftHandle:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.checkGiftAlertView];
}
#pragma mark 查看礼券
-(void)checkGiftHandle:(UIButton *)checkGiftBtn{
    
    YDLog(@"查看我的礼券");
    
    self.navigationController.hidesBottomBarWhenPushed = YES;
    
    
    self.myGiftVC = [[MyGiftCertificateVC alloc]init];
    [self.childViewControllers[self.selectedIndex] pushViewController:self.myGiftVC animated:YES];
    
    [self.checkGiftAlertView removeFromSuperview];
}



@end
