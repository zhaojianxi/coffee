//
//  YDTabBar.h
//  ListenToMe
//
//  Created by yadong on 1/24/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>

@class YDTabBar;

@protocol YDTabBarDelegate <NSObject>

@optional
-(void)tabBar:(YDTabBar *)tabBar didSelectButtonFrom:(NSInteger)from to:(NSInteger)to;

@end


@interface YDTabBar : UIView

@property(strong,nonatomic) id<YDTabBarDelegate> delegate;

-(void)addTabBarButtonWithItem:(UITabBarItem *)item;

@end
