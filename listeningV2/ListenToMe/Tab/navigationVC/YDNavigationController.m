
//
//  YDNavigationController.m
//  ListenToMe
//
//  Created by yadong on 1/26/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "YDNavigationController.h"

@interface YDNavigationController ()

@end

@implementation YDNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];

    // navigationBar背景
    self.navigationBar.barTintColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
    
    // 导航栏字体设置
    NSDictionary *textDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor], NSForegroundColorAttributeName,
                                    [UIFont systemFontOfSize:
                                     17.5], NSFontAttributeName, nil];
    [[UINavigationBar appearance] setTitleTextAttributes:textDictionary];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    for (UIView *child in self.tabBarController.tabBar.subviews) {
        // 删除系统自动生成的UITabBarButton
        if ([child isKindOfClass:[UIControl class]]) {
            [child removeFromSuperview];
        }
    }
}

// 设置状态栏字体
-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}


-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if (self.viewControllers.count > 0) {
        viewController.hidesBottomBarWhenPushed = YES;
    }
    [super pushViewController:viewController animated:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
