//
//  YDSubscribeCell.h
//  ListenToMe
//
//  Created by yadong on 2/4/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YDSubscribeCell : UITableViewCell
/**
 * 头像
 */
@property(strong,nonatomic) UIImageView *imgAvatar;
/**
 * 用户名
 */
@property(strong,nonatomic) UILabel *lbUserName;
/**
 * 约歌描述
 */
@property(strong,nonatomic) UILabel *lbDesc;
/**
 * 聚会名称
 */
@property(strong,nonatomic) UILabel *lbPartyName;
/**
 * 地点
 */
@property(strong,nonatomic) UILabel *lbLocation;
/**
 * 约歌时间
 */
@property(strong,nonatomic) UILabel *lbSubsTime;
/**
 * 距离
 */
@property(strong,nonatomic) UILabel *lbDistance;
/**
 * 约歌条件限制
 */
@property(strong,nonatomic) UIImageView *imgLimit;
@property(strong,nonatomic) UILabel *lbLinmit;
/**
 * 谁付款
 */
@property(strong,nonatomic) UIImageView *imgAffordPerson;
@property(strong,nonatomic) UILabel *lbAffordPerson;
/**
 * 申请加入 的人数
 */
@property(strong,nonatomic) UIButton *btnApply;
/**
 * 加入 了 的人数
 */
@property(strong,nonatomic) UIButton *btnJoin;
/**
 * 讨论数目
 */
@property(strong,nonatomic) UIButton *btnDiscuss;
///////////////////////////////////////////////////////////////////////////////////////////
/**
 *  发起方
 */
@property(strong,nonatomic) UIButton *btnWhoLaunch;

@end
