//
//  YDLaunchPartyVC.h
//  
//
//  Created by yadong on 3/10/15.
//
//

#import <UIKit/UIKit.h>

@interface YDLaunchPartyVC : YDBaseVC
/**
 * 聚会主题
 */
@property(strong,nonatomic) UITextView *tvPartyTheme;
/**
 * 聚会描述
 */
@property(strong,nonatomic) UITextView *tvPartyDesc;
/**
 * 不滚动的tableview
 */
@property(strong,nonatomic) UITableView *mTableView;
/**
 * 完成按钮
 */
@property(strong,nonatomic) UIButton *btnCommit;
@end
