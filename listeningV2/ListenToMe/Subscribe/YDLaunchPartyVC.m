//
//  YDLaunchPartyVC.m
//  
//
//  Created by yadong on 3/10/15.
//
//
#define frontX 15

#import "YDLaunchPartyVC.h"
#import "PartyLocationVC.h"


@interface YDLaunchPartyVC ()<UITextViewDelegate,UITableViewDataSource,UITableViewDelegate>
@property(strong,nonatomic) UISegmentedControl *segmentedControl;
@property(strong,nonatomic) UIScrollView *svBg; // 底部滚动视图
@property(nonatomic,strong) UILabel *charCountsTip; // 剩余可输入的字数
@property(strong,nonatomic) UIButton *btnAppointment; // 2人约会
@property(strong,nonatomic) UIButton *btn3To5; // 3到5人
@property(strong,nonatomic) UIButton *btn6To10; // 6到10人
@property(strong,nonatomic) UIButton *btnNotLimitNumP; // 不限制人数
@property(strong,nonatomic) UIButton *btnJustGirl; // 限女生
@property(strong,nonatomic) UIButton *btnJustBoy; // 限男生
@property(strong,nonatomic) UIButton *btnNotLimitGender; // 不限男女
@property(strong,nonatomic) UIButton *btnNoFriends; // 不能带朋友
@property(strong,nonatomic) UIButton *btnCanFriends; // 可以带朋友
@property(strong,nonatomic) UIButton *btnFree; // 免费
@property(strong,nonatomic) UIButton *btnAa; // AA
@property(strong,nonatomic) UIButton *btnGirlsFree; // 男A女免
@property(strong,nonatomic) UIImage *imgNotChoice; // 未选择图片
@property(strong,nonatomic) UIButton *btnAddPeople; // 邀请盆友加号
@property(strong,nonatomic) UIImage *imgChoice; // 已选中图片
@property(strong,nonatomic) UIDatePicker *datePicker; //时间选择
@property(strong,nonatomic) UIButton *timeCancelBtn; // 取消时间选择Btn
@property(strong,nonatomic) UIButton *timeChooseBtn; // 确定时间选择Btn
@property(strong,nonatomic) UIButton *btnGrayBg; // 灰质的背景
@property(strong,nonatomic) UIView *viewDatePickerBg; // 时间选择器的背景视图
@property(copy,nonatomic) NSString *strTime; // 时间选择器的时间str
@end

@implementation YDLaunchPartyVC
@synthesize imgNotChoice;
@synthesize imgChoice;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setNavigation];
    
    [self setUpScrollView];
    
    [self setUpSegment];
    
    [self setPartyThemeAndDes];
    
    [self setUpTableView];
    
    [self setAddbtn];
    
    [self setupDatePicker];
    
}
#pragma mark tableview
-(void)setUpTableView
{
    CGFloat mTableViewH = 383; // tableViewH的高度
    CGRect mTableViewF = CGRectMake(0, _tvPartyDesc.y + _tvPartyDesc.height + 5, screenWidth - frontX, mTableViewH);
    _mTableView = [[UITableView alloc]initWithFrame:mTableViewF];
    _mTableView.backgroundColor = [UIColor clearColor];
    _mTableView.separatorColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:0.12];
    [_svBg addSubview:_mTableView];
    
    _mTableView.delegate = self;
    _mTableView.dataSource = self;
    
    _mTableView.scrollEnabled = NO;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *ID = @"launchPartyCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    
    UILabel *lbFirst = [[UILabel alloc]init]; // cell上第一个位置的lb
    [cell addSubview:lbFirst];
    
    imgNotChoice = [UIImage imageNamed:@"notSelectedCircle.png"]; // 未选择图片
    imgChoice = [UIImage imageNamed:@"selectedCircle.png"]; // 已选中图片
    
    
    if (0 == indexPath.row) {
        lbFirst.font = [UIFont systemFontOfSize:14.0];
        lbFirst.textColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
        
        lbFirst.text = @"地点";
        lbFirst.frame = CGRectMake(frontX, 0, 35,cell.height);
        
        // 指向右侧的三角按钮
        UIButton *triangleBtn = [[UIButton alloc]init];
        UIImage *triangleImg = [UIImage imageNamed:@"pureRightTriangle.png"];
        [triangleBtn setImage:triangleImg forState:UIControlStateNormal];
        triangleBtn.frame = CGRectMake(screenWidth - frontX - triangleImg.size.width, 0, triangleImg.size.width, cell.height); //screenWidth - frontX 三角x坐标
        [cell addSubview:triangleBtn];
        
    } else if (1 == indexPath.row){
        lbFirst.font = [UIFont systemFontOfSize:14.0];
        lbFirst.textColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
        
        lbFirst.text = @"时间";
        lbFirst.frame = CGRectMake(frontX, 0, 35,cell.height);
        
        // 时间标签
        UILabel *lbTime = [[UILabel alloc]init];
        lbTime.frame = CGRectMake(screenWidth - frontX - 0.5*screenWidth, 0, 0.5*screenWidth, cell.height);
        lbTime.text = @"2014-01-12 16:40";
        lbTime.textColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
        lbTime.font = [UIFont systemFontOfSize:14.0];
        lbTime.textAlignment = NSTextAlignmentRight;
        [cell addSubview:lbTime];
    } else if(2 == indexPath.row){
        
        UIImageView *imgLineHorizon = [[UIImageView alloc]init];
        imgLineHorizon.frame = CGRectMake(frontX, 7.5, 2, 40);
        imgLineHorizon.backgroundColor = [UIColor rgbFromHexString:@"#A8AEAE" alpaa:0.58];
        [cell addSubview:imgLineHorizon];
        
    } else if(3 == indexPath.row){
        
        lbFirst.font = [UIFont systemFontOfSize:14.0];
        lbFirst.textColor = [UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0];
        
        lbFirst.text = @"聚会人数";
        lbFirst.frame = CGRectMake(frontX, 0, 60,cell.height);
        lbFirst.backgroundColor = [UIColor clearColor];
        
        CGFloat btnsMargin = 8; // 按钮间的距离
        // 2人约会
        _btnAppointment = [[UIButton alloc]init];
        _btnAppointment.frame = CGRectMake(lbFirst.x + lbFirst.width + btnsMargin, 0, 80, cell.height);
        [_btnAppointment setImage:imgNotChoice forState:UIControlStateNormal];
        [_btnAppointment setTitle:@"2人约会" forState:UIControlStateNormal];
        _btnAppointment.titleLabel.font = [UIFont systemFontOfSize:12.0];
        [_btnAppointment setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        [_btnAppointment setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
        [_btnAppointment addTarget:self action:@selector(clickbtnAppointment) forControlEvents:UIControlEventTouchUpInside];
        [_btnAppointment  addTarget:self action:@selector(clickbtnAppointment) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:_btnAppointment];
        
        // 3-5
        _btn3To5 = [[UIButton alloc]init];
        _btn3To5.frame = CGRectMake(_btnAppointment.x + _btnAppointment.width, 0, 80, cell.height);
        [_btn3To5 setImage:imgNotChoice forState:UIControlStateNormal];
        [_btn3To5 setTitle:@"3-5" forState:UIControlStateNormal];
        _btn3To5.titleLabel.font = [UIFont systemFontOfSize:12.0];
        [_btn3To5 setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        [_btn3To5 setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
         [_btn3To5  addTarget:self action:@selector(clickbtn3To5) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:_btn3To5];

        // 6-10
        _btn6To10 = [[UIButton alloc]init];
        _btn6To10.frame = CGRectMake(_btn3To5.x + _btn3To5.width, 0, 80, cell.height);
        [_btn6To10 setImage:imgNotChoice forState:UIControlStateNormal];
        [_btn6To10 setTitle:@"6-10" forState:UIControlStateNormal];
        _btn6To10.titleLabel.font = [UIFont systemFontOfSize:12.0];
        [_btn6To10 setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
          [_btn6To10 setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
          [_btn6To10  addTarget:self action:@selector(clickbtn6To10) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:_btn6To10];
     
        // 不限
        _btnNotLimitNumP = [[UIButton alloc]init];
        _btnNotLimitNumP.frame = CGRectMake(_btn6To10.x + _btn6To10.width, 0, 80, cell.height);
        [_btnNotLimitNumP setImage:imgNotChoice forState:UIControlStateNormal];
        [_btnNotLimitNumP setTitle:@"不限" forState:UIControlStateNormal];
        _btnNotLimitNumP.titleLabel.font = [UIFont systemFontOfSize:12.0];
        [_btnNotLimitNumP setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
           [_btnNotLimitNumP setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
           [_btnNotLimitNumP  addTarget:self action:@selector(clickbtnNotLimitNumP) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:_btnNotLimitNumP];

        
    }
    
    else if(4 == indexPath.row){
        lbFirst.font = [UIFont systemFontOfSize:14.0];
        lbFirst.textColor = [UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0];
        
        lbFirst.text = @"性别要求";
        lbFirst.frame = CGRectMake(frontX, 0, 60,cell.height);
        lbFirst.backgroundColor = [UIColor clearColor];
     
        // 不限
        _btnNotLimitGender = [[UIButton alloc]init];
        _btnNotLimitGender.frame = _btnNotLimitNumP.frame;
        [_btnNotLimitGender setImage:imgNotChoice forState:UIControlStateNormal];
        [_btnNotLimitGender setTitle:@"不限" forState:UIControlStateNormal];
        _btnNotLimitGender.titleLabel.font = [UIFont systemFontOfSize:12.0];
        [_btnNotLimitGender setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        [_btnNotLimitGender setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
        [_btnNotLimitGender  addTarget:self action:@selector(clickbtnNotLimitGender) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:_btnNotLimitGender];
        
        // 仅限男
        _btnJustBoy = [[UIButton alloc]init];
        _btnJustBoy.frame = _btn6To10.frame;
        [_btnJustBoy setImage:imgNotChoice forState:UIControlStateNormal];
        [_btnJustBoy setTitle:@"仅限男" forState:UIControlStateNormal];
        _btnJustBoy.titleLabel.font = [UIFont systemFontOfSize:12.0];
        [_btnJustBoy setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
         [_btnJustBoy setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
         [_btnJustBoy  addTarget:self action:@selector(clickbtnJustBoy) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:_btnJustBoy];
        
        // 仅限女
        _btnJustGirl = [[UIButton alloc]init];
        _btnJustGirl.frame = _btn3To5.frame;
        [_btnJustGirl setImage:imgNotChoice forState:UIControlStateNormal];
        [_btnJustGirl setTitle:@"仅限女" forState:UIControlStateNormal];
        _btnJustGirl.titleLabel.font = [UIFont systemFontOfSize:12.0];
        [_btnJustGirl setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
          [_btnJustGirl setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
          [_btnJustGirl  addTarget:self action:@selector(clickbtnJustGirl) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:_btnJustGirl];
        
    } else if (5 == indexPath.row){
        lbFirst.font = [UIFont systemFontOfSize:14.0];
        lbFirst.textColor = [UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0];
        
        lbFirst.text = @"能否带朋友";
        lbFirst.frame = CGRectMake(frontX, 0, 75,cell.height);
        lbFirst.backgroundColor = [UIColor clearColor];
        
        // 能
        _btnCanFriends = [[UIButton alloc]init];
        _btnCanFriends.frame = _btn6To10.frame;
        [_btnCanFriends setImage:imgNotChoice forState:UIControlStateNormal];
        [_btnCanFriends setTitle:@"能" forState:UIControlStateNormal];
        _btnCanFriends.titleLabel.font = [UIFont systemFontOfSize:12.0];
        [_btnCanFriends setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        [_btnCanFriends setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
        [_btnCanFriends  addTarget:self action:@selector(clickbtnCanFriends) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:_btnCanFriends];
        
        // 否
        _btnNoFriends = [[UIButton alloc]init];
        _btnNoFriends.frame = _btnNotLimitNumP.frame;
        [_btnNoFriends setImage:imgNotChoice forState:UIControlStateNormal];
        [_btnNoFriends setTitle:@"否" forState:UIControlStateNormal];
        _btnNoFriends.titleLabel.font = [UIFont systemFontOfSize:12.0];
        [_btnNoFriends setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
         [_btnNoFriends setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
         [_btnNoFriends  addTarget:self action:@selector(clickbtnNoFriends) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:_btnNoFriends];
        
        
    }else if (6 == indexPath.row){
        lbFirst.font = [UIFont systemFontOfSize:14.0];
        lbFirst.textColor = [UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0];
        
        lbFirst.text = @"费用";
        lbFirst.frame = CGRectMake(frontX, 0, 60,cell.height);
        lbFirst.backgroundColor = [UIColor clearColor];
        
        // 免费
        _btnFree = [[UIButton alloc]init];
        _btnFree.frame = _btn3To5.frame;
        [_btnFree setImage:imgNotChoice forState:UIControlStateNormal];
        [_btnFree setTitle:@"免费" forState:UIControlStateNormal];
        _btnFree.titleLabel.font = [UIFont systemFontOfSize:12.0];
        [_btnFree setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        [_btnFree setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
        [_btnFree addTarget:self action:@selector(clickbtnFree) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:_btnFree];
        
        // AA
        _btnAa = [[UIButton alloc]init];
        _btnAa.frame = _btn6To10.frame;
        [_btnAa setImage:imgNotChoice forState:UIControlStateNormal];
        [_btnAa setTitle:@"AA" forState:UIControlStateNormal];
        _btnAa.titleLabel.font = [UIFont systemFontOfSize:12.0];
        [_btnAa setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
         [_btnAa setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
         [_btnAa  addTarget:self action:@selector(clickbtnAa) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:_btnAa];
        
        // 男A女免
        _btnGirlsFree = [[UIButton alloc]init];
        _btnGirlsFree.frame = _btnNotLimitNumP.frame;
        [_btnGirlsFree setImage:imgNotChoice forState:UIControlStateNormal];
        [_btnGirlsFree setTitle:@"男A女免" forState:UIControlStateNormal];
        _btnGirlsFree.titleLabel.font = [UIFont systemFontOfSize:12.0];
        [_btnGirlsFree setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
          [_btnGirlsFree setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
          [_btnGirlsFree  addTarget:self action:@selector(clickbtnGirlsFree) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:_btnGirlsFree];
        
    }else if(7 == indexPath.row){
        lbFirst.font = [UIFont systemFontOfSize:14.0];
        lbFirst.textColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
        
        lbFirst.text = @"邀请TA";
        lbFirst.frame = CGRectMake(frontX, 0, 60,64); // 64是该cell的高度
        lbFirst.backgroundColor = [UIColor clearColor];
        
        // 加号
        _btnAddPeople = [[UIButton alloc]init];
        UIImage *imgAddPeople = [UIImage imageNamed:@"addPeople.png"];
        _btnAddPeople.frame = CGRectMake(screenWidth - 4 * frontX - imgChoice.size.width, 0, 80, 64); // 为什么2倍的frontX时不显示，未知，暂放
        [_btnAddPeople setImage:imgAddPeople forState:UIControlStateNormal];
        [_btnAddPeople setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
        [_btnAddPeople addTarget:self action:@selector(clickbtnAddPeople) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:_btnAddPeople];
    }
    

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];

    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 8;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row != 2 && indexPath.row != 7) {
        return 44;
    } else if (2 == indexPath.row) {
        return 55;
    }else if(7 == indexPath.row){
        return 64;
    }else{
        return 0;
    }
}

#pragma mark 完成按钮
-(void)setAddbtn
{
    UIButton *addBtn = [[UIButton alloc]initWithFrame:CGRectMake(frontX, _mTableView.y + _mTableView.height + 10, screenWidth - 2 * frontX, 44)];
    addBtn.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
    [addBtn setTitle:@"完成" forState:UIControlStateNormal];
    [addBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    addBtn.layer.masksToBounds = YES;
    addBtn.layer.cornerRadius = 8.0;
    [_svBg addSubview:addBtn];
}

#pragma mark setUpScrollView
-(void)setUpScrollView
{
    _svBg  = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    _svBg.contentSize = CGSizeMake(screenWidth, 665); // 665是滚动视图的高度
    _svBg.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_svBg];
}

#pragma mark - 聚会主题 & 描述
-(void)setPartyThemeAndDes
{
    // 主题输入框
    UIImage *imgPartyTheme = [UIImage imageNamed:@"partyTheme.png"];
    
    CGFloat copyrightTextViewW = imgPartyTheme.size.width;
    CGFloat copyrightTextViewH = imgPartyTheme.size.height;
    CGFloat copyrightTextViewX = (screenWidth - copyrightTextViewW) * 0.5;
    CGFloat copyrightTextViewY = _segmentedControl.y + _segmentedControl.height + 20;
    CGRect copyrightTextViewFrame = CGRectMake(copyrightTextViewX, copyrightTextViewY, copyrightTextViewW, copyrightTextViewH);
    _tvPartyTheme = [[UITextView alloc]initWithFrame:copyrightTextViewFrame];
    [_tvPartyTheme setBackgroundColor:[UIColor colorWithPatternImage:imgPartyTheme]];
    [_svBg addSubview:_tvPartyTheme];
    
    _tvPartyTheme.delegate = self;
    _tvPartyTheme.enablesReturnKeyAutomatically = YES;
    
    // 文字从UITextView顶端开始显示
    self.automaticallyAdjustsScrollViewInsets = NO;
    _tvPartyTheme.showsVerticalScrollIndicator = NO;
    _tvPartyTheme.text = @"请输入一个聚会主题";
    [_tvPartyTheme setFont:[UIFont systemFontOfSize:12.0]];
    _tvPartyTheme.textColor = [UIColor rgbFromHexString:@"#6C6C6C" alpaa:1.0];
    _tvPartyTheme.textAlignment = NSTextAlignmentLeft;
    
    // 字数提示-label
    CGFloat charCountsTipW = 100;
    CGFloat charCountsTipH = 20;
    CGFloat charCountsTipX = copyrightTextViewW - charCountsTipW;
    CGFloat charCountsTipY = copyrightTextViewH - charCountsTipH;
    CGRect charCountsTipFrame = CGRectMake(charCountsTipX, charCountsTipY, charCountsTipW, charCountsTipH);
    self.charCountsTip = [[UILabel alloc]initWithFrame:charCountsTipFrame];
    [_tvPartyTheme addSubview:self.charCountsTip];
    
    [self.charCountsTip setTextColor:[UIColor whiteColor]];
    [self.charCountsTip setFont:[UIFont systemFontOfSize:12]];
    [self.charCountsTip setText:[NSString stringWithFormat:@"15"]];
    self.charCountsTip.textAlignment = NSTextAlignmentRight;
    
    
    // 约Party描述
    UIImage *imgPartyDesc = [UIImage imageNamed:@"partyDesc.png"];;
    _tvPartyDesc  = [[UITextView alloc]init];
    _tvPartyDesc.frame = CGRectMake(copyrightTextViewX, copyrightTextViewY + copyrightTextViewH + 5, imgPartyDesc.size.width, imgPartyDesc.size.height);
    _tvPartyDesc.backgroundColor = [UIColor colorWithPatternImage:imgPartyDesc];
    [_svBg addSubview:_tvPartyDesc];
    
    _tvPartyDesc.delegate = self;
    _tvPartyDesc.enablesReturnKeyAutomatically = YES;
    
    // 文字从UITextView顶端开始显示
    self.automaticallyAdjustsScrollViewInsets = NO;
    _tvPartyDesc.showsVerticalScrollIndicator = NO;
    _tvPartyDesc.text = @"简单描述几句\n让大家知道你不是机器人";
    [_tvPartyDesc setFont:[UIFont systemFontOfSize:12.0]];
    _tvPartyDesc.textColor = [UIColor rgbFromHexString:@"#CD88FF" alpaa:1.0];
    _tvPartyDesc.textAlignment = NSTextAlignmentLeft;
    
}

/**
 开始编辑
 */
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if (!_tvPartyTheme.isFirstResponder) {
        if ([_tvPartyTheme.text isEqualToString:@"请输入一个聚会主题"]) {
            _tvPartyTheme.text = @"";
        }
        
        _tvPartyTheme.textColor = [UIColor rgbFromHexString:@"#573305" alpaa:1.0];
        
        return YES;
    }
    else { // _tvPartyDesc.isFirstResponder
        if ([_tvPartyDesc.text isEqualToString:@"简单描述几句\n让大家知道你不是机器人"]) {
            _tvPartyDesc.text = @"";
        }
        
        _tvPartyDesc.textColor = [UIColor rgbFromHexString:@"#CD88FF" alpaa:1.0];
        
        return YES;
    }
}

/**
 监听字数改变
 */
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if (_tvPartyTheme.isFirstResponder) {
        NSString * aString = [textView.text stringByReplacingCharactersInRange:range withString:text];
        
        if (_tvPartyTheme == textView)
        {
            if ([aString length] > 15) {
                textView.text = [aString substringToIndex:15];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                message:@"不能超过15字"
                                                               delegate:nil
                                                      cancelButtonTitle:@"确定"
                                                      otherButtonTitles:nil, nil];
                [alert show];
                return NO;
            }
        }
        
        
        if([text isEqualToString:@"\n"]) {
            
            [self.view endEditing:YES];
            
            if ([_tvPartyTheme.text isEqualToString:@""]) {
                UIAlertView *feedBackAlert = [[UIAlertView alloc]initWithTitle:nil message:@"内容不能为空" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
                [feedBackAlert show];
                
                [self performSelector:@selector(disNilFeedBackAlert:) withObject:feedBackAlert afterDelay:1.2];
            }
            
            
            return YES;
        }
    }
    
    
    if (_tvPartyDesc.isFirstResponder) {
        if (_tvPartyDesc.isFirstResponder) {
            
            if([text isEqualToString:@"\n"]) {
                
                [self.view endEditing:YES];
                
                if ([_tvPartyDesc.text isEqualToString:@""]) {
                    UIAlertView *feedBackAlert = [[UIAlertView alloc]initWithTitle:nil message:@"内容不能为空" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
                    [feedBackAlert show];
                    
                    [self performSelector:@selector(disNilFeedBackAlert:) withObject:feedBackAlert afterDelay:1.2];
                }
                
                
                return YES;
            }
        }
    }

    return YES;
}


-(void)disNilFeedBackAlert:(UIAlertView *)alert
{
    [alert dismissWithClickedButtonIndex:[alert cancelButtonIndex] animated:YES];
}

#pragma mark -segment
-(void)setUpSegment
{
    NSArray *setmentAry = [NSArray arrayWithObjects:@"所有人可见",@"被邀请人可见", nil];
    _segmentedControl = [[UISegmentedControl alloc]initWithItems:setmentAry];
    _segmentedControl.frame = CGRectMake(frontX,naviAndStatusH + 5, screenWidth - 2 * frontX, 40);
    _segmentedControl.tintColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
    _segmentedControl.selectedSegmentIndex = 0;
    [self.svBg addSubview:_segmentedControl];
}

#pragma mark 导航栏
-(void)setNavigation
{
    [self setLeftBtnItem];
    [self setRightBtnItem];
    
    self.navigationItem.title = @"发起Party";
    
}

// 导航栏左边的Btn
-(void)setLeftBtnItem
{
    UIImage *leftSearchImg = [UIImage imageNamed:@"whiteBack.png"];
    UIButton *leftBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, leftSearchImg.size.width,leftSearchImg.size.height)];
    [leftBtn setImage:leftSearchImg forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(clickLeftBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customLeftBtnItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = customLeftBtnItem;
}

// 导航栏右边的Btn
-(void)setRightBtnItem
{
    UIImage *rightBtImg = [UIImage imageNamed:@"more.png"];
    UIButton *rightBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, rightBtImg.size.width,rightBtImg.size.height)];
    [rightBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -25)];
    [rightBtn setImage:rightBtImg forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(clickRightBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customRightBtnItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = customRightBtnItem;
}

#pragma mark -点击事件
-(void)clickbtnAppointment{
     // 2人约会
    _btn3To5.selected = NO;
    _btn6To10.selected = NO;
    _btnNotLimitNumP.selected = NO;
    
    if (_btnAppointment.isSelected) {
        _btnAppointment.selected = NO;
        [_btnAppointment setImage:imgNotChoice forState:UIControlStateNormal];
        [_btnAppointment setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
    } else{
        _btnAppointment.selected = YES;
        [_btnAppointment setImage:imgChoice forState:UIControlStateNormal];
        [_btnAppointment setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateSelected];
    }
}

-(void)clickbtn3To5{
    // 3到5人
    if (_btn3To5.isSelected) {
        _btn3To5.selected = NO;
        [_btn3To5 setImage:imgNotChoice forState:UIControlStateNormal];
        [_btn3To5 setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
    } else{
        _btn3To5.selected = YES;
        [_btn3To5 setImage:imgChoice forState:UIControlStateNormal];
        [_btn3To5 setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateSelected];
    }
}

-(void)clickbtn6To10{
    // 6到10人
    if (_btn6To10.isSelected) {
        _btn6To10.selected = NO;
        [_btn6To10 setImage:imgNotChoice forState:UIControlStateNormal];
        [_btn6To10 setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
    } else{
        _btn6To10.selected = YES;
        [_btn6To10 setImage:imgChoice forState:UIControlStateNormal];
        [_btn6To10 setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateSelected];
    }
}

-(void)clickbtnNotLimitNumP{
    // 不限制人数
    if (_btnNotLimitNumP.isSelected) {
        _btnNotLimitNumP.selected = NO;
        [_btnNotLimitNumP setImage:imgNotChoice forState:UIControlStateNormal];
        [_btnNotLimitNumP setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
    } else{
        _btnNotLimitNumP.selected = YES;
        [_btnNotLimitNumP setImage:imgChoice forState:UIControlStateNormal];
        [_btnNotLimitNumP setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateSelected];
    }
}

-(void)clickbtnJustGirl{
    // 限女生
    if (_btnJustGirl.isSelected) {
        _btnJustGirl.selected = NO;
        [_btnJustGirl setImage:imgNotChoice forState:UIControlStateNormal];
        [_btnJustGirl setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
    } else{
        _btnJustGirl.selected = YES;
        [_btnJustGirl setImage:imgChoice forState:UIControlStateNormal];
        [_btnJustGirl setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateSelected];
    }
}

-(void)clickbtnJustBoy{
    // 限男生
    if (_btnJustBoy.isSelected) {
        _btnJustBoy.selected = NO;
        [_btnJustBoy setImage:imgNotChoice forState:UIControlStateNormal];
        [_btnJustBoy setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
    } else{
        _btnJustBoy.selected = YES;
        [_btnJustBoy setImage:imgChoice forState:UIControlStateNormal];
        [_btnJustBoy setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateSelected];
    }
}-(void)clickbtnNotLimitGender{
    // 不限男女
    if (_btnNotLimitGender.isSelected) {
        _btnNotLimitGender.selected = NO;
        [_btnNotLimitGender setImage:imgNotChoice forState:UIControlStateNormal];
        [_btnNotLimitGender setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
    } else{
        _btnNotLimitGender.selected = YES;
        [_btnNotLimitGender setImage:imgChoice forState:UIControlStateNormal];
        [_btnNotLimitGender setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateSelected];
    }
}

-(void)clickbtnNoFriends{
if (_btnNoFriends.isSelected) {
    _btnNoFriends.selected = NO;
    [_btnNoFriends setImage:imgNotChoice forState:UIControlStateNormal];
    [_btnNoFriends setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
} else{
    _btnNoFriends.selected = YES;
    [_btnNoFriends setImage:imgChoice forState:UIControlStateNormal];
    [_btnNoFriends setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateSelected];
}
}

-(void)clickbtnCanFriends{
    // 可以带朋友
    if (_btnCanFriends.isSelected) {
        _btnCanFriends.selected = NO;
        [_btnCanFriends setImage:imgNotChoice forState:UIControlStateNormal];
        [_btnCanFriends setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
    } else{
        _btnCanFriends.selected = YES;
        [_btnCanFriends setImage:imgChoice forState:UIControlStateNormal];
        [_btnCanFriends setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateSelected];
    }
}

-(void)clickbtnFree{
    // 免费
    if (_btnFree.isSelected) {
        _btnFree.selected = NO;
        [_btnFree setImage:imgNotChoice forState:UIControlStateNormal];
        [_btnFree setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
    } else{
        _btnFree.selected = YES;
        [_btnFree setImage:imgChoice forState:UIControlStateNormal];
        [_btnFree setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateSelected];
    }
}

-(void)clickbtnAa{
     // AA
    if (_btnAa.isSelected) {
        _btnAa.selected = NO;
        [_btnAa setImage:imgNotChoice forState:UIControlStateNormal];
        [_btnAa setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
    } else{
        _btnAa.selected = YES;
        [_btnAa setImage:imgChoice forState:UIControlStateNormal];
        [_btnAa setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateSelected];
    }
}

-(void)clickbtnGirlsFree{
    // 男A女免
    if (_btnGirlsFree.isSelected) {
        _btnGirlsFree.selected = NO;
        [_btnGirlsFree setImage:imgNotChoice forState:UIControlStateNormal];
        [_btnGirlsFree setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
    } else{
        _btnGirlsFree.selected = YES;
        [_btnGirlsFree setImage:imgChoice forState:UIControlStateNormal];
        [_btnGirlsFree setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateSelected];
    }
}

-(void)clickbtnAddPeople{
   // 邀请盆友加号
    YDLog(@"添加盆友");
}

-(void)clickLeftBarBtnItem
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)clickRightBarBtnItem
{
    YDLog(@"点击了更多按钮");
}

-(void)clickGrayBg
{
    _btnGrayBg.hidden = YES;
    YDLog(@"点击了灰质的背景");
}

#pragma mark tableView的代理 & 数据源
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   long int selectRowNum = indexPath.row;
    if (0 == selectRowNum) {
        PartyLocationVC *partyLocationVC = [[PartyLocationVC alloc]init];
        [self.navigationController pushViewController:partyLocationVC animated:YES];
    }else if(1 == selectRowNum){
        
        _btnGrayBg.hidden = NO;
        [_viewDatePickerBg setHidden:NO];
    }
    
}

#pragma mark 时间选择
#pragma mark  时间选择器
-(void)setupDatePicker
{
    // 灰质的背景
    _btnGrayBg = [[UIButton alloc]init];
    _btnGrayBg.frame = CGRectMake(0, 0, screenWidth, screenHeight);
    _btnGrayBg.backgroundColor = [UIColor rgbFromHexString:@"#A8AEAE" alpaa:0.80];
    [_btnGrayBg addTarget:self action:@selector(clickGrayBg) forControlEvents:UIControlEventTouchUpInside];
    _btnGrayBg.hidden = YES;
    [_svBg addSubview:_btnGrayBg];
    
    // 时间选择器
    CGFloat datePickerW = screenWidth;
    CGFloat datePickerH = 230;
    CGFloat datePickerX = 0;
    CGFloat datePickerY = 40 + 1;// 40是取消和完成btn的高度，1是紫色分割线的高度
    CGRect datePickerFrame = CGRectMake(datePickerX, datePickerY, datePickerW, datePickerH);
    self.datePicker = [[UIDatePicker alloc]init];
    [self.datePicker setFrame:datePickerFrame];
    
    self.datePicker.backgroundColor = [UIColor colorWithRed:255 green:255 blue:255 alpha:1.0];
    self.datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    [self.datePicker setDate:[NSDate date]];
//    [self.datePicker addTarget:self action:@selector(dateTimeChange) forControlEvents:UIControlEventValueChanged];
    
    // 取消Btn
    CGFloat timeCancelBtnW = 50;
    CGFloat timeCancelBtnH = 40;
    CGFloat timeCancelBtnX = 50;
    CGFloat timeCancelBtnY = 0;
    CGRect timeCancelBtnFrame = CGRectMake(timeCancelBtnX, timeCancelBtnY, timeCancelBtnW, timeCancelBtnH);
    self.timeCancelBtn = [[UIButton alloc]initWithFrame:timeCancelBtnFrame];
    
    [self.timeCancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [self.timeCancelBtn setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
    [self.timeCancelBtn addTarget:self action:@selector(clickTimeCancelBtn) forControlEvents:UIControlEventTouchUpInside];
    
    // 确定Btn
    CGFloat timeChooseBtnW = timeCancelBtnW;
    CGFloat timeChooseBtnH = timeCancelBtnH;
    CGFloat timeChooseBtnX = screenWidth - timeCancelBtnX - timeChooseBtnW;
    CGFloat timeChooseBtnY = timeCancelBtnY;
    CGRect timeChooseBtnFrame = CGRectMake(timeChooseBtnX, timeChooseBtnY, timeChooseBtnW, timeChooseBtnH);
    self.timeChooseBtn = [[UIButton alloc]initWithFrame:timeChooseBtnFrame];
    
    [self.timeChooseBtn setTitle:@"完成" forState:UIControlStateNormal];
    [self.timeChooseBtn setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateNormal];
    [self.timeChooseBtn addTarget:self action:@selector(clickTimeChooseBtn) forControlEvents:UIControlEventTouchUpInside];
    
    // 紫色分割线
    UIImageView *imgLine = [[UIImageView alloc]init];
    imgLine.frame = CGRectMake(0, 40, screenWidth, 1);
    imgLine.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:0.12];
    
    // 时间选择器 & 取消 & 完成 的背景视图
    _viewDatePickerBg = [[UIView alloc]init];
    CGFloat datePickerBgH = 271;// 40 + 1 + 230;
    _viewDatePickerBg.frame = CGRectMake(0,screenHeight -  datePickerBgH, screenWidth, datePickerBgH);
    _viewDatePickerBg.backgroundColor = [UIColor whiteColor];
    _viewDatePickerBg.hidden = YES;
    [_svBg addSubview:_viewDatePickerBg];
    
    [_viewDatePickerBg addSubview:self.timeCancelBtn];
    [_viewDatePickerBg addSubview:self.timeChooseBtn];
    [_viewDatePickerBg addSubview:imgLine];
    [_viewDatePickerBg addSubview:self.datePicker];

}

-(void)clickTimeChooseBtn
{
    NSDate *select = [self.datePicker date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yy-MM-dd HH:mm"];
    NSString *dateAndTime = [dateFormatter stringFromDate:select];
    _strTime = dateAndTime;
    _viewDatePickerBg.hidden = YES;
    _btnGrayBg.hidden = YES;
    [_mTableView reloadData];
}

-(void)clickTimeCancelBtn
{
    _viewDatePickerBg.hidden = YES;
    _btnGrayBg.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
