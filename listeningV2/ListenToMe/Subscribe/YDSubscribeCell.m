//
//  YDSubscribeCell.m
//  ListenToMe
//
//  Created by yadong on 2/4/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "YDSubscribeCell.h"

@interface YDSubscribeCell ()
//@property(strong,nonatomic) UIImageView *imgHorizLine;
@property(strong,nonatomic) UIImageView *imgLongVertiLine;
@property(strong,nonatomic) UIImageView *imgShortVertiLine;
@property(strong,nonatomic) UIImageView *imgTrian;
//@property(strong,nonatomic) UIImage *imgAvatar;
//@property(strong,nonatomic) UILabel *lbUserName;
//@property(strong,nonatomic) UILabel *lbDesc;
//@property(strong,nonatomic) UILabel *lbPartyName;
//@property(strong,nonatomic) UILabel *lbLocation;
//@property(strong,nonatomic) UILabel *lbSubsTime;
//@property(strong,nonatomic) UILabel *lbDistance;
//@property(strong,nonatomic) UIButton *btnLimit;
//@property(strong,nonatomic) UIButton *btnAffordPerson;
//@property(strong,nonatomic) UIButton *btnApply;
//@property(strong,nonatomic) UIButton *btnJoin;
//@property(strong,nonatomic) UIButton *btnDiscuss;
@end

@implementation YDSubscribeCell
//@synthesize imgHorizLine;
@synthesize imgLongVertiLine;
@synthesize imgShortVertiLine;
@synthesize imgAvatar;
@synthesize lbUserName;
@synthesize lbDesc;
@synthesize lbPartyName;
@synthesize lbLocation;
@synthesize lbSubsTime;
@synthesize lbDistance;
@synthesize btnApply;
@synthesize btnJoin;
@synthesize btnDiscuss;
@synthesize imgTrian;

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUI];
        
    }
    return self;
}

-(void)setUI
{
    CGFloat frontX = 15.0; // 子控件距离screen的前后间距
    
    // 头像
    CGFloat imgAvatarW = 50;
    CGFloat imgAvatarH = 50;
    CGFloat imgAvatarX = 10;
    CGFloat imgAvatarY = 10;
    CGRect imgAvatarFrame = CGRectMake(imgAvatarX, imgAvatarY, imgAvatarW, imgAvatarH);
    imgAvatar = [[UIImageView alloc]initWithFrame:imgAvatarFrame];
    imgAvatar.layer.cornerRadius = 8;
    imgAvatar.layer.masksToBounds = YES;
    [self addSubview:imgAvatar];
    
    imgAvatar.backgroundColor = [UIColor clearColor];
    
    // 用户名
    CGRect lbUserNameFrame = CGRectMake(imgAvatarX, imgAvatarY + imgAvatarH + 5, screenWidth * 0.5, 20);
    lbUserName = [[UILabel alloc]initWithFrame:lbUserNameFrame];
    [self addSubview:lbUserName];
    
    [lbUserName setFont:[UIFont systemFontOfSize:11.0]];
    [lbUserName setTextColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
    
    UIImageView *lineImg_0 = [[UIImageView alloc]initWithFrame:CGRectMake(frontX, lbUserName.y + lbUserName.height - 1, screenWidth - 2 * frontX, 1)];
    lineImg_0.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
    [self.contentView addSubview:lineImg_0];
    
    // 小三角
    UIImage *trianImg = [UIImage imageNamed:@"leftsanjiao.png"];
    imgTrian = [[UIImageView alloc]initWithFrame:CGRectMake(imgAvatarX + imgAvatarW + 5, imgAvatarY + (imgAvatarH - trianImg.size.height) * 0.5, trianImg.size.width, trianImg.size.height)];
    imgTrian.image = trianImg;
    [self addSubview:imgTrian];
    
    // 约歌描述
    UIImage *lbDescImg = [UIImage imageNamed:@"juxing.png"];
    lbDesc = [[UILabel alloc]initWithFrame:CGRectMake(imgTrian.frame.size.width + imgTrian.frame.origin.x, imgAvatarY + (imgAvatarH - lbDescImg.size.height) * 0.5, lbDescImg.size.width, lbDescImg.size.height)];
    [lbDesc setBackgroundColor:[UIColor colorWithPatternImage:lbDescImg]];
    lbDesc.layer.masksToBounds = YES;
    lbDesc.layer.cornerRadius = 6.5;
    [lbDesc setTextColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
    lbDesc.font = [UIFont systemFontOfSize:12.0];
    [self addSubview:lbDesc];

    // 聚会名称
    CGFloat lbPartyNameH = 40;
    CGFloat lbPartyNameX = imgAvatarX;
    CGFloat lbPartyNameY = lbUserName.frame.origin.y + lbUserName.frame.size.height + 1;   // 1的高度是留给后边要加上的水平线
    CGRect lbPartyNameFrame = CGRectMake(lbPartyNameX, lbPartyNameY, screenWidth, lbPartyNameH);
    lbPartyName  = [[UILabel alloc]initWithFrame:lbPartyNameFrame];
    lbPartyName.font = [UIFont systemFontOfSize:15.0];
    lbPartyName.textColor = [UIColor rgbFromHexString:@"#4A4A4A" alpaa:1.0];
    lbPartyName.backgroundColor = [UIColor clearColor];
    [self addSubview:lbPartyName];
    
    // 横线
    UIImageView *lineImg_1 = [[UIImageView alloc]initWithFrame:CGRectMake(frontX, lbPartyName.y + lbPartyName.height - 1, screenWidth - 2 * frontX, 1)];
    lineImg_1.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
    [self.contentView addSubview:lineImg_1];
    
    // 聚会距离 & 聚会地点&时间 的宽高
    CGFloat marginRight = 10;
    CGFloat lbDistanceW = 45;
    CGFloat lbDistanceH = 20;
    
    // 聚会地点
    CGFloat lbLocationW = screenWidth - marginRight - lbDistanceW - imgAvatarX;
    CGFloat lbLocationH = lbDistanceH;
    CGFloat lbLocationX = imgAvatarX;
    CGFloat lbLocationY = lbPartyNameY + lbPartyNameH + 5;
    CGRect lbLocationFrame = CGRectMake(lbLocationX, lbLocationY, lbLocationW, lbLocationH);
    lbLocation = [[UILabel alloc]initWithFrame:lbLocationFrame];
    lbLocation.font = [UIFont systemFontOfSize:12.0];
    [self addSubview:lbLocation];
    lbLocation.backgroundColor = [UIColor clearColor];
    lbLocation.textColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
    
    // 聚会时间
    CGFloat lbSubsTimeY = lbLocationY + lbLocationH;
    lbSubsTime = [[UILabel alloc]initWithFrame:CGRectMake(lbLocationX, lbSubsTimeY, lbLocationW, lbLocationH)];
    lbSubsTime.font = [UIFont systemFontOfSize:12.0];
    lbSubsTime.textColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
    [self addSubview:lbSubsTime];
    
    // 横线
    UIImageView *lineImg_2 = [[UIImageView alloc]initWithFrame:CGRectMake(frontX, lbSubsTime.y + lbSubsTime.height - 1, screenWidth - 2 * frontX, 1)];
    lineImg_2.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
    [self.contentView addSubview:lineImg_2];
    
    CGFloat lbDistanceX = lbLocationX + lbLocationW;
    lbDistance = [[UILabel alloc]initWithFrame:CGRectMake(lbDistanceX, lbLocationY , lbDistanceW, lbDistanceH)];
    [self addSubview:lbDistance];
    lbDistance.textColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
    lbDistance.font = [UIFont systemFontOfSize:12.0];
    lbDistance.backgroundColor = [UIColor clearColor];
    
    // 横线
    CGFloat limitPayCellH = 80;    // 约歌限制条件 & 谁付款cell的高度
    UIImageView *lineImg_3 = [[UIImageView alloc]initWithFrame:CGRectMake(frontX, lineImg_2.y + lineImg_2.height + limitPayCellH, screenWidth - 2 * frontX, 1)];
    lineImg_3.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
    [self.contentView addSubview:lineImg_3];
    
    // 竖直灰线
    CGFloat lineVerticalImgH = 60; // 竖直线的高度
    UIImageView *lineVerticalIMg = [[UIImageView alloc]initWithFrame:CGRectMake(2 * frontX, (limitPayCellH - lineVerticalImgH) * 0.5 + lineImg_2.y + lineImg_2.height,2, lineVerticalImgH)];
    lineVerticalIMg.backgroundColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:0.8];
    [self.contentView addSubview:lineVerticalIMg];
    
    // 约歌限制条件
    UIImage *limitImg = [UIImage imageNamed:@"fujian.png"];
    CGFloat limitAndVetticalMargin = 8;    // 约歌限制条件的图片距离竖线的距离
    CGFloat limitLbAndImg = 5;
    
    CGFloat lbLimitH = 20; // 限制条件lb的高度
    _lbLinmit = [[UILabel alloc]initWithFrame:CGRectMake(lineVerticalIMg.x + lineVerticalIMg.width + limitImg.size.width + limitAndVetticalMargin + limitLbAndImg, lineVerticalIMg.y + 5, screenWidth * 0.65, lbLimitH)];
    _lbLinmit.font = [UIFont systemFontOfSize:14.0];
    _lbLinmit.textColor = [UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0];
    [self.contentView addSubview:_lbLinmit];

    CGFloat imgAndLbMargin =    0.5 * (_lbLinmit.height - limitImg.size.height); // 限制条件的图片和限制lb的y上的偏移
    _imgLimit = [[UIImageView alloc]initWithFrame:CGRectMake(lineVerticalIMg.x + lineVerticalIMg.width + limitAndVetticalMargin, _lbLinmit.y + imgAndLbMargin, limitImg.size.width, limitImg.size.height)];
    _imgLimit.image = limitImg;
    [self.contentView addSubview:_imgLimit];
    
    
    // 谁付款
    UIImage *affordImg = [UIImage imageNamed:@"money.png"];
    _imgAffordPerson = [[UIImageView alloc]initWithFrame:CGRectMake(_imgLimit.x, lineVerticalIMg.y + lineVerticalIMg.height - affordImg.size.height- 2 * imgAndLbMargin, affordImg.size.width, affordImg.size.height)];
    _imgAffordPerson.image = affordImg;
    [self.contentView addSubview:_imgAffordPerson];
    
    _lbAffordPerson = [[UILabel alloc]initWithFrame:CGRectMake(_lbLinmit.x, _imgAffordPerson.y - imgAndLbMargin, _lbLinmit.width, _lbLinmit.height)];
    _lbAffordPerson.font = [UIFont systemFontOfSize:14.0];
    _lbAffordPerson.textColor = [UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0];
    [self.contentView addSubview:_lbAffordPerson];

    // 谁发起的
    _btnWhoLaunch = [[UIButton alloc]init];
    _btnWhoLaunch.frame = CGRectMake(frontX, lineImg_3.y + lineImg_3.height, 100, 40);
    _btnWhoLaunch.titleLabel.font = [UIFont systemFontOfSize:14.0];
    [_btnWhoLaunch setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateNormal];
    _btnWhoLaunch.titleLabel.textAlignment = NSTextAlignmentLeft;
    _btnWhoLaunch.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _btnWhoLaunch.hidden = YES; // 只有在我的约歌中才显示，其他情况隐藏
    [self.contentView addSubview:_btnWhoLaunch];
    
    // 申请人数 & 加入人数 & 讨论数
    btnApply = [[UIButton alloc]init];
    btnJoin = [[UIButton alloc]init];
    btnDiscuss = [[UIButton alloc]init];
    [self.contentView addSubview:btnApply];
    [self.contentView addSubview:btnJoin];
    [self.contentView addSubview:btnDiscuss];
    
    UIImage *applyImg = [UIImage imageNamed:@"lookrenshu.png"];
    UIImage *joingImg = [UIImage imageNamed:@"renshu.png"];
    UIImage *discussImg = [UIImage imageNamed:@"taolun.png"];
    [btnApply setImage:applyImg forState:UIControlStateNormal];
    [btnJoin setImage:joingImg forState:UIControlStateNormal];
    [btnDiscuss setImage:discussImg forState:UIControlStateNormal];
    
    CGFloat btnBottomW = 60; // 按钮的宽
    CGFloat btnBottomH = 40;
    CGFloat imgLineW = 1; // 竖线的宽
    btnDiscuss.frame = CGRectMake(screenWidth - frontX - btnBottomW, lineImg_3.y + lineImg_3.height, btnBottomW, btnBottomH);
    btnJoin.frame = CGRectMake(btnDiscuss.x - imgLineW - btnBottomW, btnDiscuss.y, btnBottomW, btnBottomH);
    btnApply.frame = CGRectMake(btnJoin.x - imgLineW - btnBottomW, btnDiscuss.y, btnBottomW, btnBottomH);
    
    [btnApply setImageEdgeInsets:UIEdgeInsetsMake(0, -25, 0, 0)];
    [btnJoin setImageEdgeInsets:UIEdgeInsetsMake(0, -25, 0, 0)];
    [btnDiscuss setImageEdgeInsets:UIEdgeInsetsMake(0, -25, 0, 0)];
    btnApply.titleLabel.textAlignment = NSTextAlignmentRight;
    
    [btnApply setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateNormal];
    [btnDiscuss setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateNormal];
    [btnJoin setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateNormal];
    btnDiscuss.titleLabel.font = [UIFont systemFontOfSize:11.0];
    btnJoin.titleLabel.font = [UIFont systemFontOfSize:11.0];
    btnApply.titleLabel.font = [UIFont systemFontOfSize:11.0];
    
    // 以上3个btn间的竖线
    UIImageView *imgVerticalImg_0 = [[UIImageView alloc]init];
    imgVerticalImg_0.frame = CGRectMake(btnDiscuss.x - 1, btnApply.y + 5, imgLineW, 30);
    imgVerticalImg_0.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:0.12];
    [self.contentView addSubview:imgVerticalImg_0];
    
    UIImageView *imgVerticalImg_1 = [[UIImageView alloc]init];
    imgVerticalImg_1.frame = CGRectMake(btnJoin.x - 1, btnApply.y + 5, imgLineW, 30);
    imgVerticalImg_1.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:0.12];
    [self.contentView addSubview:imgVerticalImg_1];
}



-(void)layoutSubviews
{
    [super layoutSubviews];
}

@end
