//
//  PartyLocationCell.h
//  ListenToMe
//
//  Created by yadong on 3/12/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PartyLocationCell : UITableViewCell
/**
 * 店名
 */
@property(strong,nonatomic) UILabel *lbSellerName;

/**
 * 位置
 */
@property(strong,nonatomic) UILabel *lbLocation;

/**
 * 距离
 */
@property(strong,nonatomic) UILabel *lbDistance;

/**
 * 消费-价格
 */
@property(strong,nonatomic) UILabel *lbPrice;

/**
* 电话
*/
@property(strong,nonatomic) UIButton *btnPhoneNum;

/**
 * 帅哥数
 */
@property(strong,nonatomic) UILabel *lbBoys;

/**
 * 美女数
 */
@property(strong,nonatomic) UILabel *lbGirls;
@end
