//
//  PartyLocationVC.m
//  ListenToMe
//
//  Created by yadong on 3/12/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "PartyLocationVC.h"
#import "PartyLocationCell.h"
#import "YDLaunchPartyVC.h"

@interface PartyLocationVC ()<UITableViewDataSource,UITableViewDelegate>
@property(strong,nonatomic) UITableView *mTableView;
@end

@implementation PartyLocationVC

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setNavigation];
    
    [self setUpTableView];
}


#pragma mark -导航栏
#pragma mark 导航栏
-(void)setNavigation
{
    [self setLeftBtnItem];
    [self setRightBtnItem];
    [self setNavigatonTitleVeiw];
}

// 导航栏的标题栏
-(void)setNavigatonTitleVeiw
{
    self.navigationItem.title = @"Party地点";
}

// 导航栏左边的Btn
-(void)setLeftBtnItem
{
    UIImage *leftBtImg = [UIImage imageNamed:@"whiteBack.png"];
    UIButton *leftBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, leftBtImg.size.width,leftBtImg.size.height)];
    [leftBtn setImage:leftBtImg forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(clickleftBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customleftBtnItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = customleftBtnItem;
}

// 导航栏右边的Btn
-(void)setRightBtnItem
{
    
    UIImage *rightSearchImg = [UIImage imageNamed:@"search.png"];
    UIButton *rightBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, rightSearchImg.size.width,rightSearchImg.size.height)];
    [rightBtn setImage:[UIImage resizableImageWithName:@"search.png"] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(clickRightBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customRightBtnItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = customRightBtnItem;
}


#pragma mark setUpTableView
-(void)setUpTableView
{
    CGRect mTableViewF = CGRectMake(0, 0, screenWidth, screenHeight);
    _mTableView = [[UITableView alloc]initWithFrame:mTableViewF style:UITableViewStyleGrouped];
    _mTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _mTableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_mTableView];
    
    _mTableView.delegate = self;
    _mTableView.dataSource = self;
}


#pragma mark -代理 & 数据源
-(PartyLocationCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *ID = @"PartyLocationCell";
    PartyLocationCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (cell == nil) {
        cell = [[PartyLocationCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    
    cell.lbSellerName.text = @"同一首歌 - 苏州桥店";
    cell.lbDistance.text = @"2KM";
    cell.lbLocation.text = @"北京朝阳区麦子店西街6号";
    cell.lbPrice.text = @"人均79元/人";
    [cell.btnPhoneNum setTitle:@"010-65656565" forState:UIControlStateNormal];
    cell.lbBoys.text = [NSString stringWithFormat:@"正在KTV  帅哥数51位"];
    cell.lbGirls.text = @"美女44位";
    
    cell.backgroundColor = [UIColor whiteColor];
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 5;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 15.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 139;
}

#pragma mark - 点击事件
-(void)clickRightBarBtnItem
{
    YDLog(@"点击了右边按钮");
}

-(void)clickleftBarBtnItem
{
    [self.navigationController popViewControllerAnimated:YES];
    YDLog(@"点击了左边按钮");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
