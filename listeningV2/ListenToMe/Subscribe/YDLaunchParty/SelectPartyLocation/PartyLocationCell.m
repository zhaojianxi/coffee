//
//  PartyLocationCell.m
//  ListenToMe
//
//  Created by yadong on 3/12/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "PartyLocationCell.h"
#define frontX 15
#define cellH 139

@implementation PartyLocationCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier]) {
        [self setUpUI];
    }
    
    return self;
}

-(void)setUpUI
{
    // 长横线
    // 高度-1
    UIImageView *imgLonfgLine_0 = [[UIImageView alloc]init];
    imgLonfgLine_0.frame = CGRectMake(0, 0, screenWidth, 1);
    imgLonfgLine_0.backgroundColor = [UIColor rgbFromHexString:@"#9B9B9B" alpaa:.12];
    [self addSubview:imgLonfgLine_0];
    
    // 店名
    // 距离顶部距离 12,自己高度 20
    _lbSellerName = [[UILabel alloc]init];
    _lbSellerName.frame = CGRectMake(frontX, 12, screenWidth * 0.7, 20);
    _lbSellerName.font = [UIFont systemFontOfSize:15.0];
    _lbSellerName.textColor = [UIColor rgbFromHexString:@"#4A4A4A" alpaa:1.0];
    [self addSubview:_lbSellerName];
    
    // 距离
    _lbDistance = [[UILabel alloc]init];
    _lbDistance.frame = CGRectMake(screenWidth - frontX - 33, _lbSellerName.y, 33, _lbSellerName.height);
    _lbDistance.font = [UIFont systemFontOfSize:12.0];
    _lbDistance.textColor = [UIColor rgbFromHexString:@"#B4B4B4" alpaa:1.0];
    [self addSubview:_lbDistance];
    
    // 位置
    // 高度 17
    _lbLocation = [[UILabel alloc]initWithFrame:CGRectMake(_lbSellerName.x, _lbDistance.y + _lbDistance.height, screenWidth - 2 * frontX, 17)];
    _lbLocation.textColor = [UIColor rgbFromHexString:@"#9B9B9B" alpaa:1.0];
    _lbLocation.font = [UIFont systemFontOfSize:12.0];
    [self addSubview:_lbLocation];
    
    // 横线
    // 距离位置12，自高 1
    UIImageView *imgLine_0 = [[UIImageView alloc]init];
    imgLine_0.frame = CGRectMake(frontX, _lbLocation.y + _lbLocation.height, screenWidth - 2 * frontX, 1);
    imgLine_0.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
    [self addSubview:imgLine_0];
    
    // 人均消费
    // 高度 38
    _lbPrice = [[UILabel alloc]init];
    _lbPrice.frame = CGRectMake(_lbSellerName.x, imgLine_0.y + imgLine_0.height, screenWidth * 0.35, 38);
    _lbPrice.textColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
    _lbPrice.font = [UIFont systemFontOfSize:15.0];
    [self addSubview:_lbPrice];
    
    // 电话
    _btnPhoneNum = [[UIButton alloc]init];
    _btnPhoneNum.frame = CGRectMake(screenWidth - frontX - screenWidth * 0.35, _lbPrice.y, screenWidth * 0.35, _lbPrice.height);
    _btnPhoneNum.titleLabel.font = [UIFont systemFontOfSize:15.0];
    [_btnPhoneNum setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateNormal];
    [_btnPhoneNum setTitleEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 0)];
    [_btnPhoneNum setImage:[UIImage imageNamed:@"partyLocationPhone@2x.png"] forState:UIControlStateNormal];
    [self addSubview:_btnPhoneNum];
    
    // 横线
    // 距离位置12，自高 1
    UIImageView *imgLine_1 = [[UIImageView alloc]init];
    imgLine_1.frame = CGRectMake(frontX, _lbPrice.y + _lbPrice.height, screenWidth - 2 * frontX, 1);
    imgLine_1.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
    [self addSubview:imgLine_1];
    
    // 帅哥美女数 & 正在Ktv
    // 高度25
    // 帅哥数
    _lbBoys = [[UILabel alloc]init];
    _lbBoys.frame = CGRectMake(frontX, imgLine_1.y + imgLine_1.height, screenWidth * 0.4, 50);
    _lbBoys.font = [UIFont systemFontOfSize:15.0];
    _lbBoys.textColor = [UIColor rgbFromHexString:@"#9B9B9B" alpaa:1.0];
    [self addSubview:_lbBoys];

    // 美女数
    _lbGirls = [[UILabel alloc]init];
    _lbGirls.frame = CGRectMake(_lbBoys.x + _lbBoys.width + 5, _lbBoys.y, 100, _lbBoys.height);
    _lbGirls.font = _lbBoys.font;
    _lbGirls.textColor = _lbBoys.textColor;
    [self addSubview:_lbGirls];
    
    // 长横线-1
    UIImageView *imgLonfgLine_1 = [[UIImageView alloc]init];
    imgLonfgLine_1.frame = CGRectMake(0, 138, screenWidth, 1);
    imgLonfgLine_1.backgroundColor = [UIColor rgbFromHexString:@"#9B9B9B" alpaa:.12];
    [self addSubview:imgLonfgLine_1];
    
}






@end
