//
//  AppDelegate.m
//  ListenToMe
//
//  Created by yadong on 1/24/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "AppDelegate.h"
#import "YDTabBarController.h"
#import "NetReqManager.h"
#import "UMSocialWechatHandler.h"
#import "UMSocialQQHandler.h"
#import "UMSocialSinaHandler.h"
#import "MainMenuViewController.h"
#import "FrontViewController.h"
#import "SWRevealViewController.h"
#import "RightViewController.h"
#import "YDSquareVC.h"
#import "YDNavigationController.h"
#import "YDSquareSearchVC.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
#if !USE_SLIDING_MENU
{
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    self.window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
    
    
    //    //everLanuched 判断是否登录 firstLanuch 判断是否是首次登录
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"everLaunched"]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"everLaunched"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstLaunch"];
        YDLog(@"第一次使用");
        //判断登录状态
        [[NSUserDefaults standardUserDefaults]boolForKey:@"launchStaue"];
    }
    else{
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"firstLaunch"];
//        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"everLaunched"];
        YDLog(@"已经不是第一次");
    }
    
    YDTabBarController *tabBarController = [[YDTabBarController alloc]init];
    self.window.rootViewController = tabBarController;
    [self.window makeKeyAndVisible];
    
    //集成社会化分享
    [self setUMSocialShare];
    
    //判断之前是否已经登录过,如果登录过那么就以之前登录过的uuid,快速登录
    [self quickLogin];
}
#else
    {

        //    //everLanuched 判断是否登录 firstLanuch 判断是否是首次登录
        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"everLaunched"]) {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"everLaunched"];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstLaunch"];
            YDLog(@"第一次使用");
            //判断登录状态
            [[NSUserDefaults standardUserDefaults]boolForKey:@"launchStaue"];
        }
        else{
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"firstLaunch"];
            //        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"everLaunched"];
            YDLog(@"已经不是第一次");
        }
        //集成社会化分享
        [self setUMSocialShare];
        
        //判断之前是否已经登录过,如果登录过那么就以之前登录过的uuid,快速登录
        [self quickLogin];
        
        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        //菜单栏
        MainMenuViewController *mainMenuController = [[MainMenuViewController alloc] init];
        YDNavigationController *navController = [[YDNavigationController alloc]initWithRootViewController:mainMenuController];

        //首页
        //FrontViewController *frontViewController = [[FrontViewController alloc] init];
        YDSquareSearchVC *frontViewController = [[YDSquareSearchVC alloc] init];
        // 2.为子控制器包装导航控制器
        YDNavigationController *navController2 = [[YDNavigationController alloc]initWithRootViewController:frontViewController];
        //YDSquareVC *frontViewController = [[YDSquareVC alloc]init];
        
        SWRevealViewController *revealViewController = [[SWRevealViewController alloc] initWithRearViewController:navController frontViewController:navController2];
        
        //右侧隐藏视图
//        RightViewController *rightViewController = [[RightViewController alloc] init];
//        revealViewController.rightViewController = rightViewController;
        
        //浮动层离左边距的宽度
        revealViewController.rearViewRevealWidth = self.window.frame.size.width/2;
        revealViewController.rightViewRevealWidth = self.window.frame.size.width/2;
        
        //是否让浮动层弹回原位
        //mainRevealController.bounceBackOnOverdraw = NO;
        [revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];
        
        self.window.rootViewController = revealViewController;
        self.window.backgroundColor = [UIColor whiteColor];
        [self.window makeKeyAndVisible];
    }
#endif
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark - setUMSocialShare

-(void)setUMSocialShare{

    [UMSocialData setAppKey:APPKEY];

    //显示所有图标
    
//    [UMSocialConfig showNotInstallPlatforms:nil];
    
    //设置QQ 微信
    
    [UMSocialWechatHandler setWXAppId:@"wxdc215ed7fa8c6824" appSecret:@"b42a9afd3244024d8350649278926645" url:@"http://www.tingwode.com/"];
    
//    [UMSocialQQHandler setQQWithAppId:@"100424468" appKey:APPKEY url:@"http://www.tingwode.com/"];
//    //设置支持没有客户端情况下是有那个soso授权
//    [UMSocialQQHandler setSupportWebView:YES];
//    //@"5211818556240bc9ee01db2f4" //
    
    [UMSocialQQHandler setQQWithAppId:@"100424468" appKey:@"c7394704798a158208a74ab60104f0ba" url:@"http://www.tingwode.com/"];//
    [UMSocialQQHandler setSupportWebView:YES];
    //
    [UMSocialSinaHandler openSSOWithRedirectURL:@"http://sns.whalecloud.com/sina2/callback"];
    
    
    
    
  
}


-(BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url{

    return [UMSocialSnsService handleOpenURL:url];
}

-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    return [UMSocialSnsService handleOpenURL:url];
}



#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.tingwode.ListenToMe" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"ListenToMe" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"ListenToMe.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        YDLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            YDLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}


#pragma mark - 快速登录
-(void)quickLogin{
    if ([ListenToMeDBManager getUuid] > 0) {
        [[NetReqManager getInstance] sendQuickLoginReq:[ListenToMeDBManager getUuid]];
    }
    
}
@end
