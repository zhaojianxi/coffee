//
//  NetHTTPTool.h
//  ListenToMe
//
//  Created by yadong on 1/28/15.
//  Copyright (c) 2015 listentome. All rights reserved.
// 网络工具类，专门负责发送网络请求

#import <Foundation/Foundation.h>

// 定义了一个successBlock的类型，TA没有返回值，也不接收任何参数
typedef void(^successBlock)(id responseObject);
typedef void(^failureBlock)(NSError *error);

@interface NetHTTPTool : NSObject

/**
 * 发送get请求
 * 
 * @param url 请求的地址
 * @param parameters 请求的参数
 * @param success 请求成功后的回调
 * @papam failure 请求失败的回调
 */
+(void)getWithUrl:(NSString *)url parameters:(NSDictionary *)parameters success:(successBlock)success failure:(failureBlock)failure;

/**
 * 发送post请求
 *
 * @param url 请求的地址
 * @param parameters 请求的参数
 * @param success 请求成功后的回调
 * @papam failure 请求失败的回调
 */
+(void)postWithUrl:(NSString *)url parameters:(NSDictionary *)parameters success:(successBlock)success failure:(failureBlock)failure;

@end
