//
//  NetReqManager.h
//  ListenToMe
//
//  Created by yadong on 1/27/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ListenToMe.pb.h"

typedef enum{
    Default,
    Sending,
    Prepareend
}SendStatus;

@interface NetReqManager : NSObject
{
    SendStatus sendStatus;
}

+(NetReqManager *)getInstance;
-(void)sendGetKtvListInfo:(int)offset;
-(void)sendGetPopularityList:(int)offset;
-(void)sendFeedback:(NSString *)feedback;
-(void)sendUpdate;
-(void)sendUploadMusicWorkCurSeq:(long long)curSeq sendDataDic:(NSMutableDictionary *)sendDataDic desc:(NSString*) sDesc songid:(long long)lSongID musicworkid:(long long)lMusicWorkID;
-(void)sendAddDateWorkInfo;
-(void)sendBindDevice:(NSString*)qrcode;
#pragma mine - zhw
-(void)sendLoginReq:(UserBaseInfoNet *)userBaseInfo;
-(void)sendQuickLoginReq:(int64_t)uuid;
-(void)sendLogoutReq:(int64_t)uuid;
-(void)sendGetKtvListInfo:(int32_t)iOffset INum:(int32_t)iNum;
-(void)sendGetKtvInfoById:(int64_t)lKtvid;
-(void)sendGetSwitchKtvUserList:(int32_t)iOffset INum:(int32_t)iNum LKtvId:(int64_t)lKtvId;
-(void)sendGetMusicWorkWithUserId:(int64_t)uuid IOffset:(int32_t)iOffset INum:(int32_t)iNum LWatchUserId:(int64_t)lWatchUserId;
-(void)sendGetMusicWorkByID:(int64_t)uuid LWorkID:(int64_t)lWorkID;
-(void)sendGetCommemorateInfo:(int64_t)uuid IOffset:(int32_t)iOffset INum:(int32_t)iNum;
-(void)sendGetPictureList:(int64_t)uuid IOffset:(int32_t)iOffset Inum:(int32_t)iNum LWatchUserId:(int64_t)lWatchUserId LCommemorateId:(int64_t)lCommenorateId;
-(void)sendKTVControlReq:(KTV_ROOM_CONTROL_TYPE)eKTVRoomControlType;
-(void)sendGetPopularityList:(int64_t)uuid IOffset:(int32_t)iOffset INum:(int32_t)iNum;
-(void)sendGetNewMusicList:(int64_t)uuid IOffset:(int32_t)iOffset INum:(int32_t)iNum;
-(void)sendGetRecommendList:(int64_t)uuid IOffset:(int32_t)iOffset INum:(int32_t)iNum;
-(void)sendGetUploadPicture:(int64_t)uuid BPictures:(NSData *)bPictures LCommenorateId:(int64_t)lCommenorateId;
-(void)sendGetDeletePicture:(int64_t)uuid LPhotoId:(int64_t)lPhotoId LCommemorateId:(int64_t)lCommemorateId;
-(void)sendgetUploadIcon:(int64_t)uuid VIconDatas:(NSData *)vIconDatas SPictureUrl:(NSString *)sPictureUrl;
-(void)sendGetSetUserInfo:(int64_t)uuid SNickName:(NSString *)sNickName IGender:(int32_t)iGender SBirthday:(NSString *)sBirthday SSignature:(NSString *)sSignature IAge:(int32_t)iAge IPrivataSwitch:(int32_t)iPrivateSwitch Address:(NSString *)address;
-(void)sendGetUserInfo:(int64_t)uuid;
-(void)sendSearchKtv:(int64_t)uuid SKeyWord:(NSString *)sKeyWord IOffset:(int32_t)iOffset INum:(int32_t)iNum;
-(void)sendSearchMusics:(int64_t)uuid SKeyWord:(NSString *)sKeyWord IOffset:(int32_t)iOffset INum:(int32_t)iNum;
-(void)sendSearchCommemorate:(int64_t)uuid SKeyWord:(NSString *)sKeyWord IOffset:(int32_t)iOffset INum:(int32_t)iNum;
-(void)sendCollectWork:(int64_t)uuid LMusicId:(int64_t)lMusicId LCommemorateId:(int64_t)lCommemorateId;
-(void)sendUnCollectWork:(int64_t)uuid LMusicId:(int64_t)lMusicId LCommemorateId:(int64_t)lCommemorateId;
-(void)sendGetCollectWorkList:(int64_t)uuid IOffset:(int32_t)iOffset INum:(int32_t)iNum;
-(void)sendGetUserCouponList:(int64_t)uuid IOffset:(int32_t)iOffset INum:(int32_t)iNum;
-(void)sendUploadPhoneNumber:(int64_t)uuid SPhoneNumber:(NSString *)sPhoneNumber;
-(void)sendUploadVerifyCode:(int64_t)uuid SVerifyCode:(NSString *)sVerfyCode;
-(void)sendDeleteCommemorateInfo:(int64_t)uuid LCommemorateId:(int64_t)lCommemorateId;
-(void)sendDeleteMyMusicWork:(int64_t)uuid LMusicid:(int64_t)lMusicid;
-(void)sendGetCommemorateInfoByUserId:(int64_t)uuid LWatchUserId:(int64_t)lWatchUserId IOffset:(int32_t)iOffset INum:(int32_t)iNum;
-(void)sendMusicAuditon:(int64_t)uuid LMusicWorkId:(int64_t)lMusicWorkId;
-(void)sendAddCommentMusic:(int64_t)uuid LMusicWorkId:(int64_t)lMusicWorkId SContent:(NSString *)sContent;
-(void)sendGetMusicCommentList:(int64_t)lMusicId IOffset:(int32_t)iOffset INum:(int32_t)iNum;
-(void)sendGetSendMusicFlower:(int64_t)uuid LMusicId:(int64_t)lMusicId IFlowerNum:(int32_t)iFlowerNum;
-(void)sendGetSendUserFlower:(int64_t)uuid LUserId:(int64_t)lUserId IFlowerNum:(int32_t)iFlowerNum;
-(void)sendGetSEndCommemorateFlower:(int64_t)uuid LCommid:(int64_t)lCommid IFlowerNum:(int32_t)iFlowerNum;
-(void)sendDeleteUserCouponList:(int64_t)uuid LCouponId:(int64_t)lCouponId;
-(void)sendSetBlackRoomState:(int64_t)uuid LToUserid:(int64_t)lToUserid BMute:(int32_t)bMute;
-(void)sendGetBlackRoomState:(int64_t)uuid LToUserId:(int64_t)lToUserId;
-(void)sendCouponToFriend:(int64_t)uuid LToUserId:(int64_t)lToUserId LCouponId:(NSArray *)lCouponId;
-(void)sendGetTypeAllNum:(int64_t)uuid SCouponType:(NSString *)sCouponType SCouponAdress:(NSString *)sCouponAdress LEndTime:(int64_t)lEndTime;
-(void)sendMsg:(int64_t)uuid LTalkWith:(int64_t)lTalkWith SMsg:(NSString *)sMsg LMsgId:(int64_t)lMsgId EMessageType:(MESSAGE_TYPE)eMessgeType;
@end
