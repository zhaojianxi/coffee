//
//  NetRecvManager.m
//  ListenToMe
//
//  Created by yadong on 1/27/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "NetRecvManager.h"
#import "ListenToMe.pb.h"
#import "NetReqManager.h"

@implementation NetRecvManager
@synthesize listenToMeData;
+(NetRecvManager *)getInstance
{
    static NetRecvManager *instance;
    @synchronized(self)
    {
        if (!instance) {
            instance = [[NetRecvManager alloc]init];
        }
    }
    return instance;
}

-(id)init
{
    if (self = [super init]) {
        listenToMeData = [ListenToMeData getInstance];
        listenToMeData.ktvListBaseInfo = [NSMutableArray array];
    }
    return self;
}

-(void)handleRsp:(NSData *)rspData
{
    //    NSData *data1 = [rspData subdataWithRange:NSMakeRange(5, rspData.length - 5)];
    NSData *data1 = [rspData subdataWithRange:NSMakeRange(5, rspData.length - 5)];
    
    Package *package = [Package parseFromData:data1];
    
    LISTENTOME_CMD cmd = package.eCmd;
    NSLog(@"response comd : %d" , cmd);
    
    switch (cmd) {
        case LISTENTOME_CMDCmdScloginRsp:{
            //注册回包
            YDLog(@"SCLoginRsp");
            SCLoginRsp* scLoginRsp = [SCLoginRsp parseFromData:package.body];
        
            if (scLoginRsp.iErrCode == 0) {
                YDLog(@"注册信息回包");
                listenToMeData.stUserBaseInfoNet = scLoginRsp.stUserBaseInfoNet;
                [ListenToMeDBManager setUuid:scLoginRsp.stUserBaseInfoNet.uuid];
                NSString *strResult = [NSString stringWithFormat:@"error : %d",scLoginRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_GET_LOGIN_PAGE_RESP obj:strResult, nil];
            }
            
            break;
        }
        case LISTENTOME_CMDCmdScquickLoginRsp:{
            
            //快速登录
            SCQuickLoginRsp *scQuickLoginRsp = [SCQuickLoginRsp parseFromData:package.body];
            YDLog(@"SCQuickLoginRsp");
            if (scQuickLoginRsp.iErrCode == 0) {
                YDLog(@"快速登录回包");
                listenToMeData.stUserBaseInfoNet = scQuickLoginRsp.stUserBaseInfoNet;
                NSString *strResult = [NSString stringWithFormat:@"error : %d",scQuickLoginRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_GET_QUICKLOGIN_PAGE_RESP obj:strResult, nil];

            }
            
            break;
            
        }
        case LISTENTOME_CMDCmdSclogoutRsp:{
            //退出登录
            SCLogoutRsp *scLogoutRsp = [SCLogoutRsp parseFromData:package.body];
            YDLog(@"SCLogoutRsp");
            if (scLogoutRsp.iErrCode == 0) {
                YDLog(@"退出登录回包");
                [ListenToMeDBManager setUuid:0];
                listenToMeData.stUserBaseInfoNet = nil;
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"退出登录" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
                [alert show];
                [self performSelector:@selector(dismisFeedbackAlert:) withObject:alert afterDelay:1.0];
                NSString *strResult = [NSString stringWithFormat:@"error : %d",scLogoutRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_GET_LOGOUT_PAGE_RESP obj:strResult, nil];
            }
            
           
            break;
        }
        case LISTENTOME_CMDCmdScgetKtvListInfoRsp:{
            //KTV列表
            SCGetKtvListInfoRsp *scGetKtvListRsp = [SCGetKtvListInfoRsp parseFromData:package.body];
            YDLog(@"SCGetKtvListInfoRsp:");
            //if (NO)
            if(scGetKtvListRsp.iErrCode == 0)
            {
                YDLog(@"ktv列表回包");
                listenToMeData.ktvListBaseInfo = [NSMutableArray arrayWithArray: scGetKtvListRsp.stKtvBaseInfo];
                listenToMeData.ktvListBannerInfo = [NSMutableArray arrayWithArray: scGetKtvListRsp.stKtvListBannerInfo ];
                NSString *strResult = [NSString stringWithFormat:@"%d",scGetKtvListRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_GET_KTVLIST_PAGE_RESP obj:strResult, nil];
            }
            
           
            
            break;
        }
        case LISTENTOME_CMDCmdScgetKtvInfoByIdRsp:{
            //KTV信息
            
            SCGetKtvInfoByIdRsp *scGetKtvInfoByIdRsp = [SCGetKtvInfoByIdRsp parseFromData:package.body];
            YDLog(@"SCGetKtvInfoByIdRsp");
            if(scGetKtvInfoByIdRsp.iErrCode == 0){
                YDLog(@"KTV信息回包");
                listenToMeData.ktvBaseInfo = scGetKtvInfoByIdRsp.stKtvBaseInfo;
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scGetKtvInfoByIdRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_GET_KTVINFO_PAGE_RESP obj:strResult, nil];
            }
            
           
            break;
        }
        case LISTENTOME_CMDCmdScgetSwitchKtvUserListRsp:{
            //KTV内换一批用户
            
            SCGetSwitchKtvUserListRsp *scGetSwitchKtvUserListRsp = [SCGetSwitchKtvUserListRsp parseFromData:package.body];
            YDLog(@"SCGetSwitchKtvUserListRsp");
            if (scGetSwitchKtvUserListRsp.iErrCode == 0) {
                YDLog(@"更换新一批用户回包");
                
                listenToMeData.arrUserBaseInfoNet = [NSMutableArray arrayWithArray:scGetSwitchKtvUserListRsp.stUserBaseInfoNet ];
             
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scGetSwitchKtvUserListRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_GET_SWITCHKTVUSERLIST_PAGE_RESP obj:strResult, nil];
            }
            
            break;
            
        }
        case LISTENTOME_CMDCmdScgetMusicWorkRsp:{
            //获取用户的作品
            SCGetMusicWorkRsp *scGetMusicWorkRsp = [SCGetMusicWorkRsp parseFromData:package.body];
            YDLog(@"SCGetMusicWorkRsp");
            if (scGetMusicWorkRsp.iErrCode == 0) {
                YDLog(@"我的音乐作品回包");
                listenToMeData.arrMusicWorkBaseInfo = [NSMutableArray arrayWithArray:scGetMusicWorkRsp.stMusicWorkBaseInfo];
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scGetMusicWorkRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_GET_MUSICWOKR_PAGE_RESP obj:strResult, nil];
            }
            
            
            break;
            
        }
        case LISTENTOME_CMDCmdScgetMusicWorkByIdrsp:{
            //根据作品ID获取作品详情
            SCGetMusicWorkByIDRsp *scGetMusicWorkByIDRsp = [SCGetMusicWorkByIDRsp parseFromData:package.body];
            YDLog(@"SCGetMusicWorkByIDRsp");
            if (scGetMusicWorkByIDRsp.iErrCode == 0) {
                YDLog(@"根据作品id获取作品详情回包");
                listenToMeData.musicWorkBaseInfo = scGetMusicWorkByIDRsp.stMusicWorkBaseInfo;
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scGetMusicWorkByIDRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_GET_MUSICWOKRBSAEINFO_PAGE_RESP obj:strResult, nil];
            }
            
           
            
            break;
            
        }
        case LISTENTOME_CMDCmdScgetCommemorateInfoRsp:{
            //k歌回忆的纪念册回包
            SCGetCommemorateInfoRsp *scGetCommemorateInfoRsp = [SCGetCommemorateInfoRsp parseFromData:package.body];
            YDLog(@"SCGetCommemorateInfoRsp");
            if(scGetCommemorateInfoRsp.iErrCode == 0){
                YDLog(@"k歌回忆纪念册回包");
                listenToMeData.arrCommemorateBaseInfo = [NSMutableArray arrayWithArray: scGetCommemorateInfoRsp.stCommemorateBaseInfo ];
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scGetCommemorateInfoRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_GET_COMMEMORATEINFO_PAGE_RESP obj:strResult, nil];
            }
            
            
            
            break;
        }
        case LISTENTOME_CMDCmdScuploadPictureRsp:{
            //上传相片
            SCUploadPictureRsp *scUploadPictureRsp = [SCUploadPictureRsp parseFromData:package.body];
            YDLog(@"SCUploadPictureRsp");
            if (scUploadPictureRsp.iErrCode == 0) {
                YDLog(@"上传照片回包");
                listenToMeData.stPictureBaseInfo = scUploadPictureRsp.stPictureBaseInfo;
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"上传照片成功" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
                [alert show];
                [self performSelector:@selector(dismisFeedbackAlert:) withObject:alert afterDelay:1.0];
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scUploadPictureRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_UPLOADPICTURE_PAGE_RESP obj:strResult, nil];

            }
            
            break;
        }
        case LISTENTOME_CMDCmdScdeletePictureRsp:{
            //删除相片
            SCDeletePictureRsp *scDeletePictureRsp = [SCDeletePictureRsp parseFromData:package.body];
            if (scDeletePictureRsp.iErrCode == 0) {
                YDLog(@"删除照片回包");
                listenToMeData.stPictureBaseInfo = scDeletePictureRsp.stPictureBaseInfo;
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"删除照片成功" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
                [alert show];
                [self performSelector:@selector(dismisFeedbackAlert:) withObject:alert afterDelay:1.0];
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scDeletePictureRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_DELETEPICTURE_PAGE_RESP obj:strResult, nil];

            }
            
            break;
        }
        case LISTENTOME_CMDCmdScgetPictureListRsp:{
            //获取相片列表, 纪念册id为空,看自己的相册 ,纪念册id不为空看纪念册里图片
            SCGetPictureListRsp *scGetPictureListRsp = [SCGetPictureListRsp parseFromData:package.body];
            YDLog(@"SCGetPictureListRsp");
            if (scGetPictureListRsp.iErrCode == 0) {
                YDLog(@"获取相片列表回包");
                listenToMeData.arrPictureBaseInfo = [NSMutableArray arrayWithArray:scGetPictureListRsp.stPictureBaseInfo ];
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scGetPictureListRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_GET_PICTURELIST_PAGE_RESP obj:strResult, nil];

            }
            
            break;
        }
        case LISTENTOME_CMDCmdScgetPopularityListRsp:{
            //人气歌王列表
            SCGetPopularityListRsp *scGetPopularityListRsp = [SCGetPopularityListRsp parseFromData:package.body];
            YDLog(@"SCGetPopularityListRsp");
            if(scGetPopularityListRsp.iErrCode == 0){
                YDLog(@"人气歌王列表回包");
                listenToMeData.arrPopularityList = [NSMutableArray arrayWithArray:scGetPopularityListRsp.stMusicWorkBaseInfo ];
                NSString *strResult = [NSString stringWithFormat:@"%d",scGetPopularityListRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_GET_POPULARITYLIST_PAGE_RESP obj:strResult, nil];
            }
            
            break;
        }
        case LISTENTOME_CMDCmdScgetNewMusicListRsp:{
            //新歌速递 对应深情星空
            SCGetNewMusicListRsp *scGetNewMusicListRsp = [SCGetNewMusicListRsp parseFromData:package.body];
            YDLog(@"SCGetNewMusicListRsp");
            if (scGetNewMusicListRsp.iErrCode == 0) {
                listenToMeData.arrNewMusicList = [NSMutableArray  arrayWithArray:scGetNewMusicListRsp.stMusicWorkBaseInfo ];
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scGetNewMusicListRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_GET_NEWMUSICLIST_PAGE_RESP obj:strResult, nil];
            }
           
            break;
        }
        case LISTENTOME_CMDCmdScgetRecommendListRsp:{
            //每日推荐列表,每日金曲,地UI有时空胶囊
            SCGetRecommendListRsp *scGetRecommendListRsp = [SCGetRecommendListRsp parseFromData:package.body];
            YDLog(@"SCGetRecommendListRsp");
            if(scGetRecommendListRsp.iErrCode == 0){
                listenToMeData.arrRecommendList = [NSMutableArray arrayWithArray:scGetRecommendListRsp.stMusicWorkBaseInfo];
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scGetRecommendListRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_GET_RECOMMENDLIST_PAGE_RESP obj:strResult, nil];
            }
            
            break;
        }
        case LISTENTOME_CMDCmdScuploadIconRsp:{
            //上传头像
            SCUploadIconRsp *scUploadIconRsp = [SCUploadIconRsp parseFromData:package.body];
            if (scUploadIconRsp.iErrCode == 0) {
                YDLog(@"上传头像回包");
                listenToMeData.sCoverUrl = [NSURL URLWithString:scUploadIconRsp.sIconUrl ];
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"上传头像成功" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
                [alert show];
                [self performSelector:@selector(dismisFeedbackAlert:) withObject:alert afterDelay:1.0];
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scUploadIconRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_UPLOADICON_PAGE_RESP obj:strResult, nil];
            }
           
            break;
        }
        case LISTENTOME_CMDCmdScgetUserInfoRsp:{
            //根据id获取用户基本信息
            SCGetUserInfoRsp *scGetUserInfoRsp = [SCGetUserInfoRsp parseFromData:package.body];
            if (scGetUserInfoRsp.iErrCode == 0) {
                YDLog(@"获取用户基本信息回包");
                listenToMeData.stUserBaseInfoNet = scGetUserInfoRsp.stUserBaseInfoNet;
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scGetUserInfoRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_GET_USERINFO_PAGE_RESP obj:strResult, nil];

            }
                       break;
        }
        case LISTENTOME_CMDCmdScsetUserInfoRsp:{
            //设置个人信息
            SCSetUserInfoRsp * scSetUserInfoRsp = [SCSetUserInfoRsp parseFromData:package.body];
            if (scSetUserInfoRsp.iErrCode == 0) {
                YDLog(@"设置个人信息回包");
                listenToMeData.stUserBaseInfoNet = scSetUserInfoRsp.stUserBaseInfoNet;
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"设置个人信息成功" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
                [alert show];
                [self performSelector:@selector(dismisFeedbackAlert:) withObject:alert afterDelay:1.0];
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scSetUserInfoRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_SET_USERINFO_PAGE_RESP obj:strResult, nil];
            }
           
            break;
        }
        case LISTENTOME_CMDCmdScsearchKtvRsp:{
            //搜索ktv
            SCSearchKtvRsp *scSeaarchKtvRsp = [SCSearchKtvRsp parseFromData:package.body];
            if(scSeaarchKtvRsp.iErrCode == 0){
                YDLog(@"搜索KTV回包");
                listenToMeData.arrSearchKtvBaseInfo = [NSMutableArray arrayWithArray:scSeaarchKtvRsp.stKtvBaseInfo ];
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scSeaarchKtvRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_GET_SEARCH_KTV_PAGE_RESP obj:strResult, nil];
            }
            
            break;
        }
        case LISTENTOME_CMDCmdScsearchMusicsRsp:{
            //搜索音乐
            SCSearchMusicsRsp *scSearchMusicsRsp = [SCSearchMusicsRsp parseFromData:package.body];
            if (scSearchMusicsRsp.iErrCode == 0) {
                YDLog(@"搜索音乐回包");
                listenToMeData.arrSearchSongInfo = [NSMutableArray arrayWithArray:scSearchMusicsRsp.stSongInfo];
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scSearchMusicsRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_GET_SEARCH_SONG_PAGE_RESP obj:strResult, nil];

            }
            break;
        }
        case LISTENTOME_CMDCmdScsearchCommemorateRsp:{
            //搜索纪念册
            SCSearchCommemorateRsp *scSearchCommemorateRsp = [SCSearchCommemorateRsp parseFromData:package.body];
            if (scSearchCommemorateRsp.iErrCode == 0) {
                YDLog(@"搜索纪念册");
                listenToMeData.arrSearchCommemorateBaseInfo = [NSMutableArray arrayWithArray:scSearchCommemorateRsp.stCommemorateBaseInfo];
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scSearchCommemorateRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_GET_SEARCH_COMMEMORATE_PAGE_RESP obj:strResult, nil];
            }
            
            break;
        }
        case LISTENTOME_CMDCmdSccollectWorkRsp:{
            //收藏
            SCCollectWorkRsp *scCollectWorkRsp = [SCCollectWorkRsp parseFromData:package.body];
            YDLog(@"SCCollectWorkRsp");
            if (scCollectWorkRsp.iErrCode == 0) {
                YDLog(@"收藏歌曲作品(或者纪念册)回包");
                listenToMeData.collectMusicWorkBaseInfo = scCollectWorkRsp.stMusicWorkBaseInfo;
                listenToMeData.collectCommemorateBaseInfo = scCollectWorkRsp.stCommemorateBaseInfo;
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scCollectWorkRsp.iErrCode];
                if (scCollectWorkRsp.stMusicWorkBaseInfo.lMusicWorkId ) {
                    [CCNotify sentNotify:NOTIFY_GET_COLLECT_MUSICWORK_PAGE_RESP obj:strResult, nil];
                }
                if (scCollectWorkRsp.stCommemorateBaseInfo.lCommid) {
                    [CCNotify sentNotify:NOTIFY_GET_COLLECT_COMMEMORATE_PAGE_RESP obj:strResult, nil];
                }
            }
            
            
            break;
        }
        case LISTENTOME_CMDCmdScunCollectWorkRsp:{
            //取消收藏
            SCUnCollectWorkRsp *scUnCollectWorkRsp = [SCUnCollectWorkRsp parseFromData:package.body];
            if (scUnCollectWorkRsp.iErrCode == 0) {
                YDLog(@"取消收藏歌曲作品(或者纪念册)");
                listenToMeData.unCollectMusicWorkBaseInfo = scUnCollectWorkRsp.stMusicWorkBaseInfo;
                listenToMeData.unCollectCommemorateBaseInfo = scUnCollectWorkRsp.stCommemorateBaseInfo;
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scUnCollectWorkRsp.iErrCode];
                if (scUnCollectWorkRsp.stMusicWorkBaseInfo.lMusicWorkId) {
                    [CCNotify sentNotify:NOTIFY_GET_UN_COLLECT_MUSICWORK_PAGE_RESP obj:strResult, nil];
                }
                
                if (scUnCollectWorkRsp.stCommemorateBaseInfo.lCommid) {
                    [CCNotify sentNotify:NOTIFY_GET_UN_COLLECT_COMMEMORATE_PAGE_RESP obj:strResult, nil];
                }
            }
            
            
            break;
        }
            
        case LISTENTOME_CMDCmdScgetCollectWorkListRsp:{
            //获取收藏列表
            SCGetCollectWorkListRsp *scGetCollectWorkListRsp = [SCGetCollectWorkListRsp parseFromData:package.body];
            if (scGetCollectWorkListRsp.iErrCode == 0) {
                YDLog(@"我的收藏列表回包");
                listenToMeData.arrCollectWorkInfo = [NSMutableArray arrayWithArray:scGetCollectWorkListRsp.stCollectWorkInfo];
                
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scGetCollectWorkListRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_GET_COLLECT_WORKLIST_PAGE_RESP obj:strResult, nil];
            }
            break;
        }
            
        case LISTENTOME_CMDCmdScgetUserCouponListRsp:{
            //获取用户的礼券列表信息
            SCGetUserCouponListRsp *scGetUserCouponListRsp = [SCGetUserCouponListRsp parseFromData:package.body];
            if (scGetUserCouponListRsp.iErrCode == 0) {
                YDLog(@"我的礼券列表回包");
                listenToMeData.arrCouponBaseInfo = [NSMutableArray arrayWithArray:scGetUserCouponListRsp.stCouponBaseInfo];
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scGetUserCouponListRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_GET_USER_COUPONLIST_PAGE_RESP obj:strResult, nil];
            }
            
            break;
        }
        case LISTENTOME_CMDCmdScuploadPhoneNumberRsp:{
            //上传绑定的手机号
            SCUploadPhoneNumberRsp *scUploadPhoneNumberRsp = [SCUploadPhoneNumberRsp parseFromData:package.body];
            if (scUploadPhoneNumberRsp.iErrCode == 0) {
                YDLog(@"上传绑定手机号回包:上传手机号成功");
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scUploadPhoneNumberRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_UPLOAD_PHONENUMBER_PAGE_RESP obj:strResult, nil];
            }
            break;
        }
        case LISTENTOME_CMDCmdScuploadVerifyCodeRsp:{
            //上传验证码
            SCUploadVerifyCodeRsp *scUploadVerifyCodeRsp = [SCUploadVerifyCodeRsp parseFromData:package.body];
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"激活成功" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
            if (scUploadVerifyCodeRsp.iErrCode == 0) {
                YDLog(@"上传验证码回包: 验证成功");
               
                //设置绑定成功
                [[NSUserDefaults standardUserDefaults]setBool:YES forKey:bindingPhoneNumSuccess];
                
                [alert show];
                
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scUploadVerifyCodeRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_UPLOAD_VERIFY_CODE_SUCCESS_PAGE_RESP obj:strResult, nil];
                
            }else{
//                YDLog(@"上传验证码回包: 验证失败");
//                alert.message = @"验证码出错,请重新输入";
//                [alert show];
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scUploadVerifyCodeRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_UPLOAD_VERIFY_CODE_FAILURE_PAGE_RESP obj:strResult, nil];
            }
            
            [self performSelector:@selector(dismisFeedbackAlert:) withObject:alert afterDelay:1.0];
            
            break;
        }
            
        case LISTENTOME_CMDCmdScdeleteCommemorateInfoRsp:{
            //删除纪念册
            SCDeleteCommemorateInfoRsp *scDeleteCommemorateInfoRsp = [SCDeleteCommemorateInfoRsp parseFromData:package.body];
            if (scDeleteCommemorateInfoRsp.iErrCode == 0) {
                YDLog(@"删除纪念册成功");
                listenToMeData.stCommemorateBaseInfo = scDeleteCommemorateInfoRsp.stCommemorateBaseInfo;
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scDeleteCommemorateInfoRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_GET_DELETE_COMMEMORATEINFO_PAGE_RESP obj:strResult, nil];
            }
            
            
            break;
        }
        case LISTENTOME_CMDCmdScdeleteMyMusicWorkRsp:{
            //删除音乐作品
            SCDeleteMyMusicWorkRsp *scDeleteMyMusicWorkRsp = [SCDeleteMyMusicWorkRsp parseFromData:package.body];
            if (scDeleteMyMusicWorkRsp.iErrCode == 0) {
                
                if (scDeleteMyMusicWorkRsp.iErrCode == 0) {
                    YDLog(@"删除我的音乐作品");
                    listenToMeData.stMusicWorkBaseInfo = scDeleteMyMusicWorkRsp.stMusicWorkBaseInfo;
                    NSString *strResult = [NSString stringWithFormat:@"error: %d",scDeleteMyMusicWorkRsp.iErrCode];
                    [CCNotify sentNotify:NOTIFY_GET_DELETE_MYMUSICWORK_PAGE_RESP obj:strResult, nil];
                }
            }
            break;
        }
            
        case LISTENTOME_CMDCmdScgetCommemorateInfoByUserIdRsp:{
            //根据id 获取纪念册信息,用在他人主页, 和 个人中心的纪念册获取
            SCGetCommemorateInfoByUserIdRsp *scGetCommemorateInfoByUserIdRsp = [SCGetCommemorateInfoByUserIdRsp parseFromData:package.body];
            if (scGetCommemorateInfoByUserIdRsp.iErrCode == 0) {
                YDLog(@"纪念册信息回包");
                listenToMeData.arrCommemorateInfoByUserId = [NSMutableArray arrayWithArray:scGetCommemorateInfoByUserIdRsp.stCommemorateBaseInfo];
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scGetCommemorateInfoByUserIdRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_GET_COMMEMORATEINFO_BYUSERID_PAGE_RESP obj:strResult, nil];
            }
            
            break;
        }
        case LISTENTOME_CMDCmdScmusicAuditionRsp:{
            //试听音乐作品
            SCMusicAuditionRsp *scMusicAuditionRsp = [SCMusicAuditionRsp parseFromData:package.body];
            if (scMusicAuditionRsp.iErrCode == 0) {
                YDLog(@"试听音乐作品");
                listenToMeData.currentMusicAudition = scMusicAuditionRsp.stMusicWorkBaseInfo;
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scMusicAuditionRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_GET_MUSIC_AUDITION_PAGE_RESP obj:strResult, nil];
            }
            
            break;
        }
        case LISTENTOME_CMDCmdScaddCommentMusicRsp:{
            //评论音乐作品
            SCAddCommentMusicRsp *scAddCommentMusicRsp = [SCAddCommentMusicRsp parseFromData:package.body];
            if (scAddCommentMusicRsp.iErrCode == 0 ) {
                YDLog(@"评论音乐作品");
                listenToMeData.addMusicCommntInfo = scAddCommentMusicRsp.stCommentInfo;
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scAddCommentMusicRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_GET_ADD_COMMENT_MUSIC_PAGE_RESP obj:strResult, nil];
            }
            
            break;
        }
        case LISTENTOME_CMDCmdScgetMusicCommentListRsp:{
            //获取音乐作品评论信息
            SCGetMusicCommentListRsp *scGetMusicCommentListRsp = [SCGetMusicCommentListRsp parseFromData:package.body];
            if (scGetMusicCommentListRsp.iErrCode == 0) {
                YDLog(@"获取音乐作品评论信息");
                listenToMeData.arrCommentListInfo = [NSMutableArray arrayWithArray:scGetMusicCommentListRsp.stCommentInfo];
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scGetMusicCommentListRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_GET_MUSIC_COMMENTLIST_PAGE_RESP obj:strResult, nil];
            }
            break;
        }
            
        case LISTENTOME_CMDCmdScsendMusicFlowerRsp:{
            //给音乐作品赠送鲜花
            SCSendMusicFlowerRsp *scSendMusicFlowerRsp = [SCSendMusicFlowerRsp parseFromData:package.body];
            if (scSendMusicFlowerRsp.iErrCode == 0) {
                YDLog(@"给歌送花回包");
                listenToMeData.stUserBaseInfoNet = scSendMusicFlowerRsp.stUserBaseInfoNet;
                listenToMeData.sendFlowerToMusicWork = scSendMusicFlowerRsp.stMusicWorkBaseInfo;
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scSendMusicFlowerRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_GET_SEND_MUSICFLOWER_PAGE_RESP obj:strResult, nil];
            }
            break;
        }
            
        case LISTENTOME_CMDCmdScsendUserFlowerRsp:{
            //给人送花
            SCSendUserFlowerRsp *scSendUserFlowerRsp = [SCSendUserFlowerRsp parseFromData:package.body];
            if (scSendUserFlowerRsp.iErrCode == 0) {
                YDLog(@"给人送花回包");
                listenToMeData.stUserBaseInfoNet = scSendUserFlowerRsp.stUserBaseInfoNet;
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scSendUserFlowerRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_GET_SEND_USERFLOWER_PAGE_RESP obj:strResult, nil];
            }
            break;
        }
        case LISTENTOME_CMDCmdScsendCommemorateFlowerRsp:{
            //给纪念册送花
            SCSendCommemorateFlowerRsp *scSendCommemorateFlowerRsp = [SCSendCommemorateFlowerRsp parseFromData:package.body];
            if (scSendCommemorateFlowerRsp.iErrCode == 0) {
                YDLog(@"给纪念册送花回包");
                listenToMeData.stUserBaseInfoNet = scSendCommemorateFlowerRsp.stUserBaseInfoNet;
                listenToMeData.sendFlowerToCommemorate = scSendCommemorateFlowerRsp.stCommemorateBaseInfo;
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scSendCommemorateFlowerRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_GET_SEND_COMMEMORATEFLOWER_PAGE_RESP obj:strResult, nil];
            }
            break;
        }
        case LISTENTOME_CMDCmdScdeleteUserCouponListRsp:{
            //删除用户礼券
            SCDeleteUserCouponListRsp *scDeleteUserCouponListRsp = [SCDeleteUserCouponListRsp parseFromData:package.body];
            if (scDeleteUserCouponListRsp.iErrCode == 0) {
                YDLog(@"删除礼券回包");
                listenToMeData.deleteCouponBaseInfo = scDeleteUserCouponListRsp.stCouponBaseInfo;
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scDeleteUserCouponListRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_GET_DELETE_COUPONLIST_PAGE_RESP obj:strResult, nil];
            }
            
            break;
        }
        case LISTENTOME_CMDCmdScsetBlackRoomStateRsp:{
            //设置用户小黑屋状态
            SCSetBlackRoomStateRsp *scSetBlackRoomStateRsp = [SCSetBlackRoomStateRsp parseFromData:package.body];
            if (scSetBlackRoomStateRsp.iErrCode == 0) {
                YDLog(@"设置用户小黑屋状态回包");
                listenToMeData.bMute = scSetBlackRoomStateRsp.bMute;
                NSString *strReslut = [NSString stringWithFormat:@"error: %d",scSetBlackRoomStateRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_SET_BLACKROOM_STATE_PAGE_RESP obj:strReslut, nil];
            }
            break;
        }
            
        case LISTENTOME_CMDCmdScgetBlackRoomStateRsp:{
            //查看用户小黑屋的状态
            SCGetBlackRoomStateRsp *scGetBlackRoomStateRsp = [SCGetBlackRoomStateRsp parseFromData:package.body];
            if (scGetBlackRoomStateRsp.iErrCode == 0) {
                YDLog(@"查看小黑状态回包");
                listenToMeData.bMute = scGetBlackRoomStateRsp.bMute;
                NSString *strResult =[NSString stringWithFormat:@"error: %d",scGetBlackRoomStateRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_GET_BLACKROOM_STATE_PAGE_RESP obj:strResult, nil];
                
            }
            
            break;
        }
            
        case LISTENTOME_CMDCmdScsendCouponToFriendRsp:{
            //给好友赠送礼券 (暂时为商定逻辑)
            SCSendCouponToFriendRsp * scSendCouponToFriendRsp =[SCSendCouponToFriendRsp parseFromData:package.body];
            if (scSendCouponToFriendRsp.iErrorCode  == 0) {
                YDLog(@"给好友赠送礼券回包");
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scSendCouponToFriendRsp.iErrorCode];
                [CCNotify sentNotify:NOTIFY_GET_SEND_COUPONTOFRIEDD_PAGE_RESP obj:strResult, nil];
            }
            break;
        }
            
        case LISTENTOME_CMDCmdScgetTypeAllNumRsp:{
            //获取相同礼券的数量
            SCGetTypeAllNumRsp *scGetTypeAllNumRsp =[SCGetTypeAllNumRsp parseFromData:package.body];
            if (scGetTypeAllNumRsp.iErrCode == 0) {
                YDLog(@"获取相同类型礼券的数量回包");
                listenToMeData.sameCouponBaseInfoArr = [NSMutableArray arrayWithArray:scGetTypeAllNumRsp.stCouponWorkInfo];
                NSString *strResult = [NSString stringWithFormat:@"error: %d",scGetTypeAllNumRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_GET_TYPEALLNUM_PAGE_RESP obj:strResult, nil];
            }
            break;
        }
    
        case LISTENTOME_CMDCmdScpushMsg:{
            //后台下发消息
            YDLog(@"后台下发消息回包");
            SCPushMsg *scPushMsg = [SCPushMsg parseFromData:package.body];
            if (scPushMsg.vMsgs.count > 0) {
                listenToMeData.arrUserMsgRecord = [NSMutableArray arrayWithArray:scPushMsg.vMsgs];
                [CCNotify sentNotify:NOTIFY_GET_PUSHMSG_PAGE_RESP obj:nil];
            }
            break;
        }
        case LISTENTOME_CMDCmdScsendMsgRsp:{
            //发送消息
            YDLog(@"发送消息回包");
            SCSendMsgRsp *scSendMsgRsp = [SCSendMsgRsp parseFromData:package.body];
            if (scSendMsgRsp.iErrCode == 0) {
                listenToMeData.lTalkWith = scSendMsgRsp.lTalkwith;
                listenToMeData.lMsgId = scSendMsgRsp.lMsgId;
                listenToMeData.sMsg = scSendMsgRsp.sMsg;
                NSString *strResult = [NSString stringWithFormat:@"error :%d",scSendMsgRsp.iErrCode];
                [CCNotify sentNotify:NOTIFY_GET_SEND_Message_PAGE_RESP obj:strResult, nil];
            }
            
            break;
        }
            
        case LISTENTOME_CMDCmdScfeedBackRsp:{
            SCFeedBackRsp* scFeedBackRsp = [SCFeedBackRsp parseFromData:package.body];
            NSLog(@"BABYTALK_CMDCmdScregisterRsp:%d err code : %d",LISTENTOME_CMDCmdScfeedBackRsp, scFeedBackRsp.iErrCode);
            
            break;
        }
        case LISTENTOME_CMDCmdScbindKtvroomRsp:{
            SCBindKTVRoomRsp* scBindKTVRoomRsp = [SCBindKTVRoomRsp parseFromData:package.body];
            NSLog(@"LISTENTOME_CMDCmdScbindKtvroomRsp:%d err code : %d",LISTENTOME_CMDCmdScbindKtvroomRsp, scBindKTVRoomRsp.iErrCode);
            if (scBindKTVRoomRsp.iErrCode == 0) {
                [ListenToMeData getInstance].stKTVRoomInfo = scBindKTVRoomRsp.stKtvroomInfo;
                [CCNotify sentNotify:NOTIFY_BIND_KTVROOM_SUCCESS obj:scBindKTVRoomRsp,nil];
            }
            break;
        }

        case LISTENTOME_CMDCmdScgetDateWorkListRsp:{
            SCGetDateWorkListRsp* stSCGetDateWorkListRsp = [SCGetDateWorkListRsp parseFromData:package.body];
            NSLog(@"LISTENTOME_CMDCmdScgetDateWorkListRsp:%d err code : %d",LISTENTOME_CMDCmdScgetDateWorkListRsp, stSCGetDateWorkListRsp.iErrCode);
            break;
        }
        case LISTENTOME_CMDCmdScgetMySongListInfoRsp:{
            SCGetMySongListInfoRsp* scGetMySongListInfoRsp = [SCGetMySongListInfoRsp parseFromData:package.body];
            NSLog(@"LISTENTOME_CMDCmdScgetMySongListInfoRsp:%d err code : %d",LISTENTOME_CMDCmdScgetMySongListInfoRsp, scGetMySongListInfoRsp.iErrCode);
            break;
        }

        case LISTENTOME_CMDCmdScupdateRsp:{
            SCUpdateRsp* stSCUpdateRsp = [SCUpdateRsp parseFromData:package.body];
            NSLog(@"LISTENTOME_CMDCmdScupdateRsp:%d err code : %d",LISTENTOME_CMDCmdScupdateRsp, stSCUpdateRsp.iNeedUpdate);
            break;
        }

        case LISTENTOME_CMDCmdScaddDateWorkInfoRsp:{
            SCAddDateWorkInfoRsp* stSCAddDateWorkInfoRsp = [SCAddDateWorkInfoRsp parseFromData:package.body];
            NSLog(@"LISTENTOME_CMDCmdScaddDateWorkInfoRsp:%d err code : %d",LISTENTOME_CMDCmdScaddDateWorkInfoRsp, stSCAddDateWorkInfoRsp.iErrCode);
            break;
        }
            
        case LISTENTOME_CMDCmdScuploadMusicWorkRsp:{
            SCUploadMusicWorkRsp* stSCUploadMusicWorkRsp = [SCUploadMusicWorkRsp parseFromData:package.body];
            
            NSLog(@"LISTENTOME_CMDCmdScupdateRsp:%d err code : %d,%lld",LISTENTOME_CMDCmdScuploadMusicWorkRsp, stSCUploadMusicWorkRsp.iErrCode,stSCUploadMusicWorkRsp.lCurrentPackageSeq);
            
            if (stSCUploadMusicWorkRsp.lCurrentPackageSeq!=0) {

                [[NetReqManager getInstance] sendUploadMusicWorkCurSeq:stSCUploadMusicWorkRsp.lCurrentPackageSeq sendDataDic:[ListenToMeData getInstance].sendingDataDic desc:@"222" songid:127 musicworkid:stSCUploadMusicWorkRsp.stMusicBaseInfo.stMusicWorkBaseInfo.lMusicWorkId];
            }
            break;
        }

    }
}

-(void)handleTimeOut:(long)tag
{
    
}

#pragma mark - alertViewFunction
-(void)dismisFeedbackAlert:(UIAlertView *)alertView
{
    [alertView dismissWithClickedButtonIndex:[alertView cancelButtonIndex] animated:YES];
}
@end
