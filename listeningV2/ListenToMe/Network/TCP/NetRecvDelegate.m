//
//  NetRecvDelegate.m
//  ListenToMe
//
//  Created by yadong on 1/27/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "NetRecvDelegate.h"
#import "NetRecvManager.h"
#import "NetReqManager.h"

@interface NetRecvDelegate ()
@property(strong,nonatomic) NSMutableData *m_fixbuffer;
@property(assign,nonatomic) int index;
@end

@implementation NetRecvDelegate
@synthesize m_fixbuffer;
@synthesize index;

-(NetRecvDelegate *)init
    {
    if (self = [super init]) {
       
        return self;
    }
    return nil;
    }

#pragma mark - 断网
-(void)onSocket:(AsyncSocket *)sock willDisconnectWithError:(NSError *)err
    {
        ListenToMeData * stListenToMeData = [ListenToMeData getInstance];
    YDLog(@"willDisconnectWithError 网络即将断开");
        if (stListenToMeData.bConnected) {
            stListenToMeData.bConnected = NO;
            [CCNotify sentNotify:NOTIFY_NET_DISCONNECT obj:nil, nil];
        }
    }

- (void)onSocketDidDisconnect:(AsyncSocket *)sock
    {
        YDLog(@"onSocketDidDisconnect: 网络已经断开");
        [self performSelector:@selector(reconnect) withObject:nil afterDelay:2.0];
        ListenToMeData *inst = [ListenToMeData getInstance];
        
        inst.bConnected = NO;
        if (inst.bConnected == YES) {
            inst.bConnected = NO;
            
            //发送通知, 通知发生断网事件
            [self performSelector:@selector(reconnect) withObject:nil afterDelay:2.0];
            [CCNotify sentNotify:NOTIFY_NET_DISCONNECT obj: nil];
        }
    }


#pragma mark - 联网
-(void)onSocket:(AsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port
{
    [ListenToMeData getInstance].bConnecting = NO;
    ListenToMeData * stListenToMeData = [ListenToMeData getInstance];
    [sock readDataWithTimeout:25 tag:0];
    YDLog(@"didConnectToHost: 已连接服务器");
    stListenToMeData.bConnected = YES;
    
    [sock readDataWithTimeout:25 tag:0 ] ;
    
    [[NetReqManager getInstance] sendQuickLoginReq:[ListenToMeDBManager getUuid]];
}

#pragma mark - 读数据
-(void)receivePacketData:(NSData *)data
{
    NSMutableData *tempData = [[NSMutableData alloc]init];
    [tempData appendData:m_fixbuffer];
    [tempData appendData:data];
    
    NSUInteger dataLength = [tempData length];
    unsigned char *temp = (unsigned char *)[tempData bytes];
    while (dataLength > 0) {
        if (dataLength <= 5) {
            [m_fixbuffer setLength:0];
            [m_fixbuffer appendBytes:(void *)(temp) length:dataLength];
            return;
        }
        
        unsigned short mark = (unsigned short)*(temp);
        temp = temp + 1;
        
        if (mark != 0xfe) {
            [m_fixbuffer setLength:0];
            YDLog(@"包头不正确");
            return;
        }
        
        unsigned int length = (unsigned int)*(temp);
        length = (length << 8|*(temp + 1));
        length = (length << 8|*(temp + 2));
        length = (length << 8|*(temp + 3));
        temp += 4;
        
        if (m_fixbuffer == nil) {
            m_fixbuffer = [[NSMutableData alloc]init];
        }
        
        if (length <= 0) {
            YDLog(@"receiveData too small");
            [m_fixbuffer setLength:0];
            return;
        }
        
        if (dataLength < length) {
            [m_fixbuffer setLength:0];
            [m_fixbuffer appendBytes:(void *)(temp - 5) length:dataLength];
            return;
        } else if(dataLength > length){
            [m_fixbuffer setLength:0];
            NSData *packageData = [[NSData alloc]initWithBytes:(void *)(temp - 5) length:length];
            
            temp += (length - 5);
            dataLength -= length;
            if (mark == 0xfe) {
                [[NetRecvManager getInstance] handleRsp:packageData];
            }
        } else if(dataLength == length){
            [m_fixbuffer setLength:0];
            NSData *packageData = [[NSData alloc]initWithBytes:(void *)(temp - 5) length:length];
            
            dataLength -= length;
            index ++;
            
            if (mark == 0xfe) {
                [[NetRecvManager getInstance] handleRsp:packageData];
            }
        }
    }
}


-(void)onSocket:(AsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag
{
    [sock readDataWithTimeout:100 tag:0];
    [self receivePacketData:data];
}

-(void)reconnect
{
    YDLog(@"reconnect--重连");
    NSError *erMsg;
    ListenToMeData * stListenToMeData = [ListenToMeData getInstance];
    
    if (stListenToMeData != nil) {
        if (stListenToMeData.bConnecting == NO) {
            stListenToMeData.bConnecting = YES;
            if (stListenToMeData.bAPMode) {
                if (![stListenToMeData.socketIns connectToHost:SERVER_IP onPort:ON_PORT error:&erMsg]) {
                    YDLog(@"connect failed,erMsg = %@",erMsg);
                    [ListenToMeData getInstance].bConnecting = NO;
                } else {

                }
            }
        }
    }
}

- (void)onSocket:(AsyncSocket *)sock didReadPartialDataOfLength:(NSUInteger)partialLength tag:(long)tag {
    YDLog(@"---onSocket:(AsyncSocket *)sock didReadPartialDataOfLength:(NSUInteger)partialLength tag:(long)");
    [sock readDataWithTimeout:25 tag:0 ] ;
}

- (void)onSocket:(AsyncSocket *)sock didWritePartialDataOfLength:(NSUInteger)partialLength tag:(long)tag {
    YDLog(@"onSocket:(AsyncSocket *)sock didWritePartialDataOfLength: partialLength=%lu tag:(long)tag called!",(unsigned long)partialLength);
}

- (NSTimeInterval)onSocket:(AsyncSocket *)sock
  shouldTimeoutReadWithTag:(long)tag
                   elapsed:(NSTimeInterval)elapsed
                 bytesDone:(NSUInteger)length {
    NSTimeInterval val = 1000;
    return val ;
}

- (NSTimeInterval)onSocket:(AsyncSocket *)sock
 shouldTimeoutWriteWithTag:(long)tag
                   elapsed:(NSTimeInterval)elapsed
                 bytesDone:(NSUInteger)length {
    NSTimeInterval val = 1000;
    YDLog(@"onSocket shouldTimeoutWriteWithTag called! %ld" ,tag);
    //超时处理
    [[NetRecvManager getInstance] handleTimeOut:tag];
    return val ;
}

- (void)onSocketDidSecure:(AsyncSocket *)sock {
    YDLog(@"onSocketDidSecure:(AsyncSocket *)sock called!");
}

#pragma mark -其它
- (BOOL)onSocketWillConnect:(AsyncSocket *)sock {
    YDLog(@"onSocketWillConnect:(AsyncSocket *)sock called!");
    return YES ;
}

- (void)onSocket:(AsyncSocket *)sock didWriteDataWithTag:(long)tag {
    //    NSLog(@"onSocket:(AsyncSocket *)sock didWriteDataWithTag:(long)tag called!") ;
    YDLog(@"onSocket:(AsyncSocket *)sock didWriteDataWithTag:(long)tag called!");
    
}

- (void)onSocket:(AsyncSocket *)sock didAcceptNewSocket:(AsyncSocket *)newSocket {
    YDLog(@"onSocket:(AsyncSocket *)sock didAcceptNewSocket:(AsyncSocket *)newSocket called!");
}

- (NSRunLoop *)onSocket:(AsyncSocket *)sock wantsRunLoopForNewSocket:(AsyncSocket *)newSocket {
    YDLog(@"onSocket:(AsyncSocket *)sock wantsRunLoopForNewSocket:(AsyncSocket *)newSocket called!");
    return nil ;
}

@end

