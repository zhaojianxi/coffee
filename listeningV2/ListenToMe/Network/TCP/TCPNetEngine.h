//
//  TCPNetEngine.h
//  ListenToMe
//
//  Created by yadong on 1/27/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TCPNetEngine : NSObject{
    int m_seqNo;
    
    NSTimeInterval timeoffset;
    long long serverCurrTime; //从服务器难道的时间
    long long serverSwitch; //控制客户端功能
    NSTimer *helloTimer;
    long long timeInterval;
    int isLoginSucceed; //登录是否成功 1 成功 0 不成功
    
}

@property(nonatomic) int m_seqNo;

+ (TCPNetEngine *)getInstance;
-(NSData*)getReqFeedbackData:(CSFeedBack*)csFeedBack;
-(NSData*)getReqUploadPhoneNumberData:(CSUploadPhoneNumber*)csUploadPhoneNumber;
-(NSData*)getReqUploadVerifyCodeData:(CSUploadVerifyCode*)csUploadVerifyCode;
-(NSData*)getReqGetCollectWorkListData:(CSGetCollectWorkList*)csGetCollectWorkList;
-(NSData*)getReqGetUserCouponListData:(CSGetUserCouponList*)csGetUserCouponList;
-(NSData*)getReqUpdate:(CSUpdate*)csUpdate;
-(NSData*)getReqUploadMusicWork:(CSUploadMusicWork*)csUploadMusicWork;
-(NSData*)getReqAddDateWorkInfo:(CSAddDateWorkInfo*)csAddDateWorkInfo;
-(NSData*)getReqBindKTVRoom:(CSBindKTVRoom*)csBindKTVRoom ;

@end


@interface TCPNetEngine (TCPRequest)
/**
 *  快速登录
 */
-(NSData *)getReqQuickLoginData:(CSQuickLogin *)csQuickLogin;
/**
 *  登录
 */
-(NSData *)getReqLoginData:(CSLogin *)cslogin;
/**
 *  退出登录
 */
-(NSData *)getReqLogoutData:(CSLogout *)csLogout;
/**
 *  获取KTV列表
 */
-(NSData *)getKtvListInfo:(CSGetKtvListInfo *)csGetKtvListInfo;
/**
 *  根据KTV的ID获取KTV店铺的信息
 */
-(NSData *)getKtvInfoById:(CSGetKtvInfoById *)csGetKtvInfoById;
/**
 *  换一批用户
 */
-(NSData *)getSwitchKtvUserList:(CSGetSwitchKtvUserList *)csGetSwitchKtvList;
/**
 *  获取用户音乐作品,如果不需要登录uuid传值0
 */
-(NSData *)getMusicWork:(CSGetMusicWork *)csGetMusicWork;
/**
 *  根据作品id获取作品详情
 */
-(NSData *)getMusicWorkByID:(CSGetMusicWorkByID *)csGetMusicWorkByID;
/**
 *  获取K歌回忆里的纪念册列表,uuid传0
 */
-(NSData *)getCommenorateInfo:(CSGetCommemorateInfo *)csGetCommemorateInfo;
/**
 *  获取人气歌王榜单 (按查看数排序,对应歌王爆炸秀)
 */
-(NSData *)getPopularityList:(CSGetPopularityList *)csGetPopularityList;
/**
 *  获取新歌速递榜单 (按发布时间排序,对应深情行星)
 */
-(NSData *)getNewMusicList:(CSGetNewMusicList *)csGetNewMusicList;
/**
 *  获取每日金曲榜单 (按分数排序,对应时空胶囊)
 */
-(NSData *)getRecommendList:(CSGetRecommendList *)csGetRecommendList;
/**
 *  获取用户相册列表
 */
-(NSData *)getPictureList:(CSGetPictureList *)csGetPictureList;
/**
 *  上传相片
 */
-(NSData *)getUpLoadPicture:(CSUploadPicture *)csGetUpLoadPicture;
/**
 *  删除相片
 */
-(NSData *)getDeletePicture:(CSDeletePicture *)csGetDeletePicture;
/**
 *  上传头像
 */
-(NSData *)getUploadIcon:(CSUploadIcon *)csUploadIcon;
/**
 *  设置个人信息
 */
-(NSData *)getSetUserInfo:(CSSetUserInfo *)csSetUserInfo;
/**
 *  根据uuid获取用户基本信息
 */
-(NSData *)getUserInfo:(CSGetUserInfo *)csGetUserInfo;
/**
 *  搜索KTV
 */
-(NSData *)getSearchKtv:(CSSearchKtv *)csSearchKtv;
/**
 *  搜索歌曲作品
 */
-(NSData *)getSearchMusics:(CSSearchMusics *)csSearchMusic;
/**
 *  搜索纪念册
 */
-(NSData *)getSearchCommemorate:(CSSearchCommemorate *)csSearchCommemorate;
/**
 *  收藏
 */
-(NSData *)getCollectWork:(CSCollectWork *)csCollectWork;
/**
 *  取消收藏
 */
-(NSData *)getUnCollectWork:(CSUnCollectWork *)csUnCollectWork;
/**
 *  获取收藏列表
 */
-(NSData *)getCollectWorkList:(CSGetCollectWorkList *)csGetCollectWorkList;
/**
 *  获取礼券列表信息
 */
-(NSData *)getUserCouponList:(CSGetUserCouponList *)csGetUserCouponList;
/**
 *  上传手机号
 */
-(NSData *)getUploadPhoneNumber:(CSUploadPhoneNumber *)csUploadPhoneNumber;
/**
 *  上传验证码
 */
-(NSData *)getUploadVerifyCode:(CSUploadVerifyCode *)csUploadVerfyCode;
/**
 *  删除纪念册
 */
-(NSData *)getDeleteCommemorate:(CSDeleteCommemorateInfo *)csDeleteCommemorateInfo;
/**
 *  删除我的音乐作品
 */
-(NSData *)getDeleteMyMusicWork:(CSDeleteMyMusicWork *)csDeleteMyMusicWork;
/**
 *  根据id获取纪念册信息,用在他人主页 个人中心 获取纪念册信息
 */
-(NSData *)getCommemorateInfoByUserId:(CSGetCommemorateInfoByUserId *)csGetCommemorateInfoByUserId;
/**
 *  试听音乐作品
 */
-(NSData *)getMusicAudition:(CSMusicAudition *)csMusicAudition;
/**
 *  评论音乐作品
 */
-(NSData *)getAddCommentMusic:(CSAddCommentMusic *)csAddCommentMusic;
/**
 *  获取音乐作品评论信息列表
 */
-(NSData *)getMusicCommentList:(CSGetMusicCommentList *)csGetMusicCommentList;
/**
 *  给音乐作品送花
 */
-(NSData *)getSendMusicFlower:(CSSendMusicFlower *)csSendMusicFlower;
/**
 *  给用户送花
 */
-(NSData *)getSendUserFlower:(CSSendUserFlower *)csSendUserFlower;
/**
 *  给纪念册送花
 */
-(NSData *)getSendCommemotateFlower:(CSSendCommemorateFlower *)csSendCommemorateFlower;
/**
 *  删除礼券
 */
-(NSData *)getDeleteUserCouponList:(CSDeleteUserCouponList *)csDeleteUserCouponList;
/**
 *  设置关进小黑屋
 */
-(NSData *)getSetBlackRoomState:(CSSetBlackRoomState *)csSetBlackRoomState;
/**
 *  查看小黑屋状态
 */
-(NSData *)getBlackRoomState:(CSGetBlackRoomState *)csGetBlackRoomState;
/**
 *  给好友赠送礼券
 */
-(NSData *)getSendCouponToFriend:(CSSendCouponToFriend *)csSendCouponToFriend;
/**
 *  获取相同类型礼券的数量
 */
-(NSData *)getTypeAllNum:(CSGetTypeAllNum *)csGetTypeAllNum;
/**
 *  根据礼券的id获取礼券的信息
 */
-(NSData *)getCouponInfo:(CSCouponInfo *)csCouponInfo;
/**
 *  发送消息
 */
-(NSData *)sendMsg:(CSSendMsg *)csSendMsg;
/**
 *  发送KTV控制
 */
-(NSData*)getReqKTVRoomData:(CSKTVRoomControl*)csKTVRoomControl;
@end