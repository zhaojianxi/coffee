//
//  TCPNetEngine.m
//  ListenToMe
//
//  Created by yadong on 1/27/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "TCPNetEngine.h"

@interface TCPNetEngine (DataHelper)

-(NSData *) preRequestBTData:(LISTENTOME_CMD)cmd  body:(NSData*)body uuid:(long long)uuid;

@end

@implementation TCPNetEngine (DataHelper)
-(NSData *) preRequestBTData:(LISTENTOME_CMD)cmd body:(NSData*)body uuid:(long long)uuid
{
    Byte byteheder[] ={0xfe};
    NSMutableData *tempData = [[NSMutableData alloc] init] ;
    
    Package *prePacked = [[[[[[Package builder] setISepNo:m_seqNo] setECmd:cmd]  setBody:body] setUuid:uuid ] build];
    
    int length = htonl(5+[prePacked data].length);
    NSData *lengthData = [NSData dataWithBytes: &length length: sizeof(length)];
    
    [tempData appendBytes:byteheder length:1];
    [tempData appendData:lengthData];
    [tempData appendData:[prePacked data]];
    
    return tempData;
}
@end

@implementation TCPNetEngine
@synthesize m_seqNo;
//初始化部分代码
+ (TCPNetEngine *)getInstance {
    static TCPNetEngine *instance;
    @synchronized(self) {
        if (!instance) {
            instance = [[TCPNetEngine alloc] init];
        }
    }
    return instance;
}

//登陆
-(NSData*)getReqLoginData:(CSLogin*)csLogin {
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCslogin body:[csLogin data]  uuid:[ListenToMeDBManager getUuid]];
    return data;
}

//登陆
-(NSData*)getReqKTVRoomData:(CSKTVRoomControl*)csKTVRoomControl {
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsktvroomControl body:[csKTVRoomControl data]  uuid:[ListenToMeDBManager getUuid]];
    return data;
}

//退出登录
-(NSData*)getReqQuickLoginData:(CSQuickLogin*)csQuickLogin {
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsquickLogin body:[csQuickLogin data]  uuid:[ListenToMeDBManager getUuid]];
    return data;
}


-(NSData*)getReqFeedbackData:(CSFeedBack*)csFeedBack {
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsfeedBack body:[csFeedBack data]  uuid:[ListenToMeDBManager getUuid]];
    return data;
}

-(NSData*)getReqLogoutData:(CSLogout*)csLogout {
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCslogout body:[csLogout data]  uuid:[ListenToMeDBManager getUuid]];
    return data;
}

-(NSData*)getReqUploadPhoneNumberData:(CSUploadPhoneNumber*)csUploadPhoneNumber {
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsuploadPhoneNumber body:[csUploadPhoneNumber data]  uuid:[ListenToMeDBManager getUuid]];
    return data;
}

-(NSData*)getReqUploadVerifyCodeData:(CSUploadVerifyCode*)csUploadVerifyCode {
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsuploadVerifyCode body:[csUploadVerifyCode data]  uuid:[ListenToMeDBManager getUuid]];
    return data;
}

-(NSData*)getReqGetCollectWorkListData:(CSGetCollectWorkList*)csGetCollectWorkList {
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsgetCollectWorkList body:[csGetCollectWorkList data]  uuid:[ListenToMeDBManager getUuid]];
    return data;
}


-(NSData*)getReqGetUserCouponListData:(CSGetUserCouponList*)csGetUserCouponList {
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsgetUserCouponList body:[csGetUserCouponList data]  uuid:[ListenToMeDBManager getUuid]];
    return data;
}


-(NSData*)getReqUpdate:(CSUpdate*)csUpdate {
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsupdate body:[csUpdate data]  uuid:[ListenToMeDBManager getUuid]];
    return data;
}

-(NSData*)getReqUploadMusicWork:(CSUploadMusicWork*)csUploadMusicWork {
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsuploadMusicWork body:[csUploadMusicWork data]  uuid:[ListenToMeDBManager getUuid]];
    return data;
}


-(NSData*)getReqAddDateWorkInfo:(CSAddDateWorkInfo*)csAddDateWorkInfo {
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsaddDateWorkInfo body:[csAddDateWorkInfo data]  uuid:[ListenToMeDBManager getUuid]];
    return data;
}

-(NSData*)getReqBindKTVRoom:(CSBindKTVRoom*)csBindKTVRoom {
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsbindKtvroom body:[csBindKTVRoom data]  uuid:[ListenToMeDBManager getUuid]];
    return data;
}

//获取KTVlist
-(NSData *)getKtvListInfo:(CSGetKtvListInfo *)csGetKtvListInfo{
    
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsgetKtvListInfo body:[csGetKtvListInfo data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}


/**
 *  根据KTV的ID获取KTV店铺的信息
 */
-(NSData *)getKtvInfoById:(CSGetKtvInfoById *)csGetKtvInfoById{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsgetKtvInfoById body:[csGetKtvInfoById data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}

/**
 *  换一批用户
 */
-(NSData *)getSwitchKtvUserList:(CSGetSwitchKtvUserList *)csGetSwitchKtvList{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsgetSwitchKtvUserList body:[csGetSwitchKtvList data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}

/**
 *  获取用户音乐作品,如果不需要登录uuid传入0
 */
-(NSData *)getMusicWork:(CSGetMusicWork *)csGetMusicWork{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsgetMusicWork body:[csGetMusicWork data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}

/**
 *  根据作品ID获取作品信息
 */
-(NSData *)getMusicWorkByID:(CSGetMusicWorkByID *)csGetMusicWorkByID{
    
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsgetMusicWorkById body:[csGetMusicWorkByID data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}

/**
 *  获取k歌回忆页面的纪念册列表
 */
-(NSData *)getCommenorateInfo:(CSGetCommemorateInfo *)csGetCommemorateInfo{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsgetCommemorateInfo body:[csGetCommemorateInfo data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}

/**
 *  获取人气榜单
 */
-(NSData *)getPopularityList:(CSGetPopularityList *)csGetPopularityList{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsgetPopularityList body:[csGetPopularityList data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}

/**
 *  获取新歌速递榜单 (按发布时间排序,对应深情行星)
 */
-(NSData *)getNewMusicList:(CSGetNewMusicList *)csGetNewMusicList{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsgetNewMusicList body:[csGetNewMusicList data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}


/**
 *  获取每日金曲榜单 (按分数排序,对应时空胶囊)
 */
-(NSData *)getRecommendList:(CSGetRecommendList *)csGetRecommendList{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsgetRecommendList body:[csGetRecommendList data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}
/**
 *  获取用户相册列表
 */
-(NSData *)getPictureList:(CSGetPictureList *)csGetPictureList{
    
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsgetPictureList body:[csGetPictureList data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}
/**
 *  上传相片
 */
-(NSData *)getUpLoadPicture:(CSUploadPicture *)csGetUpLoadPicture{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsuploadPicture body:[csGetUpLoadPicture data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}

/**
 *  删除相片
 */
-(NSData *)getDeletePicture:(CSDeletePicture *)csGetDeletePicture{
    
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsdeletePicture body:[csGetDeletePicture data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}
/**
 *  上传头像
 */
-(NSData *)getUploadIcon:(CSUploadIcon *)csUploadIcon{
    
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsuploadIcon body:[csUploadIcon data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}
/**
 *  设置个人信息
 */
-(NSData *)getSetUserInfo:(CSSetUserInfo *)csSetUserInfo{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCssetUserInfo body:[csSetUserInfo data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}
/**
 *  根据uuid获取用户基本信息
 */
-(NSData *)getUserInfo:(CSGetUserInfo *)csGetUserInfo{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsgetUserInfo body:[csGetUserInfo data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}
/**
 *  搜索KTV
 */
-(NSData *)getSearchKtv:(CSSearchKtv *)csSearchKtv{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCssearchKtv body:[csSearchKtv data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}
/**
 *  搜索歌曲
 */
-(NSData *)getSearchMusics:(CSSearchMusics *)csSearchMusic{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCssearchMusics body:[csSearchMusic data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}
/**
 *  搜索纪念册
 */
-(NSData *)getSearchCommemorate:(CSSearchCommemorate *)csSearchCommemorate{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCssearchCommemorate body:[csSearchCommemorate data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}
/**
 *  收藏
 */
-(NSData *)getCollectWork:(CSCollectWork *)csCollectWork{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCscollectWork body:[csCollectWork data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}
/**
 *  取消收藏
 */
-(NSData *)getUnCollectWork:(CSUnCollectWork *)csUnCollectWork{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsunCollectWork body:[csUnCollectWork data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}
/**
 *  获取收藏列表
 */
-(NSData *)getCollectWorkList:(CSGetCollectWorkList *)csGetCollectWorkList{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsgetCollectWorkList body:[csGetCollectWorkList data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}

/**
 *  获取用户的礼券
 */
-(NSData *)getUserCouponList:(CSGetUserCouponList *)csGetUserCouponList{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsgetUserCouponList body:[csGetUserCouponList data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}

/**
 *  上传需要绑定的手机号
 */
-(NSData *)getUploadPhoneNumber:(CSUploadPhoneNumber *)csUploadPhoneNumber{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsuploadPhoneNumber body:[csUploadPhoneNumber data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}
/**
 *  上传需要验证的验证码
 */
-(NSData *)getUploadVerifyCode:(CSUploadVerifyCode *)csUploadVerfyCode{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsuploadVerifyCode body:[csUploadVerfyCode data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}
/**
 *  删除纪念册
 */
-(NSData *)getDeleteCommemorate:(CSDeleteCommemorateInfo *)csDeleteCommemorateInfo{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsdeleteCommemorateInfo body:[csDeleteCommemorateInfo data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}
/**
 *  删除我的音乐作品
 */
-(NSData *)getDeleteMyMusicWork:(CSDeleteMyMusicWork *)csDeleteMyMusicWork{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsdeleteMyMusicWork body:[csDeleteMyMusicWork data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}

/**
 *  根据id获取纪念册信息,他人主页 个人中心
 */
-(NSData *)getCommemorateInfoByUserId:(CSGetCommemorateInfoByUserId *)csGetCommemorateInfoByUserId{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsgetCommemorateInfoByUserId body:[csGetCommemorateInfoByUserId data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}
/**
 *  试听音乐作品
 */
-(NSData *)getMusicAudition:(CSMusicAudition *)csMusicAudition{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsmusicAudition body:[csMusicAudition data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}

/**
 *  评论音乐作品
 */
-(NSData *)getAddCommentMusic:(CSAddCommentMusic *)csAddCommentMusic{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsaddCommentMusic body:[csAddCommentMusic data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}

/**
 *  获取音乐作品评论列表
 */
-(NSData *)getMusicCommentList:(CSGetMusicCommentList *)csGetMusicCommentList{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsgetMusicCommentList body:[csGetMusicCommentList data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}

/**
 *  给作品送花
 */
-(NSData *)getSendMusicFlower:(CSSendMusicFlower *)csSendMusicFlower{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCssendMusicFlower body:[csSendMusicFlower data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}

/**
 *  给用户送花
 */
-(NSData *)getSendUserFlower:(CSSendUserFlower *)csSendUserFlower{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCssendUserFlower body:[csSendUserFlower data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}
/**
 *  给纪念册送花
 */
-(NSData *)getSendCommemotateFlower:(CSSendCommemorateFlower *)csSendCommemorateFlower{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCssendCommemorateFlower body:[csSendCommemorateFlower data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}

/**
 *  删除用户礼券
 */
-(NSData *)getDeleteUserCouponList:(CSDeleteUserCouponList *)csDeleteUserCouponList{
    NSData *data =[self preRequestBTData:LISTENTOME_CMDCmdCsdeleteUserCouponList body:[csDeleteUserCouponList data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}

/**
 *  设置小黑屋
 */
-(NSData *)getSetBlackRoomState:(CSSetBlackRoomState *)csSetBlackRoomState{
    NSData *data =[self preRequestBTData:LISTENTOME_CMDCmdCssetBlackRoomState body:[csSetBlackRoomState data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}
/**
 *  查看小黑屋的状态
 */
-(NSData *)getBlackRoomState:(CSGetBlackRoomState *)csGetBlackRoomState{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsgetBlackRoomState body:[csGetBlackRoomState data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}
/**
 *  给好友赠送礼券
 */
-(NSData *)getSendCouponToFriend:(CSSendCouponToFriend *)csSendCouponToFriend{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCssendCouponToFriend body:[csSendCouponToFriend data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}
/**
 *  获取相同类型的礼券的数量
 */
-(NSData *)getTypeAllNum:(CSGetTypeAllNum *)csGetTypeAllNum{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCsgetTypeAllNum body:[csGetTypeAllNum data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}
/**
 *  根据礼券的id获取礼券的信息
 */
-(NSData *)getCouponInfo:(CSCouponInfo *)csCouponInfo{
    NSData *data  = [self preRequestBTData:LISTENTOME_CMDCmdCscouponInfo body:[csCouponInfo data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}
/**
 *  发送消息
 */
-(NSData *)sendMsg:(CSSendMsg *)csSendMsg{
    NSData *data = [self preRequestBTData:LISTENTOME_CMDCmdCssendMsg body:[csSendMsg data] uuid:[ListenToMeDBManager getUuid]];
    return data;
}

@end
