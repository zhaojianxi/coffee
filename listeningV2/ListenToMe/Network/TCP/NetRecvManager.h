//
//  NetRecvManager.h
//  ListenToMe
//
//  Created by yadong on 1/27/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <Foundation/Foundation.h>

#define STATE_CMD_REQ_LOGIN                         0x0102 //登录
#define STATE_CMD_RESP_LOGIN                        0x0202 //登录回包


#define STATE_CMD_REQ_GET_KTVLISTINFO                  0x0301 //KTVList请求
#define STATE_CMD_RESP_GET_KTVLISTINFO                 0x0401 //KTVList回包

#define STATE_CMD_REQ_GET_POPULARYLIST                0x0501 //人气榜单回包
#define STATE_CMD_REQ_GET_NEWMUSICLIST                0x0D01 //新歌列表 对应深情行星
#define STATE_CMD_REQ_GET_RECOMMENDLIST               0x0E01 //每日推荐,每日金曲 对应时空胶囊

#define STATE_CMD_REQ_QUICKLOGIN                         0x0610 //快速登录

#define STATE_CMD_REQ_LOGOUT                            0x0620 //退出登录

#define STATE_CMD_RESP_QUICKLOGIN                        0x0710 //快速登录回包

#define STATE_CMD_REQ_GET_KTVINFO                      0x0801 //KTV信息请求

#define STATE_CMD_REQ_GET_SWITCHKTVUSERLIST             0x0901 //KTV跟换一批用户回包

#define STATE_CMD_REQ_GET_MUSICWORK                     0X0A01 //获取用户的作品,如果不需要登录uuid为0

#define STATE_CMD_REQ_GET_MUSICWORKBYID                 0x0B01 //根据作品id获取作品详细信息

#define STATE_CMD_REQ_GET_COMMEMORATEINFO               0x0C01 //根据用户id 获取用户的纪念册信息

#define STATE_CMD_REQ_GET_PICTURELIST                   0x0F01 //查看相片列表

#define STATE_CMD_REQ_UPLOAD_PICTURE                    0x0F11 //上传相片

#define STATE_CMD_REQ_GET_DELETE_PICTURE                0x0F21 //删除相片

#define STATE_CMD_REQ_UPLOAD_ICON                       0x0F31 //上传头像

#define STATE_CMD_REQ_SET_USERINFO                      0x0F41 //设置个人信息

#define STATE_CMD_REQ_USERINFO                          0x0F51 //根据ID获取用户基本信息

#define STATE_CMD_REQ_SEARCH_KTV                        0x0F61 //搜索ktv

#define STATE_CMD_REQ_SEARCH_MUSICS                     0x0F71 //搜索音乐

#define STATE_CMD_REQ_SEARCH_COMMEMORATE                0x0F81 //搜索纪念册

#define STATE_CMD_REQ_COLLECTWORK                       0x0F91 //收藏

#define STATE_CMD_REQ_UN_COLLECTWORK                    0x0FA1 //取消收藏

#define STATE_CMD_REQ_GET_COLLECTWORKLIST               0x0FB1 //获取用户的收藏列表

#define STATE_CMD_REQ_GET_USER_COUPONLIST               0x0FC1 //获取用户的礼券列表

#define STATE_CMD_REQ_UPLOAD_PHONENUMBER_ICON           0x0FD1 //上传绑定的手机号

#define STATE_CMD_REQ_UPLOAD_VERIFY_CODE_ICON           0x0FE1 //上传验证码

#define STATE_CMD_REQ_DELETE_COMMEMORATEINFO            0x0FF1 //删除纪念册

#define STATE_CMD_REQ_DELETE_MYMUSICWORK                0x1001 //删除我的音乐作品

#define STATE_CMD_REQ_GET_COMMEMORATEINFO_BYUSERID      0X1011 //根据id获取纪念册册信息

#define STATE_CMD_REQ_MUSICAUDITION                     0x1021 //试听音乐作品

#define STATE_CMD_REQ_ADD_COMMENTMUSIC                  0x1031 //评论音乐作品

#define STATE_CMD_REQ_GET_MUSIC_COMMENTLIST             0x1041 //获取音乐作品评论信息

#define STATE_CMD_REQ_SEND_MUSIC_FLOWER                 0x1051 //给音乐作品赠送鲜花

#define STATE_CMD_REQ_SEND_USER_FLOWER                  0x1061 //给人送花

#define STATE_CMD_REQ_SEND_COMMEMORATE_FLOWER           0x1071 //给纪念册送花

#define STATE_CMD_REQ_DELETE_USERCOUPONLIST             0x1081 //删除用户礼券

#define STATE_CMD_REQ_SET_BLACKROOMSTATE                0x1091 //设置用户小黑屋状态

#define STATE_CMD_REQ_GET_BLACKROOMSTATE                0x10A1 //查看用户小黑屋状态

#define STATE_CMD_REQ_SEND_COUPON_TO_FRIEND             0x10B1 //给好友赠送礼券

#define STATE_CMD_REQ_GET_TYPEALLNUM                    0x10C1 //获取相同类型的礼券数量

#define STATE_CMD_REQ_GET_PUSHMSG_RSP                   0x10D1 //获取系统消息

#define STATE_CMD_REQ_SEND_MESSAGE                      0x10E1 //发送消息



@interface NetRecvManager : NSObject
+ (NetRecvManager *)getInstance;
-(void)handleRsp:(NSData*)rspData;
-(void)handleTimeOut:(long)tag;
@property(nonatomic,strong) ListenToMeData *listenToMeData;
@end
