//
//  NetReqManager.m
//  ListenToMe
//
//  Created by yadong on 1/27/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "NetReqManager.h"
#import "NetRecvManager.h"
#import "TCPNetEngine.h"
#import "NetHTTPTool.h"
#import "AFNetworking.h"

@interface NetReqManager ()
@property(strong,nonatomic) ListenToMeData *mListenToMeData;
@end

@implementation NetReqManager
@synthesize mListenToMeData;

static NetReqManager *mEngine;

+(NetReqManager *)getInstance
{
    @synchronized(self)
    {
        if (mEngine == nil) {
            mEngine = [[self alloc]init];
        }
    }
    return mEngine;
}

-(id)init
{
    if (self = [super init]) {
        sendStatus = Default;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(disconnect:) name:NOTIFY_NET_DISCONNECT object:nil];
        mListenToMeData = [ListenToMeData getInstance];
    }
    return self;
}

-(void)disconnect:(NSNotification *)nitification{
    sendStatus = Default;
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"网络断开" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert show];
}

-(BOOL)netWorkError
{
    if (!mListenToMeData.bConnected) {
        //        [self disconnect:];
        return YES;
    }
    return NO;
}


#pragma mark - 登录请求发包
-(void)sendLoginReq:(UserBaseInfoNet *)userBaseInfo{
    YDLog(@"sendLoginReq");
    
    
    if([self netWorkError]){
        YDLog(@"网络异常,无法登录");
        return;
    }
    CSLoginBuilder *csloginBuilder = [CSLogin builder];
    [csloginBuilder setStUserBaseInfoNet:userBaseInfo];
    [csloginBuilder setStrIosToken:@""];
    
    CSLogin *csLogin = [csloginBuilder build];
    NSData *tempData = [[TCPNetEngine getInstance]getReqLoginData:csLogin];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:STATE_CMD_REQ_LOGIN];
}

#pragma mark - 登录请求发包
-(void)sendKTVControlReq:(KTV_ROOM_CONTROL_TYPE)eKTVRoomControlType{
    YDLog(@"sendLoginReq");
    
//    [NetHTTPTool getWithUrl:@"http://www.baidu.com" parameters:nil success:nil failure:nil];
    
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
////    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
//    
//    AFHTTPResponseSerializer *afjrs = [AFHTTPResponseSerializer serializer];
//    afjrs.acceptableContentTypes =[NSSet setWithObject:@"text/html"];
//    manager.responseSerializer = afjrs;
//
//    
//    
//    [manager GET:@"http://192.168.2.254:8006/dbserver/playerList!add.do?token=34cb3d81a673a1e847e83cf12639a2af00ff0500819e46de97dd0bd0e9f18ce5&roomno=3&songno=2406814" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"JSON: %@", responseObject);
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        NSLog(@"Error: %@", error);
//    }];
    
    if([self netWorkError]){
        YDLog(@"网络异常,无法登录");
        return;
    }
    CSKTVRoomControlBuilder *csKTVRoomControlBuilder = [CSKTVRoomControl builder];
    [csKTVRoomControlBuilder setUuid:1001];
    [csKTVRoomControlBuilder setEKtvroomType:eKTVRoomControlType];
    
    CSKTVRoomControl *csKTVRoomControl = [csKTVRoomControlBuilder build];
    NSData *tempData = [[TCPNetEngine getInstance]getReqKTVRoomData:csKTVRoomControl];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:STATE_CMD_REQ_LOGIN];
}


#pragma mark - 快速登录发包
-(void)sendQuickLoginReq:(long long)uuid{
    CSQuickLoginBuilder *csQuickLoginBuilder = [CSQuickLogin builder];
    [csQuickLoginBuilder setUuid:uuid];
    [csQuickLoginBuilder setStrIosToken:@""];
    
    CSQuickLogin *csQuickLogin = [csQuickLoginBuilder build];
    NSData *tempData = [[TCPNetEngine getInstance] getReqQuickLoginData:csQuickLogin];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:STATE_CMD_REQ_QUICKLOGIN];
}

#pragma mark - 退出登录发包
-(void)sendLogoutReq:(int64_t)uuid{
    CSLogoutBuilder *csLogoutBuilder = [CSLogout builder];
    csLogoutBuilder.uuid = uuid;
    
    CSLogout *csLogout = [csLogoutBuilder build];
    NSData *tempData = [[TCPNetEngine getInstance] getReqLogoutData:csLogout];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:STATE_CMD_REQ_LOGOUT];
}


-(void)sendFeedback:(NSString *)feedback{
    if ([self netWorkError]) {
        
        return;
    }
    CSFeedBackBuilder* csFeedBackBuilder = [CSFeedBack builder];
    
    csFeedBackBuilder.uuid = 10080;
    csFeedBackBuilder.iSysVersion = 100;
    csFeedBackBuilder.iMobleSystem = 1;
    csFeedBackBuilder.sContent = feedback;
    
    CSFeedBack* csFeedBack = [csFeedBackBuilder build];
    
    NSData* tempData = [[TCPNetEngine getInstance] getReqFeedbackData:csFeedBack];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:0];
}


-(void)sendLogout{
    if ([self netWorkError]) {
        
        return;
    }
    CSLogoutBuilder* csCSLogoutBuilder = [CSLogout builder];
    
    csCSLogoutBuilder.uuid = 10080;
    
    CSLogout* csLogout = [csCSLogoutBuilder build];
    
    NSData* tempData = [[TCPNetEngine getInstance] getReqLogoutData:csLogout];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:0];
}

-(void)sendUploadPhoneNumber:(NSString*) sPhoneNumber{
    if ([self netWorkError]) {
        
        return;
    }
    CSUploadPhoneNumberBuilder* csUploadPhoneNumberBuilder = [CSUploadPhoneNumber builder];
    
    csUploadPhoneNumberBuilder.uuid = 10080;
    csUploadPhoneNumberBuilder.sPhoneNumber = sPhoneNumber;
    
    CSUploadPhoneNumber* csUploadPhoneNumber = [csUploadPhoneNumberBuilder build];
    
    NSData* tempData = [[TCPNetEngine getInstance] getReqUploadPhoneNumberData:csUploadPhoneNumber];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:0];
}

-(void)sendVerifyCode:(NSString*) sVerifyCode{
    if ([self netWorkError]) {
        
        return;
    }
    CSUploadVerifyCodeBuilder* csUploadVerifyCodeBuilder = [CSUploadVerifyCode builder];
    
    csUploadVerifyCodeBuilder.uuid = 10080;
    csUploadVerifyCodeBuilder.sVerifyCode = sVerifyCode;
    
    CSUploadVerifyCode* csUploadVerifyCode = [csUploadVerifyCodeBuilder build];
    
    NSData* tempData = [[TCPNetEngine getInstance] getReqUploadVerifyCodeData:csUploadVerifyCode];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:0];
}


-(void)sendGetUserCouponList:(int)iOffset num:(int)iNum{
    if ([self netWorkError]) {
        
        return;
    }
    CSGetUserCouponListBuilder* csGetUserCouponListBuilder = [CSGetUserCouponList builder];
    
    csGetUserCouponListBuilder.uuid = 10080;
    csGetUserCouponListBuilder.iOffset = iOffset;
    csGetUserCouponListBuilder.iNum = iNum;
    
    CSGetUserCouponList *csGetUserCouponList = [csGetUserCouponListBuilder build];
    
    NSData* tempData = [[TCPNetEngine getInstance] getReqGetUserCouponListData:csGetUserCouponList];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:0];
}





-(void)sendUpdate{
    if ([self netWorkError]) {
        
        return;
    }
    CSUpdateBuilder* csUpdateBuilder = [CSUpdate builder];
    
    csUpdateBuilder.uuid = 10080;
    csUpdateBuilder.iMobleSystem = 1;
    csUpdateBuilder.iSysVersion = 101;
    csUpdateBuilder.strMobileScreen = @"IOS8";
    csUpdateBuilder.strMobileScreen = @"100";
    
    CSUpdate *csUpdate = [csUpdateBuilder build];
    
    NSData* tempData = [[TCPNetEngine getInstance] getReqUpdate:csUpdate];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:0];
}


-(void)sendUploadMusicWorkCurSeq:(long long)curSeq sendDataDic:(NSMutableDictionary *)sendDataDic desc:(NSString*) sDesc songid:(long long)lSongID musicworkid:(long long)lMusicWorkID{
    if ([self netWorkError]) {
        return;
    }
    
    CSUploadMusicWorkBuilder *stCSUploadMusicWorkBuilder = [CSUploadMusicWork builder];
    [stCSUploadMusicWorkBuilder setSDesc:sDesc];
    [stCSUploadMusicWorkBuilder setLCurrentPackageSeq:curSeq];
    [stCSUploadMusicWorkBuilder setLMaxPackageNum:sendDataDic.count];
    [stCSUploadMusicWorkBuilder setLMusicWorkId:lMusicWorkID];
    [stCSUploadMusicWorkBuilder setUuid:10080];
    [stCSUploadMusicWorkBuilder setLSongId:lSongID];
    [stCSUploadMusicWorkBuilder setVMusicDatas:[sendDataDic objectForKey:[NSString stringWithFormat:@"%lld",curSeq]]];
    NSLog(@"stCSUploadMusicWorkBuilder : %lld" , curSeq);
    
    
    CSUploadMusicWork *stCSUploadMusicWork = [stCSUploadMusicWorkBuilder build];
    NSData* tempData = [[TCPNetEngine getInstance] getReqUploadMusicWork:stCSUploadMusicWork];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:0];
}


-(void)sendAddDateWorkInfo{
    if ([self netWorkError]) {
        
        return;
    }
    CSAddDateWorkInfoBuilder* csAddDateWorkInfoBuilder = [CSAddDateWorkInfo builder];
    
    csAddDateWorkInfoBuilder.uuid = 10080;
    csAddDateWorkInfoBuilder.sTheme = @"约会1";
    csAddDateWorkInfoBuilder.partyType = PARTY_TYPEPartyTypeOne;
    csAddDateWorkInfoBuilder.costType = COST_TYPECostTypeAa;
    csAddDateWorkInfoBuilder.bringFriendType = BRINGFRIEND_TYPEBringfriendTypeYes;
    csAddDateWorkInfoBuilder.sDesc = @"ddddd";
    csAddDateWorkInfoBuilder.lStopTime = (long long)[[NSDate date] timeIntervalSince1970];
    csAddDateWorkInfoBuilder.lktvId = 111111;
    [csAddDateWorkInfoBuilder addLInUserId:124];
 
    CSAddDateWorkInfo *csAddDateWorkInfo = [csAddDateWorkInfoBuilder build];
    
    NSData* tempData = [[TCPNetEngine getInstance] getReqAddDateWorkInfo:csAddDateWorkInfo];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:0];
}

-(void)sendBindDevice:(NSString*)qrcode{
    if ([self netWorkError]) {
        
        return;
    }
    CSBindKTVRoomBuilder*  csBindKTVRoomBuilder= [CSBindKTVRoom builder];
    
    csBindKTVRoomBuilder.uuid = [ListenToMeDBManager getUuid];
    csBindKTVRoomBuilder.sKtvroomCode = qrcode;
    
    CSBindKTVRoom *csBindKTVRoom = [csBindKTVRoomBuilder build];
    
    NSData* tempData = [[TCPNetEngine getInstance] getReqBindKTVRoom:csBindKTVRoom];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:0];
}



#pragma mark - KTVListInfo请求
-(void)sendGetKtvListInfo:(int32_t)iOffset INum:(int32_t)iNum{
    CSGetKtvListInfoBuilder *csGetKtvListBuilder = [CSGetKtvListInfo builder];
    csGetKtvListBuilder.uuid = [ListenToMeDBManager getUuid];
    csGetKtvListBuilder.iOffset = iOffset;
    csGetKtvListBuilder.iNum = iNum;
    
    CSGetKtvListInfo *csGetKtvListInfo = [csGetKtvListBuilder build];
    NSData *tempData = [[TCPNetEngine getInstance] getKtvListInfo:csGetKtvListInfo];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:STATE_CMD_REQ_GET_KTVLISTINFO];
}


#pragma mark - 根据KTVId获取KTV的信息
-(void)sendGetKtvInfoById:(int64_t)lKtvid{
    
    CSGetKtvInfoByIdBuilder *csGetKtvInfoByIdBuilder = [CSGetKtvInfoById builder];
    csGetKtvInfoByIdBuilder.lKtvid = lKtvid;
    
    CSGetKtvInfoById *csGetKtvInfoById = [csGetKtvInfoByIdBuilder build];
    NSData *tempData = [[TCPNetEngine getInstance] getKtvInfoById:csGetKtvInfoById];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:STATE_CMD_REQ_GET_KTVINFO];
    
}

#pragma mark - 更换新一批用户
-(void)sendGetSwitchKtvUserList:(int32_t)iOffset INum:(int32_t)iNum LKtvId:(int64_t)lKtvId{
    
    CSGetSwitchKtvUserListBuilder *csGetSwitchKtvUserListBuilder = [CSGetSwitchKtvUserList builder];
    csGetSwitchKtvUserListBuilder.lKtvId = lKtvId;
    csGetSwitchKtvUserListBuilder.iOffset = iOffset;
    csGetSwitchKtvUserListBuilder.iNum = iNum;
    
    CSGetSwitchKtvUserList *csGetSwitchKtvUserList = [csGetSwitchKtvUserListBuilder build];
    NSData *tempData = [[TCPNetEngine getInstance] getSwitchKtvUserList:csGetSwitchKtvUserList];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:STATE_CMD_REQ_GET_SWITCHKTVUSERLIST];
}


#pragma mark - 获取用户的音乐作品,如果不需要登录uuid传入为0 (LWatchUserId)被查看者id ,如果为空就是查看用户自己的作品 ,如果不为空就是用户查看别人作品
-(void)sendGetMusicWorkWithUserId:(int64_t)uuid IOffset:(int32_t)iOffset INum:(int32_t)iNum LWatchUserId:(int64_t)lWatchUserId{
 
    CSGetMusicWorkBuilder *csGetMusicWorkBuilder = [CSGetMusicWork builder];
    csGetMusicWorkBuilder.uuid = uuid;
    csGetMusicWorkBuilder.iOffset = iOffset;
    csGetMusicWorkBuilder.iNum = iNum;
    csGetMusicWorkBuilder.lWatchUserId = lWatchUserId;
    
    CSGetMusicWork *csGetMusicWork = [csGetMusicWorkBuilder build];
    NSData *tempData = [[TCPNetEngine getInstance] getMusicWork:csGetMusicWork];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:STATE_CMD_REQ_GET_MUSICWORK];
    
    
}

#pragma mark - 根据作品id获取作品详情信息
-(void)sendGetMusicWorkByID:(int64_t)uuid LWorkID:(int64_t)lWorkID{
    CSGetMusicWorkByIDBuilder *csGetMusicWorkByIDBuilder = [CSGetMusicWorkByID builder];
    csGetMusicWorkByIDBuilder.uuid = uuid;
    csGetMusicWorkByIDBuilder.lWorkId = lWorkID;
    
    CSGetMusicWorkByID *csGetMusicWorkByID = [csGetMusicWorkByIDBuilder build];
    NSData *tempData = [[TCPNetEngine getInstance] getMusicWorkByID:csGetMusicWorkByID];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:STATE_CMD_REQ_GET_MUSICWORKBYID];
}

#pragma mark - 根据用户id获取纪念册信息
-(void)sendGetCommemorateInfo:(int64_t)uuid IOffset:(int32_t)iOffset INum:(int32_t)iNum{
    
    CSGetCommemorateInfoBuilder *csGetCommemorateInfoBuilder = [CSGetCommemorateInfo builder];
    csGetCommemorateInfoBuilder.uuid = uuid;
    csGetCommemorateInfoBuilder.iOffset = iOffset;
    csGetCommemorateInfoBuilder.iNum = iNum;
    
    CSGetCommemorateInfo *csGetCommemorateInfo = [csGetCommemorateInfoBuilder build];
    NSData *tempData = [[TCPNetEngine getInstance] getCommenorateInfo:csGetCommemorateInfo];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:STATE_CMD_REQ_GET_COMMEMORATEINFO];
    
}

#pragma mark - 获取用户相片列表,传入纪念册ID为空 查看的是用户自己的相册, 传入相应的纪念册ID查看纪念册里相片
-(void)sendGetPictureList:(int64_t)uuid IOffset:(int32_t)iOffset Inum:(int32_t)iNum LWatchUserId:(int64_t)lWatchUserId LCommemorateId:(int64_t)lCommenorateId{
    CSGetPictureListBuilder *csGetPictureListBuilder = [CSGetPictureList builder];
    csGetPictureListBuilder.uuid = uuid;
    csGetPictureListBuilder.iOffset = iOffset;
    csGetPictureListBuilder.iNum = iNum;
    csGetPictureListBuilder.lWatchUserId = lWatchUserId;
    csGetPictureListBuilder.lCommemorateId = lCommenorateId;
    
    CSGetPictureList *csGetPictuerList = [csGetPictureListBuilder build];
    NSData *tempData = [[TCPNetEngine getInstance] getPictureList:csGetPictuerList];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:STATE_CMD_REQ_GET_PICTURELIST];
}

#pragma mark - 获取人气榜单列表
-(void)sendGetPopularityList:(int64_t)uuid IOffset:(int32_t)iOffset INum:(int32_t)iNum{
    CSGetPopularityListBuilder *csGetPopularityBuilder = [CSGetPopularityList builder];
    csGetPopularityBuilder.uuid = uuid;
    csGetPopularityBuilder.iOffset = iOffset;
    csGetPopularityBuilder.iNum = iNum;
    
    CSGetPopularityList *csGetPopularityList = [csGetPopularityBuilder build];
    NSData *tempData = [[TCPNetEngine getInstance] getPopularityList:csGetPopularityList];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:STATE_CMD_REQ_GET_POPULARYLIST];
}

#pragma mark - 获取新歌列表 对应深情行星
-(void)sendGetNewMusicList:(int64_t)uuid IOffset:(int32_t)iOffset INum:(int32_t)iNum{
    CSGetNewMusicListBuilder *csGetNewMusicListBuilder = [CSGetNewMusicList builder];
    csGetNewMusicListBuilder.uuid = uuid;
    csGetNewMusicListBuilder.iOffset = iOffset;
    csGetNewMusicListBuilder.iNum = iNum;
    
    CSGetNewMusicList *csGetNewMusicList = [csGetNewMusicListBuilder build];
    NSData * tempData = [[TCPNetEngine getInstance] getNewMusicList:csGetNewMusicList];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:STATE_CMD_REQ_GET_NEWMUSICLIST];
    
}

#pragma mark - 获取歌曲推荐列表,每日金曲, 对应时空胶囊
-(void)sendGetRecommendList:(int64_t)uuid IOffset:(int32_t)iOffset INum:(int32_t)iNum{
    CSGetRecommendListBuilder *csGetRecommendListBuilder = [CSGetRecommendList builder];
    csGetRecommendListBuilder.uuid = uuid;
    csGetRecommendListBuilder.iOffset = iOffset;
    csGetRecommendListBuilder.iNum = iNum;
    
    CSGetRecommendList *csGetRecommendList = [csGetRecommendListBuilder build];
    NSData *tempData  = [[TCPNetEngine getInstance] getRecommendList:csGetRecommendList];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:STATE_CMD_REQ_GET_RECOMMENDLIST];
    
}

#pragma mark - 上传照片
-(void)sendGetUploadPicture:(int64_t)uuid BPictures:(NSData *)bPictures LCommenorateId:(int64_t)lCommenorateId{
    
    CSUploadPictureBuilder *csUploadPictureBuilder = [CSUploadPicture builder];
    csUploadPictureBuilder.uuid = uuid;
    csUploadPictureBuilder.bPictures = bPictures;
    csUploadPictureBuilder.lCommemorateId = lCommenorateId;
    
    CSUploadPicture *csUploadPicture = [csUploadPictureBuilder build];
    NSData *tempData = [[TCPNetEngine getInstance] getUpLoadPicture:csUploadPicture];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:STATE_CMD_REQ_UPLOAD_PICTURE];
}

#pragma mark - 删除照片
-(void)sendGetDeletePicture:(int64_t)uuid LPhotoId:(int64_t)lPhotoId LCommemorateId:(int64_t)lCommemorateId{
    CSDeletePictureBuilder *csDeletePictureBuilder = [CSDeletePicture builder];
    csDeletePictureBuilder.uuid = uuid;
    csDeletePictureBuilder.lPhotoId = lPhotoId;
    csDeletePictureBuilder.lCommemorateId = lCommemorateId;
    
    CSDeletePicture *csDeletePicture = [csDeletePictureBuilder build];
    NSData *tempData = [[TCPNetEngine getInstance] getDeletePicture:csDeletePicture];
    [[ListenToMeData getInstance].socketIns  writeData:tempData withTimeout:100 tag:STATE_CMD_REQ_GET_DELETE_PICTURE];
}

#pragma mark - 上传头像
-(void)sendgetUploadIcon:(int64_t)uuid VIconDatas:(NSData *)vIconDatas SPictureUrl:(NSString *)sPictureUrl{
    CSUploadIconBuilder *csUploadIconBuilder = [CSUploadIcon builder];
    csUploadIconBuilder.uuid = uuid;
    csUploadIconBuilder.vIconDatas = vIconDatas;
    csUploadIconBuilder.sPictureUrl = sPictureUrl;
    
    CSUploadIcon *csUploadIcon = [csUploadIconBuilder build];
    NSData *tempData = [[TCPNetEngine getInstance] getUploadIcon:csUploadIcon];
    [[ListenToMeData getInstance].socketIns  writeData:tempData withTimeout:100 tag:STATE_CMD_REQ_UPLOAD_ICON];
}

#pragma mark - 设置个人信息
-(void)sendGetSetUserInfo:(int64_t)uuid SNickName:(NSString *)sNickName IGender:(int32_t)iGender SBirthday:(NSString *)sBirthday SSignature:(NSString *)sSignature IAge:(int32_t)iAge IPrivataSwitch:(int32_t)iPrivateSwitch Address:(NSString *)address{
    CSSetUserInfoBuilder *csSetUserInfoBuilder = [CSSetUserInfo builder];
    csSetUserInfoBuilder.uuid = uuid;
    csSetUserInfoBuilder.sNickName = sNickName;
    csSetUserInfoBuilder.iGender = iGender;
    csSetUserInfoBuilder.sBirthday = sBirthday;
    csSetUserInfoBuilder.sSignature = sSignature;
    csSetUserInfoBuilder.iAge = iAge;
    csSetUserInfoBuilder.iPrivateSwitch = iPrivateSwitch;
    csSetUserInfoBuilder.address = address;
    
    CSSetUserInfo *csSetUserInfo = [csSetUserInfoBuilder build];
    NSData *tempData = [[TCPNetEngine getInstance] getSetUserInfo:csSetUserInfo];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:STATE_CMD_REQ_SET_USERINFO];
}
#pragma mark - 获取用户基本信息
-(void)sendGetUserInfo:(int64_t)uuid{
    CSGetUserInfoBuilder *csGetUserInfoBuilder = [CSGetUserInfo builder];
    csGetUserInfoBuilder.lUserId = uuid;
    
    CSGetUserInfo *csGetUserInfo = [csGetUserInfoBuilder build];
    NSData *tempData = [[TCPNetEngine getInstance] getUserInfo:csGetUserInfo];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:STATE_CMD_REQ_USERINFO];
}
#pragma mark - 搜索ktv
-(void)sendSearchKtv:(int64_t)uuid SKeyWord:(NSString *)sKeyWord IOffset:(int32_t)iOffset INum:(int32_t)iNum{
    CSSearchKtvBuilder *csSearchKtvBuilder = [CSSearchKtv builder];
    csSearchKtvBuilder.uuid = uuid;
    csSearchKtvBuilder.sKeyWord = sKeyWord;
    csSearchKtvBuilder.iOffset = iOffset;
    csSearchKtvBuilder.iNum = iNum;
    
    CSSearchKtv *csSearchKtv = [csSearchKtvBuilder build];
    NSData *tempData = [[TCPNetEngine getInstance] getSearchKtv:csSearchKtv];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:STATE_CMD_REQ_SEARCH_KTV];
}
#pragma mark - 搜索音乐
-(void)sendSearchMusics:(int64_t)uuid SKeyWord:(NSString *)sKeyWord IOffset:(int32_t)iOffset INum:(int32_t)iNum{
    CSSearchMusicsBuilder *csSearchMusicBuilder = [CSSearchMusics builder];
    csSearchMusicBuilder.uuid = uuid;
    csSearchMusicBuilder.sKeyWord = sKeyWord;
    csSearchMusicBuilder.iOffset = iOffset;
    csSearchMusicBuilder.iNum = iNum;
    
    CSSearchMusics *csSearchMusics = [csSearchMusicBuilder build];
    NSData *tempData = [[TCPNetEngine getInstance] getSearchMusics:csSearchMusics];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:STATE_CMD_REQ_SEARCH_MUSICS];
}
#pragma mark - 搜索纪念册
-(void)sendSearchCommemorate:(int64_t)uuid SKeyWord:(NSString *)sKeyWord IOffset:(int32_t)iOffset INum:(int32_t)iNum{
    CSSearchCommemorateBuilder *csSearchCommemorateBuilder = [CSSearchCommemorate builder];
    csSearchCommemorateBuilder.uuid = uuid;
    csSearchCommemorateBuilder.sKeyWord = sKeyWord;
    csSearchCommemorateBuilder.iOffset = iOffset;
    csSearchCommemorateBuilder.iNum = iNum;
    
    CSSearchCommemorate *csSearchCommemorate = [csSearchCommemorateBuilder build];
    NSData *tempData = [[TCPNetEngine getInstance] getSearchCommemorate:csSearchCommemorate];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:STATE_CMD_REQ_SEARCH_COMMEMORATE];
}
#pragma mark - 收藏
-(void)sendCollectWork:(int64_t)uuid LMusicId:(int64_t)lMusicId LCommemorateId:(int64_t)lCommemorateId{
    CSCollectWorkBuilder *csCollectWorkBuilder = [CSCollectWork builder];
    csCollectWorkBuilder.uuid = uuid;
    csCollectWorkBuilder.lMusicId = lMusicId;
    csCollectWorkBuilder.lCommemorateId = lCommemorateId;
    
    CSCollectWork *csCollectWork  = [csCollectWorkBuilder build];
    NSData *tempData = [[TCPNetEngine getInstance] getCollectWork:csCollectWork];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:STATE_CMD_REQ_COLLECTWORK];
}

#pragma mark - 取消收藏
-(void)sendUnCollectWork:(int64_t)uuid LMusicId:(int64_t)lMusicId LCommemorateId:(int64_t)lCommemorateId{
    CSUnCollectWorkBuilder *csUnCollectWorkBuilder = [CSUnCollectWork builder];
    csUnCollectWorkBuilder.uuid = uuid;
    csUnCollectWorkBuilder.lMusicId = lMusicId;
    csUnCollectWorkBuilder.lCommemorateId = lCommemorateId;
    
    CSUnCollectWork *csUncollectWork = [csUnCollectWorkBuilder build];
    NSData *tempData = [[TCPNetEngine getInstance]getUnCollectWork:csUncollectWork];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:STATE_CMD_REQ_UN_COLLECTWORK];
}

#pragma mark - 获取我的收藏列表
-(void)sendGetCollectWorkList:(int64_t)uuid IOffset:(int32_t)iOffset INum:(int32_t)iNum{
    CSGetCollectWorkListBuilder *csGetCollectWorkListBuilder = [CSGetCollectWorkList builder];
    csGetCollectWorkListBuilder.uuid = uuid;
    csGetCollectWorkListBuilder.iOffset = iOffset;
    csGetCollectWorkListBuilder.iNum = iNum;
    
    CSGetCollectWorkList *csGetCollectWorkList = [csGetCollectWorkListBuilder build];
    NSData *tempData = [[TCPNetEngine getInstance] getCollectWorkList:csGetCollectWorkList];
    [[ListenToMeData getInstance].socketIns writeData:tempData withTimeout:100 tag:STATE_CMD_REQ_GET_COLLECTWORKLIST];
}
@end
