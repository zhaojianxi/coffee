//
//  YDSquareVC.m
//  ListenToMe
//
//  Created by yadong on 1/26/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "YDSquareVC.h"
#import "YDKTVView.h"
#import "YDRecallAlbumView.h"
#import "YDTOPListView.h"
#import "YDWantSingView.h"
#import "HMPopMenu.h"
#import "YDPopBt.h"
#import "YDSquareSearchVC.h"
#import "YDSquareCell.h"
#import "YDTopListCell.h"
#import "YDKMemmoryCell.h"
#import "YDWantSingCell.h"
#import "DaysMusicVC.h"

#import "HWKTVTrendsVC.h"
#import "HWShareAlertVC.h"
#import "HWPerHomePageVC.h"
#import "HWAlbumDetailVC.h"
#import "HWMusicPlayVC.h"
#import "LoginAlertVC.h"
#import "LoginAlertView.h"
#import "LoadDataView.h"

@interface YDSquareVC ()<UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,YDTopLiseCellDelegate,YDSquareCellDelegate>
//<UISearchBarDelegate>
@property(assign,nonatomic) int tagOfView;
@property(strong,nonatomic) UIView *tempView;
@property(strong,nonatomic) YDKTVView *ktvView;
@property(strong,nonatomic) YDRecallAlbumView *recallAlbumView;
@property(strong,nonatomic) YDTOPListView *topListView; // 产品改其为star
@property(strong,nonatomic) YDWantSingView *wantSingView;
@property(assign,nonatomic) CGRect frameFourViews;
@property(strong,nonatomic) UIButton *btnKtv;
@property(strong,nonatomic) UIButton *btnStar;
@property(strong,nonatomic) UIButton *btnMemory;
@property(strong,nonatomic) UIButton *btnSing;
@property(strong,nonatomic) UIView *segmentBgView;
@property(strong,nonatomic) UIImageView *imgSegLine;
@property(strong,nonatomic) NSTimer *timerScrollView;
@property(strong,nonatomic) NSMutableArray *arrayImgs;
@property(assign,nonatomic) int countImgs;
@property(strong,nonatomic)HWShareAlertVC *shareVC;
/**
 *  导航栏更多菜单
 */
@property(nonatomic,strong)YDPopBt *btnMsg;
@property(nonatomic,strong)YDPopBt *btnPlaying;
@property(nonatomic,strong)YDPopBt *btnRoom;
@property(nonatomic,strong)YDPopBt *btnReport;

/**
 *  正在加载数据动画
 */
@property(nonatomic,strong)LoadDataView *loadDataView;


@property(nonatomic,strong)LoginAlertVC *loginAlertVC;
@property(nonatomic,strong)NSIndexPath *selectIndexPath;
@property(nonatomic,strong)LoginAlertView *loginAlertView;

/**
 *  滑动的的距离
 */
@property(nonatomic,assign) CGFloat oldOffset;
/**
 *  纪念册列表数组
 */
@property(nonatomic,strong) NSMutableArray *arrCommemorateBaseInfo;
@end

@implementation YDSquareVC
@synthesize ktvView;
@synthesize recallAlbumView;
@synthesize topListView;
@synthesize wantSingView;
@synthesize frameFourViews;
/**
 *  导航栏更多菜单
 */
@synthesize btnMsg;
@synthesize btnPlaying;
@synthesize btnRoom;
@synthesize btnReport;

#pragma mark -生命周期
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    [self setUI];
    
    _oldOffset = self.ktvView.mTableView.height;
    
    //加载数据的动画
    [self setLoadDataGif];
    
    [self initData];
    
    //联网成功的通知
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifiLoadData) name:NOTIFY_NET_CONNENTSUCCESS object:nil];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
#warning 如果不在视图出现时将系统的UITabBarButton移除,那么在pop回来是,系统的tabar会显示出来,会跟自定义的tarbar重叠影响视图
    for (UIView *child in self.tabBarController.tabBar.subviews) {
        // 删除系统自动生成的UITabBarButton
        if ([child isKindOfClass:[UIControl class]]) {
            [child removeFromSuperview];
        }
    }
    
}


-(void)initData{
   //获取KTV列表数据
    [[NetReqManager getInstance] sendGetKtvListInfo:0 INum:5];
    
    
    self.listenToMeData = [ListenToMeData getInstance];
    
    
    //获取KTV列表数据的回包通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifiLoadData) name:NOTIFY_GET_KTVLIST_PAGE_RESP object:nil];
    //获取K星广场数据的回包通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifiLoadData) name:NOTIFY_GET_POPULARITYLIST_PAGE_RESP object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifiLoadData) name:NOTIFY_GET_NEWMUSICLIST_PAGE_RESP object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifiLoadData) name:NOTIFY_GET_RECOMMENDLIST_PAGE_RESP object:nil];
    //获取纪念册列表数据的回包通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifiLoadData) name:NOTIFY_GET_COMMEMORATEINFO_PAGE_RESP object:nil];
    
    //获取收藏列表数据的回包通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifiLoadData) name:NOTIFY_GET_COLLECT_WORKLIST_PAGE_RESP object:nil];
    
    //收藏音乐
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshCollectData) name:NOTIFY_GET_COLLECT_MUSICWORK_PAGE_RESP object:nil];
    
    //收藏纪念册
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshCollectData) name:NOTIFY_GET_COLLECT_COMMEMORATE_PAGE_RESP object:nil];
    
    //取消收藏音乐
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshCollectData) name:NOTIFY_GET_UN_COLLECT_MUSICWORK_PAGE_RESP object:nil];
    
    //取消收藏纪念册
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshCollectData) name:NOTIFY_GET_UN_COLLECT_COMMEMORATE_PAGE_RESP object:nil];
}

#pragma mark - 重新获取收藏列表,并刷新数据的显示,解决收藏显示状态同步显示的问题
-(void)refreshCollectData{
    
    if ([ListenToMeDBManager getUuid] > 0) {
        
        [[NetReqManager getInstance] sendGetCollectWorkList:[ListenToMeDBManager getUuid] IOffset:0 INum:20];
        
        if (_tagOfView == 1) {
            [self.topListView.mTableview reloadData];
        }
        
        if (_tagOfView == 2) {
            [self.recallAlbumView.mtableView reloadData];
        }
    }

    
}

#pragma mark - 回包后通知列表数据刷新显示
-(void)notifiLoadData{
    
    if (_tagOfView == 0) {
     //KTV
        [self.ktvView.mTableView reloadData];
        [self setUpKtvScrollBar];
        [self dismissLoadDataView];
    }
    
    if (_tagOfView == 1) {
      //K星电台
        
        [self.topListView.mTableview reloadData];
        [self dismissLoadDataView];
    }
    
    if(_tagOfView == 2){
       //K歌回忆
      
        [self.recallAlbumView.mtableView reloadData];
        [self dismissLoadDataView];
    }
    
    if ([ListenToMeData getInstance].bConnected) {
        if (self.loadDataView) {
            [self dismissLoadDataView];
        }
        
    }
    
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_KTVLIST_PAGE_RESP object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_POPULARITYLIST_PAGE_RESP object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_NEWMUSICLIST_PAGE_RESP object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_RECOMMENDLIST_PAGE_RESP object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_COMMEMORATEINFO_PAGE_RESP object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_COLLECT_MUSICWORK_PAGE_RESP object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_COLLECT_COMMEMORATE_PAGE_RESP object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_UN_COLLECT_MUSICWORK_PAGE_RESP object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_UN_COLLECT_COMMEMORATE_PAGE_RESP object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_COLLECT_WORKLIST_PAGE_RESP object:nil];
}

#pragma mark - 加载动画
-(void)setLoadDataGif{
    
    if (!self.loadDataView) {
        CGRect loadDataFrame = CGRectMake(0, naviAndStatusH, screenWidth, screenHeight - naviAndStatusH - tabBarH);
        self.loadDataView =[[LoadDataView alloc]initWithFrame:loadDataFrame];
    }
   
//    [[[UIApplication sharedApplication]keyWindow] addSubview:self.loadDataView];
    [self.view addSubview:self.loadDataView];
    
    
}

//动画消失
-(void)dismissLoadDataView{
    [self.loadDataView removeFromSuperview];
}

-(void)setUI
{

    
    [self customNavigationBar];
    
    [self setSegment];
    
    [self setFourViews];
    
    [self setktvUI];
    
    [self setStarUI];
    
    [self setMemoryUI];
    
    //暂时砍掉
//    [self setWantSingUI];
    
    //集成刷新控件
    [self setupRefreshView];
}


#pragma mark - setupRefreshView 集成刷新控件
-(void)setupRefreshView{
    //1 下拉刷新
   
    
}

////////////////////////////////// tableview /////////////////////////////////////////////////////////////////////////////

#pragma tableviewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    switch (_tagOfView) {
        case 0:{
            //KTV
            
            HWKTVTrendsVC *ktvTrend = [[HWKTVTrendsVC alloc]init];
            ktvTrend.ktvBaseInfo = (self.listenToMeData.ktvListBaseInfo[indexPath.section]);
            [self.navigationController pushViewController:ktvTrend animated:YES];
            
            
        }
            break;
        case 1:{
            //K星电台
            
            MusicWorkBaseInfo *musicWorkBaseInfo = nil;
            //判断数据源
            if (indexPath.section == 0) {
                 musicWorkBaseInfo = self.listenToMeData.arrPopularityList[indexPath.row];
            }
            if (indexPath.section == 1) {
                 musicWorkBaseInfo = self.listenToMeData.arrNewMusicList[indexPath.row];
            }
            
            if(indexPath.section == 2){
                 musicWorkBaseInfo = self.listenToMeData.arrRecommendList[indexPath.row];
            }
            
            HWMusicPlayVC *musicPlay = [[HWMusicPlayVC alloc]init];
            musicPlay.navigationController.navigationBarHidden = YES;
            musicPlay.musicWorkBaseInfo = musicWorkBaseInfo;
            musicPlay.lWorkID = musicWorkBaseInfo.lMusicWorkId;
            musicPlay.navigationItem.title = musicWorkBaseInfo.stSongInfo.sSongName;
            [self.navigationController pushViewController:musicPlay animated:YES];
            
            
        }
            break;
        case 2:{
            //K歌回忆
#warning K歌回忆详情是要调转到HTML页面,不需要搭建UI界面
//            HWAlbumDetailVC *albumDetailVC = [[HWAlbumDetailVC alloc]init];
//            [self.navigationController pushViewController:albumDetailVC animated:YES];
        }
            break;
        case 3:{
            //我要唱歌
        }
            break;
            
        default:
            break;
    }

}

-(id )tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (0 == _tagOfView) {
        
        
//        YDSquareCell *cell = [YDSquareCell cellWithTableView:tableView];
        NSString *identify = [NSString stringWithFormat:@"Squarecell%ld",indexPath.section];
        YDSquareCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
        if (!cell) {
            cell = [[YDSquareCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identify];
        }
        cell.delegate = self;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.ktvBaseInfo = self.listenToMeData.ktvListBaseInfo[indexPath.section];
        
        return cell;
        
    } else if(1 == _tagOfView){
        
        
        YDTopListCell *cell = [YDTopListCell cellWithTableView:tableView];
        cell.delegate = self;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        MusicWorkBaseInfo *musicWorkBaseInfo = nil;
        
        if (indexPath.section == 0) {
            
            musicWorkBaseInfo = self.listenToMeData.arrPopularityList[indexPath.row];
        }else if(indexPath.section == 1){
            
            musicWorkBaseInfo = self.listenToMeData.arrNewMusicList[indexPath.row];
        }else if(indexPath.section == 2){
            
            musicWorkBaseInfo = self.listenToMeData.arrRecommendList[indexPath.row];
        }else{
            
            cell.lbMusicName.text = @"泡沫";
            cell.lbSinger.text = @"邓紫棋";
            cell.lbTime.text = @"04:58";
            [cell.btnListener setTitle:@"12356" forState:UIControlStateNormal];
            [cell.btnFlower setTitle:@"114" forState:UIControlStateNormal];
            return cell;
        }
        UserBaseInfoNet *userBaseInfoNet = musicWorkBaseInfo.stUserBaseInfoNet;
        SongInfo *songInfo = musicWorkBaseInfo.stSongInfo;
        cell.lbMusicName.text = songInfo.sSongName;
        cell.lbSinger.text = songInfo.sSongSinger;
        [cell.imgAvatar sd_setImageWithURL:[NSURL URLWithString:userBaseInfoNet.sCovver]];
        
        UIButton *btnAvatar = [[UIButton alloc]initWithFrame:cell.imgAvatar.frame];
        btnAvatar.backgroundColor = [UIColor clearColor];
        if(indexPath.section == 0){
            btnAvatar.tag = 100 + indexPath.row;
        }
        
        if (indexPath.section == 1) {
            btnAvatar.tag = 200 + indexPath.row;
        }
        
        if (indexPath.section == 2) {
            btnAvatar.tag = 300 + indexPath.row;
        }
        
        [btnAvatar addTarget:self action:@selector(clickAvatar:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:btnAvatar];
        
        [cell.btnFlower setTitle:[NSString stringWithFormat:@"%d",musicWorkBaseInfo.stUserBaseInfoNet.iFlowerNum] forState:UIControlStateNormal];
        [cell.btnListener setTitle:[NSString stringWithFormat:@"%d",musicWorkBaseInfo.iListenNum] forState:UIControlStateNormal];
        
        cell.btnCollection.tag = 400 + indexPath.section * 10 + indexPath.row;
        cell.btnCollection.selected = NO;
        [cell.btnCollection addTarget:self action:@selector(clickCollectMusicWorkHandle:) forControlEvents:UIControlEventTouchUpInside];
        
#warning 判断获取的音乐作品是否已经被收藏了,如果已经被收藏了就改变收藏按钮的状态否则保持在未收藏的状态.
        if (self.listenToMeData.arrCollectWorkInfo != nil) {
            
            for (CollectWorkInfo *collectWrokInfo  in self.listenToMeData.arrCollectWorkInfo) {
                if (collectWrokInfo.stMusicWorkBaseInfo.lMusicWorkId == musicWorkBaseInfo.lMusicWorkId) {
                    cell.btnCollection.selected = YES;
                }
            }
        }
        
    
        return cell;
        
    }else if(2 == _tagOfView){
        YDKMemmoryCell *cell = [YDKMemmoryCell cellWithTableView:tableView];
        
        
        CommemorateBaseInfo *commemorateBaseInfo = self.listenToMeData.arrCommemorateBaseInfo[indexPath.section];
        cell.arrUserBaseInfoNet = commemorateBaseInfo.stUserBaseInfoNet;
        
        if ([commemorateBaseInfo.sCommIcon isEqualToString:@""] || commemorateBaseInfo == nil) {
            
            cell.imgCover.image = [UIImage imageNamed:@"temp5.png"];
        }else{
            
            [cell.imgCover sd_setImageWithURL:[NSURL URLWithString:commemorateBaseInfo.sCommIcon]];
        }
        
        cell.lbTheme.text = @"闺蜜秀";
        
        if ([commemorateBaseInfo.sCommName isEqualToString:@""] || commemorateBaseInfo.sCommName == nil) {
            cell.lbPartyName.text = @"咸蛋超人的生日趴";
        }else{
            
            cell.lbPartyName.text = commemorateBaseInfo.sCommName;
        }
        
        cell.imgTheme.image = [UIImage imageNamed:@"memoryTopShow.png"];
        [cell.btnNumSongs setTitle:[NSString stringWithFormat:@"%d",commemorateBaseInfo.iMusicNum] forState:UIControlStateNormal];
        [cell.btnNumAlbums setTitle:[NSString stringWithFormat:@"%d",commemorateBaseInfo.iPictureNum] forState:UIControlStateNormal];
        [cell.btnNumSee setTitle:[NSString stringWithFormat:@"%d",commemorateBaseInfo.iWatchedNum] forState:UIControlStateNormal];
        [cell.btnNumFlower setTitle:[NSString stringWithFormat:@"%d",commemorateBaseInfo.iFlowerNum] forState:UIControlStateNormal];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        cell.btnActionCollect.tag = 400 + indexPath.section;
        cell.btnActionCollect.selected = NO;
        [cell.btnActionCollect addTarget:self action:@selector(clickCollectCommemorateHandle:) forControlEvents:UIControlEventTouchUpInside];
        
#warning 判断获取的音乐作品是否已经被收藏了,如果已经被收藏了就改变收藏按钮的状态否则保持在未收藏的状态.
        if (self.listenToMeData.arrCollectWorkInfo != nil) {
            
            for (CollectWorkInfo *collectWrokInfo  in self.listenToMeData.arrCollectWorkInfo) {
                if (collectWrokInfo.stCommemorateBaseInfo.lCommid == commemorateBaseInfo.lCommid) {
                    cell.btnActionCollect.selected = YES;
                }
            }
        }
        
        
        return cell;
        
    }else if(3 == _tagOfView){
        //该功能已经删除
        YDWantSingCell *cell = [YDWantSingCell cellWithTableView:tableView];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.lbMusicName.text = @"路边的野花你不要采";
        cell.lbSinger.text = @"邓丽君";
        [cell.btnHighestMask setTitle:@"最高分3856" forState:UIControlStateNormal];
        [cell.btnNumSinged setTitle:@"398人唱过" forState:UIControlStateNormal];
        cell.lbFileSize.text = @"10.22MB";
        
        return cell;

    }else{
    
        return nil;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (0 == _tagOfView) {
        return 280;
    }else if(1 == _tagOfView){
        return 155;
    } else if(2 == _tagOfView){
        return 273;
    }else {
        return 140;
    }
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (1 == _tagOfView) {
        //K星电台
        if (section == 0) {
            
            return self.listenToMeData.arrPopularityList.count;
        }else if (section == 1){
        
            return self.listenToMeData.arrNewMusicList.count;
        }else if(section == 2){
            return self.listenToMeData.arrRecommendList.count;
        }else{
            return 0;
        }
        
    }else{
        //KTV K歌回忆
        return 1;
    }
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    if (0 == _tagOfView) {
        return self.listenToMeData.ktvListBaseInfo.count;
    }
    if (1 == _tagOfView) {
        return 3;
    }
    if (2 == _tagOfView) {
        return self.listenToMeData.arrCommemorateBaseInfo.count;
    }
    
    return 5;
    //5
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (_tagOfView == 0) {
        if (section == 0) {
            return 0;
        }else{
            return 15;
        }
    }else
        if (_tagOfView == 1) {
            if (section == 0) {
                return 33;
            }else{
                return 33;
            }
        
    }else if(_tagOfView == 3 && section == 0){
    
        return 33;
    }else{
        if (section == 0) {
            return 0.1;
        }else {
            return 15;
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
    
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *viewHeaderBg = nil;
    UIView *viewHeader = nil;
    if (_tagOfView == 1) {
        CGFloat viewHeaderH = 33;
        viewHeaderBg = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, viewHeaderH)];
        viewHeaderBg.backgroundColor = [UIColor whiteColor];
        
        viewHeader = [[UIView alloc]init];
        viewHeader.frame =viewHeaderBg.bounds; //CGRectMake(0, 0, screenWidth, viewHeaderH);
        viewHeader.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:0.35];
        [viewHeaderBg addSubview:viewHeader];
        
        UIImageView *imgLine = [[UIImageView alloc]init];
        imgLine.frame = CGRectMake(0, 0, 3, viewHeaderH);
        imgLine.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
        [viewHeader addSubview:imgLine];
        
        UILabel *lbTitle = [[UILabel alloc]init];
        lbTitle.frame = CGRectMake(imgLine.x + 25, 0, 100, viewHeaderH);
        lbTitle.textColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
        lbTitle.font = [UIFont systemFontOfSize:14.0];
        lbTitle.textAlignment = NSTextAlignmentLeft;
        
        [viewHeader addSubview:lbTitle];
        
        UIButton *btn = [[UIButton alloc]init];
        CGFloat btnW = 45;
        btn.frame = CGRectMake(screenWidth - 15 - btnW, 0, btnW, viewHeaderH);
        btn.titleLabel.font = [UIFont systemFontOfSize:11.0];
        [btn setTitleColor:[UIColor rgbFromHexString:@"#9B9B9B" alpaa:1.0] forState:UIControlStateNormal];
        [btn setTitle:@"更多" forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:@"starDetail.png"] forState:UIControlStateNormal];
        [btn setImageEdgeInsets:UIEdgeInsetsMake(0,  btnW * 0.6, 0, 0)];
        [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, - btnW * 0.6, 0, 0)];
        [btn addTarget:self action:@selector(clickMoreMusic:) forControlEvents:UIControlEventTouchUpInside];
        [viewHeader addSubview:btn];
        if (section == 0) {
            [lbTitle setText:@"歌王爆炸秀"];
            btn.tag = 100;
        }else if(section == 1){
            [lbTitle setText:@"深情行星"];
            btn.tag = 101;
        }else if(section == 2){
            [lbTitle setText:@"时空胶囊"];
            btn.tag = 102;
        }
        return viewHeaderBg;
        
    }else if (_tagOfView == 3 && section == 0) {
        
        
        CGFloat viewHeaderH = 33;
        
        viewHeaderBg = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, viewHeaderH)];
        viewHeaderBg.backgroundColor = [UIColor whiteColor];
        
        viewHeader = [[UIView alloc]init];
        viewHeader.frame = viewHeaderBg.bounds; //CGRectMake(0, 0, screenWidth, viewHeaderH);
        viewHeader.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:0.35];
        
        [viewHeaderBg addSubview:viewHeader];
        
        UIImageView *imgLine = [[UIImageView alloc]init];
        imgLine.frame = CGRectMake(0, 0, 3, viewHeaderH);
        imgLine.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
        [viewHeader addSubview:imgLine];
        
        UILabel *lbTitle = [[UILabel alloc]init];
        lbTitle.frame = CGRectMake(imgLine.x + 25, 0, 100, viewHeaderH);
        lbTitle.textColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
        lbTitle.font = [UIFont systemFontOfSize:14.0];
        lbTitle.textAlignment = NSTextAlignmentLeft;
        
        [viewHeader addSubview:lbTitle];
        
        if (section == 0) {
            [lbTitle setText:@"推荐歌曲"];
        }

        return viewHeaderBg;
    }
    
    
    return nil;
    
    
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
   
    return nil;
}
////////////////////////////////// wantSing /////////////////////////////////////////////////////////////////////////////
-(void)setWantSingUI
{
    [self setUpWantSingTableView];
}

-(void)setUpWantSingTableView
{
    wantSingView.mTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, wantSingView.viewBtnsBg.height, screenWidth, wantSingView.height - wantSingView.viewBtnsBg.height) style:UITableViewStylePlain];
    [wantSingView addSubview:wantSingView.mTableView];
    
    wantSingView.mTableView.delegate = self;
    wantSingView.mTableView.dataSource = self;
    wantSingView.mTableView.showsVerticalScrollIndicator = NO;
    wantSingView.mTableView.backgroundColor = [UIColor clearColor];
}

////////////////////////////////// memory /////////////////////////////////////////////////////////////////////////////
-(void)setMemoryUI
{
    [self setUpMemoryTableView];
}

-(void)setUpMemoryTableView
{
    recallAlbumView.mtableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, recallAlbumView.height) style:UITableViewStylePlain];
    [recallAlbumView addSubview:recallAlbumView.mtableView];
    
    recallAlbumView.mtableView.delegate = self;
    recallAlbumView.mtableView.dataSource = self;
    recallAlbumView.mtableView.showsVerticalScrollIndicator = NO;
    recallAlbumView.mtableView.backgroundColor = [UIColor clearColor];
}

////////////////////////////////// star /////////////////////////////////////////////////////////////////////////////
-(void)setStarUI
{
    [self setUpStarTableview];
}

#pragma mark toplistTableview
-(void)setUpStarTableview
{
    topListView.mTableview = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, topListView.height) style:UITableViewStylePlain];//UITableViewStyleGrouped
    [topListView addSubview:topListView.mTableview];
    
    topListView.mTableview.delegate = self;
    topListView.mTableview.dataSource = self;
    topListView.mTableview.showsVerticalScrollIndicator = NO;
    topListView.mTableview.backgroundColor = [UIColor clearColor];
    topListView.mTableview.separatorStyle = UITableViewCellSeparatorStyleNone;
}


////////////////////////////////// KTV /////////////////////////////////////////////////////////////////////////////
#pragma mark - KTV_UI
-(void)setktvUI
{
    _tagOfView = 0;
    
//    [self setUpKtvScrollBar];
    
    [self setUpKtvTableView];
    
}
#pragma mark KTVtableview
-(void)setUpKtvTableView
{
    ktvView.mTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 100, screenWidth, ktvView.height - ktvView.scrollBanner.height -100) style:UITableViewStylePlain];//ktvView.scrollBanner.height
    [ktvView addSubview:ktvView.mTableView];
    ktvView.mTableView.delegate = self;
    ktvView.mTableView.dataSource = self;
    ktvView.mTableView.showsVerticalScrollIndicator = NO;
    ktvView.mTableView.backgroundColor = [UIColor clearColor];
}


#pragma mark - UI
#pragma mark -导航栏
-(void)customNavigationBar
{
    UIButton *customButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 100, 41)];
    customButton.backgroundColor = [UIColor clearColor];
    [customButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    customButton.titleLabel.font = [UIFont systemFontOfSize:16.5];
    customButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [customButton setTitle:@"K星广场" forState:UIControlStateNormal];
    self.navigationItem.titleView = customButton;
    
    [self.navigationItem setHidesBackButton:YES];
    UIImage *searchImg = [UIImage imageNamed:@"search.png"];
    UIButton *customBackBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, searchImg.size.width, searchImg.size.height)];
    [customBackBtn addTarget:self action:@selector(customBackAction) forControlEvents:UIControlEventTouchUpInside];
    [customBackBtn setImage:searchImg forState:UIControlStateNormal];
    UIBarButtonItem *customBackItem = [[UIBarButtonItem alloc]initWithCustomView:customBackBtn];
    self.navigationItem.leftBarButtonItem = customBackItem;
    
    UIImage *rightBtImg = [UIImage imageNamed:@"more.png"];
    UIButton *rightBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, rightBtImg.size.width,rightBtImg.size.height)];
    [rightBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -25)];
    [rightBtn setImage:rightBtImg forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(clickRightBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customRightBtnItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = customRightBtnItem;
}

-(void)customBackAction
{
    YDSquareSearchVC *squareSearchVC = [[YDSquareSearchVC alloc]init];
    [self.navigationController pushViewController:squareSearchVC animated:NO];
}

#pragma mark segment对应的的四个视图
-(void)setFourViews
{
    
    
    // KTV
    ktvView = [[YDKTVView alloc]initWithFrame:frameFourViews];
    [self.view addSubview:ktvView];
    
    
    // K星电台
    recallAlbumView = [[YDRecallAlbumView alloc]initWithFrame:frameFourViews];
    recallAlbumView.hidden = YES;

    [self.view addSubview:recallAlbumView];
    
    // K歌回忆
    topListView = [[YDTOPListView alloc]initWithFrame:frameFourViews];
    topListView.hidden = YES;
    

    
    [self.view addSubview:topListView];
    
    
    // 我要唱(暂时砍掉)
//    wantSingView = [[YDWantSingView alloc]initWithFrame:frameFourViews];
//    wantSingView.hidden = YES;
//    [self.view addSubview:wantSingView];
}

#pragma mark segment控件
-(void)setSegment
{
    // 初始化segmentedControl
    // segment的底部视图
    _segmentBgView = [[UIView alloc]initWithFrame:CGRectMake(0, naviAndStatusH, screenWidth, 40 + 2)]; // 2是底部紫色横线的高度
    _segmentBgView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_segmentBgView];

    // 初始化4个按钮
    CGFloat btnWidth = screenWidth / 3;//改为3个
    _btnKtv = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, btnWidth, _segmentBgView.height - 2)];
    _btnStar = [[UIButton alloc]initWithFrame:CGRectMake(btnWidth, 0, btnWidth, _segmentBgView.height - 2)];
    _btnMemory = [[UIButton alloc]initWithFrame:CGRectMake( 2 * btnWidth, 0, btnWidth, _segmentBgView.height - 2)];
//    _btnSing = [[UIButton alloc]initWithFrame:CGRectMake( 3 * btnWidth, 0, btnWidth, _segmentBgView.height - 2)];
    
    _btnKtv.selected = YES;
    
    _btnKtv.backgroundColor = [UIColor clearColor];
    _btnStar.backgroundColor = [UIColor clearColor];
    _btnMemory.backgroundColor = [UIColor clearColor];
//    _btnSing.backgroundColor = [UIColor clearColor];
    
    [_btnKtv addTarget:self action:@selector(clickBtnKtv) forControlEvents:UIControlEventTouchUpInside];
    [_btnStar addTarget:self action:@selector(clickBtnStar) forControlEvents:UIControlEventTouchUpInside];
    [_btnMemory addTarget:self action:@selector(clickBtnMemroy) forControlEvents:UIControlEventTouchUpInside];
//    [_btnSing addTarget:self action:@selector(clickBtnSing) forControlEvents:UIControlEventTouchUpInside];
    
    // 文字设置
    [_btnKtv setTitle:@"KTV" forState:UIControlStateNormal];
    [_btnStar setTitle:@"K星电台" forState:UIControlStateNormal];
    [_btnMemory setTitle:@"K歌回忆" forState:UIControlStateNormal];
//    [_btnSing setTitle:@"我要唱" forState:UIControlStateNormal];

    
    [self setBtnTitleColor];
    
    _btnKtv.titleLabel.font = [UIFont systemFontOfSize:14.0];
    _btnStar.titleLabel.font = [UIFont systemFontOfSize:14.0];
    _btnMemory.titleLabel.font = [UIFont systemFontOfSize:14.0];
//    _btnSing.titleLabel.font = [UIFont systemFontOfSize:14.0];
    
    [_segmentBgView addSubview:_btnKtv];
    [_segmentBgView addSubview:_btnStar];
    [_segmentBgView addSubview:_btnMemory];
//    [_segmentBgView addSubview:_btnSing];

    // 初始化横线，当选中了按钮时，改变其frame

    _imgSegLine = [[UIImageView alloc]initWithFrame:CGRectMake(0,_segmentBgView.height - 2,btnWidth, 2)]; // 2是紫色横线的高度
    _imgSegLine.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
    [_segmentBgView addSubview:_imgSegLine];
    
//    segmentedControl = [[UISegmentedControl alloc]init];
//    [segmentedControl insertSegmentWithTitle:@"KTV" atIndex:0 animated:NO];
//    [segmentedControl insertSegmentWithTitle:@"K星电台" atIndex:1 animated:NO];
//    [segmentedControl insertSegmentWithTitle:@"K歌回忆" atIndex:2 animated:NO];
//    [segmentedControl insertSegmentWithTitle:@"我要唱" atIndex:3 animated:NO];
//    segmentedControl.frame = CGRectMake(0,64,screenWidth,40);
//    [self.view addSubview:segmentedControl];
//    
//    segmentedControl.backgroundColor = [UIColor orangeColor];
//    segmentedControl.tintColor = [UIColor whiteColor];
//    
//    [segmentedControl addTarget:self action:@selector(segmentPressed:) forControlEvents:UIControlEventValueChanged];
    
    // 四个视图的frame
    frameFourViews = CGRectMake(0, naviAndStatusH + _segmentBgView.height, screenWidth, screenHeight - _segmentBgView.y - _segmentBgView.height - tabBarH);
}

#pragma mark -滚动条
-(void)setUpKtvScrollBar
{
    /////////////////////////////////////////////////////////////////////////////////////////////////////
    int times = 100;
    NSMutableArray *imgArray = [[NSMutableArray alloc]init];
    
    if (self.listenToMeData.ktvListBannerInfo == nil) {
        imgArray = [NSMutableArray arrayWithObjects:@"homeTemp01.png",@"homeTemp02.png",@"homeTemp03.jpg",@"homeTemp04.png",@"homeTemp05.jpg", nil]; //图片数组

    }else{
        
        
        for (int index =0 ; index < self.listenToMeData.ktvListBannerInfo.count; index ++) {
            NSString *sBannerUrl = [self.listenToMeData.ktvListBannerInfo[index] valueForKey:@"sBannerUrl"];
             imgArray = [NSMutableArray arrayWithObjects:sBannerUrl, nil];
        }
       
    }
    NSMutableArray *tempArray = [NSMutableArray arrayWithArray:imgArray];
    for (int i = 0; i < times; i ++) {
        for (int j = 0; j <[imgArray count]; j ++) {
            [tempArray addObject:[imgArray objectAtIndex:j]];
        }
    }
    
    CGRect scrollerFrame = CGRectMake(0, ktvView.searchKtv.height, screenWidth, 100);
    EScrollerView *scroller = [[EScrollerView alloc]initWithFrameRect:scrollerFrame ImageArray:tempArray TitleArray:nil];
    scroller.delegate = self;
    [ktvView addSubview:scroller];
    /////////////////////////////////////////////////////////////////////////////////////////////////////
 
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    
//    if (_tagOfView == 0) {
//        
//        if (scrollView.contentOffset.y > _oldOffset) {
//            [self.ktvView.mTableView reloadData];
//        }
//        _oldOffset = scrollView.contentOffset.y;
//    }
}

#pragma mark -工具类
-(void)setBtnTitleColor
{
    [_btnKtv setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
    [_btnKtv setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateSelected];
    [_btnStar setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
    [_btnStar setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateSelected];
    [_btnMemory setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
    [_btnMemory setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateSelected];
//    [_btnSing setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
//    [_btnSing setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateSelected];
}


#pragma mark -点击事件
#pragma mark 点击了更多
-(void)clickRightBarBtnItem
{
    YDLog(@"点击了导航栏右边的Btn");
    
    UIImage *popImg = [UIImage imageNamed:@"popMenuBg.png"];
    CGFloat popMenuW = popImg.size.width;
    CGFloat popMenuH = popImg.size.height;
    _tempView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, popMenuW, popMenuH)];
    _tempView.userInteractionEnabled = YES;
    
    // 第一个btn的 y
    CGFloat btnW = 145;
    CGFloat btnH = 39;
    CGFloat firstBtnY = 5.5;
    
    btnMsg = [self PopBtnWithImg:@"msg.png" title:@"消息" count:18 frame:CGRectMake(0,firstBtnY, btnW, btnH)];
    btnPlaying = [self PopBtnWithImg:@"playing.png" title:@"正在播放" count:5 frame:CGRectMake(0,firstBtnY + btnH, btnW, btnH)];
    btnRoom = [self PopBtnWithImg:@"yuegebaofang.png" title:@"包房" count:0 frame:CGRectMake(0, firstBtnY + 2 * btnH,btnW, btnH)];
    btnReport = [self PopBtnWithImg:@"report.png" title:@"举报" count:0 frame:CGRectMake(0, firstBtnY + 3 * btnH,btnW, btnH)];
    btnReport.imgLine.hidden = YES;
    
    [_tempView addSubview:btnMsg];
    [_tempView addSubview:btnPlaying];
    [_tempView addSubview:btnRoom];
    [_tempView addSubview:btnReport];
    
    [btnMsg.BtnPop addTarget:self action:@selector(clickMsg) forControlEvents:UIControlEventTouchUpInside];
    [btnPlaying.BtnPop addTarget:self action:@selector(clickPlaying) forControlEvents:UIControlEventTouchUpInside];
    [btnRoom.BtnPop addTarget:self action:@selector(clickRoom) forControlEvents:UIControlEventTouchUpInside];
    [btnReport.BtnPop addTarget:self action:@selector(clickReport) forControlEvents:UIControlEventTouchUpInside];
    
    HMPopMenu *menu = [HMPopMenu popMenuWithContentView:_tempView];
    CGFloat menuW = popMenuW;
    CGFloat menuH = popMenuH;
    CGFloat menuY = 55;
    CGFloat menuX = self.view.width - menuW - 20;
    menu.dimBackground = YES;
    menu.arrowPosition = HMPopMenuArrowPositionRight;
    [menu showInRect:CGRectMake(menuX, menuY, menuW, menuH)];
}


#pragma mark -弹出窗口的点击事件
-(void)clickMsg
{
    YDLog(@"clickMsg");
    [_tempView.superview.superview removeFromSuperview];
}

-(void)clickPlaying
{
    YDLog(@"clickPlaying");
    [_tempView.superview.superview removeFromSuperview];
}

-(void)clickRoom
{
    YDLog(@"clickRoom");
    [_tempView.superview.superview removeFromSuperview];
}

-(void)clickReport
{
    YDLog(@"clickReport");
    [_tempView.superview.superview removeFromSuperview];
}

#pragma mark setment的点击事件
/**
 * 监听segment的点击
 *
 *
 */
-(void)clickBtnKtv
{
    
    
    [[NetReqManager getInstance] sendGetKtvListInfo:0 INum:5];
    
    //加载动画
    [self setLoadDataGif];
    
    _tagOfView = 0;
    if (ktvView.hidden == YES) {
        ktvView.hidden = NO;
        _btnKtv.selected = YES;
        
        topListView.hidden = YES;
        recallAlbumView.hidden = YES;
        wantSingView.hidden = YES;
        
        _btnKtv.selected = YES;
        _btnStar.selected = NO;
        _btnMemory.selected = NO;
//        _btnSing.selected = NO;
        
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.35];
        _imgSegLine.frame = CGRectMake(0,_segmentBgView.height - 2,screenWidth / 3, 2);//改为3个按钮
        [UIView commitAnimations];
        
        // 字体 & 横线
        [self setBtnTitleColor];
        
       
    }
    
    [ktvView.mTableView reloadData];
}


-(void)clickBtnStar
{
    
    //    //请求歌王爆炸秀的数据
    [[NetReqManager getInstance] sendGetPopularityList:0 IOffset:0 INum:3];
    //请求新歌推荐 对应深情星空
    [[NetReqManager getInstance] sendGetNewMusicList:0 IOffset:0 INum:2];
    //请求每日金曲 对应时空胶囊
    [[NetReqManager getInstance] sendGetRecommendList:0 IOffset:0 INum:2];
    
    //加载动画
    
    [self setLoadDataGif];
    
#warning 获取用户的收藏列表 , 通过遍历收藏的列表来判读该作品是否被收藏
    if ([ListenToMeDBManager getUuid] > 0) {
        [[NetReqManager getInstance] sendGetCollectWorkList:[ListenToMeDBManager getUuid] IOffset:0 INum:20];
        
    }
    
    

    
    _tagOfView = 1;
    
    if (topListView.hidden == YES) {
        topListView.hidden = NO;
        
        ktvView.hidden = YES;
        recallAlbumView.hidden = YES;
        wantSingView.hidden = YES;
        
        _btnKtv.selected = NO;
        _btnStar.selected = YES;
        _btnMemory.selected = NO;
//        _btnSing.selected = NO;
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.35];
        _imgSegLine.frame = CGRectMake(_btnKtv.width,_segmentBgView.height - 2,screenWidth / 3, 2);//改为3个按钮
        [UIView commitAnimations];
        
        [self setBtnTitleColor];
    }
    
    [topListView.mTableview reloadData];
}

-(void)clickBtnMemroy
{
    //获取k歌回忆纪念册列表
    [[NetReqManager getInstance] sendGetCommemorateInfo:0 IOffset:0 INum:5];
    
    //加载动画
    [self setLoadDataGif];
    
    _tagOfView = 2;
    
    if (recallAlbumView.hidden == YES) {
        recallAlbumView.hidden = NO;
        
        topListView.hidden = YES;
        ktvView.hidden = YES;
        wantSingView.hidden = YES;
        
        _btnKtv.selected = NO;
        _btnStar.selected = NO;
        _btnMemory.selected = YES;
//        _btnSing.selected = NO;
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.35];
        _imgSegLine.frame = CGRectMake(2 * _btnKtv.width,_segmentBgView.height - 2,screenWidth / 3, 2);//改为3个按钮
        [UIView commitAnimations];
        
        [self setBtnTitleColor];
        
        [recallAlbumView.mtableView reloadData];
    }
}

//   //已经去掉该功能
//-(void)clickBtnSing
//{
//    _tagOfView = 3;
//    if (wantSingView.hidden == YES) {
//        wantSingView.hidden = NO;
//        
//        recallAlbumView.hidden = YES;
//        topListView.hidden = YES;
//        ktvView.hidden = YES;
//        
//        _btnKtv.selected = NO;
//        _btnStar.selected = NO;
//        _btnMemory.selected = NO;
//        _btnSing.selected = YES;
//        
//        [UIView beginAnimations:nil context:NULL];
//        [UIView setAnimationDuration:0.35];
//        _imgSegLine.frame = CGRectMake(3 * _btnKtv.width,_segmentBgView.height - 2,screenWidth * 0.25, 2);
//        [UIView commitAnimations];
//        
//        [self setBtnTitleColor];
//        
//        [wantSingView.mTableView reloadData];
//    }
//}

#pragma mark 点击更多歌曲
-(void)clickMoreMusic:(UIButton *)button
{
    
    DaysMusicVC *daysMusicVC = [[DaysMusicVC alloc]init];
    if (button.tag == 100) {
        daysMusicVC.navigationItem.title = @"歌王爆炸秀";
//        self.listenToMeData= nil;
        daysMusicVC.ktvStartType = KTVSTARTYPE_PopularityList;
    }else if (button.tag == 101){
        daysMusicVC.navigationItem.title = @"深情行星";
//        self.listenToMeData= nil;
        daysMusicVC.ktvStartType = KTVSTARTYPE_NewMusicList;
    }else if (button.tag == 102){
        daysMusicVC.navigationItem.title = @"时空胶囊";
//        self.listenToMeData= nil;
        daysMusicVC.ktvStartType = KTVSTARTYPE_RecommendList;
    }
    [self.navigationController pushViewController:daysMusicVC animated:YES];
    YDLog(@"点击更多歌曲");
}


#pragma mark -其他 & 工具类
#pragma mark 弹出窗口

-(YDPopBt *)PopBtnWithImg:(NSString *)img title:(NSString *)title count:(int)count frame:(CGRect)frame
{
    YDPopBt *btn = [[YDPopBt alloc]initWithFrame:frame];
    
    btn.imgPop.image = [UIImage imageNamed:img];
    btn.lbPop.text = title;
    
    [btn.btnBadge setTitle:[NSString stringWithFormat:@"%d",count] forState:UIControlStateNormal];
    if ([btn.btnBadge.titleLabel.text isEqual:@""] || [btn.btnBadge.titleLabel.text isEqual:@"0" ] ) {
        btn.btnBadge.hidden = YES;
    }
    
    return btn;
}


#pragma mark -YDTopLiseCellDelegate 点击分享提示分享
-(void)alertForShare{

    _shareVC = [[HWShareAlertVC alloc]init];
    [[[UIApplication sharedApplication]keyWindow]addSubview:_shareVC.view];
    

}

#pragma mark - 点击收藏如果处于非登录状态下弹出登录提示框
-(void)alertForLogin{

    self.loginAlertVC = [[LoginAlertVC alloc]init];
    [[[UIApplication sharedApplication]keyWindow] addSubview:self.loginAlertVC.view];
    [self.topListView.mTableview reloadData];
}

#pragma mark 点击头像
-(void)clickAvatar:(UIButton *)btnAvatar{
    MusicWorkBaseInfo *musicWorkBaseInfo = nil;
    if (btnAvatar.tag >= 100 && btnAvatar.tag < 200) {
         musicWorkBaseInfo = self.listenToMeData.arrPopularityList[btnAvatar.tag - 100];
        
    }
    if (btnAvatar.tag >= 200 && btnAvatar.tag < 300) {
        musicWorkBaseInfo = self.listenToMeData.arrNewMusicList[btnAvatar.tag - 200];
        
    }
    
    if (btnAvatar.tag >= 300) {
        musicWorkBaseInfo = self.listenToMeData.arrRecommendList[btnAvatar.tag - 300];
        
    }
    
    [self browsePerHmePage:musicWorkBaseInfo.stUserBaseInfoNet];
}

#pragma mark -YDSquareCellDelegate 点击头像进入个人/他人主页
-(void)browsePerHmePage:(UserBaseInfoNet *)userBaseInfoNet{
    HWPerHomePageVC *personHomePageVC = [[HWPerHomePageVC alloc]init];
    personHomePageVC.userBaseInfoNet = userBaseInfoNet;
    [self.navigationController pushViewController:personHomePageVC animated:YES];

}

#pragma mark - 点击收藏音乐作品按钮
-(void)clickCollectMusicWorkHandle:(UIButton *)btnCollection{
    NSInteger tempTag = btnCollection.tag - 400;
    YDLog(@"%ld",tempTag);
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"launchStaue"]) {
       
        
        MusicWorkBaseInfo *musicWorkBaseInfo = nil;
        if (tempTag < 10) {
            musicWorkBaseInfo = self.listenToMeData.arrPopularityList[tempTag];
           
        }
        if (10 <= tempTag && tempTag < 20) {
            musicWorkBaseInfo = self.listenToMeData.arrNewMusicList[tempTag % 10];
        }
        
        if (20 <= tempTag) {
            musicWorkBaseInfo = self.listenToMeData.arrRecommendList[tempTag % 20];
        }
        
#warning 判断收藏需要使用歌曲作品
        
        if (btnCollection.selected) {
            btnCollection.selected = NO;
            [[NetReqManager getInstance] sendUnCollectWork:[ListenToMeDBManager getUuid] LMusicId:musicWorkBaseInfo.lMusicWorkId LCommemorateId:0];
        }else{
            btnCollection.selected = YES;
            
            [[NetReqManager getInstance] sendCollectWork:[ListenToMeDBManager getUuid] LMusicId:musicWorkBaseInfo.lMusicWorkId LCommemorateId:0];
        }
        
#warning 获取用户的收藏列表 , 通过遍历收藏的列表来判读该作品是否被收藏
        if ([ListenToMeDBManager getUuid] > 0) {
            [[NetReqManager getInstance] sendGetCollectWorkList:[ListenToMeDBManager getUuid] IOffset:0 INum:20];
            
        }
        
    }else{
        [self alertForLogin];
    }
    
}

#pragma mark - 点击收藏纪念册
-(void)clickCollectCommemorateHandle:(UIButton *)btnCollection{
    NSInteger tempTag = btnCollection.tag - 400;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"launchStaue"]) {
        
        CommemorateBaseInfo *commemorateBaseInfo = self.listenToMeData.arrCommemorateBaseInfo[tempTag];
        if (btnCollection.selected) {
            btnCollection.selected = NO;
            [[NetReqManager getInstance] sendUnCollectWork:[ListenToMeDBManager getUuid] LMusicId:0 LCommemorateId:commemorateBaseInfo.lCommid];
        }else{
            btnCollection.selected = YES;
            [[NetReqManager getInstance] sendCollectWork:[ListenToMeDBManager getUuid] LMusicId:0 LCommemorateId:commemorateBaseInfo.lCommid];
        }
        
        
        if ([ListenToMeDBManager getUuid] > 0) {
            [[NetReqManager getInstance] sendGetCollectWorkList:[ListenToMeDBManager getUuid] IOffset:0 INum:20];
        }
        
    }else{
        [self alertForLogin];
    }
}

#pragma mark - 第三方登录
-(void)weiXinLogin:(UIButton *)weiXingBtn{
    
    //需要到微信开放平台进行注册,申请开发者帐号.
    UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToWechatSession];
    
    snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
        
        if (response.responseCode == UMSResponseCodeSuccess) {
            
            UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary]valueForKey:UMShareToWechatSession];
            
            YDLog(@"WeiXin username is %@, uid is %@, token is %@ url is %@",snsAccount.userName,snsAccount.usid,snsAccount.accessToken,snsAccount.iconURL);
            
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"launchStaue"];
            
            [self.loginAlertView removeFromSuperview];
//            [self.loginAlertVC.view removeFromSuperview];
            
        }
        
    });
    
    
    
}

-(void)qqLogin:(UIButton *)qqBtn{
    
    //暂时使用QQQzone登录
    UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToQzone];
    
    snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
        
        //          获取微博用户名、uid、token等
        
        if (response.responseCode == UMSResponseCodeSuccess) {
            
            UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary] valueForKey:UMShareToQzone];
            
            YDLog(@"QQ username is %@, uid is %@, token is %@ url is %@",snsAccount.userName,snsAccount.usid,snsAccount.accessToken,snsAccount.iconURL);
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"launchStaue"];
            
            
            [self.loginAlertView removeFromSuperview];
//            [self.loginAlertVC.view removeFromSuperview];
        }});
    
    
    
}
-(void)sinaLogin:(UIButton *)sinaBtn{
    
    UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToSina];
    
    snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
        
        //        NSDictionary *dic = response.data[@"sina"];
        //        [self setNamelabelAndIcon:dic];
        //        [self setLoginStatus];
        
        if (response.responseCode == UMSResponseCodeSuccess) {
            
            UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary] valueForKey:UMShareToSina];
            
            YDLog(@"Sina username is %@, uid is %@, token is %@ url is %@",snsAccount.userName,snsAccount.usid,snsAccount.accessToken,snsAccount.iconURL);
            
            //获取新浪好友限制30个
            //            [[UMSocialDataService defaultDataService] requestSnsFriends:UMShareToSina  completion:^(UMSocialResponseEntity *response){
            //                YDLog(@"SnsFriends is %@",response.data);
            //            }];
            
            
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"launchStaue"];
            
            
            
            [self.loginAlertView removeFromSuperview];
//            [self.loginAlertVC.view removeFromSuperview];
            
        }
        
    });
    
    
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 6.0) {
        //注意self.isViewLoadde是必不可少的,其他方式访问视图会导致它加载
        //判断是否是正在使用的视图
        if (self.isViewLoaded && !self.view.window) {
            //将self.view置为nil,目的是再次进入时能够重新调用viewDidLaod函数
            self.view = nil;
            
        }
        
       
    }
    // Dispose of any resources that can be recreated.
}

@end
