//
//  YDSquareCell.h
//  ListenToMe
//
//  Created by yadong on 3/4/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//
@protocol YDSquareCellDelegate <NSObject>

/**
 *  进入个人主页
 */
@optional
-(void)browsePerHmePage:(UserBaseInfoNet *)userBaseInfoNet;

@end


#import <UIKit/UIKit.h>

@class AVAudioPlayer;

@interface YDSquareCell : UITableViewCell
/**
 *  设置一个代理属性
 */
@property(assign,nonatomic)id<YDSquareCellDelegate>delegate;
/**
 * 店名
 */
@property(strong,nonatomic) UILabel *lbSellerName;
/**
 * 距离
 */
@property(strong,nonatomic) UILabel *lbDistance;
/**
 * 帅哥数
 */
@property(strong,nonatomic) UILabel *lbBoys;
/**
 * 美女数
 */
@property(strong,nonatomic) UILabel *lbGirls;

/**
 * 在场观众数组
 */
@property(strong,nonatomic) NSMutableArray *mArrayPresent;
/**
 * 不在场观众数组
 */
@property(strong,nonatomic) NSMutableArray *mArrayNotPresent;
/**
 * 第一首歌结构体
 */
@property(nonatomic,retain) KtvBaseInfo *mKtvBaseInfo;
/**
 * 第一名歌手头像背景
 */
@property(strong,nonatomic) UIButton *btnFSinger;
/**
 *  第一名歌手头像
 */
@property(nonatomic,strong) UIImageView *fSingerImgView;
/**
 * 第一名歌曲名
 */
@property(strong,nonatomic) UILabel *lbFSongName;
/**
 * 第一名听众数目
 */
@property(strong,nonatomic) UIButton *btnFListenNum;
/**
 * 第一名鲜花数
 */
@property(strong,nonatomic) UIButton *btnFFlower;
/**
 * 第二名歌手头像背景
 */
@property(strong,nonatomic) UIButton *btnSSinger;
/**
 *  第二名歌手头像
 */
@property(nonatomic,strong) UIImageView *sSingerImgView;
/**
 * 第二名歌曲名
 */
@property(strong,nonatomic) UILabel *lbSSongName;
/**
 * 第二名听众数目
 */
@property(strong,nonatomic) UIButton *btnSListenNum;
/**
 * 第二名鲜花数
 */
@property(strong,nonatomic) UIButton *btnSFlower;
/**
 *  第一个播放按钮
 */
@property(nonatomic,strong)UIButton *playBtnFirst;
/**
 *  第二个播放按钮
 */
@property(nonatomic,strong)UIButton *playBtnSec;
/**
 *  播放状态
 */
@property(nonatomic,assign)BOOL isPlay;
/**
 *  播放音乐
 */
@property(nonatomic,strong)AVAudioPlayer *audioPlayer;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

/**
 *  隐藏相应的Ui控件
 */
-(void)hiddenPlayButtonUI:(UIButton *)playButton;
/**
 *  显示相应的UI控件
 */
-(void)showPlayButtonUI:(UIButton *)playButton;
/**
 *  停止
 */
-(void)stop;

/**
 *  KTVListBaseInfo KTV列表中返回的信息
 */
@property(nonatomic,strong)NSDictionary *ktvBaseInfo;

@end
