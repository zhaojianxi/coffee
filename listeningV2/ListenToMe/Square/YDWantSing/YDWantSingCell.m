//
//  YDWantSingCell.m
//  ListenToMe
//
//  Created by yadong on 3/9/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#define cellHeight 140  // 80 + 4 + 56  单个cell的高度 40 ，分割线 1 ，中间高的cell高度 56

#import "YDWantSingCell.h"

@implementation YDWantSingCell

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"wangSingCell";
    YDWantSingCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[YDWantSingCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        CGFloat frontX = 15; // 子控件距离screen前后的距离
        CGFloat horizonLineH = 1; // 分割线的高度
        CGFloat fsCellH = 40; // 第一个和第三个cell的高度
        
        // 歌名 & 歌者 @ 播放btn
        // 横线
        UIImageView *imgLine_0 = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, horizonLineH)];
        imgLine_0.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
        [self.contentView addSubview:imgLine_0];
        // 歌名
        _lbMusicName = [[UILabel alloc]initWithFrame:CGRectMake(frontX, horizonLineH, 0.5 * screenWidth, fsCellH)];
        _lbMusicName.font = [UIFont systemFontOfSize:14.0];
        _lbMusicName.textColor = [UIColor blackColor];
        [self.contentView addSubview:_lbMusicName];
        // 歌者
        _lbSinger = [[UILabel alloc]initWithFrame:CGRectMake(_lbMusicName.x + _lbMusicName.width, _lbMusicName.y, 0.3 * screenWidth, _lbMusicName.height)];
        _lbSinger.font = [UIFont systemFontOfSize:11.0];
        _lbSinger.textColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
        [self.contentView addSubview:_lbSinger];
        // 文件大小
        CGFloat FileSizeW = 50;
        _lbFileSize = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - frontX - FileSizeW, _lbMusicName.y, FileSizeW, _lbMusicName.height)];
        _lbFileSize.font = [UIFont systemFontOfSize:11.0];
        _lbFileSize.textColor = [UIColor rgbFromHexString:@"#9B9B9B" alpaa:1.0];
        _lbFileSize.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_lbFileSize];
        // 横线
        UIImageView *imgLine_1 = [[UIImageView alloc]initWithFrame:CGRectMake(frontX, _lbMusicName.y + _lbMusicName.height, screenWidth - 2 * frontX, horizonLineH)];
        imgLine_1.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
        [self.contentView addSubview:imgLine_1];
        
        // 波浪 & 播放btn & 时间lb
        CGFloat secondCellHeight = 56;// 该行cell的高度
        UIImage *imgPlay = [UIImage imageNamed:@"play.png"];
        _btnPlay = [[UIButton alloc]initWithFrame:CGRectMake(frontX, imgLine_1.y + horizonLineH + (secondCellHeight - imgPlay.size.height) * 0.5, imgPlay.size.width, imgPlay.size.height)];
        [_btnPlay setImage:imgPlay forState:UIControlStateNormal];
        [self.contentView addSubview:_btnPlay];
        
        // 波浪
        CGFloat waveMarginX = 8; // 波浪与前后的控件的间距
        CGFloat waveW = 150; // 波浪的宽度
        _imgWave = [[UIImageView alloc]initWithFrame:CGRectMake(_imgAvatar.x + _imgAvatar.width + waveMarginX, imgLine_1.y + imgLine_1.height, waveW, secondCellHeight)];
        [self.contentView addSubview:_imgWave];
        // 播放
        UIImage *playNotSelectImg = [UIImage imageNamed:@"wantSingNotSelect.png"];
        CGFloat playBtnW = playNotSelectImg.size.width;
        _btnPlay = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - frontX - playBtnW , imgLine_1.y + imgLine_1.height, playBtnW, secondCellHeight)];
        [_btnPlay setImage:playNotSelectImg forState:UIControlStateNormal];
        [self.contentView addSubview:_btnPlay];
        // 横线
        UIImageView *imgLine_2 = [[UIImageView alloc]initWithFrame:CGRectMake(frontX, imgLine_1.y + imgLine_1.height + secondCellHeight, screenWidth - 2 * frontX, 1)];
        imgLine_2.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
        [self.contentView addSubview:imgLine_2];
        
        // 最高分 & 多少人唱过
       
        // 最高分
        CGFloat maskAndSingedW = 80;
        self.btnNumSinged = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - frontX - maskAndSingedW, imgLine_2.y + imgLine_2.height, maskAndSingedW, fsCellH)];
        [self.btnNumSinged setTitleColor:[UIColor rgbFromHexString:@"#D5AAFF" alpaa:1.0] forState:UIControlStateNormal];
        self.btnNumSinged.titleLabel.font = [UIFont systemFontOfSize:11.0];
        [self.contentView addSubview:self.btnNumSinged];
        
        // 多少人唱过
        CGFloat shareCollectionMargin = 30;
        self.btnHighestMask = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - frontX - maskAndSingedW - maskAndSingedW - shareCollectionMargin, imgLine_2.y + imgLine_2.height, maskAndSingedW, fsCellH)];
        [self.btnHighestMask setTitleColor:[UIColor rgbFromHexString:@"#D5AAFF" alpaa:1.0] forState:UIControlStateNormal];
        self.btnHighestMask.titleLabel.font = [UIFont systemFontOfSize:11.0];
        [self.contentView addSubview:self.btnHighestMask];
       
        // 分割线
        
    }
    
    
    return self;
}


@end
