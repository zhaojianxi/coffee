//
//  YDWantSingCell.h
//  ListenToMe
//
//  Created by yadong on 3/9/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YDWantSingCell : UITableViewCell
+ (instancetype)cellWithTableView:(UITableView *)tableView;
/**
 * 文件大小
 */
@property(strong,nonatomic) UILabel *lbFileSize;
/**
 * 歌名lb
 */
@property(strong,nonatomic) UILabel *lbMusicName;

/**
 * 歌者lb
 */
@property(strong,nonatomic) UILabel *lbSinger;

/**
 * 播放btn
 */
@property(strong,nonatomic) UIButton *btnPlay;

/**
 * 头像bt
 */
@property(strong,nonatomic) UIImageView *imgAvatar;


/**
 * 多少人唱过btn
 */
@property(strong,nonatomic) UIButton *btnNumSinged;

/**
 * 最高分btn
 */
@property(strong,nonatomic) UIButton *btnHighestMask;
/**
 * 波浪
 */
@property(strong,nonatomic) UIImageView *imgWave;

@end
