//
//  MoreNavigationMenView.m
//  ListenToMe
//
//  Created by zhw on 15/4/8.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "MoreNavigationMenView.h"
#import "YDPopBt.h"
#import "HMPopMenu.h"

@interface MoreNavigationMenView ()
/**
 *  导航栏更多菜单
 */
@property(nonatomic,strong)YDPopBt *btnMsg;
@property(nonatomic,strong)YDPopBt *btnPlaying;
@property(nonatomic,strong)YDPopBt *btnRoom;
@property(nonatomic,strong)YDPopBt *btnReport;
@end

@implementation MoreNavigationMenView
/**
 *  导航栏更多菜单
 */
@synthesize btnMsg;
@synthesize btnPlaying;
@synthesize btnRoom;
@synthesize btnReport;

-(instancetype)initWithFrame:(CGRect)frame{

    self = [super initWithFrame:frame];
    if (self) {
        [self setUI];
    }
    return self;
}


-(void)setUI{

    self.userInteractionEnabled = YES;
    
//    UIImage *popImg = [UIImage imageNamed:@"popMenuBg.png"];
//    CGFloat popMenuW = popImg.size.width;
//    CGFloat popMenuH = popImg.size.height;

    
    // 第一个btn的 y
    CGFloat btnW = 145;
    CGFloat btnH = 39;
    CGFloat firstBtnY = 5.5;
    
    btnMsg = [self PopBtnWithImg:@"msg.png" title:@"消息" count:18 frame:CGRectMake(0,firstBtnY, btnW, btnH)];
    btnPlaying = [self PopBtnWithImg:@"playing.png" title:@"正在播放" count:5 frame:CGRectMake(0,firstBtnY + btnH, btnW, btnH)];
    btnRoom = [self PopBtnWithImg:@"yuegebaofang.png" title:@"包房" count:0 frame:CGRectMake(0, firstBtnY + 2 * btnH,btnW, btnH)];
    btnReport = [self PopBtnWithImg:@"report.png" title:@"举报" count:0 frame:CGRectMake(0, firstBtnY + 3 * btnH,btnW, btnH)];
    btnReport.imgLine.hidden = YES;
    
    [self addSubview:btnMsg];
    [self addSubview:btnPlaying];
    [self addSubview:btnRoom];
    [self addSubview:btnReport];
    
    [btnMsg.BtnPop addTarget:self action:@selector(clickMsg:) forControlEvents:UIControlEventTouchUpInside];
    [btnPlaying.BtnPop addTarget:self action:@selector(clickPlaying:) forControlEvents:UIControlEventTouchUpInside];
    [btnRoom.BtnPop addTarget:self action:@selector(clickRoom:) forControlEvents:UIControlEventTouchUpInside];
    [btnReport.BtnPop addTarget:self action:@selector(clickReport:) forControlEvents:UIControlEventTouchUpInside];
    
//    HMPopMenu *menu = [HMPopMenu popMenuWithContentView:self];
//    CGFloat menuW = popMenuW;
//    CGFloat menuH = popMenuH;
//    CGFloat menuY = 55;
//    CGFloat menuX = self.width - menuW - 20;
//    menu.dimBackground = YES;
//    menu.arrowPosition = HMPopMenuArrowPositionRight;
//    [menu showInRect:CGRectMake(menuX, menuY, menuW, menuH)];
    
}

#pragma mark - 相关button的点击事件处理
-(void)clickMsg:(YDPopBt *)msgBtn{

    YDLog(@"点击了Message");
    [self.superview.superview removeFromSuperview];
   
}

-(void)clickPlaying:(YDPopBt *)playingBtn{

    YDLog(@"点击了Playing");
    [self.superview.superview removeFromSuperview];
}

-(void)clickRoom:(YDPopBt *)roomBtn{
    YDLog(@"点击了包房Room");
    [self.superview.superview removeFromSuperview];

}

-(void)clickReport:(YDPopBt *)reportBtn{
    YDLog(@"点击了举报Report");
    [self.superview.superview removeFromSuperview];

}

#pragma mark - button相关属性设置的快捷方法
-(YDPopBt *)PopBtnWithImg:(NSString *)img title:(NSString *)title count:(int)count frame:(CGRect)frame
{
    YDPopBt *btn = [[YDPopBt alloc]initWithFrame:frame];
    
    btn.imgPop.image = [UIImage imageNamed:img];
        btn.lbPop.text = title;
    
    [btn.btnBadge setTitle:[NSString stringWithFormat:@"%d",count] forState:UIControlStateNormal];
    if ([btn.btnBadge.titleLabel.text isEqual:@""] || [btn.btnBadge.titleLabel.text isEqual:@"0" ] ) {
        btn.btnBadge.hidden = YES;
    }
    
    return btn;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
