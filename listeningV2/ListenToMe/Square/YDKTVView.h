//
//  YDKTVView.h
//  ListenToMe
//
//  Created by yadong on 2/9/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>
@class EScrollerView;
@interface YDKTVView : UIView
@property(strong,nonatomic) UISearchBar *searchKtv;
@property(strong,nonatomic) UIPageControl *pageSquareKtv;
//@property(strong,nonatomic) UIScrollView *scrollBanner;
@property(strong,nonatomic) UITableView *mTableView;
@property(strong,nonatomic) UIScrollView *scrollBg;
@property(strong,nonatomic) EScrollerView *scrollBanner;
@property(nonatomic,strong) ListenToMeData *listenToMeData;
@end
