//
//  YDWantSingView.m
//  ListenToMe
//
//  Created by yadong on 2/9/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "YDWantSingView.h"

@implementation YDWantSingView

-(instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        
        
        
        // 三个按钮的背景视图
        CGFloat btnBgH = 75;
        _viewBtnsBg = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, btnBgH)];
        _viewBtnsBg.backgroundColor = [UIColor whiteColor];
        [self addSubview:_viewBtnsBg];
        
        // 1.已点伴奏btn 已点伴奏lb
        UIImage *singedAccompanyImg = [UIImage imageNamed:@"singedAccompany.png"];
        CGFloat lbH = 25; // lb的高度
        CGFloat btnLbMargin = 10; // btn & lb 的 y 上的距离
        CGFloat btnW = singedAccompanyImg.size.width + 15; // 按钮的宽度  加上15为了与对应的lb的宽度相同
        CGFloat btnH = singedAccompanyImg.size.height; // 按钮的高度
        
        _btnSingedAccompany = [[UIButton alloc]initWithFrame:CGRectMake(15, 0.5 * (btnBgH - btnH - lbH - btnLbMargin),btnW, btnH)];
        [_btnSingedAccompany setImage:singedAccompanyImg forState:UIControlStateNormal];
        [_viewBtnsBg addSubview:_btnSingedAccompany];
        
        _lbSingAccompany = [[UILabel alloc]initWithFrame:CGRectMake(_btnSingedAccompany.x, _btnSingedAccompany.y + _btnSingedAccompany.height + btnLbMargin, btnW, lbH)];
        _lbSingAccompany.font = [UIFont systemFontOfSize:11.0];
        _lbSingAccompany.textColor = [UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0];
        _lbSingAccompany.textAlignment = NSTextAlignmentCenter;
        _lbSingAccompany.text = @"已点伴奏";
        [_viewBtnsBg addSubview:_lbSingAccompany];
        
        // 2.本地伴奏
        UIImage *localAccompanyImg = [UIImage imageNamed:@"localAccompany.png"];

        
        _btnLocalAccompany = [[UIButton alloc]initWithFrame:CGRectMake(0.5 * (screenWidth - btnW), 0.5 * (btnBgH - btnH - lbH - btnLbMargin),btnW, btnH)];
        [_btnLocalAccompany setImage:localAccompanyImg forState:UIControlStateNormal];
        [_viewBtnsBg addSubview:_btnLocalAccompany];
        
        _lbSingAccompany = [[UILabel alloc]initWithFrame:CGRectMake(_btnLocalAccompany.x, _btnLocalAccompany.y + _btnLocalAccompany.height + btnLbMargin, btnW, lbH)];
        _lbSingAccompany.font = [UIFont systemFontOfSize:11.0];
        _lbSingAccompany.textColor = [UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0];
        _lbSingAccompany.textAlignment = NSTextAlignmentCenter;
        _lbSingAccompany.text = @"本地伴奏";
        [_viewBtnsBg addSubview:_lbSingAccompany];
        
        
        // 3.本地录音
        UIImage *localSingImg = [UIImage imageNamed:@"loaclRecord.png"];

        _btnLocalSing = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - 15 - btnW, 0.5 * (btnBgH - btnH - lbH - btnLbMargin),btnW, btnH)];
        [_btnLocalSing setImage:localSingImg forState:UIControlStateNormal];
        [_viewBtnsBg addSubview:_btnLocalSing];
        
        _lbLocalSing = [[UILabel alloc]initWithFrame:CGRectMake(_btnLocalSing.x, _btnSingedAccompany.y + _btnSingedAccompany.height + btnLbMargin, btnW, lbH)];
        _lbLocalSing.font = [UIFont systemFontOfSize:11.0];
        _lbLocalSing.textColor = [UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0];
        _lbLocalSing.textAlignment = NSTextAlignmentCenter;
        _lbLocalSing.text = @"本地录音";
        [_viewBtnsBg addSubview:_lbLocalSing];
    }
    
    return self;
}

#pragma mark layoutSubviews
-(void)layoutSubviews
{
    [super layoutSubviews];
}

@end
