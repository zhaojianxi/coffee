//
//  YDWantSingView.h
//  ListenToMe
//
//  Created by yadong on 2/9/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YDWantSingView : UIView
@property(strong,nonatomic) UITableView *mTableView;
/**
 * 三个按钮的背景视图
 */
@property(strong,nonatomic) UIView *viewBtnsBg;

/**
 * 已点伴奏btn
 */
@property(strong,nonatomic) UIButton *btnSingedAccompany;
/**
 * 已点伴奏lb
 */
@property(strong,nonatomic) UILabel *lbSingAccompany;
/**
 * 本地伴奏bt
 */
@property(strong,nonatomic) UIButton *btnLocalAccompany;
/**
 * 本地伴奏lb
 */
@property(strong,nonatomic) UILabel *lbLocalAccompany;
/**
 * 本地录音bt
 */
@property(strong,nonatomic) UIButton *btnLocalSing;
/**
 * 本地录音lb
 */
@property(strong,nonatomic) UILabel *lbLocalSing;
@end
