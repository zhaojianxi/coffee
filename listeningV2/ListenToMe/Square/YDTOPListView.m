//
//  YDTOPListView.m
//  ListenToMe
//
//  Created by yadong on 2/9/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "YDTOPListView.h"

@implementation YDTOPListView

-(instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}

#pragma mark layoutSubviews
-(void)layoutSubviews
{
    [super layoutSubviews];
}

@end
