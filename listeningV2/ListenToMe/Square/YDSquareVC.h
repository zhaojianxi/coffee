//
//  YDSquareVC.h
//  ListenToMe
//
//  Created by yadong on 1/26/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EScrollerView.h"
@interface YDSquareVC : YDBaseVC<EScrollerViewDelegate>
@property(nonatomic,strong) ListenToMeData *listenToMeData;
@end
