//
//  YDKMemmoryCell.m
//  ListenToMe
//
//  Created by yadong on 3/9/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "YDKMemmoryCell.h"

@interface YDKMemmoryCell ()<UIScrollViewDelegate>

@end

@implementation YDKMemmoryCell

+(instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"memoryCell";
    YDKMemmoryCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[YDKMemmoryCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        CGFloat lineH = 1; // 分割横线的高度
        CGFloat frontX = 15; // 控件距离screen左右的距离
        CGFloat imgCoverH = 150; // 封面高度

        // 聚会封面图片
        _imgCover = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, imgCoverH)];
        _imgCover.contentMode = UIViewContentModeScaleToFill;
        [self.contentView addSubview:_imgCover];
        
        // 聚会主题lb
        CGFloat lbThemeW = 50;
        _lbTheme = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - frontX - lbThemeW, 10, lbThemeW, 25)];
        _lbTheme.font = [UIFont systemFontOfSize:12.0];
        _lbTheme.textColor = [UIColor whiteColor];
        _lbTheme.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_lbTheme];
        
        // 聚会主题img
        CGFloat imgThemeW = 21;
        CGFloat imgThemeH = 22;
        _imgTheme = [[UIImageView alloc]initWithFrame:CGRectMake(_lbTheme.x - imgThemeW - 6, _lbTheme.y + 5, imgThemeW, imgThemeH)]; 
        [self.contentView addSubview:_imgTheme];
        
        // 白色横线
        UIImageView *whiteLineImg = [[UIImageView alloc]initWithFrame:CGRectMake(_lbTheme.x, _lbTheme.y + _lbTheme.height, _lbTheme.width, 1)];
        whiteLineImg.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:whiteLineImg];
        
        // 聚会名字
        CGFloat lbPartyNameH = 17;
        _lbPartyName = [[UILabel alloc]initWithFrame:CGRectMake(0, (imgCoverH - lbPartyNameH) * 0.5, screenWidth, lbPartyNameH)];
        _lbPartyName.font = [UIFont systemFontOfSize:15.0];
        _lbPartyName.textColor = [UIColor whiteColor];
        _lbPartyName.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_lbPartyName];
        
        // 播放按钮
        UIImage *playImg = [UIImage imageNamed:@"btnWhitePlay.png"];
        UIButton *playBtn = [[UIButton alloc]initWithFrame:CGRectMake((screenWidth - playImg.size.width) * 0.5, 100, playImg.size.width, playImg.size.height)];
        [playBtn setImage:playImg forState:UIControlStateNormal];
        
        //去除播放
//        [self.contentView addSubview:playBtn];
        
        // 歌曲名称
        CGFloat lbMusicNameH = 17.0;
        _lbMusicName = [[UILabel alloc]initWithFrame:CGRectMake(0, playBtn.y + playBtn.height + 5, screenWidth, lbMusicNameH)];
        _lbMusicName.font = [UIFont systemFontOfSize:12.0];
        _lbMusicName.textColor = [UIColor whiteColor];
        _lbMusicName.textAlignment = NSTextAlignmentCenter;
//        [self.contentView addSubview:_lbMusicName];
        
//        // 头像
        CGFloat avatarCellH = 50; //头像行的cell的高度
//        NSMutableArray *tempAvatar = [NSMutableArray arrayWithCapacity:8];
//        for (int index = 0; index < 8; index ++) {
//            CGFloat widthAndH = 32.5; // 头像的宽高
//            CGFloat margin = 2; // 头像间的间隔
//            CGFloat avatarX = index * (widthAndH + margin) + frontX;
//            UIImageView *avatarImg = [[UIImageView alloc]initWithFrame:CGRectMake(avatarX, 0.5 * (avatarCellH - widthAndH) + imgCoverH, widthAndH, widthAndH)];
//            avatarImg.contentMode = UIViewContentModeScaleToFill;
//            [self.contentView addSubview:avatarImg];
//            [tempAvatar addObject:avatarImg];
//            avatarImg.image = [UIImage imageNamed:@"temp3.png"];
//        }
        
        
        CGFloat widthAndH = 30; // 头像的宽高
        CGFloat avatarY = 0.5 * (avatarCellH - widthAndH) + imgCoverH;
        
        _joinPerScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(frontX, avatarY, screenWidth - frontX * 2 - widthAndH, widthAndH)];
//        _joinPerScrollView.pagingEnabled = YES;
        _joinPerScrollView.delegate = self;
        _joinPerScrollView.showsHorizontalScrollIndicator = NO;
        _joinPerScrollView.showsVerticalScrollIndicator = NO;
        [self.contentView addSubview:_joinPerScrollView];
        
   
    
//        // 更多按钮
        UIImage *btnPureMoreImg = [UIImage imageNamed:@"btnPuyreMore@2x.png"];
        _btnPureMore = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - frontX - btnPureMoreImg.size.width, 0.5 * (avatarCellH - btnPureMoreImg.size.height) + imgCoverH, btnPureMoreImg.size.width,btnPureMoreImg.size.height)];
        [_btnPureMore setImage:btnPureMoreImg forState:UIControlStateNormal];
        [_btnPureMore addTarget:self action:@selector(scrollHandle:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_btnPureMore];
        
        // 分割横线
        UIImageView *lineImg_0 = [[UIImageView alloc]initWithFrame:CGRectMake(frontX, _imgCover.height + avatarCellH, screenWidth - 2 * frontX, lineH)];
        lineImg_0.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
        [self.contentView addSubview:lineImg_0];
        
        // 多个数目btn
        CGFloat btnsCellH = 30; // 多个数目按钮cell的高度
        CGFloat btnsW = 57;
        
        UIImage *btnNumSongsImg = [UIImage imageNamed:@"btnMemmorySong.png"];
        _btnNumSongs = [[UIButton alloc]initWithFrame:CGRectMake(frontX, lineImg_0.y + 0.5 * (btnsCellH - btnNumSongsImg.size.height),btnsW , btnNumSongsImg.size.height)];
        [_btnNumSongs setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        [_btnNumSongs setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
        _btnNumSongs.titleLabel.font = [UIFont systemFontOfSize:12.0];
        [_btnNumSongs setImage:btnNumSongsImg forState:UIControlStateNormal];
        [self.contentView addSubview:_btnNumSongs];
        
        UIImage *btnNumAlbumsImg = [UIImage imageNamed:@"btnMemoryAlbum.png"];
        _btnNumAlbums = [[UIButton alloc]initWithFrame:CGRectMake(frontX + btnsW, lineImg_0.y + 0.5 * (btnsCellH - btnNumSongsImg.size.height),btnsW , btnNumSongsImg.size.height)];
        [_btnNumAlbums setImage:btnNumAlbumsImg forState:UIControlStateNormal];
        [_btnNumAlbums setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        [_btnNumAlbums setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
        _btnNumAlbums.titleLabel.font = [UIFont systemFontOfSize:12.0];
        [self.contentView addSubview:_btnNumAlbums];
        
        UIImage *btnNumSee = [UIImage imageNamed:@"btnMemorySee.png"];
        _btnNumSee = [[UIButton alloc]initWithFrame:CGRectMake(frontX + 2 * btnsW, lineImg_0.y + 0.5 * (btnsCellH - btnNumSongsImg.size.height),btnsW , btnNumSongsImg.size.height)];
        [_btnNumSee setImage:btnNumSee forState:UIControlStateNormal];
        [_btnNumSee setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        [_btnNumSee setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
        _btnNumSee.titleLabel.font = [UIFont systemFontOfSize:12.0];
        [self.contentView addSubview:_btnNumSee];

        UIImage *btnNumFlowerImg = [UIImage imageNamed:@"btnMemoryFlower.png"];
        _btnNumFlower = [[UIButton alloc]initWithFrame:CGRectMake(frontX + 3 * btnsW, lineImg_0.y + 0.5 * (btnsCellH - btnNumFlowerImg.size.height),btnsW , btnNumFlowerImg.size.height)];
        [_btnNumFlower setImage:btnNumFlowerImg forState:UIControlStateNormal];
        [_btnNumFlower setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        [_btnNumFlower setTitleColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0] forState:UIControlStateNormal];
        _btnNumFlower.titleLabel.font = [UIFont systemFontOfSize:12.0];
        [self.contentView addSubview:_btnNumFlower];
        
        // 分割横线
        UIImageView *lineImg_1 = [[UIImageView alloc]initWithFrame:CGRectMake(frontX, lineImg_0.y + lineImg_0.height + 30, screenWidth - 2 * frontX, lineH)];
        lineImg_1.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
        [self.contentView addSubview:lineImg_1];
        
        // 讨论 点赞 收藏等的cell
        CGFloat discussBtnCellH = 40;
        
        UIImage *btnDisImg = [UIImage imageNamed:@"btnMemoryDis.png"];
        _btnDiscuss = [[UIButton alloc]initWithFrame:CGRectMake(frontX, lineImg_1.y + lineImg_1.height + 0.5 * (discussBtnCellH - btnDisImg.size.height), btnDisImg.size.width, btnDisImg.size.height)];
        [_btnDiscuss setImage:btnDisImg forState:UIControlStateNormal];
        [self.contentView addSubview:_btnDiscuss];
        
        UIImage *btnActionFlowersImg = [UIImage imageNamed:@"btnMemoryActionFlower.png"];
        _btnActionFlowes = [[UIButton alloc]initWithFrame:CGRectMake(_btnDiscuss.x + _btnDiscuss.width + 20, _btnDiscuss.y, btnActionFlowersImg.size.width, btnActionFlowersImg.size.height)];
        [_btnActionFlowes setImage:btnActionFlowersImg forState:UIControlStateNormal];
        [self.contentView addSubview:_btnActionFlowes];
        
        UIImage *btnActionCollectImg = [UIImage imageNamed:@"btnMemoryNotCollect.png"];
        _btnActionCollect = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - frontX - btnActionCollectImg.size.width, _btnActionFlowes.y, btnActionCollectImg.size.width, btnActionCollectImg.size.height)];
        [_btnActionCollect setImage:btnActionCollectImg forState:UIControlStateNormal];
        [_btnActionCollect setImage:[UIImage imageNamed:@"squareStarNotCollection.png"] forState:UIControlStateSelected];
        [self.contentView addSubview:_btnActionCollect];
        
        UIImage *btnActionShareImg = [UIImage imageNamed:@"btnMemoryShare.png"];
        _btnActionShare = [[UIButton alloc]initWithFrame:CGRectMake(_btnActionCollect.x - screenWidth * 0.23 - btnActionCollectImg.size.width, _btnActionFlowes.y, btnActionShareImg.size.width, btnActionShareImg.size.height)];
        [_btnActionShare setImage:btnActionShareImg forState:UIControlStateNormal];
        [self.contentView addSubview:_btnActionShare];
        

        
        // 分割横线
        UIImageView *lineImg_2 = [[UIImageView alloc]initWithFrame:CGRectMake(0, lineImg_1.y + lineImg_1.height + 40, screenWidth , lineH)];
        lineImg_2.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
        [self.contentView addSubview:lineImg_2];
        
    }
    
    return self;
        
    }
#pragma mark 改变scrollView的偏移量,滑动到最右端
-(void)scrollHandle:(UIButton *)button{
    
    if (_joinPerScrollView.contentSize.width > _joinPerScrollView.width) {
        _joinPerScrollView.contentOffset = CGPointMake(_joinPerScrollView.contentSize.width - _joinPerScrollView.width,0);
    }
    
}

-(void)setArrUserBaseInfoNet:(NSArray *)arrUserBaseInfoNet{

    
    
    
    if (arrUserBaseInfoNet) {
        _arrUserBaseInfoNet = arrUserBaseInfoNet;
        
        CGFloat widthAndH = 30; // 头像的宽高
        CGFloat margin = 2; // 头像间的间隔
        CGFloat avatarY = 0; //0.5 * (avatarCellH - widthAndH) + imgCoverH;
        
#warning 解决纪念册显示时头像数据错乱的问题
        while ([_joinPerScrollView.subviews lastObject] != nil) {
            for (UIView *view in _joinPerScrollView.subviews) {
                [view removeFromSuperview];
            }
        }
        
        _joinPerScrollView.contentSize = CGSizeMake((widthAndH + margin) * _arrUserBaseInfoNet.count, widthAndH);


        for (int index = 0; index < _arrUserBaseInfoNet.count; index ++) {
            
            UserBaseInfoNet *userBaseInfoNet = _arrUserBaseInfoNet[index];
            
             CGFloat avatarX = index * (widthAndH + margin);
            UIImageView *avatarImgView = [[UIImageView alloc]initWithFrame:CGRectMake(avatarX ,avatarY , widthAndH, widthAndH)];
            avatarImgView.contentMode = UIViewContentModeScaleToFill;
            [avatarImgView sd_setImageWithURL:[NSURL URLWithString:userBaseInfoNet.sCovver]];
//            [self.contentView addSubview:avatarImgView];
            
//            UIButton *btnAvatar = [[UIButton alloc]initWithFrame:avatarImgView.frame];
//            btnAvatar.tag = 100 + index;
//            btnAvatar.backgroundColor = [UIColor clearColor];
//            
//            [self.contentView addSubview:btnAvatar];
            
            [_joinPerScrollView addSubview:avatarImgView];

        }
        

    }
    
}

@end
