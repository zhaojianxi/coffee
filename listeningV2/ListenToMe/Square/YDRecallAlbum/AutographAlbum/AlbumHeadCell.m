//
//  AlbumHeadCell.m
//  ListenToMe
//
//  Created by zhw on 15/4/11.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "AlbumHeadCell.h"


@interface AlbumHeadCell ()<UIScrollViewDelegate>

@end

@implementation AlbumHeadCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


+(instancetype)cellWithTableView:(UITableView *)tableView{
    
    NSString *identifier = @"albumHeadCell";
    AlbumHeadCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell) {
        cell = [[AlbumHeadCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        [self setUI];
    }
    
    return self;
}


#pragma mark - setUI 布局UI
-(void)setUI{

    CGFloat fromX = 15; //控件离左侧的宽度
    CGFloat cellHeight = 38; //cell中划分4个区域(74 + 1分割线)
    //聚会纪念册名字
    CGFloat lbHeight = 16;
    self.lbAlbumName = [[UILabel alloc]initWithFrame:CGRectMake(fromX, (cellHeight - lbHeight ) * 0.5, screenWidth - fromX * 2 - 89, 16)];
    [self.lbAlbumName setFont:[UIFont systemFontOfSize:14.0]];
    [self.lbAlbumName setTextColor:[UIColor rgbFromHexString:@"#000000" alpaa:1.0]];
    [self.lbAlbumName setTextAlignment:NSTextAlignmentLeft];
//    self.lbAlbumName.backgroundColor = [UIColor orangeColor];
    [self.contentView addSubview:self.lbAlbumName];
    
    //歌曲数
    UIButton *btnSongCount = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - fromX - 89, self.lbAlbumName.centerY - 15 * 0.5, 78 * 0.5, 15)];
    [self.contentView addSubview:btnSongCount];
    
    UIImage *songImg = [UIImage imageNamed:@"btnMemmorySong.png"];
    UIImageView *songIconView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, songImg.size.width , songImg.size.height)];
    [songIconView setImage:songImg];
    [btnSongCount addSubview:songIconView];
    
    self.lbSongNum = [[UILabel alloc]initWithFrame:CGRectMake(songIconView.x + songIconView.width, 0, btnSongCount.width - songIconView.width, btnSongCount.height)];
    [self.lbSongNum setFont:[UIFont systemFontOfSize:12.0]];
    [self.lbSongNum setTextColor:[UIColor rgbFromHexString:@"#9B9B9B" alpaa:1.0]];
    [self.lbSongNum setTextAlignment:NSTextAlignmentRight];
    [btnSongCount addSubview:self.lbSongNum];
    
    //纪念册图片数
    UIImage *photoImg = [UIImage imageNamed:@"btnMemoryAlbum.png"];
    UIButton *btnAlbumPhoto = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - fromX - 32 ,(cellHeight - photoImg.size.height) * 0.5 , 64 * 0.5, 15)];
    [self.contentView addSubview:btnAlbumPhoto];
    
   
    UIImageView *photoIconView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, photoImg.size.width , photoImg.size.height)];
    [photoIconView setImage:photoImg];
    [btnAlbumPhoto addSubview:photoIconView];
    
    self.lbAlbumPhotoNum = [[UILabel alloc]initWithFrame:CGRectMake(photoIconView.x + photoIconView.width, 0, btnAlbumPhoto.width - photoIconView.width, 15)];
    [self.lbAlbumPhotoNum setFont:[UIFont systemFontOfSize:12.0]];
    [self.lbAlbumPhotoNum setTextColor:[UIColor rgbFromHexString:@"#9B9B9B" alpaa:1.0]];
    [self.lbAlbumPhotoNum setTextAlignment:NSTextAlignmentRight];
    [btnAlbumPhoto addSubview:self.lbAlbumPhotoNum];
    
    //分割线
    CGFloat lineHeight = 1;
    UIImageView *lineView1 =[[UIImageView alloc]initWithFrame:CGRectMake(fromX, cellHeight + lineHeight, screenWidth - fromX * 2, lineHeight)];
    lineView1.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:0.12];
    [self.contentView addSubview:lineView1];
    
    //ktv店名
    CGFloat lbktvNameH = 27 * 0.5;
    CGFloat lbktvNameW = 69;
    self.lbKtvName = [[UILabel alloc]initWithFrame:CGRectMake(fromX, self.lbAlbumName.y + cellHeight, screenWidth - fromX * 2 - lbktvNameW,lbktvNameH)];
    [self.lbKtvName setTextColor:[UIColor rgbFromHexString:@"#9B9B9B" alpaa:1.0]];
    [self.lbKtvName setFont: [UIFont systemFontOfSize:12.0]];
    [self.lbKtvName setTextAlignment:NSTextAlignmentLeft];
    [self.contentView addSubview:self.lbKtvName];
    
    //纪念册创建时间
    CGFloat lbTimeW = 69;
    CGFloat lbTimeH = 12;
    self.lbCreatTime = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - fromX - lbTimeW, (cellHeight - lbTimeH) * 0.5 + lineView1.y , lbTimeW, lbTimeH)];
    [self.lbCreatTime setTextColor:[UIColor rgbFromHexString:@"#9B9B9B" alpaa:1.0]];
    [self.lbCreatTime setFont:[UIFont systemFontOfSize:10.0]];
    [self.lbCreatTime setTextAlignment:NSTextAlignmentRight];
    [self.contentView addSubview:self.lbCreatTime];
    
    //分割线
    UIImageView *lineView2 = [[UIImageView alloc]initWithFrame:CGRectMake(fromX, lineView1.y + cellHeight, screenWidth - fromX * 2, lineHeight)];
    lineView2.backgroundColor =[UIColor rgbFromHexString:@"#AC56FF" alpaa:0.12];
    [self.contentView addSubview:lineView2];
    
    //参与聚会的人
    CGFloat perIconHW = 30;
    CGFloat perIconY = lineView2.y + (cellHeight - perIconHW) * 0.5;
    
    UIScrollView *JoinPerScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(fromX, perIconY, screenWidth - fromX * 2 -perIconHW, perIconHW)];
//    JoinPerScrollView.pagingEnabled = YES;
    JoinPerScrollView.delegate = self;
    JoinPerScrollView.contentSize = CGSizeMake(perIconHW * 20 + 15, perIconHW);
    JoinPerScrollView.showsHorizontalScrollIndicator = NO;
    JoinPerScrollView.showsVerticalScrollIndicator = NO;
    
    UIImage *perIcon = [UIImage imageNamed:@"temp2.png"];
    self.arrJoinPer = [[NSMutableArray alloc] initWithCapacity:9];
    for (int index = 0; index < 20; index ++) {
        UIButton *btnJoinPer = [[UIButton alloc]initWithFrame:CGRectMake((perIconHW + 1) * index, 0, perIconHW, perIconHW)];
        [btnJoinPer setImage:perIcon forState:UIControlStateNormal];
        [JoinPerScrollView addSubview:btnJoinPer];
    }

    [self.contentView addSubview:JoinPerScrollView];
    
    //查看参加聚会的其他人
    UIImage *btnPureMoreImg = [UIImage imageNamed:@"btnPuyreMore@2x.png"];
    UIButton *btnPureMore = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - fromX - btnPureMoreImg.size.width,lineView2.y + (cellHeight - btnPureMoreImg.size.height) * 0.5, btnPureMoreImg.size.width, btnPureMoreImg.size.height)];
    [btnPureMore setImage:btnPureMoreImg forState:UIControlStateNormal];
    [self.contentView addSubview:btnPureMore];
    
    //分割线
    UIImageView *lineView3 = [[UIImageView alloc]initWithFrame:CGRectMake(fromX, lineView2.y + cellHeight, screenWidth - fromX * 2, lineHeight)];
    lineView3.backgroundColor =[UIColor rgbFromHexString:@"#AC56FF" alpaa:0.12];
    [self.contentView addSubview:lineView3];
    
    //聚会查看的次数 聊天 分享 鲜花
    
    CGFloat btnsW = 57;
    
    UIImage *NumSee = [UIImage imageNamed:@"查看数.png"];
    
    UIButton *watchBtn = [[UIButton alloc]initWithFrame:CGRectMake(fromX, lineView3.y + 0.5 * (cellHeight - NumSee.size.height * 0.5),btnsW , NumSee.size.height * 0.5)];
    UIImageView *watchView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, NumSee.size.width * 0.5, NumSee.size.height * 0.5)];
    [watchView setImage:NumSee];
    [watchBtn addSubview:watchView];
    
    self.lbAlbumWatchNum = [[UILabel alloc]initWithFrame:CGRectMake(watchView.width, 0, watchBtn.width - watchView.width, watchBtn.height)];
    [self.lbAlbumWatchNum setFont:[UIFont systemFontOfSize:12.0]];
    [self.lbAlbumWatchNum setTextColor:[UIColor rgbFromHexString:@"#9B9B9B" alpaa:1.0]];
    [watchBtn addSubview:self.lbAlbumWatchNum];
    
    [self.contentView addSubview:watchBtn];
    
    
    
    UIImage *chatImg = [UIImage imageNamed:@"留言数.png"];
    UIButton *chatBtn = [[UIButton alloc]initWithFrame:CGRectMake(fromX + btnsW, lineView3.y + 0.5 * (cellHeight - chatImg.size.height * 0.5),btnsW , chatImg.size.height * 0.5)];
    UIImageView *chatView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, chatImg.size.width * 0.5, chatImg.size.height * 0.5)];
    [chatView setImage:chatImg];
    [chatBtn addSubview:chatView];
    
    self.lbAlbumChatNum = [[UILabel alloc]initWithFrame:CGRectMake(chatView.width, 0, chatBtn.width - chatView.width, chatBtn.height)];
    [self.lbAlbumChatNum setFont:[UIFont systemFontOfSize:12.0]];
    [self.lbAlbumChatNum setTextColor:[UIColor rgbFromHexString:@"#9B9B9B" alpaa:1.0]];
    [chatBtn addSubview:self.lbAlbumChatNum];
    [self.contentView addSubview:chatBtn];

    UIImage *shareImg = [UIImage imageNamed:@"分享数.png"];
    UIButton *shareBtn = [[UIButton alloc]initWithFrame:CGRectMake(fromX + btnsW * 2, lineView3.y + 0.5 * (cellHeight - shareImg.size.height * 0.5),btnsW , shareImg.size.height * 0.5)];
    UIImageView *shareView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, shareImg.size.width * 0.5, shareImg.size.height * 0.5)];
    [shareView setImage:shareImg];
    [shareBtn addSubview:shareView];
    
    self.lbShareNum = [[UILabel alloc]initWithFrame:CGRectMake(shareView.width, 0, shareBtn.width - shareView.width, shareBtn.height)];
    [self.lbShareNum setFont:[UIFont systemFontOfSize:12.0]];
    [self.lbShareNum setTextColor:[UIColor rgbFromHexString:@"#9B9B9B" alpaa:1.0]];
    [shareBtn addSubview:self.lbShareNum];
    
    [self.contentView addSubview:shareBtn];
    
    UIImage *flowerImg = [UIImage imageNamed:@"btnMemoryFlower.png"];
    UIButton *flowerBtn = [[UIButton alloc]initWithFrame:CGRectMake(fromX + btnsW * 3, lineView3.y + 0.5 * (cellHeight - flowerImg.size.height),btnsW , flowerImg.size.height )];
    UIImageView *flowerView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, flowerImg.size.width , flowerImg.size.height )];
    [flowerView setImage:flowerImg];
    [flowerBtn addSubview:flowerView];
    
    self.lbFlowerNum = [[UILabel alloc]initWithFrame:CGRectMake(flowerView.width, 0, flowerBtn.width - flowerView.width, flowerBtn.height)];
    [self.lbFlowerNum setFont:[UIFont systemFontOfSize:12.0]];
    [self.lbFlowerNum setTextColor:[UIColor rgbFromHexString:@"#9B9B9B" alpaa:1.0]];
    [flowerBtn addSubview:self.lbFlowerNum];
    
    [self.contentView addSubview:flowerBtn];
    
    

}

@end
