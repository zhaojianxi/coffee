//
//  HWAlbumDetailVC.m
//  ListenToMe
//
//  Created by zhw on 15/4/10.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "HWAlbumDetailVC.h"
#import "AlbumHeadCell.h"
#import "YDTopListCell.h"
@interface HWAlbumDetailVC ()<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong)UITableView *tableView;
/**
 *  封面
 */
@property(nonatomic,strong)UIImageView *coverImgView;
/**
 *  主题的图标
 */
@property(nonatomic,strong)UIImageView *themeIconView;
/**
 *  主题类型
 */
@property(nonatomic,strong)UILabel *lbThemeKind;
/**
 *  封面上的纪念册中的相册按钮
 */
@property(nonatomic,strong)UIButton *btnPhotoAlbum;
/**
 *  纪念册的名字
 */
@property(nonatomic,strong)UILabel *lbAlbumName;
/**
 *  精彩回忆的高度
 */
@property(nonatomic,assign)CGFloat memoryHeight;
@end

@implementation HWAlbumDetailVC
@synthesize coverImgView;
@synthesize themeIconView;
@synthesize lbThemeKind;
@synthesize btnPhotoAlbum;
@synthesize lbAlbumName;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    _memoryHeight = 0;
    
    [self setCustomizedNav];
    
    [self setUI];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear: animated];
    
    [self.tableView reloadData];
}
#pragma mark - CustomizedNavigation
-(void)setCustomizedNav{
    
    self.navigationItem.title = @"详情";
    
    [self setLeftItem];
    
    [self setRightItem];
}

-(void)setLeftItem{

    
    UIButton *leftBtn = [[UIButton alloc]init];
    UIImage *leftBtnImg = [UIImage imageNamed:@"whiteBack.png"];
    leftBtn.frame = CGRectMake(0, 0, leftBtnImg.size.width, leftBtnImg.size.height);
    [leftBtn setImage:leftBtnImg forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(clickLeftItem:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customLeftBtnItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = customLeftBtnItem;
}

-(void)setRightItem{

    UIButton *rightBtn = [[UIButton alloc]init];
    UIImage *imgMore = [UIImage imageNamed:@"more.png"];
    rightBtn.frame = CGRectMake(0, 0, imgMore.size.width, imgMore.size.height);
    [rightBtn setImage:imgMore forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(clickRightItem:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customRightBtnItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = customRightBtnItem;
}

-(void)clickLeftItem:(UIButton *)leftItem{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)clickRightItem:(UIButton *)rightItem{
    
    YDLog(@"点击了导航栏右边的按钮");
}

#pragma mark - 设置UI
-(void)setUI{

//    CGFloat lineH = 1; // 分割横线的高度
    CGFloat frontX = 15; // 控件距离screen左右的距离
    CGFloat imgCoverH = 150; // 封面高度
    
    //设置封面
    coverImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, naviAndStatusH, screenWidth, imgCoverH)];
    [coverImgView setImage:[UIImage imageNamed:@"temp5.png"]];
    [self.view addSubview:coverImgView];
    
    // 聚会主题lb
    CGFloat lbThemeW = 50;
    lbThemeKind = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - frontX - lbThemeW,naviAndStatusH + 10, lbThemeW, 25)];
    lbThemeKind.font = [UIFont systemFontOfSize:12.0];
    lbThemeKind.textColor = [UIColor whiteColor];
    lbThemeKind.textAlignment = NSTextAlignmentRight;
    lbThemeKind.text = @"闺密秀";
    [self.view addSubview:lbThemeKind];
    
    // 聚会主题img
    CGFloat imgThemeW = 21;
    CGFloat imgThemeH = 22;
    themeIconView = [[UIImageView alloc]initWithFrame:CGRectMake(lbThemeKind.x - imgThemeW - 6, lbThemeKind.y, imgThemeW, imgThemeH)];
    [themeIconView setImage:[UIImage imageNamed:@"memoryTopShow.png"]];
    [self.view addSubview:themeIconView];
    
    // 白色横线
    UIImageView *whiteLineImg = [[UIImageView alloc]initWithFrame:CGRectMake(lbThemeKind.x, lbThemeKind.y + lbThemeKind.height, lbThemeKind.width, 1)];
    whiteLineImg.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:whiteLineImg];
    
    // 查看相册按钮
    UIImage *playImg = [UIImage imageNamed:@"照片数.png"];
    btnPhotoAlbum = [[UIButton alloc]initWithFrame:CGRectMake((screenWidth - playImg.size.width) * 0.5, coverImgView.y + coverImgView.height - 10 - playImg.size.width , playImg.size.width * 0.5, playImg.size.height * 0.5)];
    [btnPhotoAlbum setImage:playImg forState:UIControlStateNormal];
    [self.view addSubview:btnPhotoAlbum];
    
    
    //设置tableview
    [self setTableviewUI];

}

#pragma mark - setTableViewUI
-(void)setTableviewUI{

    if (self.tableView == nil) {
        self.tableView = [[UITableView alloc]init];
    }
    
    self.tableView.frame =  CGRectMake(0, coverImgView.y + coverImgView.height, screenWidth, screenHeight - coverImgView.height - coverImgView.y);
//    self.tableView.backgroundColor = [UIColor orangeColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.tableView registerClass:[AlbumHeadCell class] forCellReuseIdentifier:@"albumHeadCell"];
    
}


#pragma mark - UITableViewDataSourceDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    //每个分区返回cell的个数
    return 1;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    //返回7个主题区域
    return 8;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSString *identifier = nil;
    
    switch (indexPath.section) {
        case 0:{
            identifier = @"albumHeadCell";
        }
            break;
        case 1:{
            identifier = @"";
        }
//            break;
//        case 2:{
//            identifier = @"";
//        }
//            break;
//        case 3:{
//            identifier = @"";
//        }
//            break;
//        case 4:{
//            identifier = @"";
//        }
//            break;
            
        default:{
            identifier = @"cell";
        }
            
            break;
    }

    //利用多态
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:identifier owner:self options:nil] lastObject];
    }

    switch (indexPath.section) {
        case 0:
        {
            AlbumHeadCell *albumHeadCell = (AlbumHeadCell *)cell;
            albumHeadCell.lbAlbumName.text = @"咸蛋超人的生日趴";
            albumHeadCell.lbSongNum.text = @"128";
            albumHeadCell.lbAlbumPhotoNum.text = @"24";
            albumHeadCell.lbKtvName.text = @"同一首歌苏州桥店";
            albumHeadCell.lbCreatTime.text = @"2015-02-24";
            albumHeadCell.lbAlbumWatchNum.text = @"20";
            albumHeadCell.lbAlbumChatNum.text = @"99";
            albumHeadCell.lbShareNum.text = @"18";
            albumHeadCell.lbFlowerNum.text = @"114";
            
            albumHeadCell.selectionStyle = UITableViewCellSelectionStyleNone;
            return albumHeadCell;
        }
            break;
        case 1:{
      
            YDTopListCell *topCell = [YDTopListCell cellWithTableView:tableView];
            topCell.selectionStyle = UITableViewCellSelectionStyleNone;
            topCell.lbMusicName.text = @"泡沫";
            topCell.lbSinger.text = @"邓紫棋";
            topCell.lbTime.text = @"04:58";
            [topCell.btnListener setTitle:@"12356" forState:UIControlStateNormal];
            [topCell.btnFlower setTitle:@"114" forState:UIControlStateNormal];
            topCell.btnShare.hidden = YES;
            topCell.btnCollection.hidden = YES;
            UIButton *wonderfulMemory = [[UIButton alloc] initWithFrame:CGRectMake(screenWidth - 15 - 85,(topCell.btnListener.centerY - 15 * 0.5), 85, 15)];
            wonderfulMemory.backgroundColor = [UIColor lightGrayColor];
//            [wonderfulMemory addTarget:self action:@selector(openMemory:) forControlEvents:UIControlEventTouchUpInside];
            
            [topCell.contentView addSubview:wonderfulMemory];
            return topCell;
           
        }
            break;
            
        case 2:{
            YDTopListCell *topCell = [YDTopListCell cellWithTableView:tableView];
            topCell.selectionStyle = UITableViewCellSelectionStyleNone;
            topCell.lbMusicName.text = @"泡沫";
            topCell.lbSinger.text = @"邓紫棋";
            topCell.lbTime.text = @"04:58";
            [topCell.btnListener setTitle:@"12356" forState:UIControlStateNormal];
            [topCell.btnFlower setTitle:@"114" forState:UIControlStateNormal];
            
            topCell.btnShare.hidden = YES;
            topCell.btnCollection.hidden = YES;
            
            return topCell;
        }
            break;
        case 4:{
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            UIImage *photoImg = [UIImage imageNamed:@"album.png"];
            UIImageView *photoView = [[UIImageView alloc]initWithImage:photoImg];
            photoView.frame =CGRectMake(0, 0, screenWidth, 320);
            [cell.contentView addSubview:photoView];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
            break;
            
        case 5:{
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            UIImage *photoImg = [UIImage imageNamed:@"album.png"];
            UIImageView *photoView1 = [[UIImageView alloc]initWithImage:photoImg];
            photoView1.frame =CGRectMake(0, 0, screenWidth * 0.5, 160);
            [cell.contentView addSubview:photoView1];
            
            UIImageView *photoView2 = [[UIImageView alloc]initWithImage:photoImg];
            photoView2.frame =CGRectMake(screenWidth * 0.5, 0, screenWidth * 0.5, 160);
            [cell.contentView addSubview:photoView2];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
            break;
            
        case 6:{
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            cell.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:0.12];
            UILabel *lbContent = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, screenWidth - 30, 44)];
            [lbContent setText:@"大家唱的真high,烦心事都忘记了."];
            [lbContent setTextColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
            [lbContent setFont:[UIFont systemFontOfSize:14.0]];
            [cell.contentView addSubview:lbContent];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;
        }
            break;
            
        default:{
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            cell.textLabel.text = @"数据测试";
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
            break;
    }
    
    
    return cell;
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.section) {
        case 0:{
            return 150;
        }
            break;
        case 1:{
            return 140;
        }
            break;
        case 2:{
            return 140;
        }
            break;
        case 3:{
            return 130;
        }
            break;
        case 4:{
            return 320;
        }
            break;
        case 5:{
            return 160;
        }
            break;
        case 6:{
            return 44;
        }
            break;
        case 7:{
            return 144;
        }
            break;
            
        default:
            return 0;
            break;
    }

}
#pragma mark - 设置headr的Height
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    switch (section) {
        case 0:{
            return 0;
        }
            break;
        case 1:
        case 2:
        case 4:
        case 5:
        case 6:{
            return 44;
        }
            break;
        case 3:{
            return 32;
        }
            break;
            
        default:
            return 0;
            break;
    }
    
}

#pragma mark - 设置footer的Height
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    switch (section) {
        case 0:{
            return 44;
        }
            break;
        case 3:{
            return 32;
        }
            break;
            
        default:
            return 0;
            break;
    }
}

#pragma mark - 设置HeaderView
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    UIView *headerView = [[UIView alloc]init];
   
    if(section == 0){
        return nil;
    
    }else if(section == 3){
        
        return nil;
    }else{
    
        headerView.frame = CGRectMake(0, 0, screenWidth, 44);//tableView.tableHeaderView.height
        
        
        UIImageView *lineView = [[UIImageView alloc]initWithFrame:CGRectMake(15 + 15 - 1.5, 0, 3, headerView.height)];
        lineView.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:0.12];
        [headerView addSubview:lineView];
        
        
        
        UIImageView *titleView = [[UIImageView alloc]init];
        UIImage *iconImg = nil; //图标
        
        UILabel *lbName = [[UILabel alloc]init];
        NSString *name = nil; //分区的title的内容
        name = @"Lucy"; //当前纪念册主人的名字
        
        NSString *time = nil; //动态发布的时间
        
        switch (section) {
            case 1:{
//                titleContent = @"发表作品";
                iconImg = [UIImage imageNamed:@"作品.png"];
                time = @"11:40";
            }
                break;
            case 2:{
//                titleContent = @"发表作品";
                iconImg = [UIImage imageNamed:@"作品.png"];
                time = @"11:49";
            }
                break;
            case 4:{
//                titleContent = @"发表作品";
                iconImg = [UIImage imageNamed:@"照片.png"];
                time = @"12:49";
            }
                break;
            case 5:{
//                titleContent = @"发表作品";
                iconImg = [UIImage imageNamed:@"照片.png"];
                time = @"14:49";
            }
                break;
            case 6:{
//                titleContent = @"发表作品";
                iconImg = [UIImage imageNamed:@"感言.png"];
                time = @"15:20";
            }
                break;
                
            default:
                break;
        }
        
        
        //图标
        titleView.frame = CGRectMake(15, 0, iconImg.size.width * 0.5, iconImg.size.height * 0.5);
        [titleView setImage:iconImg];
        [headerView addSubview:titleView];
        
        //添加头像
        UIImage *perIcon = [UIImage imageNamed:@"temp3.png"];
        UIImageView *perIconView = [[UIImageView alloc]initWithFrame:CGRectMake(titleView.x + titleView.width + 5, titleView.centerY - 15, 30 , 30)];
        [perIconView setImage:perIcon];
        [headerView addSubview:perIconView];
        
        //title
        lbName.frame = CGRectMake(perIconView.x + perIconView.width + 5, titleView.centerY - 44 / 2, 200, 44);
        lbName.text = name;
        lbName.textColor = [UIColor rgbFromHexString:@"#000000" alpaa:0.7];
        [headerView addSubview:lbName];
        
        //时间
        UILabel *timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - 15 - 40, titleView.centerY - 13 * 0.5, 40, 13)];
        [timeLabel setFont:[UIFont systemFontOfSize:11.0]];
        [timeLabel setTextColor:[UIColor rgbFromHexString:@"#9B9B9B" alpaa:1.0]];
        timeLabel.text = time;
        [headerView addSubview:timeLabel];
        
        
        
    }
    
    
    headerView.backgroundColor = [UIColor colorWithRed:241/255.0 green:241/255.0 blue:241/255.0 alpha:1.0];
//    headerView.layer.shadowColor = (__bridge CGColorRef)([UIColor colorWithRed:241/255.0 green:241/255.0 blue:241/255.0 alpha:1.0]);
    
    return headerView;
}

#pragma mark - 设置FooterView
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    
    UIView *headerView = [[UIView alloc]init];
    
    if (section == 0) {
        
        
        headerView.frame = CGRectMake(0, 0, screenWidth, 44);//tableView.tableHeaderView.height
        
        
        UIImageView *lineView = [[UIImageView alloc]initWithFrame:CGRectMake(15 + 15 - 1.5, 0, 3, headerView.height)];
        lineView.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:0.12];
        [headerView addSubview:lineView];
        
        
        
        UIImageView *titleView = [[UIImageView alloc]init];
        UIImage *iconImg = nil; //图标
        
        UILabel *titleLabel = [[UILabel alloc]init];
        NSString *titleContent = nil; //分区的title的内容
        
        NSString *time = nil; //动态发布的时间
        
        titleContent = @"聚会开始";
        iconImg = [UIImage imageNamed:@"纪念册.png"];
        time = @"11:13";
        [titleLabel setTextColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
        
        //图标
        titleView.frame = CGRectMake(15, 0, iconImg.size.width * 0.5, iconImg.size.height * 0.5);
        [titleView setImage:iconImg];
        [headerView addSubview:titleView];
        
        //title
        titleLabel.frame = CGRectMake(titleView.x + titleView.width + 5, titleView.centerY - 27 / 4, 200, 27 / 2);
        titleLabel.text = titleContent;
        [headerView addSubview:titleLabel];
        
        //时间
        UILabel *timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - 15 - 40, titleLabel.y, 40, 13)];
        [timeLabel setFont:[UIFont systemFontOfSize:11.0]];
        [timeLabel setTextColor:[UIColor rgbFromHexString:@"#9B9B9B" alpaa:1.0]];
        timeLabel.text = time;
        [headerView addSubview:timeLabel];
        
        //横线
        UIImageView *tempLineView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 43, headerView.width, 1)];
        tempLineView.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
        [headerView addSubview:tempLineView];
        
        headerView.backgroundColor = [UIColor colorWithRed:241/255.0 green:241/255.0 blue:241/255.0 alpha:1.0];

        return headerView;
    }else if(section == 3){
    
        UIView *footView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 32)];
        footView.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
        
        return footView;
        
    }else{
    
        return nil;
    }
    
}


#pragma mark - openMemory精彩回忆
-(void)openMemory:(UIButton *)memoryBtn{

    _memoryHeight = 155;
    [self.tableView reloadData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
