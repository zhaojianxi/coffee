//
//  AlbumHeadCell.h
//  ListenToMe
//
//  Created by zhw on 15/4/11.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlbumHeadCell : UITableViewCell

+(instancetype)cellWithTableView:(UITableView *)tableView;
/**
 *  纪念册名字
 */
@property(nonatomic,strong)UILabel *lbAlbumName;
/**
 *  点歌数
 */
@property(nonatomic,strong)UILabel *lbSongNum;
/**
 *  纪念册图片数
 */
@property(nonatomic,strong)UILabel *lbAlbumPhotoNum;
/**
 *  创建纪念册的KTV名字
 */
@property(nonatomic,strong)UILabel *lbKtvName;
/**
 *  创建纪念册的时间
 */
@property(nonatomic,strong)UILabel *lbCreatTime;
/**
 *  参加party的人
 */
@property(nonatomic,strong)UIButton *btnJoinPerNum;
/**
 *  聚会查看数
 */
@property(nonatomic,strong)UILabel *lbAlbumWatchNum;
/**
 *  聚会中的聊天数
 */
@property(nonatomic,strong)UILabel *lbAlbumChatNum;
/**
 *  分享次数
 */
@property(nonatomic,strong)UILabel *lbShareNum;
/**
 *  送花数
 */
@property(nonatomic,strong)UILabel *lbFlowerNum;
/**
 *  参加聚会的人(预留用于存储聚会上人数)
 */
@property(nonatomic,strong)NSMutableArray *arrJoinPer;
@end
