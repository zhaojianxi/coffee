//
//  HWKTVChatRecordCell.m
//  ListenToMe
//
//  Created by zhw on 15/4/2.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "HWKTVChatRecordCell.h"

@implementation HWKTVChatRecordCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+(instancetype)cellWithTableView:(UITableView *)tableView{

    NSString *identifier = @"KTVChatRecordCell";
    HWKTVChatRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell) {
        cell = [[HWKTVChatRecordCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        [self setUI];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

#pragma mark - setUI

-(void)setUI{

   
    
    CGFloat btnH = 34; //布局 button的视图的高度
    CGFloat fromX = 15; //左边的距离
    
    //竖线
    CGFloat imgLineW = 2; //竖线的宽度
    UIImageView *imgLineView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, imgLineW, btnH)];
    imgLineView.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
    [self.contentView addSubview:imgLineView];
    
     //1 K友聊一聊
    
    self.btnChatRecord = [[UIButton alloc]initWithFrame:CGRectMake(fromX, 0, screenWidth, btnH)];
    
    UILabel *lbChat = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 60, btnH)];
    lbChat.text = @"K友聊一聊";
    [lbChat setTextColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
    [lbChat setFont:[UIFont systemFontOfSize:12.0]];
//    lbChat.backgroundColor = [UIColor grayColor];
    [self.btnChatRecord addSubview:lbChat];
    
    
    //j消息背景图
    UIImage *messageImg = [UIImage imageNamed:@"messageRemind.png"];
    UIImageView *messageCountView = [[UIImageView alloc]initWithFrame:CGRectMake(lbChat.x + lbChat.width, 5, messageImg.size.width , messageImg.size.height)];
    messageCountView.image = messageImg;
    
    //消息数量
    UILabel *lbMessageCount = [[UILabel alloc]initWithFrame:messageCountView.bounds];
    lbMessageCount.text = @"15";
    [lbMessageCount setFont:[UIFont systemFontOfSize:11.0]];
    [lbMessageCount setTextColor:[UIColor rgbFromHexString:@"#FFFFFF" alpaa:1.0]];
    [lbMessageCount setTextAlignment:NSTextAlignmentCenter]; 
    [messageCountView addSubview:lbMessageCount];
    
    [self.btnChatRecord addSubview:messageCountView];
    
    [self.contentView addSubview:self.btnChatRecord];
}

@end
