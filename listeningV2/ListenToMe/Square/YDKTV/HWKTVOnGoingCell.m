//
//  HWKTVOnGoingCell.m
//  ListenToMe
//
//  Created by zhw on 15/4/1.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "HWKTVOnGoingCell.h"

@interface HWKTVOnGoingCell ()
@property(strong,nonatomic)NSArray *boysIconArray;
@property(strong,nonatomic)NSArray *girlsIocnArray;
@property(strong,nonatomic)UserBaseInfoNet *userBaseInfoNet;
@property(strong,nonatomic)UIImageView *perIconView;
/**
 *  分割线1
 */
@property(nonatomic,strong)UIImageView *imgLine_1;
@end

@implementation HWKTVOnGoingCell
@synthesize imgLine_1;
- (void)awakeFromNib {
    // Initialization code
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
+(instancetype)cellWithTableView:(UITableView *)tableView{
    
    NSString *identifier = @"KTVOnGoingCell";
    HWKTVOnGoingCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell) {
        cell = [[HWKTVOnGoingCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
    }
    
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUI];
    }
    
    return self;
}


#pragma mark - 布局cell视图
-(void)setUI{

    // 添加子控件
    CGFloat frontX = 15; // cell前排子控件开始的x坐标 & 右侧控件距离screen右侧的距离
    CGFloat cellHeight = 51; // cell中每个cell的高度
    CGFloat lineHeight = 1; // 分割线的高度
    
    // 1.正在KTV & 帅哥美女数 & 小圆点
    
    //竖线
    CGFloat imgLineW = 2; //竖线的宽度
    UIImageView *imgLineView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, imgLineW, cellHeight)];
    imgLineView.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
    [self.contentView addSubview:imgLineView];
    
    
    CGFloat imgCircleW = 5; // 圆点的宽高
    UIImageView *imgCircle = [[UIImageView alloc]initWithFrame:CGRectMake(frontX, (cellHeight - imgCircleW) * 0.5, imgCircleW, imgCircleW)];
    imgCircle.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
    imgCircle.layer.cornerRadius = 2.5;
    imgCircle.layer.masksToBounds = YES;
    [self.contentView addSubview:imgCircle];
    
    
    UILabel *lbKtvNow = [[UILabel alloc]initWithFrame:CGRectMake(imgCircle.x + imgCircleW + 15, 0, screenWidth * 0.2, cellHeight)];
    lbKtvNow.font = [UIFont systemFontOfSize:12.0];
    [lbKtvNow setTextColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
    [lbKtvNow setTextAlignment:NSTextAlignmentLeft];
    lbKtvNow.text = @"正在KTV";
    [self.contentView addSubview:lbKtvNow];
    
    CGFloat lbGirlsW = 60; // 美女标签的宽
    _lbGirls = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - frontX - lbGirlsW, 0, lbGirlsW, cellHeight)];
    [_lbGirls setTextColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0]];
    _lbGirls.font = [UIFont systemFontOfSize:12.0];
    [_lbGirls setTextAlignment:NSTextAlignmentRight];
    _lbGirls.text = [NSString stringWithFormat:@"美女%d位",44];
//    [self.contentView addSubview:_lbGirls];
    
    
    _lbBoys = [[UILabel alloc]initWithFrame:CGRectMake(_lbGirls.x - _lbGirls.width, _lbGirls.y, _lbGirls.width, _lbGirls.height)];
    [_lbBoys setTextColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0]];
    _lbBoys.font = [UIFont systemFontOfSize:12.0];
    [_lbBoys setTextAlignment:NSTextAlignmentLeft];
    _lbBoys.text = [NSString stringWithFormat:@"帅哥%d位",4];
//    [self.contentView addSubview:_lbBoys];
    
    imgLine_1 = [[UIImageView alloc]initWithFrame:CGRectMake(frontX, _lbBoys.y + _lbBoys.height, screenWidth - 2 * frontX, lineHeight)];
    imgLine_1.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
    [self.contentView addSubview:imgLine_1];
    
    // 2.boys的头像
    
    
    UIImageView *imgLine_2 = [[UIImageView alloc]initWithFrame:CGRectMake(frontX, imgLine_1.height + imgLine_1.y + cellHeight, screenWidth - 2 * frontX, lineHeight)];
    imgLine_2.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
    [self.contentView addSubview:imgLine_2];
    
    // 3.girls的头像
    
    
    
    UIImageView *imgLine_3 = [[UIImageView alloc]initWithFrame:CGRectMake(frontX, imgLine_2.height + imgLine_2.y + cellHeight, screenWidth - 2 * frontX, lineHeight)];
    imgLine_3.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
    [self.contentView addSubview:imgLine_3];
    
    
    //换一批
    CGFloat replaceH = 29; //高度
    UIImage *replace = [UIImage imageNamed:@"换一批.png"];
    self.btnRepAnotherGroup = [[UIButton alloc]initWithFrame:CGRectMake(frontX, imgLine_3.y + imgLine_3.height + (cellHeight - replaceH) * 0.5, screenWidth - frontX * 2 , replaceH)];
    [self.btnRepAnotherGroup setBackgroundImage:replace forState:UIControlStateNormal];
    [self.btnRepAnotherGroup setTitle:@"换一批" forState:UIControlStateNormal];
    [self.btnRepAnotherGroup setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateNormal];
    
//    [self.btnRepAnotherGroup addTarget:self action:@selector(switchKtvUserList) forControlEvents:UIControlEventTouchUpInside];
    [self.btnRepAnotherGroup.titleLabel setFont:[UIFont systemFontOfSize:12.0]];
    [self.contentView addSubview:self.btnRepAnotherGroup];
    

}


#pragma mark - 点击头像进入个人主页
-(void)jumpToPerHomePage:(UIButton *)button{

    if (self.userBaseInfoNet != nil) {
        self.userBaseInfoNet = nil;
        self.userBaseInfoNet = self.arrStUserBaseInfoNet[button.tag - 1000];
        
    }
    
    //使用协议代理到control中实现个人主页的页面跳转
    
    if ([self.delegate respondsToSelector:@selector(browsePerHmePage:)]) {
//        [self.delegate performSelector:@selector(browsePerHmePage) withObject:self];
        [self.delegate browsePerHmePage:self.userBaseInfoNet];
    }
    
}

#pragma mark - KTV内用户
-(void)setArrStUserBaseInfoNet:(NSArray *)arrStUserBaseInfoNet{
    
    if (arrStUserBaseInfoNet != nil) {
        _arrStUserBaseInfoNet = arrStUserBaseInfoNet;
        
        if (self.mArrayPresent) {
            for (UIImageView *perIconView in _mArrayPresent) {
                [perIconView removeFromSuperview];
            }
        }
        
        
        
        self.mArrayPresent = [[NSMutableArray alloc]initWithCapacity:self.arrStUserBaseInfoNet.count];
        int count = 0;
        for (int index = 0; index < self.arrStUserBaseInfoNet.count; index ++) {
            self.userBaseInfoNet = self.arrStUserBaseInfoNet[index];
            CGFloat imgWh = 30; // 头像的宽高都是30
            CGFloat frontX = 15;
            CGFloat cellHeight = 51;
            
            if (index * (imgWh + 5) + frontX + imgWh <= screenWidth - frontX * 2) {
                self.perIconView= [[UIImageView alloc]initWithFrame:CGRectMake(index * (imgWh + 5) + frontX, imgLine_1.y +  imgLine_1.height + (cellHeight - imgWh) * 0.5, imgWh, imgWh)];
                count = index;
            }else{
                self.perIconView= [[UIImageView alloc]initWithFrame:CGRectMake((index - count - 1) * (imgWh + 5) + frontX, imgLine_1.y +  imgLine_1.height + (cellHeight - imgWh) * 0.5 + cellHeight, imgWh, imgWh)];
            }
            
            
            [self.perIconView sd_setImageWithURL:[NSURL URLWithString:self.userBaseInfoNet.sCovver]];
            self.perIconView.userInteractionEnabled = YES;
            [_mArrayPresent addObject:self.perIconView];
            
            UIButton *boyHeadPic = [[UIButton alloc]initWithFrame:self.perIconView.bounds];
            boyHeadPic.backgroundColor = [UIColor clearColor];
            //添加点击头像进入个人界面
            [boyHeadPic addTarget:self action:@selector(jumpToPerHomePage:) forControlEvents:UIControlEventTouchUpInside];
            boyHeadPic.tag = 1000 + index;
            [self.perIconView addSubview:boyHeadPic];
            
        }
        
        for (UIImageView *perIconView in _mArrayPresent) {
            [self.contentView addSubview:perIconView];
        }
        
    }
    
}



@end
