//
//  HWKTVHotSongCell.m
//  ListenToMe
//
//  Created by zhw on 15/4/2.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "HWKTVHotSongCell.h"
#import "AudioStreamer.h"

@interface HWKTVHotSongCell ()
/**
 *  在线音乐播放
 */
@property(nonatomic,strong) AudioStreamer *audioStreamer;
/**
 *  是否在播放
 */
@property(nonatomic,assign) BOOL isPlay;
/**
 *  记录上一次点击的button的tag
 */
@property(nonatomic,assign) NSInteger oldTag;
@end

@implementation HWKTVHotSongCell
@synthesize audioStreamer;
@synthesize isPlay;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+(instancetype)cellWithTableView:(UITableView *)tableView{
    
    NSString *identifier = @"KTVHotSongCell";
    HWKTVHotSongCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell) {
        cell = [[HWKTVHotSongCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];

    }
    
    return cell;

}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        [self setUI];
        self.selectionStyle = UITableViewCellSelectionStyleNone;

    }
    
    return self;
}


#pragma mark - setUI

-(void)setUI{


    // 添加子控件
    CGFloat frontX = 15; // cell前排子控件开始的x坐标 & 右侧控件距离screen右侧的距离
    CGFloat cellHeight = 39; // cell中每个cell的高度
    CGFloat lineHeight = 1; // 分割线的高度
    
    //竖线
    CGFloat imgLineW = 2; //竖线的宽度
    UIImageView *imgLineView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, imgLineW, cellHeight)];
    imgLineView.backgroundColor = [UIColor rgbFromHexString:@"#FF0053" alpaa:1.0];
    [self.contentView addSubview:imgLineView];
    
    
    // 1热歌榜
    
    UIImage *hotSongimg = [UIImage imageNamed:@"ktv热歌.png"];
    UIImageView *hotSongView = [[UIImageView alloc]initWithFrame:CGRectMake(frontX , 0, hotSongimg.size.width * 0.5, hotSongimg.size.height * 0.5)];
    hotSongView.image = hotSongimg;
    [self.contentView addSubview:hotSongView];
    
    UILabel *hotMusicLb = [[UILabel alloc]initWithFrame:CGRectMake(frontX * 3, 0, 60, cellHeight)];
    hotMusicLb.font = [UIFont systemFontOfSize:12.0];
    hotMusicLb.textColor = [UIColor rgbFromHexString:@"FF0053" alpaa:1.0];
    hotMusicLb.text = @"KTV热歌";
    hotMusicLb.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:hotMusicLb];
    
    UIImageView *imgLine_1 = [[UIImageView alloc]initWithFrame:CGRectMake(frontX , cellHeight, screenWidth - 2 * frontX, lineHeight)];
    imgLine_1.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
    [self.contentView addSubview:imgLine_1];
    
    //更多
    CGFloat moreLbW = 24; //跟多label的宽
    CGFloat moreBtnW = 10; //更多button的宽
    
    self.btnMore = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - frontX - moreBtnW - moreLbW, 0, moreBtnW + moreLbW, cellHeight)];
    
    UILabel *lbMore = [[UILabel alloc]initWithFrame:CGRectMake(0, (self.btnMore.height - 13) * 0.5, moreLbW, 13)];
    lbMore.text = @"更多";
    [lbMore setTextColor:[UIColor rgbFromHexString:@"#9B9B9B" alpaa:1.0]];
    [lbMore setFont:[UIFont systemFontOfSize:12.0]];
    [self.btnMore addSubview:lbMore];
    
    UIImage *moreImg = [UIImage imageNamed:@"查看更多热歌.png"];
    UIImageView *moreView = [[UIImageView alloc]initWithFrame:CGRectMake(moreLbW, (self.btnMore.height - moreImg.size.height *0.5 ) * 0.5, moreImg.size.width * 0.5, moreImg.size.height * 0.5)];
    moreView.image =moreImg;
    [self.btnMore addSubview:moreView];
    
    [self.contentView addSubview:self.btnMore];
    
    
    
    UIImage *playBtnImg = [UIImage imageNamed:@"play.png"];
    UIImage *stopBtnImg = [UIImage imageNamed:@"stop.png"];
    CGFloat avatarWh = 25;
    CGFloat fFlowerW = 65;
    CGFloat fFloweMargin = 25;
    CGFloat songNameWidth = 105;
    for (int index = 0; index < 6; index ++) {
        
        UIImageView *imgLine_1 = [[UIImageView alloc]initWithFrame:CGRectMake(frontX , cellHeight * (index + 1) - lineHeight, screenWidth - 2 * frontX, lineHeight)];
        imgLine_1.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
        [self.contentView addSubview:imgLine_1];
        
        CGFloat btnSingerX = frontX;
        CGFloat btnSingerY = imgLine_1.y + imgLine_1.height + (cellHeight - avatarWh) * 0.5;
        UIButton *singerBtn = [[UIButton alloc]initWithFrame:CGRectMake(btnSingerX, btnSingerY , avatarWh, avatarWh)];
        
        [singerBtn addTarget:self action:@selector(jumpToPerHomePage:) forControlEvents:UIControlEventTouchUpInside];
        
        singerBtn.tag = 100 + index;
        
        UIImageView *singerImgView = [[UIImageView alloc]initWithFrame:singerBtn.frame];
        singerImgView.userInteractionEnabled = YES;
        singerImgView.layer.masksToBounds = YES;
        singerImgView.layer.cornerRadius = avatarWh * 0.5;
        singerImgView.tag = 200 + index;
        [self.contentView addSubview:singerImgView];
        
        [self.contentView addSubview:singerBtn];
        
        // 播放按钮
        CGFloat btnPlayX = singerBtn.x + singerBtn.width + frontX;
        CGFloat btnPlayY = imgLine_1.y + imgLine_1.height + (cellHeight - playBtnImg.size.height * 0.5) * 0.5 ;
        CGFloat btnPlayW = playBtnImg.size.width * 0.5;
        CGFloat btnPlayH = playBtnImg.size.height * 0.5;
        UIButton *playBtn = [[UIButton alloc]initWithFrame:CGRectMake(btnPlayX,btnPlayY ,btnPlayW,btnPlayH)];
        
        [playBtn setImage:playBtnImg forState:UIControlStateNormal];
        [playBtn setImage:stopBtnImg forState:UIControlStateSelected];
        
        playBtn.tag = 300 + index;
        
        [playBtn addTarget:self action:@selector(playMusic:) forControlEvents:UIControlEventTouchUpInside];
       
        [self.contentView addSubview:playBtn];
        
        // 歌曲名
        
        CGFloat lbSongNameX = playBtn.x + playBtn.width + 10;
        CGFloat lbSongNameY = imgLine_1.y + imgLine_1.height;
        CGFloat lbSongNameW = songNameWidth;
        CGFloat lbSongNameH = cellHeight;
        
        UILabel *lbSongName = [[UILabel alloc]initWithFrame:CGRectMake(lbSongNameX, lbSongNameY , lbSongNameW, lbSongNameH)];
        lbSongName.font = [UIFont systemFontOfSize:12.0];
        lbSongName.textColor = [UIColor rgbFromHexString:@"#4A4A4A" alpaa:1.0];
        lbSongName.textAlignment = NSTextAlignmentLeft;
        
        lbSongName.tag = 400 + index;
        
        [self.contentView addSubview:lbSongName];
        
        // 鲜花数
        
        CGFloat btnFlowerX = screenWidth - frontX -fFlowerW;
        CGFloat btnFlowerY = imgLine_1.y + imgLine_1.height ;
        CGFloat btnFlowerW = fFlowerW;
        CGFloat btnFlowerH = cellHeight;
        UIButton *btnFlower = [[UIButton alloc]initWithFrame:CGRectMake(btnFlowerX,btnFlowerY ,btnFlowerW ,btnFlowerH)];
        [btnFlower setImage:[UIImage imageNamed:@"flowerSquareKtv.png"] forState:UIControlStateNormal];
        [btnFlower setTitle:@"114" forState:UIControlStateNormal];
        btnFlower.titleLabel.font = [UIFont systemFontOfSize:10.0];
        [btnFlower setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateNormal];
        btnFlower.titleLabel.textAlignment = NSTextAlignmentRight;
        [btnFlower setTitleEdgeInsets:UIEdgeInsetsMake(0, fFloweMargin, 0, 0)];
        
        btnFlower.tag = 500 + index;
        
        [self.contentView addSubview:btnFlower];
        
        
        // 听众数
        CGFloat btnListenNumX = btnFlower.x - fFlowerW;
        CGFloat btnListenNumY = imgLine_1.y + imgLine_1.height;
        CGFloat btnListenNumW = fFlowerW;
        CGFloat btnListenNumH = cellHeight;
        UIButton *btnListenNum = [[UIButton alloc]initWithFrame:CGRectMake(btnListenNumX, btnListenNumY, btnListenNumW,btnListenNumH )];
        [btnListenNum setImage:[UIImage imageNamed:@"listenersCount.png"] forState:UIControlStateNormal];
        [btnListenNum setTitle:@"12306" forState:UIControlStateNormal];
        btnListenNum.titleLabel.font = [UIFont systemFontOfSize:10.0];
        [btnListenNum setTitleColor:[UIColor rgbFromHexString:@"#FF0053" alpaa:1.0] forState:UIControlStateNormal];
        btnListenNum.titleLabel.textAlignment = NSTextAlignmentRight;
        [btnListenNum setTitleEdgeInsets:UIEdgeInsetsMake(0, fFloweMargin, 0, 0)];
        
        btnListenNum.tag = 600 + index;
        
        [self.contentView addSubview:btnListenNum];
    }

}

#pragma mark - 浏览他人主页
-(void)jumpToPerHomePage:(UIButton *)button{
    
    NSInteger index = button.tag - 100;
    
    MusicWorkBaseInfo *musicWorkBaseInfo = _arrStMyMusicWorkInfo[index];
    UserBaseInfoNet *userBaseInfoNet = musicWorkBaseInfo.stUserBaseInfoNet;
        
    //使用协议代理到control中实现个人主页的页面跳转
    
    if ([self.delegate respondsToSelector:@selector(viewOthersHomePage:)]) {
        //        [self.delegate performSelector:@selector(browsePerHmePage) withObject:self];
        [self.delegate viewOthersHomePage:userBaseInfoNet];
    }

    
    
}

#pragma mark - 音乐播放
-(void)playMusic:(UIButton *)button{

    for (int index = 0 ; index < _arrStMyMusicWorkInfo.count; index ++) {
        UIButton *playButton = (UIButton *)[self viewWithTag:index + 300];
        if (button.tag == index + 300) {
            
            
            if (!isPlay) {
                
                [self playWithTag:button.tag];
                playButton.selected = YES;
                isPlay = YES;
                _oldTag = button.tag;
                
            }else{
                
                if (_oldTag != button.tag) {
                    
                    if (audioStreamer) {
                        [self destroyStreamer];
                    }
                    [self playWithTag:button.tag];
                    isPlay = YES;
                    playButton.selected = YES;
                    _oldTag = button.tag;
                }else{
                    
                    if (audioStreamer) {
                        [self destroyStreamer];
                    }
                    playButton.selected = NO;
                    _oldTag = button.tag;
                    isPlay = NO;
                }
                
                
               
            }
            
        }else{
            playButton.selected = NO;
            
            
        }
    }
    
    
}


#pragma mark -播放
-(void)playWithTag:(NSInteger)tag{
    
    if (audioStreamer) {
        return;
    }
    
    
    [self destroyStreamer];
    
    
    NSURL *onLineMusicUrl = [NSURL URLWithString:((MusicWorkBaseInfo *)self.arrStMyMusicWorkInfo[tag - 300]).stSongInfo.sSongUrl];
    audioStreamer = [[AudioStreamer alloc]initWithURL:onLineMusicUrl];
    
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(playbackStateChanged:)
     name:ASStatusChangedNotification
     object:audioStreamer];
    
    [audioStreamer start];
    
    
}
#pragma mark 播放状态改变
- (void)playbackStateChanged:(NSNotification *)aNotification
{
    
    if ([audioStreamer isWaiting])
    {
        //正在等待播放
    }
    else if ([audioStreamer isPlaying])
    {
        //正在播放
    }
    else if ([audioStreamer isIdle])
    {
        
        [self destroyStreamer];
        for (int index = 0; index < self.arrStMyMusicWorkInfo.count; index ++) {
            UIButton *button =(UIButton *)[self viewWithTag:300 + index];
            button.selected = NO;
            
        }
        
        
    }
}
#pragma mark - 销毁播放器
- (void)destroyStreamer
{
    if (audioStreamer)
    {
        [audioStreamer pause];
        [audioStreamer stop];
        audioStreamer = nil;
    }
}





#pragma mark -暂停
-(void)pause{
    [audioStreamer pause];
    
}

#pragma mark -停止
-(void)stop{
    [audioStreamer pause];
    [audioStreamer stop];
    audioStreamer = nil;
}

#pragma mark - 数据处理
-(void)setArrStMyMusicWorkInfo:(NSArray *)arrStMyMusicWorkInfo{
    
    if (arrStMyMusicWorkInfo != nil) {
        _arrStMyMusicWorkInfo = arrStMyMusicWorkInfo;
        
        
        for (int index = 0; index < _arrStMyMusicWorkInfo.count; index ++) {
            MusicWorkBaseInfo *musicWorkBaseInfo = _arrStMyMusicWorkInfo[index];
            UserBaseInfoNet *userBaseInfoNet = musicWorkBaseInfo.stUserBaseInfoNet;
            SongInfo *songInfo = musicWorkBaseInfo.stSongInfo;
//            CommentInfo *commentInfo = musicWorkBaseInfo.stCommentInfo;
            
            
//            UIButton *singerBtn = (UIButton *)[self viewWithTag:100 + index];
            UIImageView *singerImgView = (UIImageView *)[self viewWithTag:200 + index];
//            UIButton *playBtn = (UIButton *)[self viewWithTag:300 + index];
            UILabel *lbSongName = (UILabel *)[self viewWithTag:400 + index];
            UIButton *btnFlower = (UIButton *)[self viewWithTag:500 + index];
            UIButton *btnListenNum = (UIButton *)[self viewWithTag:600 + index];
            
            [singerImgView sd_setImageWithURL:[NSURL URLWithString:userBaseInfoNet.sCovver]];
            lbSongName.text = songInfo.sSongName;
            [btnFlower setTitle:[NSString stringWithFormat:@"%d",musicWorkBaseInfo.iFlowerNum ] forState:UIControlStateNormal];
            [btnListenNum setTitle:[NSString stringWithFormat:@"%d",musicWorkBaseInfo.iListenNum] forState:UIControlStateNormal];
            
            
        }
        
      
        
    }
    
}
-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:ASStatusChangedNotification
     object:audioStreamer];
    [audioStreamer pause];
    [audioStreamer stop];
    audioStreamer = nil;
    
    
}
@end
