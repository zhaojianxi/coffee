//
//  HWKTVChatRecordCell.h
//  ListenToMe
//
//  Created by zhw on 15/4/2.
//  Copyright (c) 2015年 listentome. All rights reserved.
//  K友聊天记录

#import <UIKit/UIKit.h>

@interface HWKTVChatRecordCell : UITableViewCell
/**
 *  初始化
 */
+(instancetype)cellWithTableView:(UITableView *)tableView;
/**
 *  K友聊一聊
 */
@property(nonatomic,strong)UIButton *btnChatRecord;

@end
