//
//  HWKTVTrendsVC.m
//  ListenToMe
//
//  Created by zhw on 15/3/30.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "HWKTVTrendsVC.h"
#import "HWKTVInformationCell.h"
#import "HWKTVOnGoingCell.h"
#import "HWKTVHotSongCell.h"
#import "HWKTVChatRecordCell.h"
#import "YDRoomMsgCell.h"
#import "MoreNavigationMenView.h"
#import "HMPopMenu.h"
#import "HWPerHomePageVC.h"
#import "YDLaunchPartyVC.h"
@interface HWKTVTrendsVC ()<UITableViewDataSource,UITableViewDelegate,HWKTVOnGoingCellDelegate,HWKTVHotSongCellDelegate>
@property(strong,nonatomic)UITableView *mTableView;
/**
 *  跟多导航菜单
 */
@property(strong,nonatomic)MoreNavigationMenView *moreMenuView;

@property(nonatomic,strong) ListenToMeData *listenToMeData;
/**
 *  正在KTV中的用户
 */
@property(nonatomic,strong) NSArray *arrKtvUserList;
/**
 *  更换一批
 */
@property(nonatomic,assign) NSInteger countForKtvUserList;
/**
 *  KTV热歌
 */
@property(nonatomic,strong) NSArray *arrKtvHotSongList;
@end

@implementation HWKTVTrendsVC
@synthesize countForKtvUserList;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self customizedBarItem];
    
    [self setUI];

    self.listenToMeData = [ListenToMeData getInstance];
    
     [[NetReqManager getInstance] sendGetKtvInfoById:self.ktvBaseInfo.lKtvId];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshData) name:NOTIFY_GET_KTVINFO_PAGE_RESP object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshKtvUserList) name:NOTIFY_GET_SWITCHKTVUSERLIST_PAGE_RESP object:nil];
}

-(void)refreshData{
//    self.arrKtvUserList = [self.listenToMeData.ktvBaseInfo valueForKey:@"stUserBaseInfoNet"];
    self.arrKtvUserList = self.listenToMeData.ktvBaseInfo.stUserBaseInfoNet;
//    self.arrKtvHotSongList = [self.listenToMeData.ktvBaseInfo valueForKey:@"stMyMusicWorkInfo"];
    self.arrKtvHotSongList = self.listenToMeData.ktvBaseInfo.stMyMusicWorkInfo;
    [self.mTableView reloadData];
}
-(void)refreshKtvUserList{
    if (self.arrKtvUserList) {
        self.arrKtvUserList = nil;
    }
    self.arrKtvUserList = self.listenToMeData.arrUserBaseInfoNet;
    [self.mTableView reloadData];
}
-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_KTVINFO_PAGE_RESP object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_SWITCHKTVUSERLIST_PAGE_RESP object:nil];
}



-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    [self.mTableView reloadData];
}

#pragma mark -customNavigationBar
-(void)customizedBarItem{

    [self setLeftBtnItem];
    [self setRightBtnItem];
    

    [self loadData];
}
#pragma mark - 设置左边的BarItem
-(void)setLeftBtnItem{

    UIButton *customButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 100, 41)];
    customButton.backgroundColor = [UIColor clearColor];
    [customButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    customButton.titleLabel.font = [UIFont systemFontOfSize:16.5];
    customButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [customButton setTitle:@"店内动态" forState:UIControlStateNormal];
    self.navigationItem.titleView = customButton;
    
    UIButton *leftBtn = [[UIButton alloc]init];
    UIImage *leftBtnImg = [UIImage imageNamed:@"whiteBack.png"];
    leftBtn.frame = CGRectMake(0, 0, leftBtnImg.size.width, leftBtnImg.size.height);
    [leftBtn setImage:leftBtnImg forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(clickLeftBarBtnItem:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customLeftBtnItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = customLeftBtnItem;
}

#pragma mark - 设置右边的BarItem
-(void)setRightBtnItem{

    UIButton *rightBtn = [[UIButton alloc]init];
    UIImage *imgMore = [UIImage imageNamed:@"more.png"];
    rightBtn.frame = CGRectMake(0, 0, imgMore.size.width, imgMore.size.height);
    [rightBtn setImage:imgMore forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(clickRightBarBtnItem:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customRightBtnItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = customRightBtnItem;
}

#pragma mark - BarItem的点击事件处理
-(void)clickLeftBarBtnItem:(UIButton *)backBtn
{
    [self.navigationController popViewControllerAnimated:YES];
    YDLog(@"点击了导航栏左边的Btn");
}

-(void)clickRightBarBtnItem:(UIButton *)moreBtn{
   
    UIImage *popImg = [UIImage imageNamed:@"popMenuBg.png"];
    CGFloat popMenuW = popImg.size.width;
    CGFloat popMenuH = popImg.size.height;
    
    self.moreMenuView = [[MoreNavigationMenView alloc]initWithFrame:CGRectMake(0, 0, popMenuW, popMenuH)];
    HMPopMenu *menu = [HMPopMenu popMenuWithContentView:self.moreMenuView];
    CGFloat menuW = popMenuW;
    CGFloat menuH = popMenuH;
    CGFloat menuY = 55;
    CGFloat menuX = self.view.width - menuW - 20;
    menu.dimBackground = YES;
    menu.arrowPosition = HMPopMenuArrowPositionRight;
    [menu showInRect:CGRectMake(menuX, menuY, menuW, menuH)];

}

#pragma mark - setUI 设置UI
-(void)setUI{

    self.mTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight) style:UITableViewStylePlain];
    
    _mTableView.backgroundColor = [UIColor rgbFromHexString:@"#F1F1F1" alpaa:1.0];
    _mTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    _mTableView.dataSource = self;
    _mTableView.delegate = self;
    _mTableView.showsVerticalScrollIndicator = NO;
    _mTableView.bounces = NO;
    
    
    [_mTableView registerClass:[HWKTVInformationCell class] forCellReuseIdentifier:@"KTVInfoCell"];
    [_mTableView registerClass:[HWKTVOnGoingCell class] forCellReuseIdentifier:@"KTVOnGoingCell"];
    [_mTableView registerClass:[HWKTVHotSongCell class] forCellReuseIdentifier:@"KTVHotSongCell"];
    [_mTableView registerClass:[HWKTVChatRecordCell class] forCellReuseIdentifier:@"KTVChatRecordCell"];
//    [_mTableView registerClass:[YDRoomMsgCell class] forCellReuseIdentifier:@"RoomMsgCell"];
    
    [self.view addSubview:_mTableView];

}


#pragma mark - 页面数据的处理
-(void)loadData{

}

#pragma mark - UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return 1;
    
//    if (4 == section) {
//        return 15;
//    }else{
//        return 1;
//    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    
    NSString *identifier = nil;
    
    switch (indexPath.section) {
        case 0:{
            identifier = @"KTVInfoCell";
        }
            break;
        case 1:{
            identifier = @"KTVOnGoingCell";
        }
            break;
        case 2:{
            identifier = @"KTVHotSongCell";
        }
            break;
//        case 3:{
//            identifier = @"KTVChatRecordCell";
//        }
//            break;
//        case 4:{
//            identifier = @"RoomMsgCell";
//        }
//            break;
            
        default:
            break;
    }

    //利用多态
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:identifier owner:self options:nil] lastObject];
    }
    
    
    if (0 == indexPath.section) {
        HWKTVInformationCell *ktvInfoCell = (HWKTVInformationCell *)cell;
        ktvInfoCell.lbSellerName.text = self.ktvBaseInfo.vStoreName;
        ktvInfoCell.directLongitude = [self.ktvBaseInfo.stLocationCoordinate.sLongitude floatValue];
        ktvInfoCell.directLatitude = [self.ktvBaseInfo.stLocationCoordinate.sLatitude floatValue];
        ktvInfoCell.lbSellerAddress.text = self.ktvBaseInfo.address;
        

        
    }else if(1 == indexPath.section){
        
        //正在KTV
        HWKTVOnGoingCell *ktvOnGoing = (HWKTVOnGoingCell *)cell;
        ktvOnGoing.delegate = self;
        [ktvOnGoing.btnRepAnotherGroup addTarget:self action:@selector(switchKtvUserListArr) forControlEvents:UIControlEventTouchUpInside];
//        ktvOnGoing.ktvBaseInfo = self.listenToMeData.ktvBaseInfo;
        ktvOnGoing.arrStUserBaseInfoNet = [NSMutableArray arrayWithArray:self.arrKtvUserList ];
        
    }else if(2 == indexPath.section){
        //KTV热歌
        HWKTVHotSongCell *ktvHotSongCell = (HWKTVHotSongCell *)cell;
        ktvHotSongCell.arrStMyMusicWorkInfo = self.arrKtvHotSongList;
        ktvHotSongCell.delegate = self;
    
    }else{
    
    }
   
    //已移除该功能
    
//    else if(3 == indexPath.section){
//        //K友聊一聊 单独用一个cell,显示本KTV内一些聊天条数
////        HWKTVChatRecordCell *ktvChatCount = (HWKTVChatRecordCell *)cell;
//
//    
//    
//    }
    
    
//    else if(4 == indexPath.section){
//        
//        //给聊天的记录单独创建一个类型的cell
//        YDRoomMsgCell *msgCell = (YDRoomMsgCell *)cell;
//        msgCell.contentView.backgroundColor =[UIColor rgbFromHexString:@"#F1F1F1" alpaa:1.0];
//        msgCell.selectionStyle = UITableViewCellSelectionStyleNone;
//    
//    }
    
    
    return cell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (0 ==section) {
        return 0;
    }else{
        return 30;
    }
   
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    if (4 == section) {
//        return 49;
//    }else{
//        return 0;
//    }
    
    return 0;
}

//-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
//
//    //该模块已经被砍掉
//    UIView *footView = nil;
//
//    if (4 == section) {
//       footView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 50)];
//        footView.backgroundColor = [UIColor whiteColor];
//        
//        UIButton *btnYueGe = [[UIButton alloc]initWithFrame:CGRectMake( 0 , 0, footView.width / 2 - 5, footView.height)];
//        [btnYueGe setTitle:@"在本店约歌" forState:UIControlStateNormal];
//        [btnYueGe setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateNormal];
//        [btnYueGe.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
//        [btnYueGe addTarget:self action:@selector(yuegeHandle:) forControlEvents:UIControlEventTouchUpInside];
//        [footView addSubview:btnYueGe];
//        
//        UIButton *haveAChat = [[UIButton alloc]initWithFrame:CGRectMake( screenWidth - footView.width /2 - 5, 0, footView.width / 2 - 5, footView.height)];
//        [haveAChat setTitle:@"聊一聊" forState:UIControlStateNormal];
//        [haveAChat setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateNormal];
//        [haveAChat.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
//        [haveAChat addTarget:self action:@selector(haveAChatHandle:) forControlEvents:UIControlEventTouchUpInside];
//        [footView addSubview:haveAChat];
//        
//        //竖线
//        UIImageView *lineView = [[UIImageView alloc]initWithFrame:CGRectMake(footView.width / 2 - 1, (footView.height - 25) / 2 , 1, 25)];
//        lineView.backgroundColor = [UIColor rgbFromHexString:@"AC56FF" alpaa:1.0];
//        [footView addSubview:lineView];
//    }
//    
//    return footView;
//}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    CGFloat cellHeight = 0;
    switch (indexPath.section) {
        case 0:{
            cellHeight = 80;
        }
            break;
        case 1:{
            cellHeight = 208;
        }
            break;
        case 2:{
            cellHeight = 275;
        }
            break;
        case 3:{
            cellHeight = 34;
        }
            break;
//        case 4:{
//        
//            cellHeight = 130;
//        }
//            break;
            
        default:
            break;
    }
    return cellHeight;
}



#pragma mark - HWKTVOnGoingCellDelegate
#pragma mark - 进入他人主页
-(void)browsePerHmePage:(UserBaseInfoNet *)userBaseInfoNet{

    HWPerHomePageVC *personHomePageVC = [[HWPerHomePageVC alloc]init];
    personHomePageVC.userBaseInfoNet = userBaseInfoNet;
    [self.navigationController pushViewController:personHomePageVC animated:YES];
}

-(void)viewOthersHomePage:(UserBaseInfoNet *)userBaseInfoNet{
    
    HWPerHomePageVC *personHomePageVC = [[HWPerHomePageVC alloc]init];
    personHomePageVC.userBaseInfoNet = userBaseInfoNet;
    [self.navigationController pushViewController:personHomePageVC animated:YES];
}

#pragma mark - 换一批数据
-(void)switchKtvUserListArr{
    
    [[NetReqManager getInstance] sendGetSwitchKtvUserList:(int32_t)(++countForKtvUserList) INum:(int32_t)(self.arrKtvUserList.count) LKtvId:self.listenToMeData.ktvBaseInfo.lKtvId];
    
}


//#pragma mark - 在本店约歌
//-(void)yuegeHandle:(UIButton *)btnYueGe{
//    
//    YDLaunchPartyVC *lancuhPatyVC = [[YDLaunchPartyVC alloc]init];
//    [self.navigationController pushViewController:lancuhPatyVC animated:YES];
//}
//
//#pragma mark - 聊一聊
//-(void)haveAChatHandle:(UIButton *)btHaveAChat{
//
//    YDLog(@"聊一聊");
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
