//
//  HWKTVInformationCell.h
//  ListenToMe
//
//  Created by zhw on 15/3/31.
//  Copyright (c) 2015年 listentome. All rights reserved.
//
/**
 *  KTV店铺地址 优惠 活动 电话等信息
 *
 *  @param strong    strong description
 *  @param nonatomic nonatomic description
 *
 *  @return return value description
 */

#import <UIKit/UIKit.h>

@interface HWKTVInformationCell : UITableViewCell
/**
 *  KTV店铺的名字
 */
@property(strong,nonatomic)UILabel *lbSellerName;
/**
 *  店铺地址
 */
@property(strong,nonatomic)UILabel *lbSellerAddress;
/**
 *  店铺地图定位button
 */
@property(strong,nonatomic)UIButton *btnSellerOnMap;
/**
 *  距离小图标
 */
@property(strong,nonatomic)UIImageView *distanceImg;
/**
 *  距离
 */
@property(strong,nonatomic)UILabel *lbDistance;
/**
 *  价格图标
 */
@property(strong,nonatomic)UIImageView *priceImg;
/**
 *  人均消费背景图
 */
@property(strong,nonatomic)UIImageView *perConsumptionImg;
/**
 *  人均消费标准
 */
@property(strong,nonatomic)UILabel *lbPerConsumption;
/**
 *  电话咨询/预订
 */
@property(strong,nonatomic)UIButton *btnPhoneCall;
/**
 *  优惠活动图标
 */
@property(strong,nonatomic)UIImageView *favourableActivityImg;
/**
 *  优惠活动标题
 */
@property(strong,nonatomic)UILabel *lbFavourableActivity;
/**
 *  优惠活动一
 */
@property(strong,nonatomic)UILabel *lbFirstFavAct;
/**
 *  优惠活动一更多button
 */
@property(strong,nonatomic)UIButton *btnFirstFavAct;
/**
 *  优惠活动二
 */
@property(strong,nonatomic)UILabel *lbSecondFavAct;
/**
 *  优惠活动二更多button
 */
@property(strong,nonatomic)UIButton *btnSecondFavAct;
/**
 *  查看全部优惠活动
 */
@property(strong,nonatomic)UILabel *lbBrowseAllFavAct;
/**
 *  查看全部优惠活动
 */
@property(strong,nonatomic)UIButton *btnBrowseAllFavAct;
/**
 *  定购电话
 */
@property(nonatomic,assign)NSInteger phoneNum;
/**
 *  目的地纬度
 */
@property(nonatomic,assign)double directLatitude;
/**
 *  目的地经度
 */
@property(nonatomic,assign)double directLongitude;
/**
 *  初始化
 */

+(instancetype)cellWithTableView:(UITableView *)tableView;


@end
