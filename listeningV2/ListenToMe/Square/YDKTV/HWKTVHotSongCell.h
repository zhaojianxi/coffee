//
//  HWKTVHotSongCell.h
//  ListenToMe
//
//  Created by yadong on 15/4/2.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HWKTVHotSongCellDelegate <NSObject>
/**
 *  进入他人主页
 */
-(void)viewOthersHomePage:(UserBaseInfoNet *)userBaseInfoNet;

@end

@interface HWKTVHotSongCell : UITableViewCell
@property(nonatomic,assign)id<HWKTVHotSongCellDelegate>delegate;
/**
 *  初始化
 */
+(instancetype)cellWithTableView:(UITableView *)tableView;
/**
 * 第一名歌手头像
 */
@property(strong,nonatomic) UIButton *imgFSinger;
/**
 * 第一名歌曲名
 */
@property(strong,nonatomic) UILabel *lbFSongName;
/**
 * 第一名听众数目
 */
@property(strong,nonatomic) UIButton *btnFListenNum;
/**
 * 第一名鲜花数
 */
@property(strong,nonatomic) UIButton *btnFFlower;
/**
 * 第二名歌手头像
 */
@property(strong,nonatomic) UIButton *imgSSinger;
/**
 * 第二名歌曲名
 */
@property(strong,nonatomic) UILabel *lbSSongName;
/**
 * 第二名听众数目
 */
@property(strong,nonatomic) UIButton *btnSListenNum;
/**
 * 第二名鲜花数
 */
@property(strong,nonatomic) UIButton *btnSFlower;
/**
 *  更多KTV热歌
 */
@property(strong,nonatomic) UIButton * btnMore;

/**
 *  定义一个数组用来存放单曲正在K歌的人
 */
@property(strong,nonatomic)NSMutableArray *arrHotSong;
/**
 *  KTV热歌数据
 */
@property(strong,nonatomic)NSArray *arrStMyMusicWorkInfo;
@end
