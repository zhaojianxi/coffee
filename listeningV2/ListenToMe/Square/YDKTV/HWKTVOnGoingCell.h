//
//  HWKTVOnGoingCell.h
//  ListenToMe
//
//  Created by zhw on 15/4/1.
//  Copyright (c) 2015年 listentome. All rights reserved.
//
/**
 *  正在KTV
 *
 *  @param instancetype instancetype description
 *
 *  @return return value description
 */
#import <UIKit/UIKit.h>

@protocol HWKTVOnGoingCellDelegate <NSObject>
@optional
/**
 *  进入个人主页
 */
-(void)browsePerHmePage:(UserBaseInfoNet *)userBaseInfoNet;

@end

@interface HWKTVOnGoingCell : UITableViewCell

@property(assign,nonatomic)id<HWKTVOnGoingCellDelegate>delegate;
/**
 *  初始化
 */

+(instancetype)cellWithTableView:(UITableView *)tableView;

/**
 * 帅哥数
 */
@property(strong,nonatomic) UILabel *lbBoys;
/**
 * 美女数
 */
@property(strong,nonatomic) UILabel *lbGirls;
/**
 * 在场观众数组
 */
@property(strong,nonatomic) NSMutableArray *mArrayPresent;
/**
 * 不在场观众数组
 */
@property(strong,nonatomic) NSMutableArray *mArrayNotPresent;
/**
 *  换一批
 */
@property(strong,nonatomic)UIButton *btnRepAnotherGroup;
/**
 *  真正KTV的人
 */
@property(strong,nonatomic)NSArray *arrStUserBaseInfoNet;
/**
 *  KTV信息
 */
@property(strong,nonatomic) KtvBaseInfo *ktvBaseInfo;
@end
