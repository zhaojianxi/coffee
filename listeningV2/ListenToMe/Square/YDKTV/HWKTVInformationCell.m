//
//  HWKTVInformationCell.m
//  ListenToMe
//
//  Created by zhw on 15/3/31.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "HWKTVInformationCell.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
@interface HWKTVInformationCell ()<CLLocationManagerDelegate>
/**
 *  地图定位
 */
@property(nonatomic,strong)CLLocationManager *locationManager;
@end


@implementation HWKTVInformationCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


+(instancetype)cellWithTableView:(UITableView *)tableView{

    NSString *identifier = @"KTVInfoCell";
    HWKTVInformationCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell) {
        cell = [[HWKTVInformationCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }

    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{


    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUI];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return self;
}

#pragma mark - 布局cell视图
-(void)setUI{


    CGFloat lineH = 1;//分割线的高度
    CGFloat fromLAndR = 15;//控件离屏幕两侧的距离
    
    CGFloat firsUIY = 14.5;//第一个控件的y的坐标
    
    // 1 店名 分割线
    self.lbSellerName = [[UILabel alloc]initWithFrame:CGRectMake(fromLAndR, firsUIY, screenWidth - fromLAndR * 2, 17.5)];
    [self.lbSellerName setFont:[UIFont systemFontOfSize:15.0]];
    [self.lbSellerName setTextColor:[UIColor rgbFromHexString:@"#000000" alpaa:1.0]];
    [self.lbSellerName setTextAlignment:NSTextAlignmentLeft];
    
    [self.contentView addSubview:self.lbSellerName];
    
    UIImageView *separatorLine01=[[UIImageView alloc]initWithFrame:CGRectMake(0, self.lbSellerName.y + self.lbSellerName.height + firsUIY, screenWidth, lineH)];
    separatorLine01.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
    [self.contentView addSubview:separatorLine01];
    
    
    //2 店地址 地图位置 距离
    CGFloat addressW = 172;
    self.lbSellerAddress = [[UILabel alloc]initWithFrame:CGRectMake(fromLAndR, separatorLine01.y + lineH + 10, addressW, 13.5)];
    [self.lbSellerAddress setFont:[UIFont systemFontOfSize:12.0]];
    [self.lbSellerAddress setTextColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0]];
    [self.lbSellerAddress setTextAlignment:NSTextAlignmentLeft];
    [self.contentView addSubview:self.lbSellerAddress];
    
    UIImage *onMapImg = [UIImage imageNamed:@"查看地图.png"];
    self.btnSellerOnMap = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - fromLAndR - onMapImg.size.width * 0.5, self.lbSellerAddress.y - 10, onMapImg.size.width * 0.5, onMapImg.size.height *0.5)];
    //地图定位
    [self.btnSellerOnMap addTarget:self action:@selector(postioningOnMap:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnSellerOnMap setImage:onMapImg forState:UIControlStateNormal];
    [self.contentView addSubview:self.btnSellerOnMap];
    
//    CGFloat lbDisTanceW = 30;
//    self.lbDistance = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - fromLAndR - lbDisTanceW, self.lbSellerAddress.y, lbDisTanceW, self.lbSellerAddress.height)];
//    self.lbDistance.font = [UIFont systemFontOfSize:12.0];
//    [self.lbDistance setTextColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0]];
//    [self.lbDistance setTextAlignment:NSTextAlignmentRight];
//    //移除距离显示
//    [self.contentView addSubview:self.lbDistance];
//    
//    UIImage *distanceImg = [UIImage imageNamed:@"distance.png"];
//    self.distanceImg = [[UIImageView alloc]initWithFrame:CGRectMake(screenWidth - fromLAndR - lbDisTanceW - distanceImg.size.width, self.lbDistance.y - self.lbDistance.height, distanceImg.size.width, distanceImg.size.height)];
//    self.distanceImg.image = distanceImg;
//    [self.contentView addSubview:self.distanceImg];
//    
//    UIImageView *separatorLine02=[[UIImageView alloc]initWithFrame:CGRectMake(fromLAndR, self.lbSellerAddress.y + self.lbSellerAddress.height + 10, screenWidth - fromLAndR * 2, lineH)];
//    separatorLine02.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
//    [self.contentView addSubview:separatorLine02];
//    
//    //3 价格 人均消费 电话定购
//    UIImage *price = [UIImage imageNamed:@"价格.png"];
//    self.priceImg = [[UIImageView alloc]initWithFrame:CGRectMake(fromLAndR, separatorLine02.y + 1 , price.size.width * 0.5, price.size.height * 0.5)];
//    self.priceImg.image = price;
//    [self.contentView addSubview:self.priceImg];
//    
//    UIImage *perConsumtion = [UIImage imageNamed:@"价格说明.png"];
//    self.perConsumptionImg =[[UIImageView alloc]initWithFrame:CGRectMake(self.priceImg.x + self.priceImg.width + 1, self.priceImg.y + 10, perConsumtion.size.width * 0.5, perConsumtion.size.height * 0.5)];
//    self.perConsumptionImg.image = perConsumtion;
//    
//    self.lbPerConsumption = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.perConsumptionImg.width, self.perConsumptionImg.height)];
//    [self.lbPerConsumption setTextColor:[UIColor rgbFromHexString:@"#FFFFFF" alpaa:1.0]];
//    [self.lbPerConsumption setFont:[UIFont systemFontOfSize:14.0]];
//    [self.lbPerConsumption setTextAlignment:NSTextAlignmentLeft];
//    [self.perConsumptionImg addSubview:self.lbPerConsumption];
//
//    [self.contentView addSubview:self.perConsumptionImg];
//    
//    UIImage *phone = [UIImage imageNamed:@"电话.png"];
//    self.btnPhoneCall = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - fromLAndR - phone.size.width * 0.5, self.priceImg.y , phone.size.width * 0.5, phone.size.height * 0.5)];
//    [self.btnPhoneCall setImage:phone forState:UIControlStateNormal];
//    [self.btnPhoneCall addTarget:self action:@selector(phoneBookHandle:) forControlEvents:UIControlEventTouchUpInside];
//    [self.contentView addSubview:self.btnPhoneCall];
    
    
    /*
    
    UIImageView *separatorLine03=[[UIImageView alloc]initWithFrame:CGRectMake(fromLAndR, self.priceImg.y + self.priceImg.height , screenWidth - fromLAndR * 2, lineH)];
    separatorLine03.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
    [self.contentView addSubview:separatorLine03];
    
    //优惠活动
    
    UIImage *favourable = [UIImage imageNamed:@"优惠活动"];
    self.favourableActivityImg = [[UIImageView alloc]initWithFrame:CGRectMake(fromLAndR, separatorLine03.y + 1, favourable.size.width * 0.5, favourable.size.height * 0.5)];
    self.favourableActivityImg.image = favourable;
    [self.contentView addSubview:self.favourableActivityImg];
    
    self.lbFavourableActivity = [[UILabel alloc]initWithFrame:CGRectMake(self.favourableActivityImg.x + self.favourableActivityImg.width, self.favourableActivityImg.y, 47.5, self.favourableActivityImg.height)];
    [self.lbFavourableActivity setTextColor:[UIColor rgbFromHexString:@"#FF0053" alpaa:1.0]];
    [self.lbFavourableActivity setFont:[UIFont systemFontOfSize:11.0]];
    [self.lbFavourableActivity setTextAlignment:NSTextAlignmentLeft];
    [self.contentView addSubview:self.lbFavourableActivity];
    
    UIImageView *separatorLine04=[[UIImageView alloc]initWithFrame:CGRectMake(fromLAndR, self.favourableActivityImg.y + self.favourableActivityImg.height , screenWidth - fromLAndR * 2, lineH)];
    separatorLine04.backgroundColor = [UIColor rgbFromHexString:@"#FF0053" alpaa:.12];
    [self.contentView addSubview:separatorLine04];
    
    //优惠活动1 
    
    self.lbFirstFavAct = [[UILabel alloc]initWithFrame:CGRectMake(fromLAndR * 2, separatorLine04.y + 1 + 10, 150, 15)];
    [self.lbFirstFavAct setTextColor:[UIColor rgbFromHexString:@"#4A4A4A" alpaa:1.0]];
    [self.lbFirstFavAct setFont:[UIFont systemFontOfSize:12.0]];
    [self.contentView addSubview:self.lbFirstFavAct];
    
    UIImage *moreImage = [UIImage imageNamed:@"查看更多.png"];
    self.btnFirstFavAct = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - fromLAndR - moreImage.size.width * 0.5 - 5, self.lbFirstFavAct.y - 12.5, moreImage.size.width * 0.5, moreImage.size.height * 0.5)];
    [self.btnFirstFavAct setImage:moreImage forState:UIControlStateNormal];
    [self.contentView addSubview:self.btnFirstFavAct];
    
    UIImageView *separatorLine05=[[UIImageView alloc]initWithFrame:CGRectMake(fromLAndR * 2, self.lbFirstFavAct.y + self.lbFirstFavAct.height + 10 , screenWidth - fromLAndR * 3, lineH)];
    separatorLine05.backgroundColor = [UIColor rgbFromHexString:@"#FF0053" alpaa:.12];
    [self.contentView addSubview:separatorLine05];
    
    //优惠活动2
    
    self.lbSecondFavAct = [[UILabel alloc]initWithFrame:CGRectMake(fromLAndR * 2, separatorLine05.y + 1 + 10, 169, 15)];
    [self.lbSecondFavAct setTextColor:[UIColor rgbFromHexString:@"#4A4A4A" alpaa:1.0]];
    [self.lbSecondFavAct setFont:[UIFont systemFontOfSize:12.0]];
    [self.contentView addSubview:self.lbSecondFavAct];
    
    self.btnSecondFavAct  = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - fromLAndR - moreImage.size.width * 0.5 - 5, self.lbSecondFavAct.y - 12.5, moreImage.size.width * 0.5, moreImage.size.height * 0.5)];
    [self.btnSecondFavAct setImage:moreImage forState:UIControlStateNormal];
    [self.contentView addSubview:self.btnSecondFavAct];
    
    UIImageView *separatorLine06=[[UIImageView alloc]initWithFrame:CGRectMake(fromLAndR * 2, self.lbSecondFavAct.y + self.lbSecondFavAct.height + 10 , screenWidth - fromLAndR * 3, lineH)];
    separatorLine06.backgroundColor = [UIColor rgbFromHexString:@"#FF0053" alpaa:.12];
    [self.contentView addSubview:separatorLine06];
    
    //查看更多优惠活动
    self.lbBrowseAllFavAct = [[UILabel alloc]initWithFrame:CGRectMake(fromLAndR * 2, separatorLine06.y + 1 + 10, 72, 15)];
    [self.lbBrowseAllFavAct setTextColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0]];
    [self.lbBrowseAllFavAct setFont:[UIFont systemFontOfSize:12.0]];
    [self.contentView addSubview:self.lbBrowseAllFavAct];
    
    UIImage *browseAll = [UIImage imageNamed:@"展开.png"];
    self.btnBrowseAllFavAct  = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - fromLAndR - browseAll.size.width * 0.5 - 5, self.lbBrowseAllFavAct.y - 12.5, browseAll.size.width * 0.5, browseAll.size.height * 0.5)];
    [self.btnBrowseAllFavAct setImage:browseAll forState:UIControlStateNormal];
    [self.contentView addSubview:self.btnBrowseAllFavAct];
    
    */
    
}

#pragma mark - button点击事件的处理

#pragma mark -打电话
-(void)phoneBookHandle:(UIButton *)button{

//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel://10086"]];
    //打完电话返回
    NSString *callNum = [[NSString alloc]initWithFormat:@"telprompt://%ld",self.phoneNum];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:callNum]];
}

#pragma mark -地图定位
-(void)postioningOnMap:(UIButton *)button{
    
    
    self.locationManager =  [[CLLocationManager alloc]init];
        
    self.locationManager.delegate = self;
    
    //iOS 8需要授权
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 8) {
        [self.locationManager requestWhenInUseAuthorization];
        [self.locationManager requestAlwaysAuthorization];
    }
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [self.locationManager startUpdatingHeading];
    
        
    
}

//定位delegate
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{

    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
            if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                [self.locationManager requestWhenInUseAuthorization];
                
                YDLog(@"地图定位授权");
                
                [self openMap];
            }
            break;
            
        default:
            break;
    }
}

#pragma mark - 调用系统地图查看路线
-(void)openMap{

    MKMapItem *currentLocation = [MKMapItem mapItemForCurrentLocation];
    
    
    CLLocationCoordinate2D coords = CLLocationCoordinate2DMake(self.directLongitude,self.directLatitude);
    MKMapItem *toLocation = [[MKMapItem alloc]initWithPlacemark:[[MKPlacemark alloc]initWithCoordinate:coords addressDictionary:nil]];
    toLocation.name = [NSString stringWithFormat:@"%@ : %@",self.lbSellerName.text,self.lbSellerAddress.text];
    
    [MKMapItem openMapsWithItems:@[currentLocation,toLocation] launchOptions:@{MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving,MKLaunchOptionsShowsTrafficKey:[NSNumber numberWithBool:YES]}];
    
    
  
}



@end
