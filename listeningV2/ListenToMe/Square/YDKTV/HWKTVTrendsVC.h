//
//  HWKTVTrendsVC.h
//  ListenToMe
//
//  Created by zhw on 15/3/30.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "YDBaseVC.h"

@interface HWKTVTrendsVC : YDBaseVC
/**
 *  ktv店内基本信息
 */
@property(nonatomic,strong) KtvBaseInfo *ktvBaseInfo;
@end
