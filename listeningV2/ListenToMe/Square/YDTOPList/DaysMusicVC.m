//
//  DaysMusicVC.m
//  ListenToMe
//
//  Created by yadong on 15/3/23.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "DaysMusicVC.h"
#import "YDTopListCell.h"
#import "HWShareAlertVC.h"
#import "MoreNavigationMenView.h"
#import "HMPopMenu.h"
@interface DaysMusicVC ()<YDTopLiseCellDelegate>
/**
 *  分享提示
 */
@property(strong,nonatomic)HWShareAlertVC *shareVC;
/**
 *  跟多导航菜单
 */
@property(strong,nonatomic)MoreNavigationMenView *moreMenuView;
@end

@implementation DaysMusicVC


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNavigation];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor rgbFromHexString:@"#F1F1F1" alpaa:1.0];
    
    

    
}


#pragma mark tableview
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    
    return 6;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *ID = @"daysCell";
    YDTopListCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[YDTopListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    
    cell.lbMusicName.text = @"泡沫";
    cell.lbSinger.text = @"邓紫棋";
    cell.lbTime.text = @"04:58";
    [cell.btnListener setTitle:@"12356" forState:UIControlStateNormal];
    [cell.btnFlower setTitle:@"114" forState:UIControlStateNormal];
    
    cell.lbRank.hidden = NO;
    cell.imgRank.hidden = NO;
    cell.lbRank.text = [NSString stringWithFormat:@"%ld",indexPath.row + 1];
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.delegate = self;
    return  cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 155;
}




#pragma mark 导航栏
-(void)setNavigation
{
    self.navigationItem.title = @"每日金曲";
    [self setLeftBtnItem];
    [self setRightBtnItem];
}

// 导航栏左边的Btn
-(void)setLeftBtnItem
{
    UIButton *leftBtn = [[UIButton alloc]init];
    UIImage *leftBtnImg = [UIImage imageNamed:@"whiteBack.png"];
    leftBtn.frame = CGRectMake(0, 0, leftBtnImg.size.width, leftBtnImg.size.height);
    [leftBtn setImage:leftBtnImg forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(clickLeftBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customLeftBtnItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = customLeftBtnItem;
}

// 导航栏右边的Btn
-(void)setRightBtnItem
{
    UIButton *rightBtn = [[UIButton alloc]init];
    UIImage *imgMore = [UIImage imageNamed:@"more.png"];
    rightBtn.frame = CGRectMake(0, 0, imgMore.size.width, imgMore.size.height);
    [rightBtn setImage:imgMore forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(clickRightBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customRightBtnItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = customRightBtnItem;
}


#pragma mark -点击事件
-(void)clickRightBarBtnItem
{
    YDLog(@"点击了导航栏的右边按钮");
    
    UIImage *popImg = [UIImage imageNamed:@"popMenuBg.png"];
    CGFloat popMenuW = popImg.size.width;
    CGFloat popMenuH = popImg.size.height;
    
    self.moreMenuView = [[MoreNavigationMenView alloc]initWithFrame:CGRectMake(0, 0, popMenuW, popMenuH)];
    HMPopMenu *menu = [HMPopMenu popMenuWithContentView:self.moreMenuView];
    CGFloat menuW = popMenuW;
    CGFloat menuH = popMenuH;
    CGFloat menuY = 55;
    CGFloat menuX = self.view.width - menuW - 20;
    menu.dimBackground = YES;
    menu.arrowPosition = HMPopMenuArrowPositionRight;
    [menu showInRect:CGRectMake(menuX, menuY, menuW, menuH)];
}

-(void)clickLeftBarBtnItem
{
    [self.navigationController popToRootViewControllerAnimated:YES];
    YDLog(@"点击了导航栏左边的Btn");
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark -cell中点击分享提示分享平台
-(void)alertForShare{
    _shareVC = [[HWShareAlertVC alloc]init];
    [[[UIApplication sharedApplication]keyWindow]addSubview:_shareVC.view];
    
//    [self.view.window.rootViewController presentViewController:_shareVC animated:NO completion:nil];


}
@end
