//
//  YDTopListCell.h
//  ListenToMe
//
//  Created by yadong on 3/6/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol YDTopLiseCellDelegate <NSObject>

/**
 *  点击分享提示分享平台
 */
@optional
-(void)alertForShare;

/**
 *  点击收藏是如果没有登录就弹出提示登录
 */
-(void)alertForLogin;

@end

@interface YDTopListCell : UITableViewCell
/**
 *   设置代理注意要assign
 */
@property(assign,nonatomic)id<YDTopLiseCellDelegate>delegate;

+ (instancetype)cellWithTableView:(UITableView *)tableView;
/**
 * 歌名lb
 */
@property(strong,nonatomic) UILabel *lbMusicName;

/**
* 歌者lb
*/
@property(strong,nonatomic) UILabel *lbSinger;

/**
* 播放btn
*/
@property(strong,nonatomic) UIButton *btnPlay;

/**
* 头像bt
*/
@property(strong,nonatomic) UIImageView *imgAvatar;

/**
 * 歌曲时间lb
 */
@property(strong,nonatomic) UILabel *lbTime;

/**
 * 听众数btn
 */
@property(strong,nonatomic) UIButton *btnListener;


/**
* 鲜花数btn
*/
@property(strong,nonatomic) UIButton *btnFlower;

/**
 * 分享btn
 */
@property(strong,nonatomic) UIButton *btnShare;

/**
 * 收藏btn
 */
@property(strong,nonatomic) UIButton *btnCollection;
/**
 * 波浪
 */
@property(strong,nonatomic) UIImageView *imgWave;

/**
 * 排名图片
 */
@property(strong,nonatomic) UIImageView *imgRank;
/**
 * 排名lb
 */
@property(strong,nonatomic) UILabel *lbRank;
/**
 *  定义一个bool判断是否已经收藏过了
 */
@property(assign,nonatomic)BOOL isCollected;

@end
