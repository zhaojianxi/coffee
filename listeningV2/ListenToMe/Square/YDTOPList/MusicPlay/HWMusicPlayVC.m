//
//  HWMusicPlayVC.m
//  ListenToMe
//
//  Created by zhw on 15/4/13.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "HWMusicPlayVC.h"
#import "YDRoomMsgCell.h"
#import "FriendsMsgVC.h"
#import "GivingFlowerView.h"
#import "NSDate+ZHW.h"
@interface HWMusicPlayVC ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
@property(nonatomic,strong)UITableView *tableView;
/**
 *  播放歌曲显示歌曲的封面 演唱者信息 的界面
 */
@property(nonatomic,strong)UIView *headView;
/**
 *  播放歌曲显示封面和歌词的界面
 */
@property(nonatomic,strong)UIView *songView;
/**
 *  演唱者头像btn
 */
@property(nonatomic,strong)UIButton *btnSinger;
/**
 *  歌曲播放按钮
 */
@property(nonatomic,strong)UIButton *btnSongPlay;
/**
 *  歌曲时长
 */
@property(nonatomic,strong)UILabel *lbSongLength;
/**
 *  演唱者名字
 */
@property(nonatomic,strong)UILabel *lbSingerName;
/**
 *  演唱者等级
 */
@property(nonatomic,strong)UIButton *btnSingerLevel;
/**
 *  演唱者鲜花数
 */
@property(nonatomic,strong)UIButton *btnFlowerNum;
/**
 *  所在城市
 */
@property(nonatomic,strong)UILabel *lbCity;
/**
 *  简介
 */
@property(nonatomic,strong)UILabel *lbSongIntroduction;
/**
 *  发起私聊
 */
@property(nonatomic,strong)UIButton *btnPrivateChat;
/**
 *  底部收藏 送花 讨论 分享
 */
@property(nonatomic,strong)UIView *footView;
/**
 *  收藏
 */
@property(nonatomic,strong)UIButton *btnCollection;
/**
 *  送花
 */
@property(nonatomic,strong)UIButton *btnGiveFlower;
/**
 *  讨论
 */
@property(nonatomic,strong)UIButton *btnDiscuss;
/**
 *  分享
 */
@property(nonatomic,strong)UIButton *btnShare;
/**
 *  是否收藏
 */
@property(nonatomic,assign)BOOL isCollection;
/**
 *  是否正在播放
 */
@property(nonatomic,assign)BOOL isPlaying;
/**
 *  遮盖视图
 */
@property(strong,nonatomic) UIView *coverView;
/**
 *  送鲜花弹窗背景
 */
@property(strong,nonatomic) UIButton *alertForGivingFlower;
/**
 *  送花
 */
@property(strong,nonatomic) GivingFlowerView *sendFlowerView;

/**
 *  默认送出鲜花数
 */
@property(assign,nonatomic) int sendFlowerNum;
/**
 *  用户拥有的鲜花数
 */
@property(assign,nonatomic) NSInteger haveFlowerNum;
/**
 *  用户头像
 */
@property(nonatomic,strong) UIImageView *avatarImageView;
/**
 *  数据
 */
@property(nonatomic,strong) ListenToMeData *listenToMeData;
/**
 *  输入框背景视图
 */
@property(nonatomic,strong) UIImageView *opinionView;
/**
 *  输入框
 */
@property(nonatomic,strong) UITextField * tfOpinion;
/**
 *  键盘高度
 */
@property(nonatomic,assign) NSInteger keyboardhight;
/**
 *  反馈的意见
 */
@property(nonatomic,strong) NSString *opinionContent;
@end

@implementation HWMusicPlayVC
@synthesize keyboardhight;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setsetCustomizedNav];
    
    [self setUI];
    
    [self setTableViewUI];
    
    //参与讨论的弹窗视图
    [self setDiscussView];
    
    [self initData];
    
    
}

//在遇到有输入的情况下。由于现在键盘的高度是动态变化的。中文输入与英文输入时高度不同。所以输入框的位置也要做出相应的变化
#pragma mark - keyboardHight
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerForKeyboardNotifications];
    
    
}

#pragma mark - 初始化数据
-(void)initData{
    
    self.listenToMeData = [ListenToMeData getInstance];
    
    if ([ListenToMeDBManager getUuid] > 0) {
        //如果用户已经登录,那么就获取他的收藏列表
        [[NetReqManager getInstance] sendGetCollectWorkList:[ListenToMeDBManager getUuid] IOffset:0 INum:20];
    }
    
    
    //获取评论列表的请求
    [[NetReqManager getInstance] sendGetMusicCommentList:self.musicWorkBaseInfo.lMusicWorkId IOffset:0 INum:5];
    
    //获取当前音乐作品的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMusicWorkInfo) name:NOTIFY_GET_MUSICWOKRBSAEINFO_PAGE_RESP object:nil];
    
    //获取收藏列表通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setCollectBtnState) name:NOTIFY_GET_COLLECT_WORKLIST_PAGE_RESP object:nil];
    
    //获取音乐作品评论信息列表通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTable) name:NOTIFY_GET_MUSIC_COMMENTLIST_PAGE_RESP object:nil];
    
    //用户添加评论成功的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addMusicWorkComment) name:NOTIFY_GET_ADD_COMMENT_MUSIC_PAGE_RESP object:nil];
    
    //给人送花
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendFlowerSuccess) name:NOTIFY_GET_SEND_USERFLOWER_PAGE_RESP object:nil];
}

-(void)setCollectBtnState{
//判断该音乐作品是否已经被收藏,改变收藏按钮的状态
#warning 判断获取的音乐作品是否已经被收藏了,如果已经被收藏了就改变收藏按钮的状态否则保持在未收藏的状态.
    if (self.listenToMeData.arrCollectWorkInfo != nil) {
        
        for (CollectWorkInfo *collectWrokInfo  in self.listenToMeData.arrCollectWorkInfo) {
            if (collectWrokInfo.stMusicWorkBaseInfo.lMusicWorkId == self.musicWorkBaseInfo.lMusicWorkId) {
                self.btnCollection.selected = YES;
            }
        }
    }
    
    
}


-(void)refreshTable{
    
    [self.tableView reloadData];
}

-(void)addMusicWorkComment{
    //获取添加的评论,并显示到评论列表中
    [self.listenToMeData.arrCommentListInfo insertObject:self.listenToMeData.addMusicCommntInfo atIndex:0];
    [self.tableView reloadData];
}

-(void)sendFlowerSuccess{
    //送花成功,重新设置当前收花人的花的数量
    [self.btnFlowerNum setTitle:[NSString stringWithFormat:@"%d",self.musicWorkBaseInfo.stUserBaseInfoNet.iFlowerNum + self.sendFlowerNum] forState:UIControlStateNormal];
}


-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_COLLECT_WORKLIST_PAGE_RESP object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_MUSIC_COMMENTLIST_PAGE_RESP object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_ADD_COMMENT_MUSIC_PAGE_RESP object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_MUSICWOKRBSAEINFO_PAGE_RESP object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_SEND_USERFLOWER_PAGE_RESP object:nil];
    
}

#pragma mark -setsetCustomizedNav自定义导航
-(void)setsetCustomizedNav{

//    CGFloat titleViewH = 35;
//    UIView *titleView = [[UIView alloc]initWithFrame:CGRectMake(0,0, screenWidth * 0.80, titleViewH)];
//    titleView.backgroundColor = [UIColor clearColor];
//    
//    CGFloat mySubcribeBtnW = 55;
//    UIButton *mySubcribeBtn = [[UIButton alloc]initWithFrame:CGRectMake((titleView.width - mySubcribeBtnW) * 0.5, 0, 80, titleViewH)];
//    [mySubcribeBtn setTintColor:[UIColor whiteColor]];
//    [mySubcribeBtn setBackgroundColor:[UIColor clearColor]];
//    [mySubcribeBtn setTitle:@"泡沫" forState:UIControlStateNormal];
//    [mySubcribeBtn.titleLabel setFont:[UIFont systemFontOfSize:17.5]];
//    [titleView addSubview:mySubcribeBtn];
    
//    CGFloat gotoSubcribeBtnW = 50;
//    UIImage *wantSingImg = [UIImage imageNamed:@"我要唱.png"];
//    UIButton *gotoSubcribeBtn = [[UIButton alloc]initWithFrame:CGRectMake(titleView.width - gotoSubcribeBtnW - 20, 0, gotoSubcribeBtnW, titleViewH)];
//    [gotoSubcribeBtn setTintColor:[UIColor lightTextColor]];
//    [gotoSubcribeBtn setImage:wantSingImg forState:UIControlStateNormal];
//    [gotoSubcribeBtn setImage:wantSingImg forState:UIControlStateHighlighted];
//    [gotoSubcribeBtn setTitle:@"我要唱" forState:UIControlStateNormal];
//    [gotoSubcribeBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -80)];
//    [gotoSubcribeBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -50, 0, 0)];
//    [gotoSubcribeBtn.titleLabel setFont:[UIFont systemFontOfSize:14.5]];
//    gotoSubcribeBtn.titleLabel.textAlignment = NSTextAlignmentRight;
//    gotoSubcribeBtn.backgroundColor = [UIColor clearColor];
////    [gotoSubcribeBtn addTarget:self action:@selector(clickWantSing:) forControlEvents:UIControlEventTouchUpInside];
//    [titleView addSubview:gotoSubcribeBtn];
//    self.navigationItem.titleView = titleView;
    
    [self setLeftItem];
    
    [self setRightItem];

}

-(void)clickWantSing:(UIButton *)wantSingBtn{
    
    YDLog(@"跳转到我要唱的界面");
}

-(void)setLeftItem{

    UIButton *leftBtn = [[UIButton alloc]init];
    UIImage *leftBtnImg = [UIImage imageNamed:@"whiteBack.png"];
    leftBtn.frame = CGRectMake(0, 0, leftBtnImg.size.width, leftBtnImg.size.height);
    [leftBtn setImage:leftBtnImg forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(clickLeftItem:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customLeftBtnItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = customLeftBtnItem;
}

-(void)setRightItem{

    UIButton *rightBtn = [[UIButton alloc]init];
    UIImage *imgMore = [UIImage imageNamed:@"more.png"];
    rightBtn.frame = CGRectMake(0, 0, imgMore.size.width, imgMore.size.height);
    [rightBtn setImage:imgMore forState:UIControlStateNormal];
    //    [rightBtn addTarget:self action:@selector(clickRightItem:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customRightBtnItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = customRightBtnItem;
}

-(void)clickLeftItem:(UIButton *)leftItem{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - setUI 布局视图
-(void)setUI{

    //设置播放界面 150 160
    
    CGFloat cellHeight = 40;
    
    self.headView = [[UIView alloc]initWithFrame:CGRectMake(0, naviAndStatusH, screenWidth, 310)];
    self.headView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.headView];
    
    //设置歌曲封面 歌词的界面
    self.songView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.headView.width, 150)];
    self.songView.backgroundColor = [UIColor lightGrayColor];
    
    UIImage *tempImg = [UIImage imageNamed:@"album.png"];
    UIImageView *songHomePageView = [[UIImageView alloc]initWithFrame:self.songView.bounds];
    [songHomePageView sd_setImageWithURL:[NSURL URLWithString:self.musicWorkBaseInfo.sCover] placeholderImage:tempImg];
    [self.songView addSubview:songHomePageView];
    [self.headView addSubview:self.songView];
    
    //设置演唱者相关信息
    CGFloat fromX = 15; //控件离左侧边缘的距离
    CGFloat singerHW = 30;
    self.btnSinger = [[UIButton alloc]initWithFrame:CGRectMake(fromX,self.songView.height + 5, singerHW, singerHW)];
//    self.btnSinger.layer.masksToBounds = YES;
//    self.btnSinger.layer.cornerRadius = singerHW * 0.5;
    
    self.avatarImageView = [[UIImageView alloc]initWithFrame:self.btnSinger.frame];
    [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:self.musicWorkBaseInfo.stUserBaseInfoNet.sCovver] placeholderImage:[UIImage imageNamed:@"icon.png"]];
    self.avatarImageView.userInteractionEnabled = YES;
    self.avatarImageView.layer.masksToBounds = YES;
    self.avatarImageView.layer.cornerRadius = singerHW * 0.5;
    [self.headView addSubview:self.avatarImageView];
    
    
    [self.headView addSubview:self.btnSinger];
    
    // 播放btn
    UIImage *imgPlay = [UIImage imageNamed:@"play.png"];
    UIImage *imgStop = [UIImage imageNamed:@"stop.png"];
    self.btnSongPlay = [[UIButton alloc]initWithFrame:CGRectMake(self.btnSinger.x + self.btnSinger.width + fromX, self.songView.height + (cellHeight - imgPlay.size.height * 0.5) * 0.5, imgPlay.size.width * 0.5, imgPlay.size.height * 0.5)];
    [self.btnSongPlay setImage:imgPlay forState:UIControlStateNormal];
    [self.btnSongPlay setImage:imgStop forState:UIControlStateSelected];
    self.btnSongPlay.selected = NO;
    [self.btnSongPlay addTarget:self action:@selector(musicPlayHandle:) forControlEvents:UIControlEventTouchUpInside];
    [self.headView addSubview:self.btnSongPlay];
    
    //歌曲时长
    CGFloat lbSongLengthW = 30;
    self.lbSongLength = [[UILabel alloc]initWithFrame:CGRectMake(self.headView.width - fromX - lbSongLengthW, self.songView.height + (cellHeight - 12) * 0.5, lbSongLengthW, 12)];
    [self.lbSongLength setText:@"04:20"];
    [self.lbSongLength setFont:[UIFont systemFontOfSize:10.0]];
    [self.lbSongLength setTextColor:[UIColor rgbFromHexString:@"#9B9B9B" alpaa:1.0]];
    [self.lbSongLength setTextAlignment:NSTextAlignmentRight];
    [self.headView addSubview:self.lbSongLength];
    
    // 横线
    CGFloat lingHeight = 1; //分割线的高度
    UIImageView *imgLine1 = [[UIImageView alloc]initWithFrame:CGRectMake(0 ,self.songView.height + cellHeight - lingHeight, self.headView.width, 1)];
    imgLine1.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.5];
    [self.headView addSubview:imgLine1];
    
    //演唱者名字 等级 鲜花数 所在地
    
    //名字
    CGFloat lbSNH = 35 * 0.5; //高度
    CGFloat lbSNW = 49; //宽度
    self.lbSingerName = [[UILabel alloc]initWithFrame:CGRectMake(fromX, imgLine1.y + lingHeight + (cellHeight - lbSNH) * 0.5 , lbSNW, lbSNH)];
    [self.lbSingerName setTextColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
    [self.lbSingerName setFont:[UIFont systemFontOfSize:15.0]];
    [self.lbSingerName setTextAlignment:NSTextAlignmentLeft];
    self.lbSingerName.text =self.musicWorkBaseInfo.stUserBaseInfoNet.sNick;
    [self.headView addSubview:self.lbSingerName];
    
    //等级
    
    UIImage *lvImg = [UIImage imageNamed:@"等级.png"];
//    CGFloat btnSLW = 22;
//    CGFloat btnSLH = 15;
//    self.btnSingerLevel = [[UIButton alloc]initWithFrame:CGRectMake(self.lbSingerName.x + lbSNW + fromX, imgLine1.y + lingHeight + (cellHeight - btnSLH) * 0.5, btnSLW, btnSLH)];
//    UIImageView *levelView = [[UIImageView alloc]initWithFrame:CGRectMake(0, - btnSLH, lvImg.size.width  * 0.5 , lvImg.size.height* 0.5)];
//    [levelView setImage:lvImg];
//    
//    [self.btnSingerLevel addSubview:levelView];
//    
//    UILabel *lvLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.btnSingerLevel.width - 7 , 0, 7, self.btnSingerLevel.height)];
//    lvLabel.text = @"8";
//    [lvLabel setTextColor:[UIColor rgbFromHexString:@"#FC9A69" alpaa:1.0]];
//    [lvLabel setFont:[UIFont systemFontOfSize:12.0]];
//    [self.btnSingerLevel addSubview:lvLabel];
//    [self.headView addSubview:self.btnSingerLevel];
    
    
    self.btnSingerLevel = [[UIButton alloc]initWithFrame:CGRectMake(self.lbSingerName.x + lbSNW + fromX, imgLine1.y + lingHeight,55 , cellHeight)];
    self.btnSingerLevel.titleLabel.font = [UIFont systemFontOfSize:12.0];
    [self.btnSingerLevel setTitleColor:[UIColor rgbFromHexString:@"#FC9A69" alpaa:1.0] forState:UIControlStateNormal];
    [self.btnSingerLevel setTitleEdgeInsets:UIEdgeInsetsMake(0,15, 0, 0)];
    [self.btnSingerLevel setImage:lvImg forState:UIControlStateNormal];
    [self.btnSingerLevel setTitle:@"8" forState:UIControlStateNormal];
//    [self.headView addSubview:self.btnSingerLevel];
    
    //送花
    
    UIImage *flowerImg = [UIImage imageNamed:@"flowerSquareKtv.png"];
    self.btnFlowerNum = [[UIButton alloc]initWithFrame:CGRectMake(self.btnSingerLevel.x + self.btnSingerLevel.width, imgLine1.y + lingHeight ,55 , cellHeight)];
    self.btnFlowerNum.titleLabel.font = [UIFont systemFontOfSize:12.0];
    [self.btnFlowerNum setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateNormal];
    [self.btnFlowerNum setTitleEdgeInsets:UIEdgeInsetsMake(0,15, 0, 0)];
    [self.btnFlowerNum setImage:flowerImg forState:UIControlStateNormal];
    [self.btnFlowerNum setTitle:[NSString stringWithFormat:@"%d",self.musicWorkBaseInfo.stUserBaseInfoNet.iFlowerNum] forState:UIControlStateNormal];
    [self.headView addSubview:self.btnFlowerNum];
    
    //地区
    CGFloat lbCityW = 26;
    CGFloat lbCityH = 27 * 0.5;
    self.lbCity = [[UILabel alloc]initWithFrame:CGRectMake(self.headView.width - fromX - lbCityW, imgLine1.y + lingHeight + (cellHeight - lbCityH) * 0.5, lbCityW, lbCityH)];
    [self.lbCity setTextColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0]];
    [self.lbCity setFont:[UIFont systemFontOfSize:12.0]];
    self.lbCity.text = self.musicWorkBaseInfo.stUserBaseInfoNet.sAddress;
    [self.headView addSubview:self.lbCity];
    
    
    // 横线
    UIImageView *imgLine2 = [[UIImageView alloc]initWithFrame:CGRectMake(fromX ,imgLine1.y + cellHeight, self.headView.width - fromX * 2, 1)];
    imgLine2.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
    [self.headView addSubview:imgLine2];
    
    //简介
    //背景
    UIImageView *introductionBgView =[[UIImageView alloc]initWithFrame:CGRectMake(fromX, imgLine2.y + lingHeight + 5, self.headView.width - fromX * 2, 2 * cellHeight - 10)];
    [introductionBgView setImage:[UIImage imageNamed:@"简介.png"]];
    [self.headView addSubview:introductionBgView];
    
    self.lbSongIntroduction = [[UILabel alloc] initWithFrame:CGRectMake(fromX, 10, 195, introductionBgView.height - 20)];
    [self.lbSongIntroduction setTextColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
    self.lbSongIntroduction.numberOfLines = 0;
    [self.lbSongIntroduction setFont:[UIFont systemFontOfSize:12.0]];
    self.lbSongIntroduction.text = @"北京的山东的朋友有没有周末有时间的咱们一起唱歌?";
    
    [introductionBgView addSubview:self.lbSongIntroduction];
    
    //私聊
    CGFloat privateH = 24; //私聊UI的高度
    UIImage *privateChat = [UIImage imageNamed:@"私聊.png"];
    UIImageView *privateChatView = [[UIImageView alloc]initWithFrame:CGRectMake(introductionBgView.width - fromX - 62, (introductionBgView.height - privateH )* 0.5, 62, privateH)];
    [privateChatView setImage:privateChat];
    
    
    self.btnPrivateChat = [[UIButton alloc]initWithFrame:privateChatView.bounds];
    [self.btnPrivateChat setTitle:@"私聊" forState:UIControlStateNormal];
    [self.btnPrivateChat setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateNormal];
    [self.btnPrivateChat.titleLabel setFont:[UIFont systemFontOfSize:12.0]];
    
    //添加私聊点击事件
    [self.btnPrivateChat addTarget:self action:@selector(privateChat:) forControlEvents:UIControlEventTouchUpInside];
    
    //打开ImageView的交互,否则会阻断button的点击事件
    privateChatView.userInteractionEnabled = YES;
    introductionBgView.userInteractionEnabled = YES;
    
    [privateChatView addSubview:self.btnPrivateChat];
    
    [introductionBgView addSubview:privateChatView];
    
    //设置底部视图
    self.footView = [[UIView alloc]initWithFrame:CGRectMake(0, screenHeight - 49, screenWidth, 49)];
    self.footView.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:self.footView];
    
    CGFloat btnW = screenWidth / 4;
    CGFloat btnH = 49;
    //收藏
    self.btnCollection = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, btnW, btnH)];
    UIImage *collectionImg = [UIImage imageNamed:@"songCollection.png"];
    UIImage *haveCollectImg = [UIImage imageNamed:@"songIsCollection.png"];
    [self.btnCollection setImage:collectionImg forState:UIControlStateNormal];
    [self.btnCollection setImage:haveCollectImg forState:UIControlStateSelected];
    self.btnCollection.selected = NO;
    [self.btnCollection addTarget:self action:@selector(collectionHandle:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.footView addSubview:self.btnCollection];
    
    //送花
    self.btnGiveFlower = [[UIButton alloc]initWithFrame:CGRectMake(btnW, 0, btnW, btnH)];
    UIImage *giveFloweImg = [UIImage imageNamed:@"songFlower.png"];
    [self.btnGiveFlower setImage:giveFloweImg forState:UIControlStateNormal];
    [self.btnGiveFlower addTarget:self action:@selector(giveFlowerHandle:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.footView addSubview:self.btnGiveFlower];
    
    //讨论
    self.btnDiscuss = [[UIButton alloc]initWithFrame:CGRectMake(btnW * 2, 0, btnW, btnH)];
    UIImage *discussImg = [UIImage imageNamed:@"songDiscuss.png"];
    [self.btnDiscuss setImage:discussImg forState:UIControlStateNormal];
    [self.btnDiscuss addTarget:self action:@selector(discussHandle:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.footView addSubview:self.btnDiscuss];
    
    
    //分享
    self.btnShare = [[UIButton alloc]initWithFrame:CGRectMake(btnW * 3, 0, btnW, btnH)];
    UIImage *shareImg = [UIImage imageNamed:@"songShare.png"];
    [self.btnShare setImage:shareImg forState:UIControlStateNormal];
    [self.btnShare addTarget:self action:@selector(shareHandle:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.footView addSubview:self.btnShare];
    
    UIView *tempView = [[UIView alloc]initWithFrame:CGRectMake(0, screenHeight, screenWidth, 150) ];
    tempView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:tempView];
}

#pragma mark - privateChat私聊
-(void)privateChat:(UIButton *)chatBtn{
    YDLog(@"向xxx发起私聊");
    FriendsMsgVC *chatWithFriend = [[FriendsMsgVC alloc]init];
    chatWithFriend.navigationItem.title = self.lbSingerName.text;
    chatWithFriend.friendUserBaseInfoNet = self.musicWorkBaseInfo.stUserBaseInfoNet;
    [ListenToMeData getInstance].privateChatUser = self.musicWorkBaseInfo.stUserBaseInfoNet;
    [self.navigationController pushViewController:chatWithFriend animated:YES];
}

#pragma mark - 收藏 送花 讨论 分享
-(void)collectionHandle:(UIButton *)collectionBtn{
    
    if ([ListenToMeDBManager getUuid] > 0) {
        
        if (collectionBtn.selected) {
            //取消收藏音乐作品
            collectionBtn.selected = NO;
            //发送取消收藏音乐作品请求
            
            [[NetReqManager getInstance] sendUnCollectWork:[ListenToMeDBManager getUuid] LMusicId:self.musicWorkBaseInfo.lMusicWorkId LCommemorateId:0];
        }else{
            //收藏音乐作品
            collectionBtn.selected = YES;
            //发送收藏音乐作品请求
            [[NetReqManager getInstance] sendCollectWork:[ListenToMeDBManager getUuid] LMusicId:self.musicWorkBaseInfo.lMusicWorkId LCommemorateId:0];
        }
    }
    
    
    
}

-(void)giveFlowerHandle:(UIButton *)giveFlowerBtn{
    YDLog(@"送花+1");
    
    //初始化遮盖视图
    self.coverView = [[UIView alloc]initWithFrame:[UIScreen mainScreen].bounds];
    self.coverView.backgroundColor = [UIColor clearColor];
    self.coverView.alpha  = 1.0;
    
    self.alertForGivingFlower = [[UIButton alloc]initWithFrame:[UIScreen mainScreen].bounds];
    //    [self.alertForGift setBackgroundColor:[UIColor clearColor]];
    //    self.alertForGift.alpha = 1;
    self.alertForGivingFlower.backgroundColor = [UIColor blackColor];
    self.alertForGivingFlower.alpha = 0.3;
    [self.alertForGivingFlower addTarget:self action:@selector(backToView) forControlEvents:UIControlEventTouchUpInside];
    [self.coverView addSubview:self.alertForGivingFlower];
    //弹出遮盖视图
    [[[UIApplication sharedApplication]keyWindow] addSubview:self.coverView];
    
    
    CGFloat sendFW = self.coverView.width - 30;
    CGFloat sendFH = 285;
    CGFloat sendFX = 15;
    CGFloat sendFY = 190;
    
    //设置送花的数目
    self.sendFlowerNum = 0;
    self.haveFlowerNum = self.listenToMeData.stUserBaseInfoNet.iFlowerNum;
    
    self.sendFlowerView = [[GivingFlowerView alloc]init];
    self.sendFlowerView.frame = CGRectMake(sendFX, sendFY, sendFW, sendFH);
//    self.sendFlowerView.receiveIcon = @"temp2.png";
    self.sendFlowerView.receiveIcon = self.musicWorkBaseInfo.stUserBaseInfoNet.sCovver;
    self.sendFlowerView.sendFlowerNum = self.sendFlowerNum;
    [self.sendFlowerView.btnGiveFlower addTarget:self action:@selector(sendFlowerHandle) forControlEvents:UIControlEventTouchUpInside];
    [self.sendFlowerView.btnSendFNum setTitle:[NSString stringWithFormat:@"%d",self.sendFlowerNum] forState:UIControlStateNormal];
    
    //设置颜色
    [self.sendFlowerView setLbTextForDifferentColor:self.sendFlowerView.lbRemain text:[NSString stringWithFormat:@"剩余%ld朵",(self.haveFlowerNum - self.sendFlowerNum)] rangeString:[NSString stringWithFormat:@"%ld",(self.haveFlowerNum - self.sendFlowerNum)] color:[UIColor rgbFromHexString:@"#FF0053" alpaa:1.0]];
    //减一
    [self.sendFlowerView.btnSubFNum addTarget:self action:@selector(subSendFNum) forControlEvents:UIControlEventTouchUpInside];
    //加一
    [self.sendFlowerView.btnAddFNum addTarget:self action:@selector(addSendFNum) forControlEvents:UIControlEventTouchUpInside];
    [self.coverView addSubview:self.sendFlowerView];
}

#pragma mark 送花 减一
-(void)subSendFNum{

    if (self.sendFlowerNum >= 1) {
        self.sendFlowerNum --;
        [self.sendFlowerView subFlower:self.sendFlowerNum];
        [self.sendFlowerView.btnSendFNum setTitle:[NSString stringWithFormat:@"%d",self.sendFlowerNum] forState:UIControlStateNormal];
        
        [self.sendFlowerView setLbTextForDifferentColor:self.sendFlowerView.lbRemain text:[NSString stringWithFormat:@"剩余%ld朵",(self.haveFlowerNum - self.sendFlowerNum)] rangeString:[NSString stringWithFormat:@"%ld",(self.haveFlowerNum - self.sendFlowerNum)] color:[UIColor rgbFromHexString:@"#FF0053" alpaa:1.0]];
    }
}

#pragma mark 送花 加一
-(void)addSendFNum{

    if (self.haveFlowerNum - self.sendFlowerNum > 0) {
        self.sendFlowerNum ++;
        [self.sendFlowerView addFlower:self.sendFlowerNum];
        [self.sendFlowerView.btnSendFNum setTitle:[NSString stringWithFormat:@"%d",self.sendFlowerNum] forState:UIControlStateNormal];
        [self.sendFlowerView setLbTextForDifferentColor:self.sendFlowerView.lbRemain text:[NSString stringWithFormat:@"剩余%ld朵",(self.haveFlowerNum - self.sendFlowerNum)] rangeString:[NSString stringWithFormat:@"%ld",(self.haveFlowerNum - self.sendFlowerNum)] color:[UIColor rgbFromHexString:@"#FF0053" alpaa:1.0]];
    }
}

#pragma mark - sendFlowerHandle 确定送花
-(void)sendFlowerHandle{
    //发送送花请求
    if (self.sendFlowerNum >= 1) {
        [[NetReqManager getInstance] sendGetSendUserFlower:[ListenToMeDBManager getUuid] LUserId:self.musicWorkBaseInfo.stUserBaseInfoNet.uuid IFlowerNum:self.sendFlowerNum];
        
    }
    
    [self.sendFlowerView.superview removeFromSuperview];
//    [self.sendFlowerView removeFromSuperview];
}

#pragma mark - 参与讨论
-(void)discussHandle:(UIButton *)discussBtn{
    
    if ([ListenToMeDBManager getUuid] > 0) {
        self.footView.hidden = YES;
        [self.tfOpinion becomeFirstResponder];
        
        self.opinionView.frame = CGRectMake(0, screenHeight - 55, screenWidth, 55);
        self.view.frame = CGRectMake(0, -150, screenWidth, screenHeight);
    }else{
        YDLog(@"你还未登录,无法发表评论");
    }

   
}

#pragma mark - 分享歌曲
-(void)shareHandle:(UIButton *)shareBtn{

    YDLog(@"分享到...");
}

#pragma mark - musicPlayHandle 播放
-(void)musicPlayHandle:(UIButton *)playBtn{

    if (!_isPlaying) {
        self.btnSongPlay.selected = YES;
        _isPlaying = YES;
    }else{
        self.btnSongPlay.selected = NO;
        _isPlaying = NO;
    }
}

#pragma mark - TableView
-(void)setTableViewUI{
    
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, self.headView.height + self.headView.y, screenWidth, screenHeight - self.headView.y - self.headView.height - 49)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.bounces = NO;
    [self.tableView registerClass:[YDRoomMsgCell class] forCellReuseIdentifier:@"msgRecordCell"];
    [self.view addSubview:self.tableView];
    
}

#pragma mark - tableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.listenToMeData.arrCommentListInfo.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 130;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    
    YDRoomMsgCell * cell = [tableView dequeueReusableCellWithIdentifier:@"msgRecordCell"];
    if (cell) {
        cell = [[YDRoomMsgCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"msgRecordCell"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.contentView.backgroundColor =[UIColor rgbFromHexString:@"#F1F1F1" alpaa:1.0];
    if (self.listenToMeData.arrCommentListInfo != nil) {
        CommentInfo *commentInfo = self.listenToMeData.arrCommentListInfo[indexPath.row];//(self.listenToMeData.arrCommentListInfo.count - indexPath.row -1)
        UserBaseInfoNet *userBaseInfoNet = commentInfo.stUserBaseInfoNet;
        [cell.avatarImgView sd_setImageWithURL:[NSURL URLWithString:userBaseInfoNet.sCovver] placeholderImage:[UIImage imageNamed:@"icon.png"]];
        cell.lbFromPerson.text = userBaseInfoNet.sNick;
        cell.lbContents.text = commentInfo.sCommentContent;
        cell.lbTime.text = [self getCreatedTime:commentInfo.lCommentTime];
    }
    
    return cell;
}


-(void)backToView{

    [self.coverView removeFromSuperview];
}


#pragma mark - 计算时间间隔
-(NSString *)getCreatedTime:(int64_t)lCreateTime{
    
    int64_t time = lCreateTime;
    NSDate *createdDate = [[NSDate alloc]initWithTimeIntervalSince1970:time/1000.0];
    
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"EEE MMM dd HH:mm:ss Z yyyy";
    
#warning 真机调试下, 必须加上这段
    fmt.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    
    // 2..判断创建时间 和 现在时间 的差距
    if (createdDate.isToday) { // 今天
        if (createdDate.deltaWithNow.hour >= 1) {
            return [NSString stringWithFormat:@"%ld小时前", createdDate.deltaWithNow.hour];
        } else if (createdDate.deltaWithNow.minute >= 1) {
            return [NSString stringWithFormat:@"%ld分钟前", createdDate.deltaWithNow.minute];
        } else {
            return @"刚刚";
        }
    } else if (createdDate.isYesterday) { // 昨天
        fmt.dateFormat = @"昨天 HH:mm";
        return [fmt stringFromDate:createdDate];
    } else if (createdDate.isThisYear) { // 今年(至少是前天)
        fmt.dateFormat = @"MM-dd";
        return [fmt stringFromDate:createdDate];
    } else { // 非今年
        fmt.dateFormat = @"yyyy-MM-dd";
        return [fmt stringFromDate:createdDate];
    }
    
}

#pragma mark - 设置弹出讨论视图
-(void)setDiscussView{
    //参与讨论
    CGFloat opinionH = 50;
    CGFloat opinionW = screenWidth;
    CGFloat opinionX = 0;
    CGFloat opinionY = screenHeight;//screenHeight - 55;
    self.opinionView = [[UIImageView alloc]initWithFrame:CGRectMake(opinionX, opinionY, opinionW, opinionH)];
    self.opinionView.backgroundColor = [UIColor whiteColor];
//    UIImage *opinionBgImg = [UIImage imageNamed:@"opinionBgImg"];
//    [self.opinionView setImage:opinionBgImg];
    self.opinionView.userInteractionEnabled = YES;
    [self.view addSubview:self.opinionView];
//    self.opinionView.hidden = YES;
    
    CGFloat tfOpW = self.opinionView.width - 30;
    CGFloat tfOpH = 30;
    CGFloat tfOpX = 15;
    CGFloat tfOpY = (self.opinionView.height - tfOpH) * 0.5;
    self.tfOpinion = [[UITextField alloc]initWithFrame:CGRectMake(tfOpX, tfOpY, tfOpW, tfOpH)];
    [self.tfOpinion setFont:[UIFont systemFontOfSize:12.0]];
    [self.tfOpinion setBackgroundColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:0.3]];
    self.tfOpinion.placeholder = @"参与讨论";
    self.tfOpinion.layer.masksToBounds = YES;
    self.tfOpinion.layer.cornerRadius = 5;
    self.tfOpinion.keyboardType = UIKeyboardAppearanceDefault;
    self.tfOpinion.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.tfOpinion.delegate = self;
    [self.opinionView addSubview:self.tfOpinion];
    
    
}

#pragma mark - 监测键盘
- (void)registerForKeyboardNotifications
{
    //使用NSNotificationCenter 鍵盤出現時
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWasShown:)
     
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    //使用NSNotificationCenter 鍵盤隐藏時
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWillBeHidden:)
     
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    
}

//实现当键盘出现的时候计算键盘的高度大小。用于输入框显示位置
- (void)keyboardWasShown:(NSNotification*)aNotification
{
   
    
    NSDictionary* info = [aNotification userInfo];
    //kbSize即為鍵盤尺寸 (有width, height)
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;//得到鍵盤的高度
    keyboardhight = kbSize.height;
    //输入框位置动画加载
    [self begainMoveUpAnimation:keyboardhight];
    
}

-(void)begainMoveUpAnimation:(NSInteger) keyboardH{
    
    CGFloat opinionH = 55;
    CGFloat opinionW = screenWidth;
    CGFloat opinionX = 0;
    CGFloat opinionY = screenHeight - 55 - keyboardH + self.songView.height;
    self.opinionView.frame = CGRectMake(opinionX, opinionY, opinionW, opinionH);
}

//当键盘隐藏的时候
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    CGFloat opinionH = 55;
    CGFloat opinionW = screenWidth;
    CGFloat opinionX = 0;
    CGFloat opinionY = screenHeight;
    self.view.frame = CGRectMake(0, 0, screenWidth, screenHeight);
    self.opinionView.frame = CGRectMake(opinionX, opinionY, opinionW, opinionH);
    self.footView.hidden = NO;
    
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
   
    if(textField.text.length > 0){
        
        //评论音乐作品请求
        [[NetReqManager getInstance] sendAddCommentMusic:[ListenToMeDBManager getUuid] LMusicWorkId:self.musicWorkBaseInfo.lMusicWorkId SContent:textField.text];
    }
    
    //显示讨论的底图视图

    CGFloat opinionH = 55;
    CGFloat opinionW = screenWidth;
    CGFloat opinionX = 0;
    CGFloat opinionY = screenHeight;
    self.view.frame = CGRectMake(0, 0, screenWidth, screenHeight);
    self.opinionView.frame = CGRectMake(opinionX, opinionY, opinionW, opinionH);
    self.footView.hidden = NO;
    
    //将输入框内的内容清空
    textField.text = nil;
    
    //关闭键盘
    [textField resignFirstResponder];
    return NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
