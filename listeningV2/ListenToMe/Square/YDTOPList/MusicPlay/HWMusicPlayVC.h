//
//  HWMusicPlayVC.h
//  ListenToMe
//
//  Created by zhw on 15/4/13.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "YDBaseVC.h"

@interface HWMusicPlayVC : YDBaseVC
/**
 *  用户信息
 */
@property(nonatomic,strong) UserBaseInfoNet *userBaseInfoNet;
/**
 *  试听音乐作品的ID
 */
@property(nonatomic,assign) int64_t lWorkID;
/**
 *  播放的音乐作品信息
 */
@property(nonatomic,strong) MusicWorkBaseInfo *musicWorkBaseInfo;
@end
