//
//  DaysMusicVC.m
//  ListenToMe
//
//  Created by yadong on 15/3/23.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "DaysMusicVC.h"
#import "YDTopListCell.h"
#import "HWShareAlertVC.h"
#import "MoreNavigationMenView.h"
#import "HMPopMenu.h"
#import "HWMusicPlayVC.h"
#import "HWPerHomePageVC.h"
#import "LoginAlertVC.h"
@interface DaysMusicVC ()<YDTopLiseCellDelegate>
/**
 *  分享提示
 */
@property(strong,nonatomic)HWShareAlertVC *shareVC;
/**
 *  跟多导航菜单
 */
@property(strong,nonatomic)MoreNavigationMenView *moreMenuView;
@property(nonatomic,strong) ListenToMeData *listenToMeData;
@property(nonatomic,strong)LoginAlertVC *loginAlertVC;
@end

@implementation DaysMusicVC


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNavigation];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor rgbFromHexString:@"#F1F1F1" alpaa:1.0];
    
    
    [self initData];
    
}

-(void)initData{
    
    self.listenToMeData = [ListenToMeData getInstance];
    
    if ([ListenToMeDBManager getUuid] > 0) {
        
        [[NetReqManager getInstance] sendGetCollectWorkList:[ListenToMeDBManager getUuid] IOffset:0 INum:20];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifiLoadData) name:NOTIFY_GET_COLLECT_WORKLIST_PAGE_RESP object:nil];
    
    
    if (self.ktvStartType == KTVSTARTYPE_PopularityList) {
        [[NetReqManager getInstance] sendGetPopularityList:0 IOffset:0 INum:5];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifiLoadData) name:NOTIFY_GET_POPULARITYLIST_PAGE_RESP object:nil];
    }
    
    if(self.ktvStartType == KTVSTARTYPE_NewMusicList){
        [[NetReqManager getInstance] sendGetNewMusicList:0 IOffset:0 INum:5];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifiLoadData) name:NOTIFY_GET_NEWMUSICLIST_PAGE_RESP object:nil];
    }
    
    if(self.ktvStartType == KTVSTARTYPE_RecommendList){
        [[NetReqManager getInstance] sendGetRecommendList:0 IOffset:0 INum:5];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifiLoadData) name:NOTIFY_GET_RECOMMENDLIST_PAGE_RESP object:nil];
    }
    
    
}

-(void)notifiLoadData{

    [self.tableView reloadData];
    
}

-(void)dealloc{
    if (self.ktvStartType == KTVSTARTYPE_PopularityList) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_POPULARITYLIST_PAGE_RESP object:nil];
    }
    if (self.ktvStartType == KTVSTARTYPE_NewMusicList) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_NEWMUSICLIST_PAGE_RESP object:nil];
    }
    
    if (self.ktvStartType == KTVSTARTYPE_RecommendList) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_RECOMMENDLIST_PAGE_RESP object:nil];
    }
    
    
    
}

#pragma mark tableview
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return 1;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (self.ktvStartType == KTVSTARTYPE_PopularityList) {
        return self.listenToMeData.arrPopularityList.count;
    }
    if (self.ktvStartType == KTVSTARTYPE_NewMusicList) {
        return self.listenToMeData.arrNewMusicList.count;
    }
    if (self.ktvStartType == KTVSTARTYPE_RecommendList) {
        return self.listenToMeData.arrRecommendList.count;
    }
    
    return 5;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *ID = @"daysCell";
    YDTopListCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[YDTopListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.delegate = self;
    
    
    
    MusicWorkBaseInfo *musicWorkBaseInfo = nil;
    
    if (self.ktvStartType == KTVSTARTYPE_PopularityList) {
        
        musicWorkBaseInfo = self.listenToMeData.arrPopularityList[indexPath.section];
    }
    if(self.ktvStartType == KTVSTARTYPE_NewMusicList){
        
        musicWorkBaseInfo = self.listenToMeData.arrNewMusicList[indexPath.section];
    }
    if(self.ktvStartType == KTVSTARTYPE_RecommendList){
        
        musicWorkBaseInfo = self.listenToMeData.arrRecommendList[indexPath.section];
    }
    
    
    UserBaseInfoNet *userBaseInfoNet = musicWorkBaseInfo.stUserBaseInfoNet;
    SongInfo *songInfo = musicWorkBaseInfo.stSongInfo;
    cell.lbMusicName.text = songInfo.sSongName;
    cell.lbSinger.text = songInfo.sSongSinger;
    [cell.imgAvatar sd_setImageWithURL:[NSURL URLWithString:userBaseInfoNet.sCovver]];
    
    UIButton *btnAvatar = [[UIButton alloc]initWithFrame:cell.imgAvatar.frame];
    btnAvatar.backgroundColor = [UIColor clearColor];
    [cell.contentView addSubview:btnAvatar];
    btnAvatar.tag = 1000 + indexPath.section;
    [btnAvatar addTarget:self action:@selector(clickAvatar:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [cell.btnFlower setTitle:[NSString stringWithFormat:@"%d",musicWorkBaseInfo.iFlowerNum] forState:UIControlStateNormal];
    [cell.btnListener setTitle:[NSString stringWithFormat:@"%d",musicWorkBaseInfo.iListenNum] forState:UIControlStateNormal];
    
    
    cell.btnCollection.tag = 100 + indexPath.section;
    cell.btnCollection.selected = NO;
    [cell.btnCollection addTarget:self action:@selector(clickCollectMusicWorkHandle:) forControlEvents:UIControlEventTouchUpInside];
#warning 判断获取的音乐作品是否已经被收藏了,如果已经被收藏了就改变收藏按钮的状态否则保持在未收藏的状态.
    if (self.listenToMeData.arrCollectWorkInfo != nil) {
        
        for (CollectWorkInfo *collectWrokInfo  in self.listenToMeData.arrCollectWorkInfo) {
            if (collectWrokInfo.stMusicWorkBaseInfo.lMusicWorkId == musicWorkBaseInfo.lMusicWorkId) {
                cell.btnCollection.selected = YES;
            }
        }
    }
    
    
    return  cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 155;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}

#pragma mark - tableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    MusicWorkBaseInfo *musicWorkBaseInfo = nil;
    
    if (self.ktvStartType == KTVSTARTYPE_PopularityList) {
        
        musicWorkBaseInfo = self.listenToMeData.arrPopularityList[indexPath.section];
    }
    if(self.ktvStartType == KTVSTARTYPE_NewMusicList){
        
        musicWorkBaseInfo = self.listenToMeData.arrNewMusicList[indexPath.section];
    }
    if(self.ktvStartType == KTVSTARTYPE_RecommendList){
        
        musicWorkBaseInfo = self.listenToMeData.arrRecommendList[indexPath.section];
    }
    
    
    HWMusicPlayVC *musicPlayVC =[[HWMusicPlayVC alloc]init];
    musicPlayVC.musicWorkBaseInfo = musicWorkBaseInfo;
    musicPlayVC.lWorkID = musicWorkBaseInfo.lMusicWorkId;
    [self.navigationController pushViewController:musicPlayVC animated:YES];
    
}


#pragma mark - 点击了头像
-(void)clickAvatar:(UIButton *)btnAvatar{
    NSInteger tempTag = btnAvatar.tag - 1000;
    MusicWorkBaseInfo *musicWorkBaseInfo = nil;
    if (self.ktvStartType == KTVSTARTYPE_PopularityList) {
        musicWorkBaseInfo = self.listenToMeData.arrPopularityList[tempTag];
    }
    if (self.ktvStartType == KTVSTARTYPE_NewMusicList) {
        musicWorkBaseInfo = self.listenToMeData.arrNewMusicList[tempTag];
    }
    
    if (self.ktvStartType == KTVSTARTYPE_RecommendList) {
        musicWorkBaseInfo = self.listenToMeData.arrRecommendList[tempTag];
    }
    HWPerHomePageVC *perHomePageVC = [[HWPerHomePageVC alloc]init];
    perHomePageVC.userBaseInfoNet = musicWorkBaseInfo.stUserBaseInfoNet;
    [self.navigationController pushViewController:perHomePageVC animated:YES];
    
}

#pragma mark 导航栏
-(void)setNavigation
{
//    self.navigationItem.title = @"每日金曲";
    [self setLeftBtnItem];
    [self setRightBtnItem];
}

// 导航栏左边的Btn
-(void)setLeftBtnItem
{
    UIButton *leftBtn = [[UIButton alloc]init];
    UIImage *leftBtnImg = [UIImage imageNamed:@"whiteBack.png"];
    leftBtn.frame = CGRectMake(0, 0, leftBtnImg.size.width, leftBtnImg.size.height);
    [leftBtn setImage:leftBtnImg forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(clickLeftBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customLeftBtnItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = customLeftBtnItem;
}

// 导航栏右边的Btn
-(void)setRightBtnItem
{
    UIButton *rightBtn = [[UIButton alloc]init];
    UIImage *imgMore = [UIImage imageNamed:@"more.png"];
    rightBtn.frame = CGRectMake(0, 0, imgMore.size.width, imgMore.size.height);
    [rightBtn setImage:imgMore forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(clickRightBarBtnItem) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customRightBtnItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = customRightBtnItem;
}


#pragma mark -点击事件
-(void)clickRightBarBtnItem
{
    YDLog(@"点击了导航栏的右边按钮");
    
    UIImage *popImg = [UIImage imageNamed:@"popMenuBg.png"];
    CGFloat popMenuW = popImg.size.width;
    CGFloat popMenuH = popImg.size.height;
    
    self.moreMenuView = [[MoreNavigationMenView alloc]initWithFrame:CGRectMake(0, 0, popMenuW, popMenuH)];
    HMPopMenu *menu = [HMPopMenu popMenuWithContentView:self.moreMenuView];
    CGFloat menuW = popMenuW;
    CGFloat menuH = popMenuH;
    CGFloat menuY = 55;
    CGFloat menuX = self.view.width - menuW - 20;
    menu.dimBackground = YES;
    menu.arrowPosition = HMPopMenuArrowPositionRight;
    [menu showInRect:CGRectMake(menuX, menuY, menuW, menuH)];
}

-(void)clickLeftBarBtnItem
{
//    self.listenToMeData = nil;
    if (self.ktvStartType == KTVSTARTYPE_PopularityList) {
        self.listenToMeData.arrPopularityList = nil;
        [[NetReqManager getInstance] sendGetPopularityList:0 IOffset:0 INum:3];
    }
    if (self.ktvStartType == KTVSTARTYPE_NewMusicList) {
        self.listenToMeData.arrNewMusicList = nil;
        [[NetReqManager getInstance] sendGetNewMusicList:0 IOffset:0 INum:2];
    }
    if (self.ktvStartType == KTVSTARTYPE_RecommendList) {
        self.listenToMeData.arrRecommendList = nil;
        [[NetReqManager getInstance] sendGetRecommendList:0 IOffset:0 INum:2];
    }
    [self.navigationController popToRootViewControllerAnimated:YES];
   
    YDLog(@"点击了导航栏左边的Btn");
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark -cell中点击分享提示分享平台
-(void)alertForShare{
    _shareVC = [[HWShareAlertVC alloc]init];
    [[[UIApplication sharedApplication]keyWindow]addSubview:_shareVC.view];
    
//    [self.view.window.rootViewController presentViewController:_shareVC animated:NO completion:nil];


}


#pragma mark - 点击收藏音乐作品按钮
-(void)clickCollectMusicWorkHandle:(UIButton *)btnCollection{
    NSInteger tempTag = btnCollection.tag - 100;
    YDLog(@"%ld",tempTag);
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"launchStaue"]) {
        
        MusicWorkBaseInfo *musicWorkBaseInfo = nil;
        
        if (self.ktvStartType == KTVSTARTYPE_PopularityList) {
            musicWorkBaseInfo = self.listenToMeData.arrPopularityList[tempTag];
        }
        if (self.ktvStartType == KTVSTARTYPE_NewMusicList) {
            musicWorkBaseInfo = self.listenToMeData.arrNewMusicList[tempTag];
        }
        if (self.ktvStartType == KTVSTARTYPE_RecommendList) {
            musicWorkBaseInfo = self.listenToMeData.arrRecommendList[tempTag];
        }
        
#warning 判断收藏需要使用歌曲作品
        
        if (btnCollection.selected) {
            btnCollection.selected = NO;
            [[NetReqManager getInstance] sendUnCollectWork:[ListenToMeDBManager getUuid] LMusicId:musicWorkBaseInfo.lMusicWorkId LCommemorateId:0];
        }else{
            btnCollection.selected = YES;
            
            [[NetReqManager getInstance] sendCollectWork:[ListenToMeDBManager getUuid] LMusicId:musicWorkBaseInfo.lMusicWorkId LCommemorateId:0];
        }
        
#warning 获取用户的收藏列表 , 通过遍历收藏的列表来判读该作品是否被收藏
        if ([ListenToMeDBManager getUuid] > 0) {
            [[NetReqManager getInstance] sendGetCollectWorkList:[ListenToMeDBManager getUuid] IOffset:0 INum:20];
            
        }
        
    }else{
        [self alertForLogin];
    }
    
}

#pragma mark - 点击收藏如果处于非登录状态下弹出登录提示框
-(void)alertForLogin{
    
    self.loginAlertVC = [[LoginAlertVC alloc]init];
    [[[UIApplication sharedApplication]keyWindow] addSubview:self.loginAlertVC.view];
    [self.tableView reloadData];
}

@end
