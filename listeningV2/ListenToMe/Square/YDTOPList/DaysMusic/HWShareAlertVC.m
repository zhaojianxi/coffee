//
//  HWShareAlertVC.m
//  ListenToMe
//
//  Created by zhw on 15/3/30.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "HWShareAlertVC.h"
#import "UMSocialWechatHandler.h"
#import "UMSocialQQHandler.h"
#import "UMSocialSinaHandler.h"
@interface HWShareAlertVC ()<UIGestureRecognizerDelegate,UMSocialDataDelegate,UMSocialUIDelegate>
/**
 *  添加点击手势
 */
@property(strong,nonatomic)UITapGestureRecognizer *singleTap;
@end

@implementation HWShareAlertVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.frame =[UIScreen mainScreen].bounds;
    self.view.backgroundColor = [UIColor colorWithRed:203/255.0 green:203/255.0 blue:203/255.0 alpha:0.5];
    
//    UIView *backView = [[UIView alloc]initWithFrame:self.view.bounds];
//    backView.backgroundColor =[UIColor rgbFromHexString:@"#000000" alpaa:0.3];
//    [self.view addSubview:backView];
    
    //布局视图
    [self setUI];
    
#warning 临时测试音乐分享,需要用添加一个歌曲的连接地址的属性songUrl,需要从其他VC中传入要分享的歌曲URL地址
    [UMSocialWechatHandler setWXAppId:@"wxdc215ed7fa8c6824" appSecret:@"b42a9afd3244024d8350649278926645" url:@"http://182.92.214.218/data/ktv_data/huasha.mp3"];
    
    
    [UMSocialQQHandler setQQWithAppId:@"100424468" appKey:@"c7394704798a158208a74ab60104f0ba" url:@"http://182.92.214.218/data/ktv_data/huasha.mp3"];//
    [UMSocialQQHandler setSupportWebView:YES];
    //
    [UMSocialSinaHandler openSSOWithRedirectURL:@"http://sns.whalecloud.com/sina2/callback"];
}


#pragma mark - 布局登录提示的视图
-(void)setUI{
    
    
    
    //添加一个点击手势
    _singleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(backToView)];
    [self.view addGestureRecognizer:_singleTap];
    _singleTap.delegate = self;
    _singleTap.cancelsTouchesInView = YES;
    
    //布局子视图
    [self setSubView];
    
}
#pragma mark - UIGestrueRecognizerDelegate
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}


#pragma mark - 布局子视图
-(void)setSubView{
    CGFloat xDistance = 30;
    
    UIView *alertView = [[UIView alloc]initWithFrame:CGRectMake(xDistance, 191, screenWidth - xDistance *2, 146)];
    
    alertView.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
    
    UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(0, 45, alertView.width, 1)];
    line.backgroundColor = [UIColor whiteColor];
    [alertView addSubview:line];
    
    CGFloat bWidth = 14;//button的宽高
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(alertView.width - bWidth - 10, bWidth , bWidth, bWidth)];
    [backButton setImage:[UIImage imageNamed:@"whiteButten.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backToView) forControlEvents:UIControlEventTouchUpInside];
    [alertView addSubview:backButton];
    
    CGFloat label_x = 10;
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(label_x, 90, alertView.width - label_x * 2, 17.5)];
    title.text = @"加入听我的";
    [title setTextColor:[UIColor rgbFromHexString:@"#FFFFFF" alpaa:1.0]];
    [title setTextAlignment:NSTextAlignmentLeft];
    [title setFont:[UIFont systemFontOfSize:15.0]];
    [alertView addSubview:title];
    
    UILabel *detail = [[UILabel alloc]initWithFrame:CGRectMake(label_x, title.y + title.height, title.width, 14.5)];
    detail.text = @"成为最善良(cuican)的那颗星";
    [detail setTextColor:[UIColor rgbFromHexString:@"#FFFFFF" alpaa:1.0]];
    [detail setFont:[UIFont systemFontOfSize:12.0]];
    [detail setTextAlignment:NSTextAlignmentLeft];
    [alertView addSubview:detail];
    
    [alertView resignFirstResponder];
    
    //选择分享方式 微信 QQ 新浪微博
    
    UIView *shareView = [[UIView alloc]initWithFrame:CGRectMake(alertView.x, alertView.y + alertView.height, alertView.width, 130)];
    shareView.backgroundColor = [UIColor rgbFromHexString:@"#FFFFFF" alpaa:1.0];
    
    UILabel *title2 = [[UILabel alloc]initWithFrame:CGRectMake(xDistance * 2, 16 , shareView.width - xDistance * 4, 13.5)];
    title2.text = @"通过以下方式加入";
    [title2 setTextColor:[UIColor grayColor]];
    [title2 setFont:[UIFont systemFontOfSize:12.0]];
    [title2 setTextAlignment:NSTextAlignmentLeft];
    [shareView addSubview:title2];
    
    
    CGFloat btWidth = 60;//button宽高
    CGFloat bt_x = 25;//button之间的间隔
    //微信分享
    UIButton *weiXinBtn = [[UIButton alloc]initWithFrame:CGRectMake(title2.x, title2.y + title2.height + 5, btWidth, btWidth)];
    [weiXinBtn setImage:[UIImage imageNamed:@"weiXin.png"] forState:UIControlStateNormal];
    [weiXinBtn addTarget:self action:@selector(weiXinShareHandle:) forControlEvents:UIControlEventTouchUpInside];
    [shareView addSubview:weiXinBtn];
    
    //QQ分享
    UIButton *qqBtn = [[UIButton alloc]initWithFrame:CGRectMake(weiXinBtn.x + btWidth + bt_x, weiXinBtn.y, btWidth, btWidth)];
    [qqBtn setImage:[UIImage imageNamed:@"qq.png"] forState:UIControlStateNormal];
    [qqBtn addTarget:self action:@selector(qqShareHandle:) forControlEvents:UIControlEventTouchUpInside];
    [shareView addSubview:qqBtn];
    //新浪分享
    UIButton *xinLangBtn = [[UIButton alloc]initWithFrame:CGRectMake(qqBtn.x + btWidth + bt_x ,weiXinBtn.y, btWidth, btWidth)];
    [xinLangBtn setImage:[UIImage imageNamed:@"sina.png"] forState:UIControlStateNormal];
    [xinLangBtn addTarget:self action:@selector(sinaShareHandle:) forControlEvents:UIControlEventTouchUpInside];
    [shareView addSubview:xinLangBtn];
    

    [self.view addSubview:alertView];
    [self.view addSubview:shareView];
}

#pragma mark - 平台分享点击事件
-(void)weiXinShareHandle:(UIButton *)shareBtn{
    
    //听我的,一款KTV音乐交友APP值得拥有。
    
    //平台分享
//    [UMSocialSnsService presentSnsIconSheetView:self appKey:APPKEY shareText:@"" shareImage:[UIImage imageNamed:@"icon.png"] shareToSnsNames:@[UMShareToWechatSession,UMShareToWechatTimeline,UMShareToWechatFavorite] delegate:self];
    
    
    //自定义分享
//    [[UMSocialDataService defaultDataService] postSNSWithTypes:@[UMShareToWechatTimeline] content:@"听我的,一款KTV音乐交友APP值得拥有" image:[UIImage imageNamed:@"icon.png"] location:nil urlResource:nil presentedController:self completion:^(UMSocialResponseEntity *response) {
//        if (response.responseCode == UMSResponseCodeSuccess) {
//            YDLog(@"weixin Share success");
//        }
//    }];
    
//    [UMSocialData defaultData].extConfig.wechatTimelineData.url = @"http://www.tingwode.com/";
//    
//    [UMSocialData defaultData].extConfig.wechatTimelineData.title = @"Listen To Me";
//    
    
    [[UMSocialControllerService defaultControllerService] setShareText:@"听我的,一款KTV音乐交友APP值得拥有。" shareImage:[UIImage imageNamed:@"icon.png"] socialUIDelegate:self];
    
    [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToWechatTimeline].snsClickHandler(self,[UMSocialControllerService defaultControllerService],YES);
}

-(void)qqShareHandle:(UIButton *)shareBtn{

    //平台分享
//    [UMSocialSnsService presentSnsIconSheetView:self appKey:APPKEY shareText:@"this is a test for share" shareImage:[UIImage imageNamed:@"icon.png"] shareToSnsNames:[NSArray arrayWithObjects:UMShareToQQ, nil] delegate:nil];
    
    
    //自定义分享
    [[UMSocialDataService defaultDataService] postSNSWithTypes:@[UMShareToQQ] content:@"听我的,一款KTV音乐交友APP值得拥有。" image:[UIImage imageNamed:@"icon.png"] location:nil urlResource:nil presentedController:self completion:^(UMSocialResponseEntity *response) {
        if (response.responseCode == UMSResponseCodeSuccess) {
            YDLog(@"QQ Share success");
            
        }
        
    }];
    
    [UMSocialData defaultData].extConfig.qqData.url = @"http://www.tingwode.com/";
    
    [UMSocialData defaultData].extConfig.qqData.title = @"Listen To Me";
}

-(void)sinaShareHandle:(UIButton *)shareBtn{

//    平台分享
//    [UMSocialSnsService presentSnsIconSheetView:self appKey:APPKEY shareText:@"this is a test for share" shareImage:[UIImage imageNamed:@"icon.png"] shareToSnsNames:[NSArray arrayWithObjects:UMShareToSina, nil] delegate:self];
    
    //自定义分享
    
    [UMSocialData defaultData].extConfig.sinaData.urlResource.url = @"http://www.tingwode.com/";
    
    
    
    //有分享编辑页面
    [[UMSocialControllerService defaultControllerService] setShareText:@"听我的,一款KTV音乐交友APP值得拥有,http://182.92.214.218/data/ktv_data/huasha.mp3" shareImage:[UIImage imageNamed:@"icon.png"] socialUIDelegate:self];
    //设置分享内容和回调对象
    [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToSina].snsClickHandler(self,[UMSocialControllerService defaultControllerService],YES);
//
//    //可以在控制台输出相应的错误代码
//    [UMSocialData openLog:YES];
    
    
    /*
    //直接分享底层接口(授权 + 分享接口分离)
    [UMSocialAccountManager isOauthAndTokenNotExpired:UMShareToSina];
    
    //进入授权页面
    [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToSina].loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
        if (response.responseCode == UMSResponseCodeSuccess) {
            //获取微博用户名 uid token等
            UMSocialAccountEntity *snsAcccount = [[UMSocialAccountManager socialAccountDictionary]valueForKey:UMShareToSina];
            YDLog(@"userName is %@, uid is %@ ,token is %@",snsAcccount.userName,snsAcccount.usid,snsAcccount.accessToken);
            
            //进入分享页面
            
     
     
            //直接后台分享
//            
//            [[UMSocialDataService defaultDataService] postSNSWithTypes:@[UMShareToSina] content:@"听我的,一款KTV音乐交友APP值得拥有,http://www.tingwode.com/" image:[UIImage imageNamed:@"icon.png"] location:nil urlResource:nil presentedController:self completion:^(UMSocialResponseEntity *response) {
//                
//                if (response.responseCode == UMSResponseCodeSuccess) {
//                    YDLog(@"Sina Share Success");
//                }
//            }];
            
        }
    
    
    });
    
    */
    
    
}

#pragma mark - UMSocial Delegate

-(void)didFinishGetUMSocialDataInViewController:(UMSocialResponseEntity *)response{
    
    //根据responseCode得到的发送的结果
    if (response.responseCode == UMSResponseCodeSuccess) {
        //得到分享到的平台名称
        YDLog(@"You share to sns name is %@",[[response.data allKeys] objectAtIndex:0]);
        [self.view removeFromSuperview];
    }

}
-(void)didFinishGetUMSocialDataResponse:(UMSocialResponseEntity *)response{

    //根据responseCode得到的发送的结果
    if (response.responseCode == UMSResponseCodeSuccess) {
        //得到分享到的平台名称
        YDLog(@"You share to sns name is %@",[[response.data allKeys] objectAtIndex:0]);
    }

}

//-(BOOL)isDirectShareInIconActionSheet{
//    //点击分享进入内容编辑页面,若想点击后直接分享,不进行编辑,返回值要设为yes,默认为no
//    return YES;
//}

#pragma mark -backHandle点击事件,移除登录提示视图
-(void)backToView{

    [self.view removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
