//
//  DaysMusicVC.h
//  ListenToMe
//
//  Created by yadong on 15/3/23.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(SInt32, KTVSTARTYPE){
    KTVSTARTYPE_PopularityList = 0,
    KTVSTARTYPE_NewMusicList = 1,
    KTVSTARTYPE_RecommendList = 2

} ;

@interface DaysMusicVC : UITableViewController
/**
 *  K星电台中三个分类, 歌王秀0 新歌推荐1(深情时空) 每日金曲2(时空胶囊)
 */
@property(nonatomic,assign) int ktvStartType;
@end
