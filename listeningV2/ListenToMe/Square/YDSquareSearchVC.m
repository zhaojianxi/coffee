//
//  YDSquareSearchVC.m
//  ListenToMe
//
//  Created by zhw on 3/10/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "YDSquareSearchVC.h"
#import "AppDelegate.h"
#import "SWRevealViewController.h"
@interface YDSquareSearchVC ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>
/**
 *  自定义搜索框
 */
@property(nonatomic,strong) UITextField *mysearchBar;
/**
 *  数据
 */
@property(nonatomic,strong) ListenToMeData *listenToMeData;
/**
 *  用于展示搜索结果
 */
@property(nonatomic,strong) UITableView *mTableView;
@end

@implementation YDSquareSearchVC
@synthesize mysearchBar;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self customNavigationBar];
 
    [self setTableView];
    
    [self initData];
#if USE_SLIDING_MENU
    //获取根视图控制器
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    SWRevealViewController *revealController = (SWRevealViewController *)delegate.window.rootViewController;
    //添加页面滑动事件（展开隐藏菜单）
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
#endif
}

-(void)initData{
    
    self.listenToMeData = [ListenToMeData getInstance];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTable) name:NOTIFY_GET_SEARCH_SONG_PAGE_RESP object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTable) name:NOTIFY_GET_SEARCH_KTV_PAGE_RESP object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTable) name:NOTIFY_GET_SEARCH_COMMEMORATE_PAGE_RESP object:nil];
    
}

-(void)refreshTable{
//    CGFloat tableViewH = (self.listenToMeData.arrSearchSongInfo.count + self.listenToMeData.arrSearchKtvBaseInfo.count + self.listenToMeData.arrSearchCommemorateBaseInfo.count + 3) * 50 + 2 * 5;
//    self.mTableView.frame = CGRectMake(0, 0, screenWidth, tableViewH);
    [self.mTableView reloadData];
}

-(void)dealloc{
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_SEARCH_SONG_PAGE_RESP object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_SEARCH_KTV_PAGE_RESP object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_SEARCH_COMMEMORATE_PAGE_RESP object:nil];
}



#pragma mark - UI
#pragma mark -导航栏
-(void)customNavigationBar
{
    [self.navigationItem setHidesBackButton:YES]; // 隐藏返回按钮
    
    mysearchBar=[[UITextField alloc]init];
    mysearchBar.adjustsFontSizeToFitWidth = YES;
    mysearchBar.delegate = self;
    mysearchBar.clearButtonMode = UITextFieldViewModeWhileEditing;
    mysearchBar.clearsOnBeginEditing = YES;
    [mysearchBar addTarget:self action:@selector(searchInfo) forControlEvents:UIControlEventAllEditingEvents];
    //设置frmae
    mysearchBar.width = screenWidth - 40;
    mysearchBar.height = 29;
    //设置背景图片
    mysearchBar.background=[UIImage imageNamed:@"搜索背景.png"];
    //
    //设置文字内容垂直居中
    mysearchBar.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    mysearchBar.placeholder = @"搜索:";
    
    mysearchBar.leftViewMode = UITextFieldViewModeAlways;
//    mysearchBar.rightViewMode = UITextFieldViewModeAlways;
    
    //设置左边的放大镜
    UIImageView *leftView=[[UIImageView alloc]init];
    leftView.width = 29;
    leftView.height = 29;
    
    UIImage *searchImg = [UIImage imageNamed:@"searchImg.png"];
    UIImageView *searchView = [[UIImageView alloc]initWithFrame:CGRectMake(2, (leftView.width - searchImg.size.height * 0.5 ) * 0.5, searchImg.size.height * 0.5, searchImg.size.height * 0.5)];
    searchView.image = searchImg;
    [leftView addSubview:searchView];
    leftView.contentMode = UIViewContentModeCenter;

    mysearchBar.leftView=leftView;
    
    //设置右边取消按钮
    UIButton *cancleBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 33, 29)];

    [cancleBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cancleBtn.titleLabel setFont:[UIFont systemFontOfSize:15.0]];
    [cancleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    cancleBtn.backgroundColor = [UIColor clearColor];
    [cancleBtn addTarget:self action:@selector(cancleHandle:) forControlEvents:UIControlEventTouchUpInside];
    
    
    self.navigationItem.rightBarButtonItem  = [[UIBarButtonItem alloc]initWithCustomView:cancleBtn];
    //[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancle:)];
    
    
    
    //添加到导航栏中
    self.navigationItem.titleView=mysearchBar;
    
}

#pragma mark - 自定义取消搜索按钮
-(void)cancleHandle:(UIButton *)cancleBtn{
    mysearchBar.text = @"";
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma 根据输入框内,输入的内容来搜索相关信息 歌曲 KTV 纪念册
-(void)searchInfo{
    YDLog(@"%@",mysearchBar.text);
    if ([mysearchBar.text isEqualToString:@""]) {
        //do nothing
        self.mTableView.hidden = YES;
        
    }else{
        self.mTableView.hidden = NO;
        //先检索歌曲
        [[NetReqManager getInstance] sendSearchMusics:0 SKeyWord:mysearchBar.text IOffset:0 INum:2];
        //再检索KTV
        [[NetReqManager getInstance] sendSearchKtv:0 SKeyWord:mysearchBar.text IOffset:0 INum:1];
        //然后再检索联系人(未开通接口)
        
        
        //然后在检索纪念册
        
        [[NetReqManager getInstance] sendSearchCommemorate:0 SKeyWord:mysearchBar.text IOffset:0 INum:1];
    }
    
    
}

-(void)setTableView{
    self.mTableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    
    [self.view addSubview:self.mTableView];
    self.mTableView.delegate = self;
    self.mTableView.dataSource = self;
    self.mTableView.hidden = YES;
}


#pragma mark - UITableviewDataSourceDelegate and UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return self.listenToMeData.arrSearchSongInfo.count + 1;
    }
    if (section == 1) {
        return self.listenToMeData.arrSearchKtvBaseInfo.count + 1;
    }
    
    if (section == 2) {
        return self.listenToMeData.arrSearchCommemorateBaseInfo.count + 1;
    }
    
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 0;
    }
    return 5;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"searchCell"];
//    if (!cell) {
//        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"searchCell"];
//    }
//    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    cell.textLabel.textColor = [UIColor lightGrayColor];
    cell.textLabel.font = [UIFont systemFontOfSize:10];

    
    if ([mysearchBar.text isEqualToString:@""]) {
        //do nothing
    }else{
        if ((indexPath.section == 0)  && (indexPath.row == 0)) {
            cell.textLabel.text = @"歌曲";
            
        }else if ((indexPath.section == 1) && (indexPath.row == 0)){
            cell.textLabel.text = @"KTV店";
        }else if ((indexPath.section == 2) && (indexPath.row == 0)){
            cell.textLabel.text = @"纪念册";
        }else{
            cell.textLabel.text = @"测试";
            cell.textLabel.textColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
            cell.textLabel.font = [UIFont systemFontOfSize:15];
        }
    }
    
   
    
   
    return cell;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
