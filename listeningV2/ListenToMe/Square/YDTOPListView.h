//
//  YDTOPListView.h
//  ListenToMe
//
//  Created by yadong on 2/9/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YDTOPListView : UIView
@property(strong,nonatomic) UITableView *mTableview;
@end
