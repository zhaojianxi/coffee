//
//  YDSquareCell.m
//  ListenToMe
//
//  Created by yadong on 3/4/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#define KCellHeight 280 // cell的高度

#import "YDSquareCell.h"
#import <AVFoundation/AVFoundation.h>
#import "AudioStreamer.h"

@interface YDSquareCell ()<AVAudioPlayerDelegate>
/**
 *  分割线1
 */
@property(nonatomic,strong)UIImageView *imgLine_1;
/**
 *  ktv内用户头像
 */
@property(nonatomic,strong)UIImageView *perIconView;
/**
 *  点击播放,音乐播放时要显示的界面,停止播放时,将页面隐藏
 */
@property(nonatomic,strong)UIView *playingMusicView;
/**
 *  音乐播放进度的背景视图
 */
@property(nonatomic,strong)UIImageView *musicLengthBgView;
/**
 *  音乐播放当前进度视图
 */
@property(nonatomic,strong)UIImageView *musicLengthCurrView;
/**
 *  音乐播放时间显示
 */
@property(nonatomic,strong)UILabel *musicLength;
/**
 *  播放显示界面2
 */
@property(nonatomic,strong)UIView *playingMusicView2;
/**
 *  音乐播放进度的背景视图2
 */
@property(nonatomic,strong)UIImageView *musicLengthBgView2;
/**
 *  音乐播放当前进度视图2
 */
@property(nonatomic,strong)UIImageView *musicLengthCurrView2;
/**
 *  音乐播放时间显示
 */
@property(nonatomic,strong)UILabel *musicLength2;
/**
 *  ktv中的用户数组
 */
@property(nonatomic,strong) NSArray *arrStUserBaseInfoNet;
/**
 *  ktv中排名数组
 */
@property(nonatomic,strong) NSArray *arrStMyMusicWorkInfo;
/**
 *  选中的用户
 */
@property(nonatomic,strong) UserBaseInfoNet *stUserBaseInfoNet;
/**
 *  在线音乐播放
 */
@property(nonatomic,strong) AudioStreamer *audioStreamer;
/**
 *  定时器,用来控制显示当前音乐的播放进度
 */
@property(nonatomic,strong) NSTimer *progressUpdaterTimer;
/**
 *  显示歌曲时间的格式
 */
@property(nonatomic,strong)NSDateFormatter *formatter;
/**
 *  记录当前点击播放的button的tag
 */
@property(nonatomic,assign)NSInteger playTag;
@end


@implementation YDSquareCell
@synthesize playBtnFirst;
@synthesize playBtnSec;
@synthesize isPlay;
@synthesize audioStreamer;
@synthesize progressUpdaterTimer;

+ (instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"squarecell";
    YDSquareCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[YDSquareCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
        
    }
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // 添加子控件
        CGFloat frontX = 15; // cell前排子控件开始的x坐标 & 右侧控件距离screen右侧的距离
        CGFloat cellHeight = 39; // cell中每个cell的高度
        CGFloat lineHeight = 1; // 分割线的高度
        
        // 1.店名 & 距离 & 底部分割（线）
        _lbSellerName = [[UILabel alloc]initWithFrame:CGRectMake(frontX, 0, screenWidth * 0.75, cellHeight)];
        _lbSellerName.font = [UIFont systemFontOfSize:15.0];
        [_lbSellerName setTextColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
        [_lbSellerName setTextAlignment:NSTextAlignmentLeft];
        [self.contentView addSubview:_lbSellerName];
        
        CGFloat lbDisTanceW = 30;
        _lbDistance = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - frontX - lbDisTanceW, 0, lbDisTanceW, cellHeight)];
        _lbDistance.font = [UIFont systemFontOfSize:12.0];
        [_lbDistance setTextColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0]];
        [_lbDistance setTextAlignment:NSTextAlignmentRight];
        
        //去掉距离显示
//        [self.contentView addSubview:_lbDistance];
        
        UIImage *distanceImg = [UIImage imageNamed:@"distance.png"];
        UIImageView *imgDistance = [[UIImageView alloc]initWithFrame:CGRectMake(screenWidth - frontX - lbDisTanceW - distanceImg.size.width, 0, distanceImg.size.width, cellHeight)];
        imgDistance.image = distanceImg;
//        [self.contentView addSubview:imgDistance];
        
        UIImageView *imgLine_0 = [[UIImageView alloc]initWithFrame:CGRectMake(0, _lbSellerName.height, screenWidth, lineHeight)];
        imgLine_0.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
        [self.contentView addSubview:imgLine_0];
        
        // 2.正在KTV & 帅哥美女数 & 小圆点
        CGFloat imgCircleW = 5; // 圆点的宽高
        UIImageView *imgCircle = [[UIImageView alloc]initWithFrame:CGRectMake(frontX, imgLine_0.y + imgLine_0.height + (cellHeight - imgCircleW) * 0.5, imgCircleW, imgCircleW)];
        imgCircle.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
        imgCircle.layer.cornerRadius = 2.5;
        imgCircle.layer.masksToBounds = YES;
        [self.contentView addSubview:imgCircle];
        
        UILabel *lbKtvNow = [[UILabel alloc]initWithFrame:CGRectMake(imgCircle.x + imgCircleW + 15, imgLine_0.y + lineHeight, screenWidth * 0.2, cellHeight)];
        lbKtvNow.font = [UIFont systemFontOfSize:12.0];
        [lbKtvNow setTextColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
        [lbKtvNow setTextAlignment:NSTextAlignmentLeft];
        lbKtvNow.text = @"正在KTV";
        [self.contentView addSubview:lbKtvNow];
        
        CGFloat lbGirlsW = 60; // 美女标签的宽
        _lbGirls = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - frontX - lbGirlsW, imgLine_0.y + lineHeight, lbGirlsW, cellHeight)];
        [_lbGirls setTextColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0]];
        _lbGirls.font = [UIFont systemFontOfSize:12.0];
        [_lbGirls setTextAlignment:NSTextAlignmentRight];
        _lbGirls.text = [NSString stringWithFormat:@"美女%d位",44];
//        [self.contentView addSubview:_lbGirls];
        
        
        _lbBoys = [[UILabel alloc]initWithFrame:CGRectMake(_lbGirls.x - _lbGirls.width, _lbGirls.y, _lbGirls.width, _lbGirls.height)];
        [_lbBoys setTextColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0]];
        _lbBoys.font = [UIFont systemFontOfSize:12.0];
        [_lbBoys setTextAlignment:NSTextAlignmentLeft];
        _lbBoys.text = [NSString stringWithFormat:@"帅哥%d位",4];
//        [self.contentView addSubview:_lbBoys];
        
        _imgLine_1 = [[UIImageView alloc]initWithFrame:CGRectMake(frontX, _lbBoys.y + _lbBoys.height, screenWidth - 2 * frontX, lineHeight)];
        _imgLine_1.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
        [self.contentView addSubview:_imgLine_1];
        
        
        UIImageView *imgLine_2 = [[UIImageView alloc]initWithFrame:CGRectMake(frontX, _imgLine_1.height + _imgLine_1.y + cellHeight, screenWidth - 2 * frontX, lineHeight)];
        imgLine_2.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
        [self.contentView addSubview:imgLine_2];
        
        
        
        UIImageView *imgLine_3 = [[UIImageView alloc]initWithFrame:CGRectMake(frontX, imgLine_2.height + imgLine_2.y + cellHeight, screenWidth - 2 * frontX, lineHeight)];
        imgLine_3.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
        [self.contentView addSubview:imgLine_3];
        
        
        // 5.竖线
        CGFloat imgVerticalH = 95;
        UIImageView *imgVertical = [[UIImageView alloc]initWithFrame:CGRectMake(frontX, imgLine_3.height + imgLine_3.y + (cellHeight * 3 + lineHeight * 3 - imgVerticalH) * 0.5 , 1.5, imgVerticalH)];
        imgVertical.backgroundColor = [UIColor redColor];
        imgVertical.alpha = 0.68;
        [self.contentView addSubview:imgVertical];
        
        // 6.热歌榜
        UILabel *hotMusicLb = [[UILabel alloc]initWithFrame:CGRectMake(frontX * 2, imgLine_3.y + imgLine_3.height, 60, cellHeight)];
        hotMusicLb.font = [UIFont systemFontOfSize:12.0];
        hotMusicLb.textColor = [UIColor rgbFromHexString:@"#FF0053" alpaa:1.0];
        hotMusicLb.text = @"KTV热歌";
        hotMusicLb.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:hotMusicLb];
        
        UIImageView *imgLine_4 = [[UIImageView alloc]initWithFrame:CGRectMake(frontX * 2, imgLine_3.height + imgLine_3.y + cellHeight, screenWidth - 3 * frontX, lineHeight)];
        imgLine_4.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
        [self.contentView addSubview:imgLine_4];
        
        // 7.好歌第一名
        // 头像
        CGFloat avatarWh = 25;
        _btnFSinger = [[UIButton alloc]initWithFrame:CGRectMake(2 * frontX, imgLine_4.y + imgLine_4.height + (cellHeight - avatarWh) * 0.5 , avatarWh, avatarWh)];
    
        //进入个人主页
        [_btnFSinger addTarget:self action:@selector(jumpToPerHomePage:) forControlEvents:UIControlEventTouchUpInside];
        _btnFSinger.tag = 2000;
        
        self.fSingerImgView = [[UIImageView alloc]initWithFrame:_btnFSinger.frame];
        self.fSingerImgView.userInteractionEnabled = YES;
        self.fSingerImgView.layer.masksToBounds = YES;
        self.fSingerImgView.layer.cornerRadius = avatarWh * 0.5;
        [self.contentView addSubview:self.fSingerImgView];
        
        [self.contentView addSubview:_btnFSinger];
        
        UIImageView *imgLine_5 = [[UIImageView alloc]initWithFrame:CGRectMake(frontX * 2, imgLine_4.height + imgLine_4.y + cellHeight, screenWidth - 3 * frontX, lineHeight)];
        imgLine_5.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:.12];
        [self.contentView addSubview:imgLine_5];
        
        // 播放按钮
        UIImage *playBtnImg = [UIImage imageNamed:@"play.png"];
        playBtnFirst = [[UIButton alloc]initWithFrame:CGRectMake(_btnFSinger.x + _btnFSinger.width + frontX, imgLine_4.y + imgLine_4.height + (cellHeight - playBtnImg.size.height * 0.5) * 0.5, playBtnImg.size.width * 0.5, playBtnImg.size.height * 0.5)];
        [playBtnFirst setImage:playBtnImg forState:UIControlStateNormal];
        
        [playBtnFirst setImage:[UIImage imageNamed:@"stop.png"] forState:UIControlStateSelected];
        
        playBtnFirst.selected = NO;
        
        
        [playBtnFirst addTarget:self action:@selector(clickPlayBtn:) forControlEvents:UIControlEventTouchUpInside];
        playBtnFirst.tag = 1000;
        [self.contentView addSubview:playBtnFirst];
        
       
        
        // 鲜花数
        CGFloat fFlowerW = 65;
        CGFloat fFloweMargin = 25;
        _btnFFlower = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - frontX -fFlowerW,imgLine_4.y + imgLine_4.height, fFlowerW, cellHeight)];
        [_btnFFlower setImage:[UIImage imageNamed:@"flowerSquareKtv.png"] forState:UIControlStateNormal];
//        [_btnFFlower setTitle:@"114" forState:UIControlStateNormal];
        _btnFFlower.titleLabel.font = [UIFont systemFontOfSize:10.0];
        [_btnFFlower setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateNormal];
        _btnFFlower.titleLabel.textAlignment = NSTextAlignmentRight;
        [_btnFFlower setTitleEdgeInsets:UIEdgeInsetsMake(0, fFloweMargin, 0, 0)];
        [self.contentView addSubview:_btnFFlower];
        
        // 听众数
        _btnFListenNum = [[UIButton alloc]initWithFrame:CGRectMake(_btnFFlower.x - fFlowerW,imgLine_4.y + imgLine_4.height, fFlowerW, cellHeight)];
        [_btnFListenNum setImage:[UIImage imageNamed:@"listenersCount.png"] forState:UIControlStateNormal];
//        [_btnFListenNum setTitle:@"12306" forState:UIControlStateNormal];
        _btnFListenNum.titleLabel.font = [UIFont systemFontOfSize:10.0];
        [_btnFListenNum setTitleColor:[UIColor rgbFromHexString:@"#FF0053" alpaa:1.0] forState:UIControlStateNormal];
        _btnFListenNum.titleLabel.textAlignment = NSTextAlignmentRight;
        [_btnFListenNum setTitleEdgeInsets:UIEdgeInsetsMake(0, fFloweMargin, 0, 0)];
        [self.contentView addSubview:_btnFListenNum];
        
        // 歌曲名
        CGFloat songNameWidth = screenHeight - _btnFListenNum.x - playBtnFirst.x - playBtnFirst.width;
        _lbFSongName = [[UILabel alloc]initWithFrame:CGRectMake(playBtnFirst.x + playBtnFirst.width + 10, imgLine_4.y + imgLine_4.height, songNameWidth, cellHeight)];
        _lbFSongName.font = [UIFont systemFontOfSize:12.0];
        _lbFSongName.textColor = [UIColor rgbFromHexString:@"#4A4A4A" alpaa:1.0];
        _lbFSongName.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_lbFSongName];
        
        
        //音乐播放时显示的界面
        self.playingMusicView = [[UIView alloc]initWithFrame:CGRectMake(playBtnFirst.x + playBtnFirst.width, imgLine_4.y + imgLine_4.height, screenWidth - frontX - playBtnFirst.x - playBtnFirst.width, cellHeight)];
        //播放进度
        
        //播放时长
        self.musicLength = [[UILabel alloc]initWithFrame:CGRectMake(self.playingMusicView.width - 40, 0, 40, cellHeight)];
        [self.musicLength setTextColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0]];
        [self.musicLength setTextAlignment:NSTextAlignmentRight];
        [self.musicLength setFont:[UIFont systemFontOfSize:10.0]];
        [self.playingMusicView addSubview:self.musicLength];
        
        
        //播放音乐的进度条
//        //1背景视图
        UIImage *musicLengImg =  [UIImage imageNamed:@"灰色01.png"];
        CGFloat musicLengthBg_X = 5;
        CGFloat musicLengthBg_Y = (self.playingMusicView.height - musicLengImg.size.height) * 0.5;
        CGFloat musicLengthBg_W = self.playingMusicView.width - self.musicLength.width - 10;
        CGFloat musicLengthBg_H =  musicLengImg.size.height ;
        self.musicLengthBgView = [[UIImageView alloc] initWithFrame:CGRectMake(musicLengthBg_X, musicLengthBg_Y, musicLengthBg_W, musicLengthBg_H)];
        self.musicLengthBgView.backgroundColor = [UIColor colorWithPatternImage:musicLengImg];
        self.musicLengthBgView.clipsToBounds = YES;
        [self.playingMusicView addSubview:self.musicLengthBgView];
        
        //2音乐播放时的进度显示视图,需要动态改变是宽度
        self.musicLengthCurrView = [[UIImageView alloc] initWithFrame:CGRectMake(musicLengthBg_X, musicLengthBg_Y, 0, musicLengthBg_H)];
        self.musicLengthCurrView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"紫色02.png"]];
        [self.playingMusicView addSubview:self.musicLengthCurrView];
        
        
        [self.contentView addSubview:self.playingMusicView];
        
        
        //默认将播放显示界面隐藏掉
        self.playingMusicView.hidden = YES;
        
        // 8.好歌第二名
        _btnSSinger = [[UIButton alloc]initWithFrame:CGRectMake(2 * frontX, imgLine_5.y + imgLine_5.height + (cellHeight - avatarWh) * 0.5 , avatarWh, avatarWh)];
//        [_imgSSinger setImage:[UIImage imageNamed:@"temp3.png"] forState:UIControlStateNormal];
        
        //进入个人主页
        [_btnSSinger addTarget:self action:@selector(jumpToPerHomePage:) forControlEvents:UIControlEventTouchUpInside];
        _btnSSinger.tag = 2001;
        
        
        self.sSingerImgView = [[UIImageView alloc]initWithFrame:_btnSSinger.frame];
        self.sSingerImgView.userInteractionEnabled = YES;
        self.sSingerImgView.layer.masksToBounds = YES;
        self.sSingerImgView.layer.cornerRadius = avatarWh * 0.5;
        [self.contentView addSubview:self.sSingerImgView];
        
        [self.contentView addSubview:_btnSSinger];
        
        // 播放按钮
        playBtnSec = [[UIButton alloc]initWithFrame:CGRectMake(_btnSSinger.x + _btnSSinger.width + frontX, imgLine_5.y + imgLine_4.height + (cellHeight - playBtnImg.size.height * 0.5) * 0.5, playBtnImg.size.width * 0.5, playBtnImg.size.height * 0.5)];
        [playBtnSec setImage:playBtnImg forState:UIControlStateNormal];
        
        [playBtnSec setImage:[UIImage imageNamed:@"stop.png"] forState:UIControlStateSelected];
        playBtnSec.selected = NO;
        
        [playBtnSec addTarget:self action:@selector(clickPlayBtn:) forControlEvents:UIControlEventTouchUpInside];
        playBtnSec.tag = 1001;
        [self.contentView addSubview:playBtnSec];
        
        // 歌曲名
        _lbSSongName = [[UILabel alloc]initWithFrame:CGRectMake(playBtnFirst.x + playBtnFirst.width + 10, imgLine_5.y + imgLine_5.height, songNameWidth, cellHeight)];
        _lbSSongName.font = [UIFont systemFontOfSize:12.0];
        _lbSSongName.textColor = [UIColor rgbFromHexString:@"#4A4A4A" alpaa:1.0];
        _lbSSongName.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_lbSSongName];
        
        // 鲜花数
        _btnSFlower = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - frontX -fFlowerW,imgLine_5.y + imgLine_5.height, fFlowerW, cellHeight)];
        [_btnSFlower setImage:[UIImage imageNamed:@"flowerSquareKtv.png"] forState:UIControlStateNormal];
//        [_btnSFlower setTitle:@"114" forState:UIControlStateNormal];
        _btnSFlower.titleLabel.font = [UIFont systemFontOfSize:10.0];
        [_btnSFlower setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateNormal];
        _btnSFlower.titleLabel.textAlignment = NSTextAlignmentRight;
        [_btnSFlower setTitleEdgeInsets:UIEdgeInsetsMake(0, fFloweMargin, 0, 0)];
        [self.contentView addSubview:_btnSFlower];
        
    
        // 听众数
        _btnSListenNum = [[UIButton alloc]initWithFrame:CGRectMake(_btnFFlower.x - fFlowerW,imgLine_5.y + imgLine_5.height, fFlowerW, cellHeight)];
        [_btnSListenNum setImage:[UIImage imageNamed:@"listenersCount.png"] forState:UIControlStateNormal];
//        [_btnSListenNum setTitle:@"12306" forState:UIControlStateNormal];
        _btnSListenNum.titleLabel.font = [UIFont systemFontOfSize:10.0];
        [_btnSListenNum setTitleColor:[UIColor rgbFromHexString:@"#FF0053" alpaa:1.0] forState:UIControlStateNormal];
        _btnSListenNum.titleLabel.textAlignment = NSTextAlignmentRight;
        [_btnSListenNum setTitleEdgeInsets:UIEdgeInsetsMake(0, fFloweMargin, 0, 0)];
        [self.contentView addSubview:_btnSListenNum];
        
        //音乐播放时显示的界面
        self.playingMusicView2 = [[UIView alloc]initWithFrame:CGRectMake(playBtnSec.x + playBtnSec.width, imgLine_5.y + imgLine_5.height, screenWidth - frontX - playBtnSec.x - playBtnSec.width, cellHeight)];
        self.playingMusicView2.hidden = YES;
        //播放进度
        
        //播放时长
        self.musicLength2 = [[UILabel alloc]initWithFrame:CGRectMake(self.playingMusicView2.width - 40, 0, 40, cellHeight)];
        [self.musicLength2 setTextColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0]];
        [self.musicLength2 setTextAlignment:NSTextAlignmentRight];
        [self.musicLength2 setFont:[UIFont systemFontOfSize:10.0]];
        [self.playingMusicView2 addSubview:self.musicLength2];
        
        
      
        CGFloat musicLengthBg2_X = 5;
        CGFloat musicLengthBg2_Y = (self.playingMusicView2.height - musicLengImg.size.height) * 0.5;
        CGFloat musicLengthBg2_W = self.playingMusicView2.width - self.musicLength2.width - 10;
        CGFloat musicLengthBg2_H =  musicLengImg.size.height ;
        self.musicLengthBgView2 = [[UIImageView alloc] initWithFrame:CGRectMake(musicLengthBg2_X, musicLengthBg2_Y, musicLengthBg2_W, musicLengthBg2_H)];
        self.musicLengthBgView2.backgroundColor = [UIColor colorWithPatternImage:musicLengImg];
        self.musicLengthBgView2.clipsToBounds = YES;
        [self.playingMusicView2 addSubview:self.musicLengthBgView2];
        
        //2音乐播放时的进度显示视图,需要动态改变是宽度
        self.musicLengthCurrView2 = [[UIImageView alloc] initWithFrame:CGRectMake(musicLengthBg2_X, musicLengthBg2_Y, 0, musicLengthBg2_H)];
        self.musicLengthCurrView2.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"紫色02.png"]];
        [self.playingMusicView2 addSubview:self.musicLengthCurrView2];

        
        
        [self.contentView addSubview:self.playingMusicView2];
        //默认将播放显示界面隐藏掉
        self.playingMusicView2.hidden = YES;
        
        
        //设置歌曲显示时间的格式
        self.formatter= [[NSDateFormatter alloc]init];
        [self.formatter setDateFormat:@"mm:ss"];
        
    }
    
    return self;
}


#pragma mark - 播放按钮
-(void)clickPlayBtn:(UIButton *)button{
    

    //给buttont添加一个属性 是否处于播放状态
    //第一个button处于播放状态时,将第二的button的播放状态置为暂停;
    
    self.playTag = button.tag - 1000;
    YDLog(@"%ld",self.playTag);
    
    if (button.tag == 1000) {
        if (playBtnFirst.selected == NO) {
            
            //改变第一个播放按钮的状态
            playBtnFirst.selected = YES;
            
            //判断播放状态
            if (!isPlay) {
                [self playWithTag:self.playTag];
                isPlay = YES;
                
                [self hiddenPlayButtonUI:playBtnFirst];
            }else{
                isPlay = NO;
                [self stop];
                
                [self showPlayButtonUI:playBtnFirst];
            }
            
            //判断第二个播放按钮的状态
            if (playBtnSec.selected == NO) {
                
            }else{
                //改变播放的状态
                playBtnSec.selected = NO;
                
                if (!isPlay) {
                    [self playWithTag:self.playTag];
                    isPlay = YES;
                    
                    [self showPlayButtonUI:playBtnSec];
                    [self hiddenPlayButtonUI:playBtnFirst];
                }else{
                    isPlay = NO;
                    [self stop];
                    
                    [self showPlayButtonUI:playBtnSec];
                }
            }
        }else{
            playBtnFirst.selected = NO;
            
            if (!isPlay) {
                [self playWithTag:self.playTag];
                isPlay = YES;
                
                [self hiddenPlayButtonUI:playBtnFirst];
            }else{
                isPlay = NO;
                [self stop];
                
                [self showPlayButtonUI:playBtnFirst];
            }
        }
    }
    
    
    if (button.tag == 1001) {
        if (playBtnSec.selected == NO) {
            playBtnSec.selected = YES;
            if (!isPlay) {
                [self playWithTag:self.playTag];
                isPlay = YES;
                
                [self hiddenPlayButtonUI:playBtnSec];
            }else{
                isPlay = NO;
                [self stop];
                
                [self showPlayButtonUI:playBtnSec];
            }
            if (playBtnFirst.selected == NO) {
                
            }else{
                playBtnFirst.selected = NO;
                if (!isPlay) {
                    [self playWithTag:self.playTag];
                    isPlay = YES;
                    
                    [self showPlayButtonUI:playBtnFirst];
                    [self hiddenPlayButtonUI:playBtnSec];
                }else{
                    isPlay = NO;
                    [self stop];
                    
                    [self showPlayButtonUI:playBtnFirst];
                }
            }
        }else{
            playBtnSec.selected = NO;
            if (!isPlay) {
                [self playWithTag:self.playTag];
                isPlay = YES;
                
                [self hiddenPlayButtonUI:playBtnSec];
            }else{
                isPlay = NO;
                [self stop];
                
                [self showPlayButtonUI:playBtnSec];
            }
        }
    }
 
    


}

-(void)hiddenPlayButtonUI:(UIButton *)playButton{

    if (playButton.tag == 1000) {
        [self hiddenLbSongName:_lbFSongName btnListenNum:_btnFListenNum btnFlower:_btnFFlower];
        self.playingMusicView.hidden = NO;
    }
    
    if (playButton.tag == 1001) {
        [self hiddenLbSongName:_lbSSongName btnListenNum:_btnSListenNum btnFlower:_btnSFlower];
        self.playingMusicView2.hidden = NO;
    }
}

-(void)showPlayButtonUI:(UIButton *)playButton{
    
    if (playButton.tag == 1000) {
        [self showLbSongName:_lbFSongName btnListenNum:_btnFListenNum btnFlower:_btnFFlower];
        self.playingMusicView.hidden = YES;
    }
    
    if (playButton.tag == 1001) {
        [self showLbSongName:_lbSSongName btnListenNum:_btnSListenNum btnFlower:_btnSFlower];
        self.playingMusicView2.hidden = YES;
    }
}


#pragma mark - 播放是隐藏相应的控件如 歌曲名 听众数 鲜花数
-(void)hiddenLbSongName:(UILabel *)lbSongName btnListenNum:(UIButton *)btnListenNum btnFlower:(UIButton *)btnFlowerNum{
    
    lbSongName.hidden = YES;
    btnFlowerNum.hidden = YES;
    btnListenNum.hidden = YES;
}

#pragma mark - 不播放时显示相应的控件 歌曲名 听众数 鲜花数
-(void)showLbSongName:(UILabel *)lbSongName btnListenNum:(UIButton *)btnListenNum btnFlower:(UIButton *)btnFlowerNum{
    
    lbSongName.hidden = NO;
    btnFlowerNum.hidden = NO;
    btnListenNum.hidden = NO;
}


#pragma mark -播放
-(void)playWithTag:(NSInteger)tag{
    
    
    [self destroyStreamer];
    NSString *urlString = ((MusicWorkBaseInfo *)self.arrStMyMusicWorkInfo[tag]).stSongInfo.sSongUrl;
    NSString *escapedValue = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(nil,(CFStringRef)urlString , NULL, NULL, kCFStringEncodingUTF8));
    
    NSURL *onLineMusicUrl = [NSURL URLWithString:escapedValue];

    audioStreamer = [[AudioStreamer alloc]initWithURL:onLineMusicUrl];
    
    //需要加一个定时器,来控制进度条显示的显示
    if(progressUpdaterTimer == nil){
        progressUpdaterTimer = [[NSTimer alloc]init];
    }
    progressUpdaterTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateProgress:) userInfo:nil repeats:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playbackStateChanged:) name:ASStatusChangedNotification object:audioStreamer];
    
    [audioStreamer start];
    
    
  
    

}
#pragma mark 播放状态改变
- (void)playbackStateChanged:(NSNotification *)aNotification
{
    
    if ([audioStreamer isWaiting])
    {
        //正在等待播放
    }
    
     if ([audioStreamer isPlaying])
    {
        //正在播放
        YDLog(@"%f",audioStreamer.progress);
    }
    
    if ([audioStreamer isIdle])
    {
        
        [self destroyStreamer];
        
        for (int index = 0; index < self.arrStMyMusicWorkInfo.count; index ++) {
            UIButton *button =(UIButton *)[self viewWithTag:1000 + index];
            button.selected = NO;
            [self showPlayButtonUI:button];
            
        }
        
        isPlay = NO;
        
        
        
    }
}

- (void)destroyStreamer
{
    if (audioStreamer)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:ASStatusChangedNotification object:audioStreamer];
        
        [progressUpdaterTimer invalidate];
        progressUpdaterTimer = nil;
        
        
        [audioStreamer stop];
//        audioStreamer = nil;
        
        self.musicLengthCurrView.size = CGSizeMake(0, self.musicLengthBgView.height);
        self.musicLengthCurrView2.size = CGSizeMake(0, self.musicLengthBgView2.height);
    }
}

#pragma mark - 定时器
- (void)updateProgress:(NSTimer *)updatedTimer
{
    if (audioStreamer.bitRate != 0.0)
    {
        double progress = audioStreamer.progress;
        double duration = audioStreamer.duration;
        
        if (duration > 0 && self.playTag == 0)
        {

            self.musicLength.text = [self.formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:audioStreamer.duration]];
            self.musicLengthCurrView.size = CGSizeMake((progress / duration) * self.musicLengthBgView.width, self.musicLengthBgView.height);
            
//            self.musicLengthCurrView.frame = CGRectMake(self.musicLengthBgView.x, self.musicLengthBgView.y, (progress / duration) * self.musicLengthBgView.width, self.musicLengthBgView.height);
           
        }
        
        if (duration > 0 && self.playTag == 1) {
            self.musicLength2.text = [self.formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:audioStreamer.duration]];
            self.musicLengthCurrView2.size = CGSizeMake((progress / duration) * self.musicLengthBgView2.width, self.musicLengthBgView2.height);
//            self.musicLengthCurrView2.frame = CGRectMake(self.musicLengthBgView2.x, self.musicLengthBgView2.y, (progress / duration) * self.musicLengthBgView2.width, self.musicLengthBgView2.height);
        }
     
    }
    
}

#pragma mark -暂停
-(void)pause{
    [audioStreamer pause];

}

#pragma mark -停止
-(void)stop{
    [self destroyStreamer];
    
}


- (void)dealloc
{
    [self destroyStreamer];
    if (progressUpdaterTimer)
    {
        [progressUpdaterTimer invalidate];
        progressUpdaterTimer = nil;
    }
}



#pragma mark - 点击头像进入个人主页
-(void)jumpToPerHomePage:(UIButton *)button{
    
    if (self.stUserBaseInfoNet != nil) {
        self.stUserBaseInfoNet = nil;
        if (button.tag < 2000) {
            self.stUserBaseInfoNet = self.arrStUserBaseInfoNet[button.tag - 1000];
        }else{
            self.stUserBaseInfoNet = ((MusicWorkBaseInfo *)self.arrStMyMusicWorkInfo[button.tag - 2000]).stUserBaseInfoNet;
        }
        
    }
    
    //使用协议代理到control中实现个人主页的页面跳转
    
    if ([self.delegate respondsToSelector:@selector(browsePerHmePage:)]) {
        [self.delegate browsePerHmePage:self.stUserBaseInfoNet];
    }
    
    

}


-(void)setKtvBaseInfo:(NSDictionary *)ktvBaseInfo{
    if (ktvBaseInfo != nil) {
        
        _ktvBaseInfo = ktvBaseInfo;
        self.arrStUserBaseInfoNet = [[NSArray alloc]initWithArray:[_ktvBaseInfo valueForKey:@"stUserBaseInfoNet"]];
        self.arrStMyMusicWorkInfo = [[NSArray alloc]initWithArray:[_ktvBaseInfo valueForKey:@"stMyMusicWorkInfo"]];
        
        
        [self.fSingerImgView sd_setImageWithURL:[NSURL URLWithString:((MusicWorkBaseInfo *)self.arrStMyMusicWorkInfo[0]).stUserBaseInfoNet.sCovver]];
        

        [self.sSingerImgView sd_setImageWithURL:[NSURL URLWithString:((MusicWorkBaseInfo *)self.arrStMyMusicWorkInfo[1]).stUserBaseInfoNet.sCovver]];

        self.lbSellerName.text = [_ktvBaseInfo valueForKey:@"vStoreName"];//@"同一首歌 - 苏州桥店";
        self.lbFSongName.text = ((MusicWorkBaseInfo *)self.arrStMyMusicWorkInfo[0]).stSongInfo.sSongName;//@"泡沫";
        self.lbSSongName.text = ((MusicWorkBaseInfo *)self.arrStMyMusicWorkInfo[1]).stSongInfo.sSongName;

        [self.btnFListenNum setTitle:[NSString stringWithFormat:@"%d",((MusicWorkBaseInfo *)self.arrStMyMusicWorkInfo[0]).iListenNum] forState:UIControlStateNormal];
        [self.btnSListenNum setTitle:[NSString stringWithFormat:@"%d",((MusicWorkBaseInfo *)self.arrStMyMusicWorkInfo[1]).iListenNum] forState:UIControlStateNormal];
       
        [self.btnFFlower setTitle:[NSString stringWithFormat:@"%d",((MusicWorkBaseInfo *)self.arrStMyMusicWorkInfo[0]).iFlowerNum] forState:UIControlStateNormal];
        [self.btnSFlower setTitle:[NSString stringWithFormat:@"%d",((MusicWorkBaseInfo *)self.arrStMyMusicWorkInfo[1]).iFlowerNum] forState:UIControlStateNormal];

        self.mArrayPresent = [[NSMutableArray alloc]initWithCapacity:self.arrStUserBaseInfoNet.count];
        int count = 0; //统计第一行显示头像的个数
        CGFloat imgWh = 30; // 头像的宽高都是30
        CGFloat frontX = 15;
        CGFloat cellHeight = 39;
        
        CGFloat tempImgViewX = 0;
        CGFloat tempImgViewY = _imgLine_1.y +  _imgLine_1.height + (cellHeight - imgWh) * 0.5; //显示头像的第一行
        CGFloat tempImgViewY2 = 0; //显示头像的第二行的y
        
        for (int index = 0; index < self.arrStUserBaseInfoNet.count; index ++) {
            
            self.stUserBaseInfoNet = self.arrStUserBaseInfoNet[index];
            
            CGFloat maxWidth = screenWidth - frontX *2; //显示头像区域的最大宽度
            CGFloat lastWidth = index * (imgWh + 5) + frontX + imgWh; //下一个图像所在位置
           
            UIImageView *tempImgView = [[UIImageView alloc]init]; //加载头像
            tempImgView.userInteractionEnabled = YES;
            
            if (lastWidth <= maxWidth) {

                tempImgViewX = index * (imgWh + 5) + frontX;
                
                tempImgView.frame = CGRectMake(tempImgViewX, tempImgViewY, imgWh, imgWh);
                
                count = index;
            }else{

                tempImgViewX = (index - count - 1) * (imgWh + 5) + frontX;
                tempImgViewY2 = tempImgViewY + cellHeight;
                tempImgView.frame = CGRectMake(tempImgViewX, tempImgViewY2, imgWh, imgWh);
            }
            
            
            [tempImgView sd_setImageWithURL:[NSURL URLWithString:self.stUserBaseInfoNet.sCovver]];
            tempImgView.userInteractionEnabled = YES;
            [self.contentView addSubview:tempImgView];
            
            UIButton *headPic = [[UIButton alloc]initWithFrame:tempImgView.frame];
            headPic.backgroundColor = [UIColor clearColor];
            //添加点击头像进入个人界面
            [headPic addTarget:self action:@selector(jumpToPerHomePage:) forControlEvents:UIControlEventTouchUpInside];
            headPic.tag = 1000 + index;
            [self.contentView addSubview:headPic];
            
        }
        

    }
}
@end

