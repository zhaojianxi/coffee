//
//  UIColor+YD.h
//  ListenToMe
//
//  Created by yadong on 2/9/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (YD)
// 十六进制颜色，转换为UIColor对象
+ (UIColor *)rgbFromHexString:(NSString *)color alpaa:(float)alpha;
@end
