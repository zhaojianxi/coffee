//
//  CCNotify.m
//  ListenToMe
//
//  Created by yadong on 1/27/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "CCNotify.h"

@implementation CCNotify

+(id)getObj:(NSDictionary *)obj byNum:(int)index
{
    return [obj objectForKey:[NSNumber numberWithInt:index]];
}

+(void)sentNotify:(NSString *)type obj:(id)content, ...
{
    va_list args;
    va_start(args, content);
    __autoreleasing NSMutableDictionary *obj = [[NSMutableDictionary alloc]init];
    int i = 0;
    for (id arg = content; arg != nil; arg = va_arg(args, id),i ++) {
        [obj setObject:arg forKey:[NSNumber numberWithInt:i]];
    }
    va_end(args);
    
    [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:type object:obj]];
}
@end
