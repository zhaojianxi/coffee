//
//  YDPopBt.h
//  ListenToMe
//
//  Created by yadong on 2/15/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YDPopBt : UIView
/**
 * 白色线条
 */
@property(strong,nonatomic) UIImageView *imgLine;
@property(strong,nonatomic) UIButton *BtnPop;
/**
 * 按钮图片
 */
@property(strong,nonatomic) UIImageView *imgPop;

/**
 * 按钮标签
 */
@property(strong,nonatomic) UILabel *lbPop;
/**
 * 数字提醒按钮
 */
@property(strong,nonatomic) UIButton *btnBadge;



@end
