//
//  YDCircleAvatarBtn.m
//  ListenToMe
//
//  Created by yadong on 2/6/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "YDCircleAvatarBtn.h"

@implementation YDCircleAvatarBtn

-(instancetype)initWithFrame:(CGRect)frame
{
    if ([super initWithFrame:frame]) {

        self.layer.cornerRadius = frame.size.width * 0.5;
        self.layer.masksToBounds = YES;
        
    }
    
    return self;
}

//+(instancetype)circleAvatarWithFrame:(CGRect)frame avatarType:(avatarType)avatarType
//{
//    [self init];
//}

//-(instancetype)init
//{
//    self = [super init];
//    
//    return self;
//}

@end
