//
//  UuidTool.m
//  ListenToMe
//
//  Created by zhw on 15/4/24.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "UuidTool.h"

@implementation UuidTool

#pragma mark setUuid
+(void)setUuid:(long long)uuid{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if (uuid != 0) {
        
        NSString *sUuid = [NSString stringWithFormat:@"%lli",uuid];
        [userDefaults setObject:sUuid forKey:@"sUuid"];
        YDLog(@"uuid is %lli uuidString is %@",uuid,sUuid);
    }
    [userDefaults synchronize];
}

#pragma mark getUuid
+(long long)getUuid{
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *sUuid = [userDefaults stringForKey:@"sUuid"];
    long long uuid = [sUuid longLongValue];
    return uuid;
}
@end
