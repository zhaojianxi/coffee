//
//  UuidTool.h
//  ListenToMe
//
//  Created by zhw on 15/4/24.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UuidTool : NSObject
/**
 *  定义一个ListenToMeData
 */
@property(nonatomic,strong) ListenToMeData * listenToMeData;

//用户ID相关存储操作

+ (long long)getUuid;
+ (void)setUuid:(long long)uuid;
@end
