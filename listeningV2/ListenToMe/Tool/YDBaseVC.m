//
//  YDBaseVC.m
//  ListenToMe
//
//  Created by yadong on 2/7/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "YDBaseVC.h"

@interface YDBaseVC ()

@end

@implementation YDBaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor rgbFromHexString:@"#F1F1F1" alpaa:1.0];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
