//
//  YDBadgeBtn.h
//  ListenToMe
//
//  Created by yadong on 2/6/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YDBadgeBtn : UIButton

/**
 * 提醒数字
 */
@property(assign,nonatomic) int  badgeValue;
@end
