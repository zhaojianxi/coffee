//
//  YDPopBt.m
//  ListenToMe
//
//  Created by yadong on 2/15/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#import "YDPopBt.h"

@interface YDPopBt ()<UIGestureRecognizerDelegate>
@property(nonatomic,strong)UIButton *selectBtn;
@end

@implementation YDPopBt

-(instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        // frame的值在内部已经确定
        CGFloat btnW = 145;
        CGFloat btnH = 37.5;
        
        CGFloat btnBadgeW = 22;
        CGFloat btnBadgeH = 17;
        
        // 按钮图片
        CGFloat imgPopW = 46;
        CGFloat imgPopH = 39;
        
     
        
        
        _imgPop = [[UIImageView alloc]initWithFrame:CGRectMake(0,0,imgPopW, imgPopH)];
        [self addSubview:_imgPop];
        
        // 标签
        _lbPop = [[UILabel alloc]initWithFrame:CGRectMake(imgPopW,0,btnW - imgPopW - btnBadgeW - 5, btnH)];
        [_lbPop setFont:[UIFont systemFontOfSize:15.0]];
        [_lbPop setTextColor:[UIColor whiteColor]];
        [self addSubview:_lbPop];
        [_lbPop setBackgroundColor:[UIColor clearColor]];

        
        // 数字提醒按钮
        CGFloat btnBadgeMargin = 8;
        _btnBadge = [[UIButton alloc]initWithFrame:CGRectMake(btnW - btnBadgeW - btnBadgeMargin, (btnH - btnBadgeH) * 0.5 , btnBadgeW, btnBadgeH)];
        [self addSubview:_btnBadge];
        [_btnBadge setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_btnBadge.titleLabel setFont:[UIFont systemFontOfSize:12.0]];
        _btnBadge.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"badge.png"]];
        if ([_btnBadge.titleLabel.text isEqual:@""] || [_btnBadge.titleLabel.text isEqual:@"0" ] ) {
            _btnBadge.hidden = YES;
        }
    
        
        // 按钮
        self.BtnPop = [[UIButton alloc]initWithFrame:CGRectMake(0, 0,btnW,btnH)];
        _BtnPop.backgroundColor = [UIColor clearColor];
//        [_BtnPop addTarget:self action:@selector(dismissHandle) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_BtnPop];
        
        // 白色分割线
        _imgLine = [[UIImageView alloc]initWithFrame:CGRectMake(0, btnH - 1, btnW, 1)];
        _imgLine.backgroundColor = [UIColor colorWithWhite:0.9 alpha:0.8];
        [_BtnPop addSubview:_imgLine];
        
//        self.selectBtn = [[UIButton alloc]initWithFrame:self.bounds];
//        [self.selectBtn addSubview:_imgPop];
//        [self.selectBtn addSubview:_lbPop];
//        [self.selectBtn addSubview:_btnBadge];
        
    }
    
    return self;
}

-(void)dismissHandle{

    YDLog(@"dain");
}
@end
