//
//  YDBadgeBtn.m
//  ListenToMe
//
//  Created by yadong on 2/6/15.
//  Copyright (c) 2015 listentome. All rights reserved.
//

#define KmaxBadgeValue 100

#import "YDBadgeBtn.h"

@implementation YDBadgeBtn

-(instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        // 设置自己的背景
        self.backgroundColor = [UIColor redColor];
        // font
        self.titleLabel.font = [UIFont systemFontOfSize:10];
        self.bounds = CGRectMake(0, 0, 15, 15);
        // 设置是否可以点击
        self.userInteractionEnabled = NO;
        
        // 使其成为circle
        self.layer.cornerRadius = 7.5;
        self.layer.masksToBounds = YES;
    }
    return self;
}

-(void)setBadgeValue:(int)badgeValue
{
    _badgeValue = badgeValue;
    
    if (_badgeValue > 0) {
        // 1.显示提醒按钮
        self.hidden = NO;
        
        // 提醒数字
        // 2.提醒数字
        if (_badgeValue >= KmaxBadgeValue) {
            // 2.1
            [self setTitle:@"99+" forState:UIControlStateNormal];
        }else{
            // 2.2 具体数字
            [self setTitle:[NSString stringWithFormat:@"%d",_badgeValue] forState:UIControlStateNormal];
        }
    }else{
        self.hidden = YES;
    }
}
@end
