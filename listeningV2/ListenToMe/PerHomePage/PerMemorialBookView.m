//
//  PerMemorialBookView.m
//  ListenToMe
//
//  Created by zhw on 15/4/4.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "PerMemorialBookView.h"
#import "YDKMemmoryCell.h"

@interface PerMemorialBookView ()
@property(nonatomic,strong) ListenToMeData *listenToMeData;
/**
 *  用户纪念册数组
 */
@property(nonatomic,strong) NSMutableArray *arrCommemorateInfoByUserId;
///**
// *  纪念册信息
// */
//@property(nonatomic,strong) CommemorateBaseInfo *commemorateBaseInfo;
@end

@implementation PerMemorialBookView

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self setTableView];
        [self initData];
    }
    return self;
    
}

-(void)setTableView{
    
    self.mTableView = [[UITableView alloc]initWithFrame:self.bounds];
    self.mTableView.delegate = self;
    self.mTableView.dataSource = self;
    [self addSubview:self.mTableView];
    
    
}

-(void)initData{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTableView) name:NOTIFY_GET_COMMEMORATEINFO_BYUSERID_PAGE_RESP object:nil];
    
    self.listenToMeData = [ListenToMeData getInstance];
}

-(void)refreshTableView{
    self.arrCommemorateInfoByUserId = self.listenToMeData.arrCommemorateInfoByUserId;
    [self.mTableView reloadData];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_COMMEMORATEINFO_BYUSERID_PAGE_RESP object:nil];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *ID = @"myWorkscell";
    YDKMemmoryCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (cell == nil) {
        cell = [[YDKMemmoryCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    
    CommemorateBaseInfo *commemorateBaseInfo = self.arrCommemorateInfoByUserId[indexPath.section];
    
    cell.arrUserBaseInfoNet = commemorateBaseInfo.stUserBaseInfoNet;

    
    if ([commemorateBaseInfo.sCommIcon isEqualToString:@""] || commemorateBaseInfo == nil) {
        
        cell.imgCover.image = [UIImage imageNamed:@"temp5.png"];
    }else{
    
        [cell.imgCover sd_setImageWithURL:[NSURL URLWithString:commemorateBaseInfo.sCommIcon]];
    }
    
    cell.lbTheme.text = @"闺蜜秀";
    
    if ([commemorateBaseInfo.sCommName isEqualToString:@""] || commemorateBaseInfo.sCommName == nil) {
        cell.lbPartyName.text = @"咸蛋超人的生日趴";
    }else{
        
        cell.lbPartyName.text = commemorateBaseInfo.sCommName;
    }
    
    
    cell.imgTheme.image = [UIImage imageNamed:@"memoryTopShow.png"];
    [cell.btnNumSongs setTitle:[NSString stringWithFormat:@"%d",commemorateBaseInfo.iMusicNum] forState:UIControlStateNormal];
    [cell.btnNumAlbums setTitle:[NSString stringWithFormat:@"%d",commemorateBaseInfo.iPictureNum] forState:UIControlStateNormal];
    [cell.btnNumSee setTitle:[NSString stringWithFormat:@"%d",commemorateBaseInfo.iWatchedNum] forState:UIControlStateNormal];
    [cell.btnNumFlower setTitle:[NSString stringWithFormat:@"%d",commemorateBaseInfo.iFlowerNum] forState:UIControlStateNormal];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 273;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return 1;
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return self.arrCommemorateInfoByUserId.count;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 15;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
