//
//  PerWorksView.m
//  ListenToMe
//
//  Created by zhw on 15/4/4.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "PerWorksView.h"
#import "YDTopListCell.h"
#import "NSDate+ZHW.h"

@interface PerWorksView ()<YDTopLiseCellDelegate>
@property(nonatomic,strong)ListenToMeData *listenToMeData;
/**
 *  他人作品
 */
@property(strong,nonatomic) NSMutableArray *arrMusicWorkBaseInfo;
@end

@implementation PerWorksView

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self setTableView];
        [self initData];
    }
    return self;

}



-(void)setTableView{
    
    self.mTableView = [[UITableView alloc]initWithFrame:self.bounds];
    self.mTableView.delegate = self;
    self.mTableView.dataSource = self;
    [self addSubview:self.mTableView];
    

}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_GET_MUSICWOKR_PAGE_RESP object:nil];
}

#pragma mark 初始化数据
-(void)initData{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTableView) name:NOTIFY_GET_MUSICWOKR_PAGE_RESP object:nil];
    self.listenToMeData = [ListenToMeData getInstance];
    

}

-(void)refreshTableView{
    self.arrMusicWorkBaseInfo = self.listenToMeData.arrMusicWorkBaseInfo;
    [self.mTableView reloadData];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *ID = @"myWorkscell";
    YDTopListCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (cell == nil) {
        cell = [[YDTopListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    MusicWorkBaseInfo *musicWorkBaseInfo = self.arrMusicWorkBaseInfo[indexPath.section];
    UserBaseInfoNet *userBaseInfoNet =  musicWorkBaseInfo.stUserBaseInfoNet;
    

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.delegate = self;
  
    
    cell.lbMusicName.text = musicWorkBaseInfo.stSongInfo.sSongName;
    cell.lbSinger.text = musicWorkBaseInfo.stSongInfo.sSongSinger;
    cell.lbTime.text = [NSString stringWithFormat:@"%@",[self getCreatedTime:musicWorkBaseInfo.lCreateTime]];
    
    [cell.imgAvatar sd_setImageWithURL:[NSURL URLWithString:userBaseInfoNet.sCovver]];
    [cell.btnListener setTitle:[NSString stringWithFormat:@"%d",musicWorkBaseInfo.iListenNum]forState:UIControlStateNormal];
    [cell.btnFlower setTitle:[NSString stringWithFormat:@"%d",musicWorkBaseInfo.iFlowerNum] forState:UIControlStateNormal];
            
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MusicWorkBaseInfo *musicWorkBaseInfo = self.arrMusicWorkBaseInfo[indexPath.row];
//    UserBaseInfoNet *userBaseInfoNet =  musicWorkBaseInfo.stUserBaseInfoNet;
    
    if ([self.delegate respondsToSelector:@selector(musicPlay:)]) {
        [self.delegate musicPlay:musicWorkBaseInfo];
    }
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 140;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return 1;
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return self.arrMusicWorkBaseInfo.count;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 0.1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 15;
}


#pragma mark - 计算时间间隔
-(NSString *)getCreatedTime:(int64_t)lCreateTime{
 
    int64_t time = lCreateTime;
    NSDate *createdDate = [[NSDate alloc]initWithTimeIntervalSince1970:time/1000.0];
    
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"EEE MMM dd HH:mm:ss Z yyyy";
    
#warning 真机调试下, 必须加上这段
    fmt.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    
    // 2..判断创建时间 和 现在时间 的差距
    if (createdDate.isToday) { // 今天
        if (createdDate.deltaWithNow.hour >= 1) {
            return [NSString stringWithFormat:@"%ld小时前", createdDate.deltaWithNow.hour];
        } else if (createdDate.deltaWithNow.minute >= 1) {
            return [NSString stringWithFormat:@"%ld分钟前", createdDate.deltaWithNow.minute];
        } else {
            return @"刚刚";
        }
    } else if (createdDate.isYesterday) { // 昨天
        fmt.dateFormat = @"昨天 HH:mm";
        return [fmt stringFromDate:createdDate];
    } else if (createdDate.isThisYear) { // 今年(至少是前天)
        fmt.dateFormat = @"MM-dd";
        return [fmt stringFromDate:createdDate];
    } else { // 非今年
        fmt.dateFormat = @"yyyy-MM-dd";
        return [fmt stringFromDate:createdDate];
    }

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
