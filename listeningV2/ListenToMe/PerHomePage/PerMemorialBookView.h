//
//  PerMemorialBookView.h
//  ListenToMe
//
//  Created by zhw on 15/4/4.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PerMemorialBookView : UIView<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong)UITableView *mTableView;
@end
