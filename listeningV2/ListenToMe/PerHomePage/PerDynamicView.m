//
//  PerDynamicView.m
//  ListenToMe
//
//  Created by zhw on 15/4/4.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "PerDynamicView.h"
#import "YDTopListCell.h"
#import "YDSubscribeCell.h"
#import "YDKMemmoryCell.h"
@implementation PerDynamicView

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
//        self.backgroundColor = [UIColor clearColor];
        [self setTableView];
    }
    return self;
    
}

-(void)setTableView{
    
    self.mTableView = [[UITableView alloc]initWithFrame:self.bounds];
    self.mTableView.delegate = self;
    self.mTableView.dataSource = self;
    [self addSubview:self.mTableView];
    
    [self.mTableView registerClass:[YDTopListCell class] forCellReuseIdentifier:@"myWorkscell"];
    [self.mTableView registerClass:[YDSubscribeCell class] forCellReuseIdentifier:@"subscribe"];
    [self.mTableView registerClass:[YDKMemmoryCell class] forCellReuseIdentifier:@"memory"];
    
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    NSString *identifier = nil;
    
    if (0 == indexPath.section || 2 == indexPath.section) {
        identifier = @"myWorkscell";
    }else if(1 == indexPath.section || 3 == indexPath.section){
    
        identifier = @"subscribe";
    }else{
        identifier = @"memory";
    }
    
    
    //利用多态
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:identifier owner:self options:nil] lastObject];
    }
    
    if (0 == indexPath.section || 2 == indexPath.section) {
        
       
        YDTopListCell *worksCell = (YDTopListCell *)cell;
        if (worksCell == nil) {
            worksCell = [[YDTopListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        worksCell.lbMusicName.text = @"泡沫";
        worksCell.lbSinger.text = @"邓紫棋";
        worksCell.lbTime.text = @"04:58";
        [worksCell.btnListener setTitle:@"12356" forState:UIControlStateNormal];
        [worksCell.btnFlower setTitle:@"114" forState:UIControlStateNormal];
        
        worksCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        return worksCell;
    }
    
    
    if (1 == indexPath.section || 3 == indexPath.section) {
        
        
        YDSubscribeCell *subscribeCell = (YDSubscribeCell *)cell;
        if (cell == nil) {
            subscribeCell = [[YDSubscribeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        
        [subscribeCell.imgAvatar setImage:[UIImage imageNamed:@"temp3.png"]];
        subscribeCell.lbDesc.text = @"北京的山东的朋友，有没有时间聚聚";
        subscribeCell.lbUserName.text = @"痘痘天使";
        subscribeCell.lbPartyName.text = @"山东朋友聚会";
        subscribeCell.lbLocation.text = @"同一首歌 - 苏州桥店";
        subscribeCell.lbSubsTime.text = @"2014-01-12 16:40";
        subscribeCell.lbDistance.text = @"5.8KM";
        subscribeCell.lbLinmit.text = @"5-10人 不限男女 可带朋友";
        subscribeCell.lbAffordPerson.text = @"AA";
        [subscribeCell.btnApply setTitle:@"20" forState:UIControlStateNormal];
        [subscribeCell.btnDiscuss setTitle:@"5" forState:UIControlStateNormal];
        [subscribeCell.btnJoin setTitle:@"5" forState:UIControlStateNormal];
        
        subscribeCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        return subscribeCell;
        
        
    }
    
    if (4 == indexPath.section) {
        

        YDKMemmoryCell *memoryCell = (YDKMemmoryCell *)cell;
        if (memoryCell == nil) {
            memoryCell = [[YDKMemmoryCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        
        
        memoryCell.imgCover.image = [UIImage imageNamed:@"temp5.png"];
        memoryCell.lbTheme.text = @"闺蜜秀";
        memoryCell.lbPartyName.text = @"咸蛋超人的生日趴";
        memoryCell.lbMusicName.text = @"Almost lover";
        memoryCell.imgTheme.image = [UIImage imageNamed:@"memoryTopShow.png"];
        [memoryCell.btnNumSongs setTitle:@"128" forState:UIControlStateNormal];
        [memoryCell.btnNumAlbums setTitle:@"24" forState:UIControlStateNormal];
        [memoryCell.btnNumSee setTitle:@"20" forState:UIControlStateNormal];
        [memoryCell.btnNumFlower setTitle:@"114" forState:UIControlStateNormal];
        
        memoryCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return memoryCell;
    }
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (indexPath.section) {
        case 0:
            return 140;
            break;
            case 1:
            return 292;
            break;
            case 2:
            return 140;
            break;
            case 3:
            return 292;
            case 4:
            return 273;
            break;
            
        default:
            return 0;
            break;
    }
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return 1;
    
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 5;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{

    
    return 43;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}



-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 43)];//tableView.tableHeaderView.height

    
    UIImageView *lineView = [[UIImageView alloc]initWithFrame:CGRectMake(15 + 15 - 1.5, 0, 3, 43)];
    lineView.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:0.12];
    [headerView addSubview:lineView];
    
    UIImageView *titleView = [[UIImageView alloc]init];
     UIImage *iconImg = nil; //图标
    
    UILabel *titleLabel = [[UILabel alloc]init];
    NSString *titleContent = nil; //分区的title的内容
    
    NSString *time = nil; //动态发布的时间
    
    switch (section) {
        case 0:{
            titleContent = @"发表作品";
            iconImg = [UIImage imageNamed:@"作品.png"];
            time = @"2小时";
            [titleLabel setTextColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
        }
            break;
        case 1:{
            titleContent = @"参加90后姐妹聚会约歌";
            iconImg = [UIImage imageNamed:@"参与约歌.png"];
            time = @"4小时";
            [titleLabel setTextColor:[UIColor rgbFromHexString:@"#9B9B9B" alpaa:1.0]];
        }
            break;
        case 2:{
            titleContent = @"转发张三的作品";
            iconImg = [UIImage imageNamed:@"作品1.png"];
            time = @"3天前";
            [titleLabel setTextColor:[UIColor rgbFromHexString:@"#9B9B9B" alpaa:1.0]];
        }
            break;
        case 3:{
            titleContent = @"约歌";
            iconImg = [UIImage imageNamed:@"约歌.png"];
            time = @"3天前";
            [titleLabel setTextColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
        }
            break;
        case 4:{
            titleContent = @"发表纪念册";
            iconImg = [UIImage imageNamed:@"纪念册.png"];
            time = @"8个月前";
            [titleLabel setTextColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
        }
            break;
            
            
            
        default:
            break;
    }
   
    //图标
    titleView.frame = CGRectMake(15, 0, iconImg.size.width * 0.5, iconImg.size.height * 0.5);
    [titleView setImage:iconImg];
    [headerView addSubview:titleView];
    
    //title
    titleLabel.frame = CGRectMake(titleView.x + titleView.width + 5, titleView.centerY - 27 / 4, 200, 27 / 2);
    titleLabel.text = titleContent;
    [headerView addSubview:titleLabel];
    
    //时间
    UILabel *timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - 15 - 40, titleLabel.y, 40, 13)];
    [timeLabel setFont:[UIFont systemFontOfSize:11.0]];
    [timeLabel setTextColor:[UIColor rgbFromHexString:@"#9B9B9B" alpaa:1.0]];
    timeLabel.text = time;
    [headerView addSubview:timeLabel];
    
    headerView.backgroundColor = [UIColor colorWithRed:241/255.0 green:241/255.0 blue:241/255.0 alpha:1.0];
    
    return headerView;
    
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
