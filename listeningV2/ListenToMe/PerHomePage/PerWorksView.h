//
//  PerWorksView.h
//  ListenToMe
//
//  Created by zhw on 15/4/4.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PerWorksViewDelegate <NSObject>

@optional
-(void)musicPlay:(MusicWorkBaseInfo *)musicWorkBaseInfo;
@end

@interface PerWorksView : UIView<UITableViewDataSource,UITableViewDelegate>
@property(strong,nonatomic) UITableView *mTableView;
@property(nonatomic,assign) id<PerWorksViewDelegate>delegate;
@end
