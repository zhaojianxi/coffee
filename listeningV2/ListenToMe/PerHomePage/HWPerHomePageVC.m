//
//  HWPerHomePageVC.m
//  ListenToMe
//
//  Created by zhw on 15/4/3.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "HWPerHomePageVC.h"
#import "MyAlbumVC.h"
#import "PerWorksView.h"
#import "PerMemorialBookView.h"
#import "PerDynamicView.h"
#import "FriendsMsgVC.h"
#import "HWMusicPlayVC.h"
@interface HWPerHomePageVC ()<UIGestureRecognizerDelegate,PerWorksViewDelegate>
/**
 *  整个页面的滚动视图
 */
@property(strong,nonatomic) UIScrollView *mScrollView;
/**
 *  tableview
 */
@property(strong,nonatomic) UITableView *mTableView;
/**
 *  头像 & 个性签名 等个人信息的承载视图
 */
@property(strong,nonatomic) UIView *viewUserInfo;
/**
 *  相册封面页
 */
@property(strong,nonatomic) UIImageView *imgAlbum;
/**
 *  头像
 */
@property(strong,nonatomic) UIButton *btnAvatar;
/**
 *  头像外围的白框
 */
@property(strong,nonatomic) UIImageView *imgMask;
/**
 *  用户名
 */
@property(strong,nonatomic) UILabel *lbUserName;
/**
 *  等级
 */
@property(strong,nonatomic) UIButton *btnRank;
/**
 *  送花
 */
@property(strong,nonatomic) UIButton *btnFlower;
/**
 *  个性签名
 */
@property(strong,nonatomic) UILabel *lbDesc;
/**
 *   私聊Btn
 */
@property(strong,nonatomic) UIButton *btnPrivateChat;
/**
 *  位置
 */
@property(strong,nonatomic) UILabel *lbLocation;

/**
 *  封面相册点击手势
 */
@property(strong,nonatomic)UITapGestureRecognizer *singleTap;
/**
 *  控制三个视图的根视图
 */
@property(strong,nonatomic)UIView *segmentBgView;
/**
 *  作品
 */
@property(strong,nonatomic)UIButton *btnPerWorks;
/**
 *  动态
 */
@property(strong,nonatomic)UIButton *btnPerDynamic;
/**
 *  纪念册
 */
@property(strong,nonatomic)UIButton *btnPerBookAlbum;
/**
 *  button上方的横线
 */
@property(strong,nonatomic)UIImageView *imgSegLine;
/**
 *  3个视图的frame
 */
@property(assign,nonatomic) CGRect frameThreeViews;
/**
 *  个人作品
 */
@property(nonatomic,strong)PerWorksView *perWorksView;
/**
 *  个人动态
 */
@property(nonatomic,strong)PerDynamicView *perDynamicView;
/**
 *  个人纪念册
 */
@property(nonatomic,strong)PerMemorialBookView *perMemoryBookView;
/**
 *  数据
 */
@property(nonatomic,strong)ListenToMeData *listenToMeData;
@end

@implementation HWPerHomePageVC
@synthesize mScrollView;
@synthesize mTableView;
@synthesize viewUserInfo;
@synthesize imgAlbum;
@synthesize btnAvatar;
@synthesize imgMask;
@synthesize lbUserName;
@synthesize btnRank;
@synthesize btnFlower;
@synthesize lbDesc;
@synthesize btnPrivateChat;
@synthesize singleTap;
@synthesize frameThreeViews;



#pragma mark - 视图的生命周期
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
   
    
//    self.navigationItem.title = @"ListenToMe";
    self.navigationItem.title = self.userBaseInfoNet.sNick;
    
    [self setUI];

    
}
#warning 解决在私聊界面送歌后,他人主页的音乐作品数据跟用户自己的音乐作品数据重用的问题
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    //请求用户作品
    [[NetReqManager getInstance] sendGetMusicWorkWithUserId:0 IOffset:0 INum:1 LWatchUserId:self.userBaseInfoNet.uuid];
    
    //请求用户纪念册
    [[NetReqManager getInstance] sendGetCommemorateInfoByUserId:[ListenToMeDBManager getUuid] LWatchUserId:self.userBaseInfoNet.uuid IOffset:0 INum:2];
}

#pragma mark - 布局视图
-(void)setUI{
    
    [self setCustomizedBarButtonItem];
    
    // 个人主页相册 个人头像 名字 等级 鲜花 地区 签名等
    [self setInformationView];
    
    
    
}


#pragma mark - 自定义BarButtonItem
-(void)setCustomizedBarButtonItem{
    
    UIButton *leftBtn = [[UIButton alloc]init];
    UIImage *leftBtnImg = [UIImage imageNamed:@"whiteBack.png"];
    leftBtn.frame = CGRectMake(0, 0, leftBtnImg.size.width, leftBtnImg.size.height);
    [leftBtn setImage:leftBtnImg forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(clickLeftBarBtnItem:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customLeftBtnItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = customLeftBtnItem;
    
    
    UIButton *rightBtn = [[UIButton alloc]init];
    UIImage *imgRight = [UIImage imageNamed:@"more.png"];
    rightBtn.frame = CGRectMake(0, 0, imgRight.size.width, imgRight.size.height);
    [rightBtn setImage:imgRight forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(clickRightBarBtnItem:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customRightBtnItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = customRightBtnItem;
}

#pragma mark - 自定义返回button
-(void)clickLeftBarBtnItem:(UIButton *)backBtn{

    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 导航栏更多菜单
-(void)clickRightBarBtnItem:(UIButton *)moreBtn{

    YDLog(@"点击了跟多导航栏");

}


#pragma mark - setInformationView 个人信息展示
-(void)setInformationView{

//    mScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
//    mScrollView.contentSize = CGSizeMake(screenWidth, 1000);
//    [self.view addSubview:mScrollView];
//    mScrollView.showsVerticalScrollIndicator = NO;
    
    //设置个人首页封面相册
    [self setImgAlbum];
    
    //设置个人信息
    [self setViewUserInfo];
    
    
    //作品 动态 纪念册的自定义的segment
    
    [self setThreeSegment];
    
    
    frameThreeViews = CGRectMake(0, naviAndStatusH + self.segmentBgView.height + self.imgAlbum.height + self.viewUserInfo.height, screenWidth, screenHeight - self.segmentBgView.y - self.segmentBgView.height);
    
    
    //设置三个视图
    [self setThreeVC];
    

}



#pragma mark - setImgAlbum 设置封面相册
-(void)setImgAlbum{

    imgAlbum = [[UIImageView alloc]initWithFrame:CGRectMake(0, naviAndStatusH, screenWidth, 175)];
    if ([self.userBaseInfoNet.sBackground  isEqual: @""]) {
        
        imgAlbum.image = [UIImage imageNamed:@"album.png"];
    }else{
       
        [imgAlbum sd_setImageWithURL:[NSURL URLWithString:self.userBaseInfoNet.sBackground]];
        
    }
    
//    imgAlbum.backgroundColor = [UIColor clearColor];
    [self.view addSubview:imgAlbum];
    imgAlbum.layer.masksToBounds = YES;
    imgAlbum.contentMode = UIViewContentModeScaleToFill;
    imgAlbum.userInteractionEnabled = YES;
    

//    //透明的button点击进入相册
//    UIButton *btnAlbum = [[UIButton alloc]initWithFrame:imgAlbum.frame];
//    [mScrollView addSubview:btnAlbum];
//    btnAlbum.backgroundColor = [UIColor clearColor];
//    [btnAlbum addTarget:self action:@selector(viewAlbumHandle:) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:btnAlbum];
    
    //添加一个点击手势
    singleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewAlbumHandle:)];
    [imgAlbum addGestureRecognizer:singleTap];
    singleTap.delegate = self;
   
    
}

#pragma mark - UIGestrueRecognizerDelegate
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}


#pragma mark - viewAlbumHandle 浏览相册
-(void)viewAlbumHandle:(UITapGestureRecognizer *)tap{

    MyAlbumVC *myAlbumVC = [[MyAlbumVC alloc]init];
    myAlbumVC.navigationItem.title = [NSString stringWithFormat:@"%@的相册",self.userBaseInfoNet.sNick];
    myAlbumVC.uuid = 0;
    myAlbumVC.iOffset = 0;
    myAlbumVC.iNum =self.userBaseInfoNet.iPictureNum;
    myAlbumVC.lWatchUserId = self.userBaseInfoNet.uuid;
    myAlbumVC.lCommemorateId = 0;
    myAlbumVC.isPushFromPerCenter = NO;
    [self.navigationController pushViewController:myAlbumVC animated:YES];
}


#pragma mark - setViewUserInfo 设置个人主页信息
-(void)setViewUserInfo{


    // 个人信息承载页
    viewUserInfo = [[UIView alloc]initWithFrame:CGRectMake(0,imgAlbum.frame.origin.y + imgAlbum.frame.size.height, screenWidth, 70 + 40 )];
//    viewUserInfo.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:0.12];
    viewUserInfo.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:viewUserInfo];
    
    // 头像
    CGFloat marginX = 15;

    
    UIView *avatarBgView = [[UIView alloc]initWithFrame:CGRectMake(marginX - 1, imgAlbum.y + imgAlbum.height - 15, 42, 42)];
    avatarBgView.backgroundColor = [UIColor whiteColor];
    avatarBgView.layer.masksToBounds = YES;
    avatarBgView.layer.cornerRadius = 5;
    [self.view addSubview:avatarBgView];
    
    //已经将头像button的交互关掉
    btnAvatar = [[UIButton alloc]initWithFrame:CGRectMake(1, 1 , 40, 40)];
    [avatarBgView addSubview:btnAvatar];
    
    CGFloat avatarImgViewX = 0 ;
    CGFloat avatarImgViewY =  0;
    CGFloat avatarImgViewWH = 40;
    UIImageView *avatarImgView = [[UIImageView alloc]initWithFrame:CGRectMake(avatarImgViewX, avatarImgViewY, avatarImgViewWH, avatarImgViewWH)];
    [avatarImgView sd_setImageWithURL:[NSURL URLWithString:self.userBaseInfoNet.sCovver]];
    
    avatarImgView.layer.masksToBounds = YES;
    avatarImgView.layer.cornerRadius = 5;
    avatarImgView.userInteractionEnabled = NO;
    [btnAvatar addSubview:avatarImgView];
    
    
    
    
    // 用户名
    lbUserName = [[UILabel alloc]initWithFrame:CGRectMake(marginX + 40 + 10, 10, 100, 20)];
    lbUserName.backgroundColor = [UIColor clearColor];
//    lbUserName.text = @"ListenToMe";
    lbUserName.text = self.userBaseInfoNet.sNick;
    lbUserName.font = [UIFont systemFontOfSize:15.0];
    lbUserName.textColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
    [viewUserInfo addSubview:lbUserName];
    
    //分割线
    UIImageView *line1 = [[UIImageView alloc]initWithFrame:CGRectMake(marginX, lbUserName.y + lbUserName.height + 10, screenWidth - marginX * 2, 1)];
    [line1 setBackgroundColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:0.12]];
    [viewUserInfo addSubview:line1];
    
    //个性签名的淡紫色背景
    UIImageView *desBgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, line1.y + line1.height, screenWidth, viewUserInfo.height - line1.y + line1.height)];
    desBgView.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:0.12];
    desBgView.userInteractionEnabled = YES;
    [viewUserInfo addSubview:desBgView];
    
    
    // 个性签名
    lbDesc = [[UILabel alloc]initWithFrame:CGRectMake(marginX, viewUserInfo.height - 70, screenWidth - marginX, 70)]; // 35是签名的高度
    lbDesc.numberOfLines = 0;
    lbDesc.backgroundColor = [UIColor clearColor];
    if([self.userBaseInfoNet.sDesc  isEqual: @""] || self.userBaseInfoNet.sDesc == nil){
        lbDesc.text = @"唱不唱我都在这里，来不来该唱还唱\n第二行状态(个性签名)";

    }else{
        
        lbDesc.text = self.userBaseInfoNet.sDesc;
    }
    
    
    
    
    lbDesc.textColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
    lbDesc.font = [UIFont systemFontOfSize:12.0];
    [viewUserInfo addSubview:lbDesc];
    
    // 私聊
    CGFloat privateH = 24; //私聊UI的高度
    UIImage *privateChat = [UIImage imageNamed:@"私聊.png"];
    UIImageView *privateChatView = [[UIImageView alloc]initWithFrame:CGRectMake(screenWidth - 15 - 49, lbDesc.centerY - privateH * 0.5, 49, privateH)];
    [privateChatView setImage:privateChat];
    
    
    btnPrivateChat = [[UIButton alloc]initWithFrame:privateChatView.bounds];
    [btnPrivateChat setTitle:@"私聊" forState:UIControlStateNormal];
    [btnPrivateChat setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateNormal];
    [btnPrivateChat.titleLabel setFont:[UIFont systemFontOfSize:12.0]];
    
    //添加私聊点击事件
    [btnPrivateChat addTarget:self action:@selector(privateChatHandle:) forControlEvents:UIControlEventTouchUpInside];
    [privateChatView addSubview:btnPrivateChat];
    privateChatView.userInteractionEnabled = YES;
    [viewUserInfo addSubview:privateChatView];
    
//    // 等级
//    btnRank = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - 230, lbUserName.y, 30, 20)];
//    [btnRank setTitle:@"lv 9" forState:UIControlStateNormal];
//    btnFlower.titleLabel.font = [UIFont systemFontOfSize:12.0];
//    [btnRank setTitleColor:[UIColor rgbFromHexString:@"#FC9A69" alpaa:1.0] forState:UIControlStateNormal];
//    [viewUserInfo addSubview:btnRank];
    
    // 花
    btnFlower = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - 160, lbUserName.y, 70, 20)];
//    [btnFlower setTitle:@"999" forState:UIControlStateNormal];
    
    [btnFlower setTitle:[NSString stringWithFormat:@"%d",self.userBaseInfoNet.iFlowerNum] forState:UIControlStateNormal];
    
    [btnFlower setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateNormal];
    [btnFlower.titleLabel setFont:[UIFont systemFontOfSize:12.0]];
    [viewUserInfo addSubview:btnFlower];
    
    //鲜花图标
    UIImage *flower = [UIImage imageNamed:@"歌曲鲜花.png"];
    UIImageView *flowerView = [[UIImageView alloc]initWithFrame:CGRectMake(btnFlower.x - flower.size.width * 0.5, 0, flower.size.width * 0.5, flower.size.height * 0.5)];
    flowerView.centerY = btnFlower.centerY;
    flowerView.image = flower;
    [viewUserInfo addSubview:flowerView];
    
    // 位置 （省份 || 城市）
    _lbLocation = [[UILabel alloc]init];
    _lbLocation.frame = CGRectMake(screenWidth - marginX - 50, lbUserName.y, 50, 20);
    _lbLocation.font = [UIFont systemFontOfSize:12.0];
    [_lbLocation setTextColor:[UIColor rgbFromHexString:@"#A8AEAE" alpaa:1.0]];
    _lbLocation.text = self.userBaseInfoNet.sAddress;
    _lbLocation.textAlignment = NSTextAlignmentRight;
    [viewUserInfo addSubview:_lbLocation];
    
    
   
    
    
    
    
}


#pragma mark- privateChatHandle 点击私聊事件
-(void)privateChatHandle:(UIButton *)privateBtn{
    YDLog(@"向XXX发起私聊");
    FriendsMsgVC *friendMsgVC = [[FriendsMsgVC alloc]init];
    friendMsgVC.navigationItem.title = self.lbUserName.text;
    friendMsgVC.friendUserBaseInfoNet = self.userBaseInfoNet;
    [ListenToMeData getInstance].privateChatUser = self.userBaseInfoNet;
    [self.navigationController pushViewController:friendMsgVC animated:YES];
}


#pragma mark - setThree 设置三个视图
-(void)setThreeSegment{

    self.segmentBgView = [[UIView alloc]initWithFrame:CGRectMake(0, self.viewUserInfo.y + self.viewUserInfo.height, screenWidth, 39)];

    _segmentBgView.backgroundColor = [UIColor whiteColor];

     [self.view addSubview:self.segmentBgView];
    
    //分割线
    UIImageView *line = [[UIImageView alloc]initWithFrame:CGRectMake(0,0, screenWidth, 1)];
    [line setBackgroundColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:0.5]];
    [_segmentBgView addSubview:line];
    
    
    //初始化button上方的横线
    // 初始化横线，当选中了按钮时，改变其frame
    
//    self.imgSegLine = [[UIImageView alloc]initWithFrame:CGRectMake(screenWidth * 0.5,1,30, 2)]; // 2是紫色横线的高度
//    _imgSegLine.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
//    [self.segmentBgView addSubview:_imgSegLine];
    
    //初始化3个button
    
    CGFloat interval = screenWidth * 0.5 / 3; //3个button之间的间隔
    CGFloat btnW = 30; //button宽度
    
    //作品
    self.btnPerWorks = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth * 0.5 + interval, 0, btnW, self.segmentBgView.height - 1)];
    self.btnPerWorks.backgroundColor = [UIColor clearColor];
    
    [self.btnPerWorks setImage:[UIImage imageNamed:@"作品01.png"] forState:UIControlStateNormal];
    [self.btnPerWorks setImage:[UIImage imageNamed:@"作品02.png"] forState:UIControlStateSelected];
    [self.btnPerWorks addTarget:self action:@selector(clickWorksHandle:) forControlEvents:UIControlEventTouchUpInside];
    //第一个button默认下是选中状态
    self.btnPerWorks.selected = YES;
    [self.segmentBgView addSubview:self.btnPerWorks];
    
//    //动态(该功能已经移除)
//    self.btnPerDynamic = [[UIButton alloc]initWithFrame:CGRectMake(  screenWidth * 0.5 + interval , 0, 30, self.segmentBgView.height - 1)];
//    self.btnPerDynamic.backgroundColor = [UIColor clearColor];
//    
//    [self.btnPerDynamic setImage:[UIImage imageNamed:@"动态01.png"] forState:UIControlStateNormal];
//    [self.btnPerDynamic setImage:[UIImage imageNamed:@"动态02.png"] forState:UIControlStateSelected];
//    [self.btnPerDynamic addTarget:self action:@selector(clickDynamicHandle:) forControlEvents:UIControlEventTouchUpInside];
//    //第一个button默认下是选中状态
//    self.btnPerDynamic.selected = NO;
//    [self.segmentBgView addSubview:self.btnPerDynamic];
    
    //纪念册
    self.btnPerBookAlbum = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth - btnW - 15, 0, btnW, self.segmentBgView.height - 1)];
    self.btnPerBookAlbum.backgroundColor = [UIColor clearColor];
    
    [self.btnPerBookAlbum setImage:[UIImage imageNamed:@"纪念册01.png"] forState:UIControlStateNormal];
    [self.btnPerBookAlbum setImage:[UIImage imageNamed:@"纪念册02.png"] forState:UIControlStateSelected];
    [self.btnPerBookAlbum addTarget:self action:@selector(clickBookAlbumHandle:) forControlEvents:UIControlEventTouchUpInside];
    //第一个button默认下是选中状态
    self.btnPerBookAlbum.selected = NO;
    [self.segmentBgView addSubview:self.btnPerBookAlbum];
    
    
    
    
   
    
    
    
}


#pragma mark - clickWorksHandle 点击作品,事件处理

-(void)clickWorksHandle:(UIButton *)button{

    if (button.selected ==  YES) {
        
    }else{
        
        button.selected = YES;
//        self.btnPerDynamic.selected = NO;
        self.btnPerBookAlbum.selected = NO;
        
        //
        [self.perWorksView.mTableView reloadData];
        [self.view bringSubviewToFront:self.perWorksView];


    }
    
}

//#pragma mark - clickDynamicHandle 点击动态,事件处理(他人动态功能已经移除)
//-(void)clickDynamicHandle:(UIButton *)button{
//
//    
//    if (button.selected ==  YES) {
//        
//    }else{
//        
//
//        button.selected = YES;
//        self.btnPerWorks.selected = NO;
//        self.btnPerBookAlbum.selected = NO;
//
//        [self.perDynamicView.mTableView reloadData];
//        
//        [self.view bringSubviewToFront:self.perDynamicView];
//      
//        
//       
//    }
//
//}

#pragma mark - clickBookAlbumHandle 点击纪念册,事件处理
-(void)clickBookAlbumHandle:(UIButton *)button{


    if (button.selected ==  YES) {
        
    }else{

        button.selected = YES;
//        self.btnPerDynamic.selected = NO;
        self.btnPerWorks.selected = NO;
        
        
        [self.perMemoryBookView.mTableView reloadData];
        [self.view bringSubviewToFront:self.perMemoryBookView];
        
    }
}



#pragma mark - setThreeVC 设置三个视图
-(void)setThreeVC{

    
    //添加纪念册视图
    self.perMemoryBookView = [[PerMemorialBookView alloc]initWithFrame:frameThreeViews];
    
    
    
    [self.view addSubview:self.perMemoryBookView];
    
    //添加动态视图
//    self.perDynamicView = [[PerDynamicView alloc]initWithFrame:frameThreeViews];
//    
//    [self.view addSubview:self.perDynamicView];
    
    //添加作品视图
    self.perWorksView = [[PerWorksView alloc]initWithFrame:frameThreeViews];
    
    self.perWorksView.delegate = self;
    
    
    [self.view addSubview:self.perWorksView];
    
    
}

#pragma mark - 相关代理
-(void)musicPlay:(MusicWorkBaseInfo *)musicWorkBaseInfo{
    
    HWMusicPlayVC *musicPlayVc = [[HWMusicPlayVC alloc]init];
    musicPlayVc.userBaseInfoNet = musicWorkBaseInfo.stUserBaseInfoNet;
    musicPlayVc.musicWorkBaseInfo = musicWorkBaseInfo;
    musicPlayVc.lWorkID = musicWorkBaseInfo.lMusicWorkId;
    [self.navigationController pushViewController:musicPlayVc animated:YES];
    
}

-(void)gotoMusicPlay{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
