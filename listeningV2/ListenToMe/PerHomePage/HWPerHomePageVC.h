//
//  HWPerHomePageVC.h
//  ListenToMe
//
//  Created by zhw on 15/4/3.
//  Copyright (c) 2015年 listentome. All rights reserved.
//
/**
 *  个人主页
 */
#import "YDBaseVC.h"

@interface HWPerHomePageVC : YDBaseVC
/**
 *  用户基本信息
 */
@property(nonatomic,strong)UserBaseInfoNet *userBaseInfoNet;
@end
