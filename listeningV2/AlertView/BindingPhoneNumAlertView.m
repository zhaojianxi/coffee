//
//  BindingPhoneNumAlertView.m
//  ListenToMe
//
//  Created by zhw on 15/4/20.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "BindingPhoneNumAlertView.h"

@interface BindingPhoneNumAlertView ()<UITextFieldDelegate>
/**
 *  标题背景
 */
@property(nonatomic,strong)UIImageView *titleBgView;
/**
 *  标题
 */
@property(nonatomic,strong)UILabel *lbTitle;
/**
 *  关闭按钮
 */
@property(nonatomic,strong)UIButton *btnClose;
/**
 *  填写手机号
 */
@property(nonatomic,strong)UITextField *tfPhoneNum;
/**
 *  获取验证码
 */
@property(nonatomic,strong)UIButton *btnGetCode;
/**
 *  完成
 */
@property(nonatomic,strong)UIButton *btnDone;
/**
 *  验证码获取定时器
 */
@property(nonatomic,strong)NSTimer *timer;
/**
 *  当前秒
 */
@property(nonatomic,assign) NSInteger seconds;
/**
 *  临时保存手机号
 */
@property(nonatomic,strong) NSString *phongNum;
/**
 *  遮盖视图
 */
@property(nonatomic,strong) UIView *coverView;
@end


@implementation BindingPhoneNumAlertView
@synthesize timer;
@synthesize seconds;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(instancetype)init{
    self = [super init];
    
    if (self) {
        [self setUI];
    }
    
    return self;
}

-(void)setUI{
    
    self.frame = [UIScreen mainScreen].bounds;
    self.backgroundColor = [UIColor colorWithRed:203/255.0 green:203/255.0 blue:203/255.0 alpha:0.5];
    UIView *backView = [[UIView alloc]initWithFrame:self.bounds];
    backView.backgroundColor =[UIColor rgbFromHexString:@"#000000" alpaa:0.4];
    [self addSubview:backView];
    
    
    //布局子视图
    [self setSubView];
    
    
}

#pragma mark -布局子视图
-(void)setSubView{
    
    //背景
    CGFloat frontX = 15;
    CGFloat alertY = 150;
    UIImage *alertBgImg = [UIImage imageNamed:@"background.png"];
    self.alertBgView = [[UIImageView alloc]initWithFrame:CGRectMake(frontX, alertY, screenWidth - frontX * 2, alertBgImg.size.height * 0.5)];
    [self.alertBgView setImage:alertBgImg];
    self.alertBgView.userInteractionEnabled = YES;
    [self addSubview:self.alertBgView];
    
    //标题 背景
    CGFloat titleBgH = 44;
    UIImage *titleBgImg = [UIImage imageNamed:@"background04.png"];
    self.titleBgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.alertBgView.width, titleBgH)];
    [self.titleBgView setImage:titleBgImg];
    self.titleBgView.userInteractionEnabled = YES;
    [self.alertBgView addSubview:self.titleBgView];
    
    CGFloat titleW = self.alertBgView.width - 60;
    self.lbTitle = [[UILabel alloc]initWithFrame:CGRectMake(frontX, 0, titleW, titleBgH)];
    self.lbTitle.text = @"绑定手机号激活礼券";
    [self.lbTitle setFont:[UIFont systemFontOfSize:12.0]];
    [self.lbTitle setTextColor:[UIColor rgbFromHexString:@"#E6CDFF" alpaa:1.0]];
    [self.lbTitle setTextAlignment:NSTextAlignmentLeft];
    [self.titleBgView addSubview:self.lbTitle];
    
    //关闭
    CGFloat closeWH = 15;
    CGFloat closeX = self.titleBgView.width - frontX - closeWH;
    CGFloat closeY= (self.titleBgView.height - closeWH) * 0.5;
    UIImage *closeImg = [UIImage imageNamed:@"close.png"];
    self.btnClose = [[UIButton alloc]initWithFrame:CGRectMake(closeX, closeY,closeWH , closeWH)];
    [self.btnClose setImage:closeImg forState:UIControlStateNormal];
    [self.btnClose addTarget:self action:@selector(backToVC:) forControlEvents:UIControlEventTouchUpInside];
    [self.titleBgView addSubview:self.btnClose];
    
    //手机号 验证码 获取验证码
    
    CGFloat phoneNumX = 25;
    CGFloat phoneNumY = 13;
    CGFloat phoneNumW = self.alertBgView.width - phoneNumX * 2;
    CGFloat phoneNumH = 44;
    UIImage *phoneNumImg = [UIImage imageNamed:@"background03.png"];
    self.tfPhoneNum = [[UITextField alloc]initWithFrame:CGRectMake(phoneNumX,self.titleBgView.height + phoneNumY, phoneNumW, phoneNumH)];
    [self.tfPhoneNum setBackground:phoneNumImg];
    self.tfPhoneNum.placeholder = @" 请填写手机号码";
    [self.tfPhoneNum setFont:[UIFont systemFontOfSize:12.0]];
    [self.tfPhoneNum setTextAlignment:NSTextAlignmentLeft];
    [self.tfPhoneNum setTextColor:[UIColor rgbFromHexString:@"#000000" alpaa:1.0]];//C4C3C8
    self.tfPhoneNum.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    self.tfPhoneNum.tag = 1001;
    self.tfPhoneNum.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.tfPhoneNum.delegate = self;
    [self.tfPhoneNum addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.alertBgView addSubview:self.tfPhoneNum];
    
    CGFloat getBtnW = 84;
    CGFloat getBtnH = 44;
    CGFloat getBtnX = self.alertBgView.width - phoneNumX - getBtnW;
    CGFloat getBtnY = self.tfPhoneNum.y + self.tfPhoneNum.height + 5;
    UIImage *getCodeImg = [UIImage imageNamed:@"background01.png"];
    UIImage *getedCodeImg = [UIImage imageNamed:@"background00.png"];
    self.btnGetCode = [[UIButton alloc]initWithFrame:CGRectMake(getBtnX, getBtnY, getBtnW, getBtnH)];
    [self.btnGetCode setImage:getCodeImg forState:UIControlStateNormal];
    [self.btnGetCode setImage:getedCodeImg forState:UIControlStateSelected];
    [self.btnGetCode setTitle:@"获取验证码" forState:UIControlStateNormal];
    [self.btnGetCode setTitleEdgeInsets:UIEdgeInsetsMake(0, - (getBtnW * 2), 0, 0)];
    [self.btnGetCode setTitleColor:[UIColor rgbFromHexString:@"#FFFFFF" alpaa:1.0] forState:UIControlStateNormal];
    [self.btnGetCode setTitleColor:[UIColor rgbFromHexString:@"#FFFFFF" alpaa:1.0] forState:UIControlStateSelected];
    [self.btnGetCode addTarget:self action:@selector(getCodeHandle:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnGetCode.titleLabel setFont:[UIFont systemFontOfSize:12.0]];
    [self.alertBgView addSubview:self.btnGetCode];
    
    self.coverView = [[UIView alloc]initWithFrame:self.btnGetCode.frame];
    self.coverView.backgroundColor = [UIColor clearColor];
    
    CGFloat checkCodeX = self.tfPhoneNum.x;
    CGFloat checkCodeY = self.btnGetCode.y;
    CGFloat checkCodeW = self.tfPhoneNum.width - self.btnGetCode.width - 5;
    CGFloat checkCodeH = 44;
    UIImage *checkCodeImg = [UIImage imageNamed:@"background02.png"];
    self.tfCheckCode = [[UITextField alloc]initWithFrame:CGRectMake(checkCodeX, checkCodeY, checkCodeW, checkCodeH)];
    [self.tfCheckCode setBackground:checkCodeImg];
    self.tfCheckCode.placeholder = @" 验证码";
    [self.tfCheckCode setFont:[UIFont systemFontOfSize:12.0]];
    [self.tfCheckCode setTextColor:[UIColor rgbFromHexString:@"#000000" alpaa:1.0]];//C4C3C8
    [self.tfCheckCode setTextAlignment:NSTextAlignmentLeft];
    self.tfCheckCode.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
//    self.tfCheckCode.secureTextEntry = YES;
    self.tfCheckCode.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.tfCheckCode.enabled = NO;
    self.tfCheckCode.tag = 1002;
    self.tfCheckCode.delegate = self;
    [self.alertBgView addSubview:self.tfCheckCode];
    
    self.lbFailure = [[UILabel alloc]initWithFrame:CGRectMake(self.tfPhoneNum.x,self.tfCheckCode.y + self.tfCheckCode.height,phoneNumW,phoneNumH)];
    self.lbFailure.font = [UIFont systemFontOfSize:12.0];
    self.lbFailure.textColor = [UIColor rgbFromHexString:@"#FC6B6A" alpaa:1.0];
    self.lbFailure.text = @"验证码错误,请重新输入!";
    self.lbFailure.textAlignment = NSTextAlignmentCenter;
    [self.alertBgView addSubview:self.lbFailure];
    self.lbFailure.hidden = YES;
    
    //分割线
    
    UIImageView *line1 = [[UIImageView alloc]initWithFrame:CGRectMake(0, self.tfCheckCode.y + self.tfCheckCode.height + 66, self.alertBgView.width, 1)];
    line1.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:0.8];
    [self.alertBgView addSubview:line1];
    
    //完成
    CGFloat doneX = phoneNumX;
    CGFloat doneY = line1.y + line1.height + 21;
    CGFloat doneW = self.alertBgView.width - doneX * 2;
    CGFloat doneH = 44;
    UIImage *doImg = [UIImage imageNamed:@"background05.png"];
    UIImage *doneImg = [UIImage imageNamed:@"background06.png"];
    self.btnDone = [[UIButton alloc]initWithFrame:CGRectMake(doneX, doneY, doneW, doneH)];
    [self.btnDone setImage:doImg forState:UIControlStateNormal];
    [self.btnDone setImage:doneImg forState:UIControlStateSelected];
    [self.btnDone setTitle:@"完成" forState:UIControlStateNormal];
    [self.btnDone setTitleColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0] forState:UIControlStateNormal];
    [self.btnDone setTitleColor:[UIColor rgbFromHexString:@"#FFFFFF" alpaa:1.0] forState:UIControlStateSelected];
    [self.btnDone addTarget:self action:@selector(doneHandle:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnDone setTitleEdgeInsets:UIEdgeInsetsMake(0, - doneW - doneW * 0.5, 0, 0)];
    [self.alertBgView addSubview:self.btnDone];
    
    
}

//关闭绑定手机提示
-(void)backToVC:(UIButton *)button{
    [self removeFromSuperview];
}
//将手机号输入框设为第一响应者
//-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
//    [self.tfPhoneNum becomeFirstResponder];
//}

#pragma mark - UITextFieldDelegate
//换行
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (textField.tag == 1001) {
        [self.tfCheckCode becomeFirstResponder];
    }else if (textField.tag == 1002){
        [self.tfCheckCode resignFirstResponder];
    }
    
    return YES;
}

//限制输入
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSInteger existedLength = textField.text.length ;
    NSInteger selectedLength = range.length;
    NSInteger replaceLength = string.length;
    NSInteger currentLength = existedLength - selectedLength + replaceLength;
    
//    if (textField == self.tfPhoneNum) {
//        if (string.length == 0){
//            self.btnGetCode.selected = NO;
//            return YES;
//        }else if ( currentLength == 11) {
//            
//            self.btnGetCode.selected = NO;
//            return YES;
//        }else if(currentLength > 11){
//        
//            return NO;
//        }else{
//            self.btnGetCode.selected = NO;
//            return YES;
//        }
//    }
    
    if (textField == self.tfCheckCode) {
        if (string.length == 0) {
            self.btnDone.selected = NO;
            return YES;
        }else if(currentLength == 6){
            self.btnDone.selected = YES;
            return YES;
        }else if (currentLength > 6) {
            return NO;
        }else{
            self.btnDone.selected = NO;
            return YES;
        }
    }
   return YES;
    
    
    
}

- (void)textFieldDidChange:(UITextField *)textField
{
    if (textField == self.tfPhoneNum) {
        if (textField.text.length >= 11) {
            textField.text = [textField.text substringToIndex:11];
        }
    }
}

#pragma mark - 获取验证码
-(void)getCodeHandle:(UIButton *)button{
    //判断改手机号是否可用合法
    if ([self isMobileNumber:self.tfPhoneNum.text]) {
        timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerFireMethod:) userInfo:nil repeats:YES];
        seconds = 60;
        self.btnGetCode.selected = YES;
        self.tfCheckCode.enabled = YES;
        [self.tfCheckCode becomeFirstResponder];
        //本地保存手机号和验证码
        self.phongNum = self.tfPhoneNum.text;
        
        //上传手机号
        [[NetReqManager getInstance] sendUploadPhoneNumber:[ListenToMeDBManager getUuid] SPhoneNumber:self.tfPhoneNum.text];
        
    }else{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"输入的号码不合法" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
    
    }
    
}

#pragma mark - 绑定手机号成功
-(void)doneHandle:(UIButton *)button{
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"激活成功" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alertView setTintColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
    
    //预留判断(使用了临时手机号,需要后期修改)
    if ([self.tfPhoneNum.text isEqualToString:self.phongNum]) {
        YDLog(@"%@",self.tfPhoneNum.text);
        self.btnDone.selected = NO;
        
        //上传验证码
        [[NetReqManager getInstance] sendUploadVerifyCode:[ListenToMeDBManager getUuid] SVerifyCode:self.tfCheckCode.text];
        
        
        
    }else{
        [alertView setMessage:@"手机号(验证码)错误,请重新输入"];
        [alertView show];
    }
    
    
}

#pragma mark - 手机号码的有效性判断
//检测是否是手机号码
- (BOOL)isMobileNumber:(NSString *)mobileNum
{
    if (mobileNum.length == 11) {
    
    
    /**
     * 手机号码
     * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     * 联通：130,131,132,152,155,156,185,186
     * 电信：133,1349,153,180,189
     */
    NSString * MOBILE = @"^1(3[0-9]|5[0-35-9]|8[025-9])\\d{8}$";
    /**
     10         * 中国移动：China Mobile
     11         * 134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     12         */
    NSString * CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[278])\\d)\\d{7}$";
    /**
     15         * 中国联通：China Unicom
     16         * 130,131,132,152,155,156,185,186
     17         */
    NSString * CU = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
    /**
     20         * 中国电信：China Telecom
     21         * 133,1349,153,180,189
     22         */
    NSString * CT = @"^1((33|53|8[09])[0-9]|349)\\d{7}$";
    /**
     25         * 大陆地区固话及小灵通
     26         * 区号：010,020,021,022,023,024,025,027,028,029
     27         * 号码：七位或八位
     28         */
    // NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    
    if (([regextestmobile evaluateWithObject:mobileNum] == YES)
        || ([regextestcm evaluateWithObject:mobileNum] == YES)
        || ([regextestct evaluateWithObject:mobileNum] == YES)
        || ([regextestcu evaluateWithObject:mobileNum] == YES))
    {
        return YES;
    }
    else
    {
        return NO;
    }
    
    }else{
        return NO;
    }
}

#pragma mark - 定时器
//倒计时方法验证码实现倒计时60秒，60秒后按钮变换开始的样子
-(void)timerFireMethod:(NSTimer *)theTimer {
    if (seconds == 1) {
        [theTimer invalidate];
        seconds = 60;
        [self.btnGetCode setTitle:@"获取验证码" forState: UIControlStateNormal];
        self.btnGetCode.selected = NO;
        
        [self.coverView removeFromSuperview];
    }else{
        seconds--;
        NSString *title = [NSString stringWithFormat:@"%ldS",seconds];
        [self.btnGetCode setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.btnGetCode.selected = YES;
        [self.btnGetCode setTitle:title forState:UIControlStateNormal];
        
        [self.alertBgView addSubview:self.coverView];
    }
}
@end
