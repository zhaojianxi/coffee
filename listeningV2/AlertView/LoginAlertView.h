//
//  LoginAlertView.h
//  ListenToMe
//
//  Created by zhw on 15/4/17.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginAlertView : UIView
/**
 *  微信登录
 */
@property(nonatomic,strong)UIButton *weiXinLoginBtn;
/**
 *  qq登录
 */
@property(nonatomic,strong)UIButton *qqLoginBtn;
/**
 *  sina新浪微博登录
 */
@property(nonatomic,strong)UIButton *sinaLoginBtn;
@end
