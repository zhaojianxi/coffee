//
//  FirstLaunchAlertVC.h
//  ListenToMe
//
//  Created by zhw on 15/4/14.
//  Copyright (c) 2015年 listentome. All rights reserved.
//
/**
 *  第一个启动应用弹出的弹窗
 */
#import <UIKit/UIKit.h>

@interface FirstLaunchAlertVC : UIViewController
/**
 *  微信登录
 */
@property(nonatomic,strong)UIButton *weiXinLoginBtn;
/**
 *  qq登录
 */
@property(nonatomic,strong)UIButton *qqLoginBtn;
/**
 *  sina新浪微博登录
 */
@property(nonatomic,strong)UIButton *sinaLoginBtn;
@end
