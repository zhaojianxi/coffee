//
//  GiftGivenAlertView.m
//  ListenToMe
//
//  Created by zhw on 15/4/15.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "GiftGivenAlertView.h"
@implementation GiftGivenAlertView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(instancetype)init{
    self = [super init];
    if (self) {
        [self setUI];
    }

    return self;
}

-(void)setUI{
    
    self.frame =[UIScreen mainScreen].bounds;
    self.backgroundColor = [UIColor colorWithRed:203/255.0 green:203/255.0 blue:203/255.0 alpha:0.5];
    UIView *backView = [[UIView alloc]initWithFrame:self.bounds];
    backView.backgroundColor =[UIColor rgbFromHexString:@"#000000" alpaa:0.5];
    [self addSubview:backView];
    
    
    
    //
    CGFloat xDistance = 15;
    UIImage *alertBackImg = [UIImage imageNamed:@"giftAlertBackImg.png"];
    
    UIImageView *alertView = [[UIImageView alloc]initWithFrame:CGRectMake(xDistance, 191 + 64, screenWidth - xDistance *2, 217)];
    [alertView setImage:alertBackImg];
    alertView.layer.masksToBounds = YES;
    alertView.layer.cornerRadius = 5;
    alertView.userInteractionEnabled = YES;
    [self addSubview:alertView];
    
    UIImage *giftImg = [UIImage imageNamed:@"giftImg2.png"];
    UIImageView *giftView = [[UIImageView alloc]initWithFrame:CGRectMake(xDistance, 0, giftImg.size.width, giftImg.size.height)];
    [giftView setImage:giftImg];
    [alertView addSubview:giftView];
    
    CGFloat bWidth = 15;//button的宽高
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(alertView.width - bWidth - 10, bWidth , bWidth, bWidth)];
    UIImage *closeImg = [UIImage imageNamed:@"colorButten.png"];
    [backButton setImage:closeImg forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backToHome) forControlEvents:UIControlEventTouchUpInside];
    [alertView addSubview:backButton];
    
    
    UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(0, giftView.y + giftView.height, alertView.width, 1)];
    line.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:0.5];
    [alertView addSubview:line];
    
    
    CGFloat label_x = 30;
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(label_x, giftView.y + giftView.height + label_x, alertView.width - label_x * 2, 17.5)];
    label1.text = @"客官";
    [label1 setTextColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
    [label1 setTextAlignment:NSTextAlignmentLeft];
    [label1 setFont:[UIFont systemFontOfSize:15.0]];
    [alertView addSubview:label1];
    
    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(label_x, label1.y + label1.height, label1.width, label1.height)];
    label2.text = @"您的兜已经被我们用礼券塞满";
    [label2 setTextColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
    [label2 setTextAlignment:NSTextAlignmentLeft];
    [label2 setFont:[UIFont systemFontOfSize:15.0]];
    [alertView addSubview:label2];
    
    UIImage *btnBagImg = [UIImage imageNamed:@"btnBackImg.png"];
    self.btnCheckGift = [[UIButton alloc]initWithFrame:CGRectMake(0, label2.y + label2.height + label_x, alertView.width, btnBagImg.size.height * 0.5)];
    [self.btnCheckGift setImage:btnBagImg forState:UIControlStateNormal];
    [alertView addSubview:self.btnCheckGift];
    
    UILabel *lbCheck = [[UILabel alloc]initWithFrame:CGRectMake(label_x, (self.btnCheckGift.height - 17.5) * 0.5, 100, 17.5)];
    lbCheck.text = @"查看";
    [lbCheck setTextColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
    [lbCheck setFont:[UIFont systemFontOfSize:15.0]];
    [lbCheck setTextAlignment:NSTextAlignmentLeft];
    [self.btnCheckGift addSubview:lbCheck];
    
    
    UIImage *checkImg = [UIImage imageNamed:@"checkImg.png"];
    UIImageView *checkView = [[UIImageView alloc]initWithFrame:CGRectMake(self.btnCheckGift.width - label_x - checkImg.size.width * 0.5, (self.btnCheckGift.height - checkImg.size.height * 0.5) * 0.5, checkImg.size.width * 0.5, checkImg.size.height * 0.5)];
    [checkView setImage:checkImg];
    [self.btnCheckGift addSubview:checkView];
    YDLog(@"%@",NSStringFromCGRect(self.btnCheckGift.frame));
    
    
}


-(void)backToHome{
    [self removeFromSuperview];
}

@end
