//
//  FirstLaunchAlertVC.m
//  ListenToMe
//
//  Created by zhw on 15/4/14.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "FirstLaunchAlertVC.h"

@interface FirstLaunchAlertVC ()

@end

@implementation FirstLaunchAlertVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.frame =[UIScreen mainScreen].bounds;
    self.view.backgroundColor = [UIColor colorWithRed:203/255.0 green:203/255.0 blue:203/255.0 alpha:0.5];
    UIView *backView = [[UIView alloc]initWithFrame:self.view.bounds];
    backView.backgroundColor =[UIColor rgbFromHexString:@"#000000" alpaa:0.5];
    [self.view addSubview:backView];
    
    //布局视图
    [self setUI];
}



#pragma mark - 布局登录提示的视图
-(void)setUI{
    
    
    
    //添加一个点击手势
//    _singleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(backToView)];
//    [self.view addGestureRecognizer:_singleTap];
//    _singleTap.delegate = self;
//    _singleTap.cancelsTouchesInView = YES;
    
    //布局子视图
    [self setSubView];
    
}


#pragma mark - 布局子视图
-(void)setSubView{
    CGFloat xDistance = 15;
    
    UIView *alertView = [[UIView alloc]initWithFrame:CGRectMake(xDistance, 191, screenWidth - xDistance *2, 300)];
    alertView.backgroundColor = [UIColor rgbFromHexString:@"#FFFFFF" alpaa:1.0];
    alertView.layer.masksToBounds = YES;
    alertView.layer.cornerRadius = 5;
//    alertView.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
    
    UIImageView *headView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, alertView.width, 45)];
    [headView setImage:[UIImage imageNamed:@"whiteButtonBgimg.png"]];
    [alertView addSubview:headView];
    
    UILabel *lbLogin = [[UILabel alloc]initWithFrame:CGRectMake((alertView.width - 100) * 0.5, 0, 100, headView.height)];
    lbLogin.text = @"登录";
    [lbLogin setTextColor:[UIColor rgbFromHexString:@"#FFFFFF" alpaa:1.0]];
    [lbLogin setFont:[UIFont systemFontOfSize:15.0]];
    [lbLogin setTextAlignment:NSTextAlignmentCenter];
    [headView addSubview:lbLogin];
    
 
    
    CGFloat bWidth = 15;//button的宽高
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(alertView.width - bWidth - 10, bWidth , bWidth, bWidth)];
    UIImage *closeImg = [UIImage imageNamed:@"whiteButten.png"];
    [backButton setImage:closeImg forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backToView) forControlEvents:UIControlEventTouchUpInside];
    [alertView addSubview:backButton];
    
    UIImageView *middleView = [[UIImageView alloc]initWithFrame:CGRectMake(0, headView.height, alertView.width, 62)];
    [middleView setImage:[UIImage imageNamed:@"giftimg.png"]];
    [alertView addSubview:middleView];
    
    UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(0, middleView.y + middleView.height - 1, alertView.width, 1)];
    line.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:0.12];
    [alertView addSubview:line];
    
    CGFloat label_x = 30;
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(label_x, middleView.y + middleView.height + label_x, alertView.width - label_x * 2, 17.5)];
    label1.text = @"客官";
    [label1 setTextColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
    [label1 setTextAlignment:NSTextAlignmentLeft];
    [label1 setFont:[UIFont systemFontOfSize:15.0]];
    [alertView addSubview:label1];
    
    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(label_x, label1.y + label1.height, label1.width, label1.height)];
    label2.text = @"恕臣妾不知";
    [label2 setTextColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
    [label2 setTextAlignment:NSTextAlignmentLeft];
    [label2 setFont:[UIFont systemFontOfSize:15.0]];
    [alertView addSubview:label2];
    
    UILabel *label3 = [[UILabel alloc]initWithFrame:CGRectMake(label_x, label2.y + label1.height, label2.width, label2.height)];
    label3.text = @"孝敬您的礼券该放哪个兜?";
    [label3 setTextColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
    [label3 setTextAlignment:NSTextAlignmentLeft];
    [label3 setFont:[UIFont systemFontOfSize:15.0]];
    [alertView addSubview:label3];
    
//
//    UILabel *detail = [[UILabel alloc]initWithFrame:CGRectMake(label_x, title.y + title.height, title.width, 14.5)];
//    detail.text = @"成为最善良(cuican)的那颗星";
//    [detail setTextColor:[UIColor rgbFromHexString:@"#FFFFFF" alpaa:1.0]];
//    [detail setFont:[UIFont systemFontOfSize:12.0]];
//    [detail setTextAlignment:NSTextAlignmentLeft];
//    [alertView addSubview:detail];
//    
//    [alertView resignFirstResponder];
    
    //选择分享方式 微信 QQ 新浪微博
    
    UIView *shareView = [[UIView alloc]initWithFrame:CGRectMake(0, 200, alertView.width, 98)];
    shareView.backgroundColor = [UIColor rgbFromHexString:@"#FFFFFF" alpaa:1.0];
    
//    UILabel *title2 = [[UILabel alloc]initWithFrame:CGRectMake(xDistance * 2, 16 , shareView.width - xDistance * 4, 13.5)];
////    title2.text = @"通过以下方式加入";
//    [title2 setTextColor:[UIColor grayColor]];
//    [title2 setFont:[UIFont systemFontOfSize:12.0]];
//    [title2 setTextAlignment:NSTextAlignmentLeft];
//    [shareView addSubview:title2];
    
    
    CGFloat btWidth = 60;//button宽高
    CGFloat bt_x = 25;//button之间的间隔
    //微信分享
//    UIButton *weiXinBtn
    self.weiXinLoginBtn = [[UIButton alloc]initWithFrame:CGRectMake(xDistance , 5, btWidth, btWidth)];
    [self.weiXinLoginBtn setImage:[UIImage imageNamed:@"weiXin.png"] forState:UIControlStateNormal];
//    [self.weiXinLoginBtn addTarget:self action:@selector(weiXinLoginHandle:) forControlEvents:UIControlEventTouchUpInside];
    [shareView addSubview:self.weiXinLoginBtn];
    
    //QQ分享
//    UIButton *qqBtn
    self.qqLoginBtn = [[UIButton alloc]initWithFrame:CGRectMake(self.weiXinLoginBtn.x + btWidth + bt_x, self.weiXinLoginBtn.y, btWidth, btWidth)];
    [self.qqLoginBtn setImage:[UIImage imageNamed:@"qq.png"] forState:UIControlStateNormal];
//    [self.qqLoginBtn addTarget:self action:@selector(qqLoginHandle:) forControlEvents:UIControlEventTouchUpInside];
    [shareView addSubview:self.qqLoginBtn];
    //新浪分享
//    UIButton *xinLangBtn
    self.sinaLoginBtn = [[UIButton alloc]initWithFrame:CGRectMake(self.qqLoginBtn.x + btWidth + bt_x ,self.weiXinLoginBtn.y, btWidth, btWidth)];
    [self.sinaLoginBtn setImage:[UIImage imageNamed:@"sina.png"] forState:UIControlStateNormal];
//    [self.sinaLoginBtn addTarget:self action:@selector(sinaLoginHandle:) forControlEvents:UIControlEventTouchUpInside];
    [shareView addSubview:self.sinaLoginBtn];
    
    [alertView addSubview:shareView];
    [self.view addSubview:alertView];
    
    
    //登录完成后还需要将该弹窗关闭掉,查看友盟的
}


/*
#pragma mark - 第三方登录
-(void)weiXinLoginHandle:(UIButton *)weiXingBtn{

    //需要到微信开放平台进行注册,申请开发者帐号.
    UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToWechatSession];
    
    snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
        
        if (response.responseCode == UMSResponseCodeSuccess) {
            
            UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary]valueForKey:UMShareToWechatSession];
            
            NSLog(@"username is %@, uid is %@, token is %@ url is %@",snsAccount.userName,snsAccount.usid,snsAccount.accessToken,snsAccount.iconURL);
            
            
        }
        
    });
    
    [self backToView];
}

-(void)qqLoginHandle:(UIButton *)qqBtn{

    //暂时使用QQQzone登录
    UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToQzone];
    
    snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
        
        //          获取微博用户名、uid、token等
        
        if (response.responseCode == UMSResponseCodeSuccess) {
            
            UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary] valueForKey:UMShareToQzone];
            
            NSLog(@"username is %@, uid is %@, token is %@ url is %@",snsAccount.userName,snsAccount.usid,snsAccount.accessToken,snsAccount.iconURL);
            
        
            
            
            
        }});

    [self backToView];

}
-(void)sinaLoginHandle:(UIButton *)sinaBtn{

    UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToSina];
    
    snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
        
//        NSDictionary *dic = response.data[@"sina"];
//        [self setNamelabelAndIcon:dic];
//        [self setLoginStatus];
        
        if (response.responseCode == UMSResponseCodeSuccess) {
            
            UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary] valueForKey:UMShareToSina];
            
            NSLog(@"username is %@, uid is %@, token is %@ url is %@",snsAccount.userName,snsAccount.usid,snsAccount.accessToken,snsAccount.iconURL);
            
            //获取新浪好友限制30个
            [[UMSocialDataService defaultDataService] requestSnsFriends:UMShareToSina  completion:^(UMSocialResponseEntity *response){
                NSLog(@"SnsFriends is %@",response.data);
            }];
            
            
            
        }
        
    });
    
    [self backToView];
    

}


//- (void)setNamelabelAndIcon:(NSDictionary *)dict {
//    
//    
//    if (dict[@"username"] != nil) {
////        self.nameLabel.text = dict[@"username"];
////        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:dict[@"icon"]]];
////        [self.iconBtn setImage:[UIImage imageWithData:data] forState:UIControlStateNormal ];
////        NSLog(@"%@",dict[@"icon"]);
//        
//        YDLog(@"%@ , %@",dict[@"username"],dict[@"icon"]);
//    }
//    
//    //在授权完成后调用获取用户信息的方法
//    [[UMSocialDataService defaultDataService] requestSnsInformation:UMShareToSina  completion:^(UMSocialResponseEntity *response){
//        NSLog(@"SnsInformation is %@",response.data);
//    }];
//    
//    //获取好友列表调用下面的方法,由于新浪官方限制，获取好友列表只能获取到30%好友
//    
//    [[UMSocialDataService defaultDataService] requestSnsFriends:UMShareToSina  completion:^(UMSocialResponseEntity *response){
//        NSLog(@"SnsFriends is %@",response.data);
//    }];
//    
//}

- (void)setLoginStatus {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"everLaunched"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    BOOL isLaunch = [[NSUserDefaults standardUserDefaults] boolForKey:@"everLaunched"];
    if (isLaunch) {
        //         这里判断是否已经登录
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"礼券已经收入囊中"
                                                      message:@"进入App"
                                                     delegate:self
                                            cancelButtonTitle:@"我知道了"
                                            otherButtonTitles:nil];
        [alert show];
    }else{
        
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"还未登录"
                                                      message:@"进入App"
                                                     delegate:self
                                            cancelButtonTitle:@"我知道了"
                                            otherButtonTitles:nil];
        [alert show];
    }
    
}


 */

-(void)backToView{

    [self.view removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
