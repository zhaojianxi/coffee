//
//  GiftGivenAlertView.h
//  ListenToMe
//
//  Created by zhw on 15/4/15.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GiftGivenAlertView : UIView
@property(nonatomic,strong)UIButton *btnCheckGift;
@end
