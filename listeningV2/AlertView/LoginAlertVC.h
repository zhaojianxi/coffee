//
//  LoginAlertVC.h
//  ListenToMe
//
//  Created by zhw on 15/4/15.
//  Copyright (c) 2015年 listentome. All rights reserved.
//
/**
 *  需要登录时才能使用相关功能的弹出框
 */
#import <UIKit/UIKit.h>

@interface LoginAlertVC : UIViewController
/**
 *  微信登录
 */
@property(nonatomic,strong)UIButton *weiXinLoginBtn;
/**
 *  qq登录
 */
@property(nonatomic,strong)UIButton *qqLoginBtn;
/**
 *  sina新浪微博登录
 */
@property(nonatomic,strong)UIButton *sinaLoginBtn;

/**
 *  返回一个登录对象
 */
@property(nonatomic,strong) UMSocialAccountEntity *account;
/**
 *  listenToMeData
 */
@property(nonatomic,strong) ListenToMeData *listenToMeData;
@end
