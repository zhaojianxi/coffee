//
//  BindingPhoneNumAlertView.h
//  ListenToMe
//
//  Created by zhw on 15/4/20.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BindingPhoneNumAlertView : UIView
/**
 *  背景
 */
@property(nonatomic,strong)UIImageView *alertBgView;
/**
 *  填写验证码
 */
@property(nonatomic,strong)UITextField *tfCheckCode;
/**
 *  验证码错误信息
 */
@property(nonatomic,strong)UILabel *lbFailure;
@end
