//
//  LoginAlertView.m
//  ListenToMe
//
//  Created by zhw on 15/4/17.
//  Copyright (c) 2015年 listentome. All rights reserved.
//

#import "LoginAlertView.h"

@interface LoginAlertView()<UIGestureRecognizerDelegate,UMSocialDataDelegate,UMSocialUIDelegate>
/**
 *  添加点击手势
 */
@property(strong,nonatomic)UITapGestureRecognizer *singleTap;
@end

@implementation LoginAlertView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(instancetype)init{

    self = [super init];
    
    @synchronized(self){
        if (self) {
            [self setUI];
        }
    
    }
    
    return self;
}

#pragma mark - 布局UI
-(void)setUI{

    
    self.frame =[UIScreen mainScreen].bounds;
    self.backgroundColor = [UIColor colorWithRed:203/255.0 green:203/255.0 blue:203/255.0 alpha:0.5];
    
    //添加一个点击手势
    _singleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(backToView)];
    [self addGestureRecognizer:_singleTap];
    _singleTap.delegate = self;
    _singleTap.cancelsTouchesInView = YES;
    
    //布局子视图
    [self setSubView];

    
}


#pragma mark - 布局子视图
-(void)setSubView{
    CGFloat xDistance = 15;
    
    UIView *alertView = [[UIView alloc]initWithFrame:CGRectMake(xDistance, 191, screenWidth - xDistance *2, 300)];
    alertView.backgroundColor = [UIColor rgbFromHexString:@"#FFFFFF" alpaa:1.0];
    alertView.layer.masksToBounds = YES;
    alertView.layer.cornerRadius = 5;
    //    alertView.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0];
    
    UIImageView *headView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, alertView.width, 45)];
    //    [headView setImage:[UIImage imageNamed:@"whiteButtonBgimg.png"]];
    [alertView addSubview:headView];
    
    UILabel *lbLogin = [[UILabel alloc]initWithFrame:CGRectMake((alertView.width - 100) * 0.5, 0, 100, headView.height)];
    lbLogin.text = @"登录";
    [lbLogin setTextColor:[UIColor rgbFromHexString:@"#AC56FF" alpaa:1.0]];
    [lbLogin setFont:[UIFont systemFontOfSize:15.0]];
    [lbLogin setTextAlignment:NSTextAlignmentCenter];
    [headView addSubview:lbLogin];
    
    UILabel *line1 = [[UILabel alloc]initWithFrame:CGRectMake(0, headView.y + headView.height - 1, alertView.width, 1)];
    line1.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:0.12];
    [alertView addSubview:line1];
    
    
    CGFloat bWidth = 15;//button的宽高
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(alertView.width - bWidth - 10, bWidth , bWidth, bWidth)];
    UIImage *closeImg = [UIImage imageNamed:@"colorButten.png"];
    [backButton setImage:closeImg forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backToView) forControlEvents:UIControlEventTouchUpInside];
    [alertView addSubview:backButton];
    
    UIImage *middleImg = [UIImage imageNamed:@"loginBackImg.png"];
    UIImageView *middleView = [[UIImageView alloc]initWithFrame:CGRectMake(0, headView.height, alertView.width, middleImg.size.height * 0.8)];
    [middleView setImage:middleImg];
    [alertView addSubview:middleView];
    
    UILabel *line2 = [[UILabel alloc]initWithFrame:CGRectMake(0, middleView.y + middleView.height - 1, alertView.width, 1)];
    line2.backgroundColor = [UIColor rgbFromHexString:@"#AC56FF" alpaa:0.12];
    [alertView addSubview:line2];
    
    
    //选择分享方式 微信 QQ 新浪微博
    
    UIView *shareView = [[UIView alloc]initWithFrame:CGRectMake(0, middleView.y + middleView.height, alertView.width, 98)];
    shareView.backgroundColor = [UIColor rgbFromHexString:@"#FFFFFF" alpaa:1.0];
    
    UILabel *title2 = [[UILabel alloc]initWithFrame:CGRectMake(0, 15 , shareView.width - xDistance * 4, 13.5)];
    title2.text = @"通过以下方式加入";
    [title2 setTextColor:[UIColor grayColor]];
    [title2 setFont:[UIFont systemFontOfSize:12.0]];
    [title2 setTextAlignment:NSTextAlignmentCenter];
    [shareView addSubview:title2];
    
    
    CGFloat btWidth = 60;//button宽高
    CGFloat bt_x = (shareView.width - xDistance * 4 - btWidth * 3) * 0.5;//button之间的间隔
    //微信分享
    //    UIButton *weiXinBtn
    self.weiXinLoginBtn = [[UIButton alloc]initWithFrame:CGRectMake(xDistance * 2  , 15 + title2.y + title2.height, btWidth, btWidth)];
    [self.weiXinLoginBtn setImage:[UIImage imageNamed:@"weiXin.png"] forState:UIControlStateNormal];
//    [self.weiXinLoginBtn addTarget:self action:@selector(weiXinLoginHandle:) forControlEvents:UIControlEventTouchUpInside];
    [shareView addSubview:self.weiXinLoginBtn];
    
    //QQ分享
    //    UIButton *qqBtn
    self.qqLoginBtn = [[UIButton alloc]initWithFrame:CGRectMake(self.weiXinLoginBtn.x + btWidth + bt_x, self.weiXinLoginBtn.y, btWidth, btWidth)];
    [self.qqLoginBtn setImage:[UIImage imageNamed:@"qq.png"] forState:UIControlStateNormal];
//    [self.qqLoginBtn addTarget:self action:@selector(qqLoginHandle:) forControlEvents:UIControlEventTouchUpInside];
    [shareView addSubview:self.qqLoginBtn];
    //新浪分享
    //    UIButton *xinLangBtn
    self.sinaLoginBtn = [[UIButton alloc]initWithFrame:CGRectMake(self.qqLoginBtn.x + btWidth + bt_x ,self.weiXinLoginBtn.y, btWidth, btWidth)];
    [self.sinaLoginBtn setImage:[UIImage imageNamed:@"sina.png"] forState:UIControlStateNormal];
//    [self.sinaLoginBtn addTarget:self action:@selector(sinaLoginHandle:) forControlEvents:UIControlEventTouchUpInside];
    [shareView addSubview:self.sinaLoginBtn];
    
    [alertView addSubview:shareView];
    [self addSubview:alertView];
}


#pragma mark - UIGestrueRecognizerDelegate
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}




//#pragma mark - 第三方登录
//-(void)weiXinLoginHandle:(UIButton *)weiXingBtn{
//    
//    //需要到微信开放平台进行注册,申请开发者帐号.
//    UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToWechatSession];
//    
//    snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
//        
//        if (response.responseCode == UMSResponseCodeSuccess) {
//            
//            UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary]valueForKey:UMShareToWechatSession];
//            
//            NSLog(@"WeiXin username is %@, uid is %@, token is %@ url is %@",snsAccount.userName,snsAccount.usid,snsAccount.accessToken,snsAccount.iconURL);
//            
//            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"launchStaue"];
//            
//            [self backToView];
//        }
//        
//    });
//    
//    
//    
//}
//
//-(void)qqLoginHandle:(UIButton *)qqBtn{
//    
//    //暂时使用QQQzone登录
//    UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToQzone];
//    
//    snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
//        
//        //          获取微博用户名、uid、token等
//        
//        if (response.responseCode == UMSResponseCodeSuccess) {
//            
//            UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary] valueForKey:UMShareToQzone];
//            
//            NSLog(@"QQ username is %@, uid is %@, token is %@ url is %@",snsAccount.userName,snsAccount.usid,snsAccount.accessToken,snsAccount.iconURL);
//            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"launchStaue"];
//            
//            [self backToView];
//        }});
//    
//    
//    
//}
//-(void)sinaLoginHandle:(UIButton *)sinaBtn{
//    
//    UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToSina];
//    
//    snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
//        
//        //        NSDictionary *dic = response.data[@"sina"];
//        //        [self setNamelabelAndIcon:dic];
//        //        [self setLoginStatus];
//        
//        if (response.responseCode == UMSResponseCodeSuccess) {
//            
//            UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary] valueForKey:UMShareToSina];
//            
//            NSLog(@"Sina username is %@, uid is %@, token is %@ url is %@",snsAccount.userName,snsAccount.usid,snsAccount.accessToken,snsAccount.iconURL);
//            
//            //获取新浪好友限制30个
//            //            [[UMSocialDataService defaultDataService] requestSnsFriends:UMShareToSina  completion:^(UMSocialResponseEntity *response){
//            //                NSLog(@"SnsFriends is %@",response.data);
//            //            }];
//            
//            
//            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"launchStaue"];
//            
//            
//            [self backToView];
//        }
//        
//    });
//    
//    
//    
//    
//}
//


#pragma mark - UMSocial Delegate

-(void)didFinishGetUMSocialDataInViewController:(UMSocialResponseEntity *)response{
    
    //根据responseCode得到的发送的结果
    if (response.responseCode == UMSResponseCodeSuccess) {
        //得到分享到的平台名称
        YDLog(@"You share to sns name is %@",[[response.data allKeys] objectAtIndex:0]);
        [self removeFromSuperview];
    }
    
}
-(void)didFinishGetUMSocialDataResponse:(UMSocialResponseEntity *)response{
    
    //根据responseCode得到的发送的结果
    if (response.responseCode == UMSResponseCodeSuccess) {
        //得到分享到的平台名称
        YDLog(@"You share to sns name is %@",[[response.data allKeys] objectAtIndex:0]);
        
    }
    
}


#pragma mark -backHandle点击事件,移除登录提示视图
-(void)backToView{
    
    [self removeFromSuperview];
}

@end
